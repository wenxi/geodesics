//#ifndef geodesic_regression_h
//#define geodesic_regression_h
//
//#include "geodesic_base_elements.h"
//#include "geodesic_mesh_data.h"
//
//
//#include <Eigen/Core>
//#include <Eigen/Eigen>
//#include <Eigen/Eigenvalues>
//#include <Eigen/Sparse>
//
//#include <iostream>
//#include <fstream>
//
//
//using namespace std;
//using namespace Eigen;
//#define PI 3.14159265354 
//
//void geodesic(MeshData * mesh_data, bool is_pull_back, vector<Point> & curvePointArray ,float regression_ratio,vector<Point> normalArray){
//
//	vector<Point> initial_path;
//	initial_path = curvePointArray;
//
//
//	int window_size = 5;
//	int dim = 3;
//	int k = 1;
//	ANNpoint queryPoint;
//	queryPoint = annAllocPt(dim);
//
//
//	ofstream file("matrix equation solvent.txt");
//
//
//	int number_point_in_range = 0;
//
//	ANNidxArray nnIdx; // near neighbor indices
//
//
//	ANNdistArray dists; // near neighbor distances
//
//
//
//
//
//	float search_radius = 0.0005f;
//	ANNdist sqRad = sqrt(search_radius);
//
//
//
//	//ofstream regionFile("result.txt");
//
//	float u = regression_ratio;
//	int sparse_matrix_size = (int)initial_path.size();
//
//	SparseMatrix<float> meanPointArray(sparse_matrix_size,3);
//	SparseMatrix<float> A_m_3(sparse_matrix_size,3);
//
//
//
//	if(is_pull_back){
//
//		for(int i = 0;i<sparse_matrix_size;i++ ){
//
//			queryPoint[0] = initial_path[i].x;
//			queryPoint[1] = initial_path[i].y;
//			queryPoint[2] = initial_path[i].z;
//
//			//int num_points = meshKdTree->annkFRSearch(queryPoint,sqRad,0,NULL,NULL,0.0);
//			int num_points = 7;
//
//			nnIdx = new ANNidx[num_points];
//			dists = new ANNdist[num_points];
//
//			mesh_data->meshKdTree->annkSearch(queryPoint,num_points,nnIdx,dists,0.00000f);
//
//
//			//nnIdx = new ANNidx[num_points];
//			//dists = new ANNdist[num_points];
//			//meshKdTree->annkFRSearch(queryPoint,sqRad,num_points,nnIdx,dists,0.0);
//			//MString msg;
//			//msg = num_points;
//			//MGlobal::displayInfo(msg);
//
//
//
//
//			float x = 0, y = 0,z = 0;
//			for(int b = 0; b < num_points ; ++b){
//				x += mesh_data->getVertex(nnIdx[b])[0];
//				y += mesh_data->getVertex(nnIdx[b])[1];
//				z += mesh_data->getVertex(nnIdx[b])[2];
//			}
//
//
//
//			//file<<"spaceLocator -p "<<x/num_points<<" "<<y/num_points<<" "<<z/num_points<<";"<<endl;
//
//			//file<<"spaceLocator -p "<<queryPoint[0]<<" "<<queryPoint[1]<<" "<<queryPoint[2]<<";"<<endl;
//
//			A_m_3.coeffRef(i,0) = x/num_points;
//			A_m_3.coeffRef(i,1) = y/num_points;
//			A_m_3.coeffRef(i,2) = z/num_points;
//
//			delete []nnIdx;
//			delete []dists;
//		}
//
//	}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//	SparseMatrix<float> sMat_Xt_m_3(sparse_matrix_size,3);
//	for(int i = 0;i<sparse_matrix_size;i++ ){
//
//		sMat_Xt_m_3.coeffRef(i,0) = (float)initial_path[i].x;
//		sMat_Xt_m_3.coeffRef(i,1) = (float)initial_path[i].y;
//		sMat_Xt_m_3.coeffRef(i,2) = (float)initial_path[i].z;
//	}
//
//	SparseMatrix<float> sMat_Xt_1_m_3(sparse_matrix_size,3);
//
//
//
//
//	SparseMatrix<float> sMat_Nt_3m_2_m_2((sparse_matrix_size-2)*3,(sparse_matrix_size-2));	
//	SparseMatrix<float> sMat_N_m_2_3(sparse_matrix_size-2,3);
//
//
//
//
//	for(int i = 0 ; i < sparse_matrix_size-2; i++){
//
//		sMat_Nt_3m_2_m_2.coeffRef(i*3,i) = (float)normalArray[i+1].x;
//		sMat_Nt_3m_2_m_2.coeffRef(i*3+1,i) = (float)normalArray[i+1].y;
//		sMat_Nt_3m_2_m_2.coeffRef(i*3+2,i) = (float)normalArray[i+1].z;
//
//	}
//
//
//
//	for(int i = 0 ; i < sparse_matrix_size-2; i++){
//
//		sMat_N_m_2_3.coeffRef(i,0) = (float)normalArray[i+1].x;
//		sMat_N_m_2_3.coeffRef(i,1) = (float)normalArray[i+1].y;
//		sMat_N_m_2_3.coeffRef(i,2) = (float)normalArray[i+1].z;
//
//	}
//
//
//
//	SparseMatrix<float> sMat_K_m_m(sparse_matrix_size,sparse_matrix_size);
//
//	for(int i = 1;i<sparse_matrix_size-1;i++ ){
//
//		sMat_K_m_m.coeffRef(i,i-1) = 1;
//		sMat_K_m_m.coeffRef(i,i) = -2;
//		sMat_K_m_m.coeffRef(i,i+1) = 1;
//
//	}
//
//
//
//	SparseMatrix<float> sMat_ks_m_3m_2(sparse_matrix_size,(sparse_matrix_size-2)*3);
//	for(int i = 1 ; i < sparse_matrix_size-1 ; i++){
//
//		int j = (i-1)*3;
//		sMat_ks_m_3m_2.coeffRef(i,j) = 1;
//		sMat_ks_m_3m_2.coeffRef(i,j+1) = -2;
//		sMat_ks_m_3m_2.coeffRef(i,j+2) = 1;
//
//	}
//
//
//
//	SparseMatrix<float> sMat_Xts_3m_2_3m_2((sparse_matrix_size-2)*3,(sparse_matrix_size-2)*3);
//	for(int i = 0 ; i < sparse_matrix_size-2; i++){
//		for(int a = 0; a < 3;a++){
//
//
//			sMat_Xts_3m_2_3m_2.coeffRef(i*3+a,i*3) = sMat_Xt_m_3.coeffRef(i+a,0);
//			sMat_Xts_3m_2_3m_2.coeffRef(i*3+a,i*3+1) = sMat_Xt_m_3.coeffRef(i+a,1);
//			sMat_Xts_3m_2_3m_2.coeffRef(i*3+a,i*3+2) = sMat_Xt_m_3.coeffRef(i+a,2);
//
//		}
//
//	}
//
//
//
//	SparseMatrix<float> I_mat_m_m(sparse_matrix_size,sparse_matrix_size);
//
//	for(int i = 0 ; i < sparse_matrix_size; i++){
//
//		I_mat_m_m.coeffRef(i,i) = 1.0f;
//	}
//
//
//	SparseMatrix<float> NtN((sparse_matrix_size-2)*3,3);
//	NtN = sMat_Nt_3m_2_m_2 * sMat_N_m_2_3;
//
//
//
//
//
//	/////////////////////////////////////////
//	//////////////solve the matrix equation:
//
//	MatrixXf sub_mat,inv_mat,dMat_Xt_1_m_3;
//	if(!is_pull_back){
//
//		sub_mat.resize(sparse_matrix_size,sparse_matrix_size);
//		inv_mat.resize(sparse_matrix_size,sparse_matrix_size);
//		dMat_Xt_1_m_3.resize(sparse_matrix_size,3);
//
//
//		sub_mat = I_mat_m_m - sMat_K_m_m * u;
//
//		FullPivLU<MatrixXf> lu(sub_mat);
//		inv_mat = lu.inverse();
//
//
//
//		dMat_Xt_1_m_3 = inv_mat * (sMat_Xt_m_3 - sMat_ks_m_3m_2 * sMat_Xts_3m_2_3m_2 * NtN * u);
//
//	}else{
//
//		sub_mat.resize(sparse_matrix_size,sparse_matrix_size);
//		inv_mat.resize(sparse_matrix_size,sparse_matrix_size);
//		dMat_Xt_1_m_3.resize(sparse_matrix_size,3);
//
//
//		sub_mat = I_mat_m_m * (1 + u) - sMat_K_m_m * u;
//
//		FullPivLU<MatrixXf> lu(sub_mat);
//		inv_mat = lu.inverse();
//
//
//
//		dMat_Xt_1_m_3 = inv_mat * (sMat_Xt_m_3 - sMat_ks_m_3m_2 * sMat_Xts_3m_2_3m_2 * NtN * u + A_m_3 * u);
//
//	}
//	////////////////////////////////////////////////
//	////////////////////////////////////////////////
//
//
//
//
//
//
//
//
//	///////////////////////////////////////////////////
//	///////////////////////////////////////////////////
//
//	for(unsigned int i = 0; i < curvePointArray.size();i++){
//
//		curvePointArray[i].x = dMat_Xt_1_m_3(i,0);
//		curvePointArray[i].y = dMat_Xt_1_m_3(i,1);
//		curvePointArray[i].z = dMat_Xt_1_m_3(i,2);
//
//	}
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//}
//
//
//
//
//#endif