#include "geodesic_base_elements.h"
#include "geodesic_mesh_data.h"


//#include <Eigen/Core>
//#include <Eigen/Eigen>
//#include <Eigen/Eigenvalues>
//#include <Eigen/Sparse>

#include <iostream>
#include <fstream>

#include "geodesic_project_utilities.h"
#define PI 3.14159265354 


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

void updata_path(MeshData * mesh_data, std::vector<initPoint> & initial_path, std::vector<Point> & curvePointArray ){
	//define variables
	Eigen::Vector3f cutPlaneNormal;
	Eigen::Vector3f pointOnCutPlane;
	
	std::vector<initPoint> projectedPath;
	std::vector<Eigen::Vector3f> projectDirectionArray;
	initial_path[0].flag = -2;
	initial_path[initial_path.size() - 1].flag = -2;

	float tol = 0.0001f;

	Eigen::Vector3f projectDirection;
	int sourceID = initial_path[0].vertexID;
	int destinationID = initial_path[initial_path.size() - 1].vertexID;
	
	Eigen::Vector3f sourceNormal;
	sourceNormal[0] = mesh_data->getNormal(sourceID)[0];
	sourceNormal[1] = mesh_data->getNormal(sourceID)[1];
	sourceNormal[2] = mesh_data->getNormal(sourceID)[2];
	projectDirectionArray.push_back(sourceNormal);


	initial_path[0].location = initial_path[0].neighbourFacesID;
	initial_path[initial_path.size() - 1].location = initial_path[initial_path.size() - 1].neighbourFacesID;

	//////////////////////////////////////////////////////////////////
	//// STEP 1, Project floating points on their individual face/////
	//////////////////////////////////////////////////////////////////

	 

	for( int a = 1; a < curvePointArray.size()-1;++a)
	{	//////////////////////////////////////////////////////////////////
		///For each floating point, calculate its Css as project direction
		projectDirection = getCurveNormal(curvePointArray[a - 1], curvePointArray[a], curvePointArray[a + 1]);
		Eigen::Vector3f projectDirection2, initialNormal;
		
		initialNormal[0] = initial_path[a].n_x;
		initialNormal[1] = initial_path[a].n_y;
		initialNormal[2] = initial_path[a].n_z;

		projectDirection2 = projectDirection;
		projectDirection2 = projectDirection2 / projectDirection2.norm();

		initPoint projectPoint;
		if(projectDirection.norm() < std::sqrt(tol) || projectDirection2.dot(initialNormal) < 0.1736 )
		{
			std::vector<int> one_ring_vertices;
			std::vector<int> one_ring_faces;
			one_ring_faces = initial_path[a].neighbourFacesID;
			int * faceVertexID = mesh_data->getFace(one_ring_faces[one_ring_faces.size() - 1]);
			one_ring_faces.pop_back();
			one_ring_vertices.push_back(faceVertexID[0]);
			one_ring_vertices.push_back(faceVertexID[1]);
			one_ring_vertices.push_back(faceVertexID[2]);

			for(int i = 0; i < one_ring_faces.size(); ++i)
			{
				int * faceVertexID = mesh_data->getFace(one_ring_faces[i]);
				std::vector<int> tmp;
				tmp.push_back(faceVertexID[0]);
				tmp.push_back(faceVertexID[1]);
				tmp.push_back(faceVertexID[2]);
				appendVector(tmp, one_ring_vertices);

			}
	
			Eigen::Vector3f one_ring_vertices_normal;
			one_ring_vertices_normal = getSVDnomralofOnRingFace(mesh_data, one_ring_vertices);
			projectDirection = one_ring_vertices_normal;
		}else
		{
			//projectDirection = projectDirection / projectDirection.norm();
			Eigen::Vector3f v0,v1,v2,v3,v4,v5,v6;
			v0[0] = curvePointArray[a].x -  initial_path[a - 1].x;
			v0[1] = curvePointArray[a].y -  initial_path[a - 1].y;
			v0[2] = curvePointArray[a].z -  initial_path[a - 1].z;

			v1[0] = curvePointArray[a].x -  curvePointArray[a + 1].x;
			v1[1] = curvePointArray[a].y -  curvePointArray[a + 1].y;
			v1[2] = curvePointArray[a].z -  curvePointArray[a + 1].z;
			
			Eigen::Vector3f new_css;
			new_css = getCurveNormal(initial_path[a - 1], curvePointArray[a], curvePointArray[a + 1]);

			if(new_css.norm() < std::sqrt(tol) || new_css.dot(initialNormal) < 0.1736 )
			{
				std::vector<int> one_ring_vertices;
				std::vector<int> one_ring_faces;
				one_ring_faces = initial_path[a].neighbourFacesID;
				int * faceVertexID = mesh_data->getFace(one_ring_faces[one_ring_faces.size() - 1]);
				one_ring_faces.pop_back();
				one_ring_vertices.push_back(faceVertexID[0]);
				one_ring_vertices.push_back(faceVertexID[1]);
				one_ring_vertices.push_back(faceVertexID[2]);

				for(int i = 0; i < one_ring_faces.size(); ++i)
				{
					int * faceVertexID = mesh_data->getFace(one_ring_faces[i]);
					std::vector<int> tmp;
					tmp.push_back(faceVertexID[0]);
					tmp.push_back(faceVertexID[1]);
					tmp.push_back(faceVertexID[2]);
					appendVector(tmp, one_ring_vertices);

				}
		
				Eigen::Vector3f one_ring_vertices_normal;
				one_ring_vertices_normal = getSVDnomralofOnRingFace(mesh_data, one_ring_vertices);
				projectDirection = one_ring_vertices_normal;
			}else
			{
				v3 = projectDirection;
				v2 = v0.cross(v1);
				v6 = v2 / v2.norm();
				float dotV = (v2 / v2.norm()).dot(v3);
				v4 = v3 - dotV * v6;
				projectDirection = v4 / v4.norm();
			}
		}






		std::vector<int> one_ring_faces;
		one_ring_faces = initial_path[a].neighbourFacesID;
		std::vector<initPoint> projectPointArray;
		for(int b = 0; b < one_ring_faces.size(); b++)
		{
			Point floatPoint;
			floatPoint = curvePointArray[a];

			Eigen::Vector3f projectedPoint;
			bool is_in_triangle = projectFloatingPointOnMesh(mesh_data,  projectDirection, floatPoint, projectPoint, one_ring_faces[b], tol);
			if(is_in_triangle)
			{
				projectPointArray.push_back(projectPoint);
			}
		}

		if(projectPointArray.size() > 1)
		{	
			float dist = 9999.9f;
			int index = 0;
			initPoint tmpPoint;
			for(int j = 0; j < projectPointArray.size(); j++)
			{
				float dist_c = Distl(projectPointArray[j],initial_path[a]);
				if(dist_c < dist)
				{
					dist = dist_c;
					index = j;
				}
			}
			tmpPoint = projectPointArray[index];
			initial_path[a] = tmpPoint;
		}else if(projectPointArray.size() == 1) 
		{
			initial_path[a] = projectPointArray[0];
		}else
		{
			std::cout<<"";
		}
		
		projectDirectionArray.push_back(projectDirection);
	}

	for( int a = 1; a < curvePointArray.size()-1;++a)
	{
		curvePointArray[a].x = initial_path[a].x;
		curvePointArray[a].y = initial_path[a].y;
		curvePointArray[a].z = initial_path[a].z;
	}


	Eigen::Vector3f destinationNormal;
	destinationNormal[0] = mesh_data->getNormal(sourceID)[0];
	destinationNormal[1] = mesh_data->getNormal(sourceID)[1];
	destinationNormal[2] = mesh_data->getNormal(sourceID)[2];
	projectDirectionArray.push_back(destinationNormal);




	//////////////////////////////////////////////////////////////////
	//// STEP 2, trace path on mesh, start from source			 /////
	//////////////////////////////////////////////////////////////////
/*
	int count = 0;
	int cpnum = 1;

	std::vector<int> temp;
	initPoint sourcePoint;
	sourcePoint.x = curvePointArray[0].x;
	sourcePoint.y = curvePointArray[0].y;
	sourcePoint.z = curvePointArray[0].z;
	sourcePoint.n_x = mesh_data->getNormal(sourceID)[0];
	sourcePoint.n_y = mesh_data->getNormal(sourceID)[1];
	sourcePoint.n_z = mesh_data->getNormal(sourceID)[2];
	sourcePoint.flag = -2;
	sourcePoint.location = initial_path[0].neighbourFacesID;
	sourcePoint.neighbourFacesID = initial_path[0].neighbourFacesID;
	sourcePoint.vertexID = sourceID;
	projectedPath.push_back(sourcePoint);


	std::vector<int> nbf;
	nbf = mesh_data->one_ring_face_array[sourceID];
	std::vector<int> nbf2;
	nbf2 = initial_path[1].location;

	std::vector<int> tmp;
	tmp = setIntersection(nbf , nbf2);

	int success = 0;
	if(tmp.size() == 0)
	{
		appendVector(initial_path[1].neighbourFacesID, nbf2);
		tmp = setIntersection(nbf , nbf2);
	}else
	{
		success = 1;
	}

	//std::vector<int> adjFace;
	//adjFace = setDiff(nbf2,tmp);


	if(initial_path[1].flag == 3)
	{
		Eigen::Vector3f currentProjectDirection;
		currentProjectDirection[0] = initial_path[1].n_x;
		currentProjectDirection[1] = initial_path[1].n_y;
		currentProjectDirection[2] = initial_path[1].n_z;
		buildCuttingPlane(sourcePoint, initial_path[1], currentProjectDirection, cutPlaneNormal, pointOnCutPlane);
	}else
	{
		buildCuttingPlane(sourcePoint, initial_path[1], projectDirectionArray[1], cutPlaneNormal, pointOnCutPlane);
	}





	for(int i = 0; i < tmp.size(); i++)
	{
		int faceID = tmp[i];
		initPoint cutPoint;
		bool isCut = getCutPoint( sourcePoint.vertexID, mesh_data , pointOnCutPlane, cutPlaneNormal, faceID, cutPoint);
		if(isCut)
		{
			projectedPath.push_back(cutPoint);
			cpnum = 1;
			break;
		}
	}

	if(success == 1)
		count = 2;
	else
		count = 1;


	for(int i = count; i < initial_path.size()-1 ;i++)
	{
		if(projectedPath[cpnum].flag == 0)
		{
			tmp = setIntersection(initial_path[i - 1].location, initial_path[i].location);
			tmp = setIntersection(projectedPath[cpnum].location[0], tmp);
		}else if(projectedPath[cpnum].flag == 1)
		{
			tmp = setIntersection(initial_path[i - 1].location, initial_path[i].location);
			tmp = setIntersection(projectedPath[cpnum].location, tmp);
		}

		//tmp = setIntersection(projectedPath[cpnum].location , initial_path[i].location);
		if(tmp.size())
		{
			cpnum = cpnum - 1;
			projectedPath.pop_back();
		}



		if(initial_path[i].flag == 3 && initial_path[i + 1].flag == 3)
		{
			Eigen::Vector3f currentProjectDirection;
			currentProjectDirection[0] = initial_path[i].n_x;
			currentProjectDirection[1] = initial_path[i].n_y;
			currentProjectDirection[2] = initial_path[i].n_z;
			buildCuttingPlane(projectedPath[cpnum], curvePointArray[i], currentProjectDirection, cutPlaneNormal, pointOnCutPlane);
		}else
		{
			if(isPathStraight(projectedPath[cpnum], curvePointArray[i],curvePointArray[i+1]))
			{
				buildCuttingPlane(projectedPath[cpnum], curvePointArray[i], initial_path[i+1], cutPlaneNormal, pointOnCutPlane);	
			}
			else
			{
				buildCuttingPlane(projectedPath[cpnum], curvePointArray[i], curvePointArray[i+1], cutPlaneNormal, pointOnCutPlane);	
			}
		}

		//if(initial_path[i].flag == 3 || initial_path[i].flag == -2)
		//{
		//	Eigen::Vector3f currentProjectDirection;
		//	currentProjectDirection[0] = initial_path[i].n_x;
		//	currentProjectDirection[1] = initial_path[i].n_y;
		//	currentProjectDirection[2] = initial_path[i].n_z;
		//	buildCuttingPlane(projectedPath[cpnum], initial_path[i], currentProjectDirection, cutPlaneNormal, pointOnCutPlane);
		//}else
		//{
		//	buildCuttingPlane(projectedPath[cpnum], initial_path[i], projectDirectionArray[i], cutPlaneNormal, pointOnCutPlane);
		//}

		while(true)
		{
			if(projectedPath[cpnum].flag == 1 )
			{
				std::vector<int> nbface;
				nbface = setDiff(projectedPath[cpnum].neighbourFacesID, projectedPath[cpnum].location);

				std::vector<int> direction;
				direction = setIntersection(nbface,initial_path[i].location);

				if(direction.size() == 0)
					direction = setIntersection(initial_path[i].neighbourFacesID,nbface);
				nbface = direction;

				for(int j = 0; j < nbface.size(); j++)
				{
					int faceID = nbface[j];
					initPoint cutPoint;
					bool isCut = getCutPoint( projectedPath[cpnum].vertexID, mesh_data , pointOnCutPlane, cutPlaneNormal, faceID, cutPoint);
					if(isCut)
					{
						projectedPath.push_back(cutPoint);
						cpnum++;
						break;
					}
				}
			}
			//else if( projectedPath[cpnum].flag == -2)
			//{
			//	std::vector<int> nbface;
			//	nbface = projectedPath[cpnum].neighbourFacesID;

			//	std::vector<int> direction;
			//	direction = setIntersection(nbface,initial_path[i].location);

			//	if(direction.size() == 0)
			//		direction = setIntersection(initial_path[i].neighbourFacesID,nbface);
			//	nbface = direction;

			//	for(int j = 0; j < nbface.size(); j++)
			//	{
			//		int faceID = nbface[j];
			//		initPoint cutPoint;
			//		bool isCut = getCutPoint( projectedPath[cpnum].vertexID, mesh_data , pointOnCutPlane, cutPlaneNormal, faceID, cutPoint);
			//		if(isCut)
			//		{
			//			projectedPath.push_back(cutPoint);
			//			cpnum++;
			//			break;
			//		}
			//	}			
			//
			//}
			else
			{
				int faceID;
				faceID = projectedPath[cpnum].location[1];
				initPoint cutPoint;
				bool isCut = getCutPoint( projectedPath[cpnum].cutEdge, mesh_data , pointOnCutPlane, cutPlaneNormal, faceID, cutPoint);
				if(isCut)
				{
					projectedPath.push_back(cutPoint);
					cpnum++;

				}
			}


			if(projectedPath[projectedPath.size() - 1].flag == 1)
			{
				if(projectedPath[projectedPath.size() - 1].location.size() == 1)
				{
					isAdjacent( mesh_data, projectedPath.size() - 1].location[0], int faceID1)
				}
				else
				{

				}
			}



			tmp = setIntersection(projectedPath[projectedPath.size() - 1].location[0], initial_path[i].location);

			if(tmp.size())
			{
				break;
			}

		}
	}

	projectedPath[projectedPath.size() - 1].flag = 1;

	for( int a = 1; a < curvePointArray.size()-1;++a)
	{
		curvePointArray[a].x = initial_path[a].x;
		curvePointArray[a].y = initial_path[a].y;
		curvePointArray[a].z = initial_path[a].z;
	}




    mesh_data->projected_path_array[ initial_path[initial_path.size() - 1].vertexID ] = projectedPath;

*/
}