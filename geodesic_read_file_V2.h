#ifndef geodesic_read_file_h
#define geodesic_read_file_h

#include <assert.h>
#include <math.h>
#include <limits>
#include <fstream>
#include "geodesic_mesh_data.h"



#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif



namespace geodesic_file{

	inline void build_one_ring_vertex_list(MeshData& mesh_data, int point_0, int point_1, int point_2)
	{
		bool is_exist = false;
		///////////////build one-ring-vertex list:
		// do point_0
		for(unsigned int a = 0; a < mesh_data.one_ring_vertex_array[point_0].size();++a){
			if(mesh_data.one_ring_vertex_array[point_0][a] == point_1){
				is_exist = true;
				break;
			}
		}
		if(is_exist == false){
			mesh_data.one_ring_vertex_array[point_0].push_back(point_1);
		}

		is_exist = false;
		for(unsigned int a = 0; a < mesh_data.one_ring_vertex_array[point_0].size();++a){
			if(mesh_data.one_ring_vertex_array[point_0][a] == point_2){
				is_exist = true;
				break;
			}
		}
		if(is_exist == false){
			mesh_data.one_ring_vertex_array[point_0].push_back(point_2);
		}
	};



	inline void build_one_ring_face_to_vertex_list(MeshData& mesh_data, int point_0, int index)
	{
		bool is_exist = false;
		for(unsigned int a = 0; a < mesh_data.one_ring_face_array[point_0].size();++a){
			if(index == mesh_data.one_ring_face_array[point_0][a]){
				is_exist = true;
				break;
			}
		}
		if(is_exist == false){
			mesh_data.one_ring_face_array[point_0].push_back(index);
		}
	};


	inline void build_adjacent_faces_to_face_list(MeshData& mesh_data, int facePointID0, int facePointID1, int facePointID2, int index)
	{
		for(int a = 0; a < index; ++a)
		{
			int counter = 0;
			int * facePointIDs = mesh_data.getFace(a);
			for(unsigned int i = 0; i < 3; ++i)
			{
				if(facePointIDs[i] == facePointID0 || facePointIDs[i] == facePointID1 || facePointIDs[i] == facePointID2)
					counter++;
			}
			if( counter == 2 )
			{
				mesh_data.adjacent_face_array[index].push_back(a);
				mesh_data.adjacent_face_array[a].push_back(index);
			}
		}
	}


	//template<class Points, class Faces>
	inline bool read_mesh_from_file(char* filename, MeshData& mesh_data){


		std::ifstream file(filename);
		assert(file.is_open());
		if(!file.is_open()) return false;

		int num_points;
		file >> num_points;
		assert(num_points>=3);

		int num_faces;
		file >> num_faces;

		int num_vertex_normals;
		file>> num_vertex_normals;

		mesh_data.init(num_points, num_faces);

		float* points = mesh_data.getVertex(0);
		int* faces = mesh_data.getFace(0);
		float * vertex_normals = mesh_data.getVertexNormal(0);

		//bool * vertex_tags = mesh_data.getTag(0);



		//ANNpointArray pointArray = annAllocPts(num_points,3);


		for(int index = 0; index < num_points; ++index){
			file >> *points;
			//pointArray[index][0] =  points[0];
			++points;
			file >> *points;
			//pointArray[index][1] =  points[0];
			++points;
			file >> *points;
			//pointArray[index][2] =  points[0];
			++points;
			//vertex_tags[index] = false;
		}


		for(int index = 0; index < num_faces; ++index){
			int point_0, point_1, point_2;
			file >> *faces;
			point_0 = *faces;
			++faces;
			file >> *faces;
			point_1 = *faces;
			++faces;
			file >> *faces;
			point_2 = *faces;
			++faces;
			

			bool is_exist = false;
			///////////////build one-ring-vertex list:
			// do point_0
			build_one_ring_vertex_list(mesh_data, point_0, point_1, point_2);
			build_one_ring_vertex_list(mesh_data, point_1, point_0, point_2);
			build_one_ring_vertex_list(mesh_data, point_2, point_0, point_1);

			build_one_ring_face_to_vertex_list( mesh_data, point_0, index);
			build_one_ring_face_to_vertex_list( mesh_data, point_1, index);
			build_one_ring_face_to_vertex_list( mesh_data, point_2, index);

			build_adjacent_faces_to_face_list( mesh_data, point_0, point_1, point_2, index);
		}

	
		for(int index = 0; index < num_vertex_normals; ++index){
			file >> *vertex_normals; ++vertex_normals;
			file >> *vertex_normals; ++vertex_normals;
			file >> *vertex_normals; ++vertex_normals;
		}

		//for(int index = 0; index < num_face_normals; ++index){
		//	file >> *face_normals; ++face_normals;
		//	file >> *face_normals; ++face_normals;
		//	file >> *face_normals; ++face_normals;
		//}

		//int leafsize = 1;	
		//mesh_data.meshKdTree= new ANNkd_tree(					// build search structure
		//	pointArray,					// the data points
		//	num_points,						// number of points
		//	3,
		//	leafsize,
		//	ANN_KD_SUGGEST);	


		file.close();

		return true;
	};
};


#endif
