#ifndef geodesic_cage_h
#define geodesic_cage_h



#include "geodesic_mesh_data.h"
#include "geodesic_base_elements.h"





class CELL
{
public: 
	CELL():is_valid(false), ps_point_count(0){}

public:
	//Point cell_MAX;
	Point cell_MIN;
//	vector<Point> contained_point;
	int ps_point_count;
	std::vector<int> contained_point_id;
	bool is_valid;
};

class CAGE
{
public:
	CAGE(std::vector<Mesh_Point> geodesic_array, int seg){
		cage_seg = seg;
		float min_x = 99999.9f;
		float min_y = 99999.9f;
		float min_z = 99999.9f;
		float max_x = -99999.9f;
		float max_y = -99999.9f;
		float max_z = -99999.9f;

		for(int i = 0; i < geodesic_array.size();++i){
			if(geodesic_array[i].point.x < min_x)	min_x = (float)geodesic_array[i].point.x;
			if(geodesic_array[i].point.y < min_y)	min_y = (float)geodesic_array[i].point.y;
			if(geodesic_array[i].point.z < min_z)	min_z = (float)geodesic_array[i].point.z;

			if(geodesic_array[i].point.x > max_x)	max_x = (float)geodesic_array[i].point.x;
			if(geodesic_array[i].point.y > max_y)	max_y = (float)geodesic_array[i].point.y;
			if(geodesic_array[i].point.z > max_z)	max_z = (float)geodesic_array[i].point.z;
		}

		MIN.x = min_x ;
		MIN.y = min_y ;
		MIN.z = min_z ;

		MAX.x = max_x;
		MAX.y = max_y;
		MAX.z = max_z;

		init( MIN,geodesic_array);
	} 



	inline void init(Point min_point,std::vector<Mesh_Point> geodesic_array){

		float x_length = (float)( MAX.x - MIN.x);
		float y_length = (float)( MAX.y - MIN.y);
		float z_length = (float)( MAX.z - MIN.z);
		int cell_number = 0;


		if(x_length < y_length){
			if(x_length < z_length){

				cell_size = (x_length / cage_seg);
				x_seg = cage_seg;
				y_seg = (int)ceil((y_length - x_length) / cell_size ) + x_seg;
				z_seg = (int)ceil((z_length - x_length) / cell_size ) + x_seg;
				cell_number = x_seg * y_seg * z_seg;

			}else{

				cell_size = (z_length / cage_seg);
				z_seg = cage_seg;
				y_seg = (int)ceil((y_length - z_length) / cell_size ) + z_seg;
				x_seg = (int)ceil((x_length - z_length) / cell_size ) + z_seg;
				cell_number = x_seg * y_seg * z_seg;
			}
		}else{
			if(y_length < z_length){
				cell_size = (y_length / cage_seg);
				y_seg = cage_seg;
				z_seg = (int)ceil((z_length - y_length) / cell_size ) + y_seg;
				x_seg = (int)ceil((x_length - y_length) / cell_size ) + y_seg;
				cell_number = x_seg * y_seg * z_seg;

			}else{
				cell_size = (z_length / cage_seg);
				z_seg = cage_seg;
				y_seg = (int)ceil((y_length - z_length) / cell_size ) + z_seg;
				x_seg = (int)ceil((x_length - z_length) / cell_size ) + z_seg;
				cell_number = x_seg * y_seg * z_seg;
			}
		}

		//allocate the memory for the cage
		CELL_ARRAY.resize(cell_number);


		//fill the cage with vertex on the mesh
		for(int i = 0; i < geodesic_array.size();i++){
			int x_id,y_id,z_id;

			x_id = (int)floor((geodesic_array[i].point.x  - MIN.x) / cell_size );
			y_id = (int)floor((geodesic_array[i].point.y  - MIN.y) / cell_size );
			z_id = (int)floor((geodesic_array[i].point.z  - MIN.z) / cell_size );

			if(x_id == x_seg) x_id--;
			if(y_id == y_seg) y_id--;
			if(z_id == z_seg) z_id--;


			int cell_id = x_id + x_seg * y_id + x_seg * y_seg * z_id;

			if(cell_id > cell_number){
				std::cout<<"break";
			}

			CELL_ARRAY[cell_id].contained_point_id.push_back(geodesic_array[i].ID);
			CELL_ARRAY[cell_id].is_valid = true;
			CELL_ARRAY[cell_id].cell_MIN.x = x_id * cell_size + MIN.x;
			CELL_ARRAY[cell_id].cell_MIN.y = y_id * cell_size + MIN.y;
			CELL_ARRAY[cell_id].cell_MIN.z = z_id * cell_size + MIN.z;
			
		}

	}



	inline int getCellIndex(float x, float y, float z){

			int x_id,y_id,z_id;
			x_id = (int)floor(( x  - MIN.x ) / cell_size ) ;
			y_id = (int)floor(( y  - MIN.y ) / cell_size ) ;
			z_id = (int)floor(( z  - MIN.z ) / cell_size ) ;

			if(x_id == x_seg) x_id--;
			if(y_id == y_seg) y_id--;
			if(z_id == z_seg) z_id--;

			return  x_id + x_seg * y_id + x_seg * y_seg * z_id;
	}
public:
	Point MIN;
	Point MAX;
	float cell_size;
	int x_seg,y_seg,z_seg;
	int cage_seg;
	std::vector<CELL> CELL_ARRAY;

};



#endif