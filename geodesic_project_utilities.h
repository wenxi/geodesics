#pragma once

#include "geodesic_base_elements.h"
#include "geodesic_mesh_data.h"


#include <Eigen/Core>
#include <Eigen/Eigen>
#include <Eigen/Eigenvalues>
#include <Eigen/Sparse>






/////////////////////////////////////////////////////////////////

//float Distl(Point point0, Point point1){
//	return sqrt((point0.x - point1.x) * (point0.x - point1.x) + (point0.y - point1.y) * (point0.y - point1.y) + (point0.z - point1.z) * (point0.z - point1.z)); 
//}
//
//
//float Distl(initPoint point0, initPoint point1){
//	return sqrt((point0.x - point1.x) * (point0.x - point1.x) + (point0.y - point1.y) * (point0.y - point1.y) + (point0.z - point1.z) * (point0.z - point1.z)); 
//}


template<typename T1, typename T2>
float Distl(T1 point0, T2 point1){
	return sqrt((point0.x - point1.x) * (point0.x - point1.x) + (point0.y - point1.y) * (point0.y - point1.y) + (point0.z - point1.z) * (point0.z - point1.z)); 
}


template<typename T1, typename T2>
float sqrtDist(T1 point0, T2 point1){
	return sqrt((point0.x - point1.x) * (point0.x - point1.x) + (point0.y - point1.y) * (point0.y - point1.y) + (point0.z - point1.z) * (point0.z - point1.z)); 
}


/////////////////////////////////////////////////////////////////


void appendVector(std::vector<int> a, std::vector<int> & b)
{
	for(int i = 0; i < a.size();i++)
	{	
		bool is_exist = false;
		for(int j = 0; j < b.size();j++)
		{
			if(a[i] == b[j])
			{
				is_exist = true;
				break;
			}
		}

		if(!is_exist)
		{
			b.push_back(a[i]);
		}
	}
}

//////////////////////////////////////////////////////////////////////

void appendVector(int a, std::vector<int> & b)
{

		bool is_exist = false;
		for(int j = 0; j < b.size();j++)
		{
			if(a == b[j])
			{
				is_exist = true;
				break;
			}
		}

		if(!is_exist)
		{
			b.push_back(a);
		}

}

//////////////////////////////////////////////////////////////////////


std::vector<int> setIntersection( std::vector<int> a , std::vector<int> b)
{
	std::vector<int> intersection;

	for(int i = 0; i < a.size(); i++)
	{
		for(int j = 0; j < b.size(); j++)
		{
			if( a[i] == b[j] )
			{
				intersection.push_back(a[i]);
			}
		}
	}
	return intersection;
}


//////////////////////////////////////////////////////////////////////


std::vector<int> setIntersection( int a , std::vector<int> b)
{
	std::vector<int> intersection;

	for(int j = 0; j < b.size(); j++)
	{
		if( a == b[j] )
		{
			intersection.push_back(a);
		}
	}

	return intersection;
}

//////////////////////////////////////////////////////////////////////

std::vector<int> setDiff( std::vector<int> a , std::vector<int> b )
{
	std::vector<int> difference;
	std::vector<int> intersection;
	intersection = setIntersection( a , b );

	for(int i = 0; i < a.size(); i++)
	{
		bool is_exist = false;
		for(int j = 0; j < intersection.size(); j++)
		{		
			if( intersection[j] == a[i] ) 
			{
				is_exist = true;
				break;
			}
		}
		if(!is_exist)
		{
			difference.push_back(a[i]);
		}
	}

	return difference;
}

//////////////////////////////////////////////////////////////////////

std::vector<int> setDiff( std::vector<int> a , int b )
{
	std::vector<int> difference;
	std::vector<int> intersection;

	for(int i = 0; i < a.size(); i++)
	{

		if( b == a[i] ) 
			continue;

		difference.push_back(a[i]);
	}

	return difference;
}



//////////////////////////////////////////////////////////////////////

float area(Eigen::Vector3f & p0, Eigen::Vector3f & p1, Eigen::Vector3f & p2)
{
	Eigen::Vector3f edge0,edge1,v1;
	edge0 = p1 - p0;
	edge1 = p2 - p0;
	v1 = edge0.cross(edge1);
	return v1.norm() / 2;
}

/////////////////////////////////////////////////////////////////

int updateProjectionIfOnEdge(Eigen::Vector3f & p, Eigen::Vector3f & p1, Eigen::Vector3f & p2, float tol)
{
	if((p - p1).norm() < tol)
	{
		p = p1;
		return 0;
	}
	else if((p - p2).norm() < tol)
	{
		p = p2;
		return 1;
	}
	else
	{
		p = ((p - p1).dot(p2 - p1) / (p2 - p1).norm())*(p2 - p1) / (p2 - p1).norm() + p1;
		return 2;
	}
}

/////////////////////////////////////////////////////////////////
bool is_in_triangle(MeshData * mesh_data, Eigen::Vector3f & p, int faceID, int pID0, int pID1, int pID2, initPoint & projection, float tol)
{

	initPoint  projectPoint;
	Eigen::Vector3f p0, p1, p2, n_p0, n_p1, n_p2, new_n;
	p0[0] = mesh_data->getPoint(pID0).x;
	p0[1] = mesh_data->getPoint(pID0).y;
	p0[2] = mesh_data->getPoint(pID0).z;
	p1[0] = mesh_data->getPoint(pID1).x;
	p1[1] = mesh_data->getPoint(pID1).y;
	p1[2] = mesh_data->getPoint(pID1).z;
	p2[0] = mesh_data->getPoint(pID2).x;
	p2[1] = mesh_data->getPoint(pID2).y;
	p2[2] = mesh_data->getPoint(pID2).z;

	
	float triangle_area = area(p0,p1,p2);
	float area1 = area(p,p1,p2);
	float area2 = area(p0,p,p2);
	float area3 = area(p0,p1,p);



	n_p0[0] = mesh_data->getVertexNormal(pID0)[0];
	n_p0[1] = mesh_data->getVertexNormal(pID0)[1];
	n_p0[2] = mesh_data->getVertexNormal(pID0)[2];
	n_p1[0] = mesh_data->getVertexNormal(pID1)[0];
	n_p1[1] = mesh_data->getVertexNormal(pID1)[1];
	n_p1[2] = mesh_data->getVertexNormal(pID1)[2];
	n_p2[0] = mesh_data->getVertexNormal(pID2)[0];
	n_p2[1] = mesh_data->getVertexNormal(pID2)[1];
	n_p2[2] = mesh_data->getVertexNormal(pID2)[2];
	n_p0 = n_p0 / n_p0.norm();
	n_p1 = n_p1 / n_p1.norm();
	n_p2 = n_p2 / n_p2.norm();


	float projected_area = area1 + area2 + area3;

	if (std::abs(projected_area - triangle_area) < tol )
	{
		if(area1 < tol )
		{ 
			switch( updateProjectionIfOnEdge(p,p1,p2,tol)) 
			{
			case 0:
				projectPoint.x = p1[0];
				projectPoint.y = p1[1];
				projectPoint.z = p1[2];
				projectPoint.n_x = n_p1[0];
				projectPoint.n_y = n_p1[1];
				projectPoint.n_z = n_p1[2];
				projectPoint.flag = 1;
				projectPoint.vertexID = pID1;
				projectPoint.location = mesh_data->one_ring_face_array[pID1];
				projectPoint.neighbourFacesID = mesh_data->one_ring_face_array[pID1];
				break;
			case 1:
				projectPoint.x = p2[0];
				projectPoint.y = p2[1];
				projectPoint.z = p2[2];
				projectPoint.n_x = n_p2[0];
				projectPoint.n_y = n_p2[1];
				projectPoint.n_z = n_p2[2];
				projectPoint.flag = 1;
				projectPoint.vertexID = pID2;
				projectPoint.location = mesh_data->one_ring_face_array[pID2];
				projectPoint.neighbourFacesID = mesh_data->one_ring_face_array[pID2];
				break;
			case 2:
				projectPoint.x = p[0];
				projectPoint.y = p[1];
				projectPoint.z = p[2];
				std::vector<int> neighbour1;
				std::vector<int> neighbour2;
				std::vector<int> sharedFace;
				Eigen::Vector3f n1,n2,n;
				n1 = n_p1;
				n2 = n_p2;
				n = ((p - p2).norm()/(p1 - p2).norm())*n1 + ((p - p1).norm()/(p1 - p2).norm())*n2;
				n = n/n.norm();
				projectPoint.n_x = n[0];
				projectPoint.n_y = n[1];
				projectPoint.n_z = n[2];
				projectPoint.flag = 0;
				std::vector<int> edge;
				edge.push_back(pID1);
				edge.push_back(pID2);
				projectPoint.cutEdge = edge;
				neighbour1 = mesh_data->one_ring_face_array[pID1];
				neighbour2 = mesh_data->one_ring_face_array[pID2];
				sharedFace = setIntersection(neighbour1 , neighbour2);
				projectPoint.location = sharedFace;
				projectPoint.neighbourFacesID = mesh_data->get_one_ring_faces_of_face(sharedFace[0]);
				appendVector(mesh_data->get_one_ring_faces_of_face(sharedFace[1]) , projectPoint.neighbourFacesID);
				break;
			}
		}
		else if(area2  < tol )
		{
			switch( updateProjectionIfOnEdge(p,p0,p2,tol) )
			{
			case 0:
				projectPoint.x = p0[0];
				projectPoint.y = p0[1];
				projectPoint.z = p0[2];
				projectPoint.n_x = n_p0[0];
				projectPoint.n_y = n_p0[1];
				projectPoint.n_z = n_p0[2];
				projectPoint.flag = 1;
				projectPoint.vertexID = pID0;
				projectPoint.location = mesh_data->one_ring_face_array[pID0];
				projectPoint.neighbourFacesID = mesh_data->one_ring_face_array[pID0];
				break;
			case 1:
				projectPoint.x = p2[0];
				projectPoint.y = p2[1];
				projectPoint.z = p2[2];
				projectPoint.n_x = n_p2[0];
				projectPoint.n_y = n_p2[1];
				projectPoint.n_z = n_p2[2];
				projectPoint.flag = 1;
				projectPoint.vertexID = pID2;
				projectPoint.location = mesh_data->one_ring_face_array[pID2];
				projectPoint.neighbourFacesID = mesh_data->one_ring_face_array[pID2];
				break;
			case 2:
				projectPoint.x = p[0];
				projectPoint.y = p[1];
				projectPoint.z = p[2];
				std::vector<int> neighbour1;
				std::vector<int> neighbour2;
				std::vector<int> sharedFace;
				Eigen::Vector3f n1,n2,n;
				n1[0] = n_p0[0];
				n1[1] = n_p0[1];
				n1[2] = n_p0[2];
				n2[0] = n_p2[0];
				n2[1] = n_p2[1];
				n2[2] = n_p2[2];
				n = ((p - p2).norm()/(p0 - p2).norm())*n1 + ((p - p0).norm()/(p0 - p2).norm())*n2;
				n = n/n.norm();
				projectPoint.n_x = n[0];
				projectPoint.n_y = n[1];
				projectPoint.n_z = n[2];
				projectPoint.flag = 0;
				std::vector<int> edge;
				edge.push_back(pID0);
				edge.push_back(pID2);
				projectPoint.cutEdge = edge;
				neighbour1 = mesh_data->one_ring_face_array[pID0];
				neighbour2 = mesh_data->one_ring_face_array[pID2];
				sharedFace = setIntersection(neighbour1 , neighbour2);
				projectPoint.location = sharedFace;
				projectPoint.neighbourFacesID = mesh_data->get_one_ring_faces_of_face(sharedFace[0]);
				appendVector(mesh_data->get_one_ring_faces_of_face(sharedFace[1]) , projectPoint.neighbourFacesID);
				break;
			}
		}
		else if(area3  < tol )
		{
			switch (updateProjectionIfOnEdge(p,p0,p1,tol))
			{
			case 0:
				projectPoint.x = p0[0];
				projectPoint.y = p0[1];
				projectPoint.z = p0[2];
				projectPoint.n_x = n_p0[0];
				projectPoint.n_y = n_p0[1];
				projectPoint.n_z = n_p0[2];
				projectPoint.flag = 1;
				projectPoint.vertexID = pID0;
				projectPoint.location = mesh_data->one_ring_face_array[pID0];
				projectPoint.neighbourFacesID = mesh_data->one_ring_face_array[pID0];
				break;
			case 1:
				projectPoint.x = p1[0];
				projectPoint.y = p1[1];
				projectPoint.z = p1[2];
				projectPoint.n_x = n_p1[0];
				projectPoint.n_y = n_p1[1];
				projectPoint.n_z = n_p1[2];
				projectPoint.flag = 1;
				projectPoint.vertexID = pID1;
				projectPoint.location = mesh_data->one_ring_face_array[pID1];
				projectPoint.neighbourFacesID = mesh_data->one_ring_face_array[pID1];
				break;
			case 2:
				projectPoint.x = p[0];
				projectPoint.y = p[1];
				projectPoint.z = p[2];
				Eigen::Vector3f n1,n2,n;
				n1 = n_p0;
				n2 = n_p1;
				n = ((p - p2).norm()/(p0 - p1).norm())*n1 + ((p - p0).norm()/(p0 - p1).norm())*n2;
				n = n/n.norm();
				projectPoint.n_x = n[0];
				projectPoint.n_y = n[1];
				projectPoint.n_z = n[2];
				projectPoint.flag = 0;
				std::vector<int> neighbour1;
				std::vector<int> neighbour2;
				std::vector<int> sharedFace;
				std::vector<int> edge;
				edge.push_back(pID0);
				edge.push_back(pID1);
				projectPoint.cutEdge = edge;
				neighbour1 = mesh_data->one_ring_face_array[pID0];
				neighbour2 = mesh_data->one_ring_face_array[pID1];
				sharedFace = setIntersection(neighbour1 , neighbour2);
				projectPoint.location = sharedFace;
				projectPoint.neighbourFacesID = mesh_data->get_one_ring_faces_of_face(sharedFace[0]);
				appendVector(mesh_data->get_one_ring_faces_of_face(sharedFace[1]) , projectPoint.neighbourFacesID);
				break;
			}
		}
		else
		{
			projectPoint.x = p[0];
			projectPoint.y = p[1];
			projectPoint.z = p[2];
			new_n = (area1 / triangle_area) * n_p0 + (area2 / triangle_area) * n_p1 + (area3 / triangle_area) * n_p2;
			new_n = new_n/new_n.norm();
			projectPoint.n_x = new_n[0];
			projectPoint.n_y = new_n[1];
			projectPoint.n_z = new_n[2];
			projectPoint.flag = 2;
			projectPoint.location.push_back( faceID );
			projectPoint.neighbourFacesID = mesh_data->get_one_ring_faces_of_face(faceID);
		}
		projection = projectPoint;
		return true;
	}
	else
	{
		return false;
	}
}

/////////////////////////////////////////////////////////////////

bool getIntersection(Eigen::Vector3f ep_0, Eigen::Vector3f ep_1, Eigen::Vector3f cp_p, Eigen::Vector3f cp_n, Eigen::Vector3f &ip, float tol ,float & lamda)
{
	lamda = ( (cp_p - ep_1).dot(cp_n) )/( (ep_0 - ep_1).dot(cp_n) );
	ip = lamda * ep_0 + (1 - lamda) * ep_1;

	float n_0 = (ip - ep_0).norm();
	float n_1 = (ip - ep_1).norm();



	if( n_0 < tol )
	{
		ip = ep_0;
		lamda = 1.0f;
	}
	else if( n_1 < tol )
	{
		ip = ep_1;
		lamda = 0.0f;
	}

	if(lamda >= 0.0f && lamda <= 1.0f)
	{	
		return true;
	}
	else
	{
		return false;
	}
}

/////////////////////////////////////////////////////////////////

int getNextFace(MeshData * mesh_data, int cutFace, std::vector<int> previousCutEdge, std::vector<std::vector<int>> & nextCutEdges)
{
	int previousCutFace = cutFace;
	std::vector<int> adjacentFaces;
	adjacentFaces = mesh_data->adjacent_face_array[previousCutFace];
	int nextFaceID = 0;

	for(int a = 0; a < adjacentFaces.size(); ++a)
	{
		int counter = 0;
		int * facePointIDs =  mesh_data->getFace(adjacentFaces[a]);
		for(unsigned int i = 0; i < 3; ++i)
		{
			if(facePointIDs[i] == previousCutEdge[0] || facePointIDs[i] == previousCutEdge[1])
				counter++;
		}
		if( counter == 2 )
		{
			int nextFaceID = adjacentFaces[a];
			int * nextFaceVerticesID = mesh_data->getFace(nextFaceID);
			for(int b = 0; b < 3; b++)
			{
				if(nextFaceVerticesID[b] != previousCutEdge[0] && nextFaceVerticesID[b] != previousCutEdge[1])
				{
					std::vector<int> newCutEdge;
					newCutEdge.push_back(nextFaceVerticesID[b]);
					newCutEdge.push_back(previousCutEdge[0]);
					nextCutEdges.push_back(newCutEdge);
					newCutEdge.clear();

					newCutEdge.push_back(nextFaceVerticesID[b]);
					newCutEdge.push_back(previousCutEdge[1]);
					nextCutEdges.push_back(newCutEdge);
					newCutEdge.clear();
				}
			}
			return adjacentFaces[a];
		}
	}

	return -1;
}

/////////////////////////////////////////////////////////////////

Eigen::Vector3f getNewNormalforCutPoint(MeshData * mesh_data, std::vector<int> cutEdge, Eigen::Vector3f ip )
{
	Eigen::Vector3f n_1, n_2, ep_1, ep_2, normal;
	n_1[0] = mesh_data->getVertexNormal(cutEdge[0])[0];
	n_1[1] = mesh_data->getVertexNormal(cutEdge[0])[1];
	n_1[2] = mesh_data->getVertexNormal(cutEdge[0])[2];
	n_2[0] = mesh_data->getVertexNormal(cutEdge[1])[0];
	n_2[1] = mesh_data->getVertexNormal(cutEdge[1])[1];
	n_2[2] = mesh_data->getVertexNormal(cutEdge[1])[2];

	ep_1[0] = mesh_data->getPoint(cutEdge[0]).x;
	ep_1[1] = mesh_data->getPoint(cutEdge[0]).y;
	ep_1[2] = mesh_data->getPoint(cutEdge[0]).z;
	ep_2[0] = mesh_data->getPoint(cutEdge[1]).x;
	ep_2[1] = mesh_data->getPoint(cutEdge[1]).y;
	ep_2[2] = mesh_data->getPoint(cutEdge[1]).z;

	normal = (ip - ep_2).norm() * n_1 / (ep_1 - ep_2).norm() + (ip - ep_1).norm() * n_2 / (ep_1 - ep_2).norm();
	normal = normal/normal.norm();
	return normal;
}

/////////////////////////////////////////////////////////////////

template <typename T1, typename T2, typename T3>
bool isPathStraight(T1 point0, T2 point1, T3 point2)
{
	Eigen::Vector3f floatPoint_0, floatPoint_1, floatPoint_2;
	floatPoint_0[0] = point0.x;
	floatPoint_0[1] = point0.y;
	floatPoint_0[2] = point0.z;
	floatPoint_1[0] = point1.x;
	floatPoint_1[1] = point1.y;
	floatPoint_1[2] = point1.z;
	floatPoint_2[0] = point2.x;
	floatPoint_2[1] = point2.y;
	floatPoint_2[2] = point2.z;

	float a = area(floatPoint_0,floatPoint_1,floatPoint_2);

	if(a < 0.001)
		return true;
	else
		return false;

}

/////////////////////////////////////////////////////////////////////////////////

template <typename T1, typename T2, typename T3>
Eigen::Vector3f getCurveNormal( T1 point0, T2 point1, T3 point2 )
{
	Eigen::Vector3f floatingP_0, floatingP_1, floatingP_2, Css;
	floatingP_0[0] = point0.x;
	floatingP_0[1] = point0.y;
	floatingP_0[2] = point0.z;
	floatingP_1[0] = point1.x;
	floatingP_1[1] = point1.y;
	floatingP_1[2] = point1.z;
	floatingP_2[0] = point2.x;
	floatingP_2[1] = point2.y;
	floatingP_2[2] = point2.z;


	//float g = (floatingP_0 - floatingP_1).norm();
	//float f = (floatingP_2 - floatingP_1).norm();
	//
	//g = 1.0f;
	//f = 1.0f;
	//Css = 2*floatingP_2 / (f *(g + f)) + 2*floatingP_0 / (g * (g + f)) - 2 * floatingP_1 / (g * f);
	//Css = floatingP_2 / (2 * f) + floatingP_0 / (2 * g) - floatingP_1*(f+g) / (2 * g * f);
	Css = floatingP_2 + floatingP_0 - floatingP_1 * 2;
	return Css;
}

/////////////////////////////////////////////////////////////////////////////////

Eigen::Vector3f getSVDnomralofOnRingFace(MeshData * mesh_data, std::vector<int> one_ring_vertices)
{
	int row = (int)one_ring_vertices.size();
	Eigen::MatrixX3f eigenPointMatrix;
	eigenPointMatrix.resize(row,3);
	Eigen::MatrixX3f transposeMatrix;
	transposeMatrix.resize(row,3);
	Eigen::VectorXf meanVector;

	for(int a = 0; a < one_ring_vertices.size();++a ){
		float n_x, n_y, n_z;
		n_x = mesh_data->getVertexNormal(one_ring_vertices[a])[0];
		n_y = mesh_data->getVertexNormal(one_ring_vertices[a])[1];
		n_z = mesh_data->getVertexNormal(one_ring_vertices[a])[2];	
		eigenPointMatrix(a,0) = n_x;
		eigenPointMatrix(a,1) = n_y;
		eigenPointMatrix(a,2) = n_z;	
	}

	//std::ofstream eigenPointMatrixFile("eigenPointMatrix.txt");
	//	for(int a = 0; a < eigenPointMatrix.rows();a++)
	//{
	//	for(int b = 0; b < eigenPointMatrix.cols();b++)
	//	{
	//		eigenPointMatrixFile<<eigenPointMatrix(a,b)<<" ";
	//	}
	//	eigenPointMatrixFile<<std::endl;
	//}
	//eigenPointMatrixFile.close();


	//std::ofstream covarianceFile("covariance.txt");
	Eigen::MatrixXf covariance =  eigenPointMatrix.transpose()* eigenPointMatrix;

	//for(int a = 0; a < covariance.rows();a++)
	//{
	//	for(int b = 0; b < covariance.cols();b++)
	//	{
	//		covarianceFile<<covariance(a,b)<<" ";
	//	}
	//	covarianceFile<<std::endl;
	//}
	//covarianceFile.close();


	Eigen::EigenSolver<Eigen::MatrixXf> m_solve(covariance);
	Eigen::VectorXf eigenvalues = m_solve.eigenvalues().real();
	Eigen::MatrixXf eignevectors = m_solve.eigenvectors().real();
	Eigen::Vector3f vector(0.0,0.0,0.0);
	if(eigenvalues(0) > eigenvalues(1))
	{
		if (eigenvalues(0) > eigenvalues(2))
		{
			vector[0] = eignevectors(0,0);
			vector[1] = eignevectors(1,0);
			vector[2] = eignevectors(2,0);
		}
		else
		{
			vector[0] = eignevectors(0,2);
			vector[1] = eignevectors(1,2);
			vector[2] = eignevectors(2,2);
		}
	}
	else{
		if(eigenvalues(1) > eigenvalues(2))
		{
			vector[0] = eignevectors(0,1);
			vector[1] = eignevectors(1,1);
			vector[2] = eignevectors(2,1);
		}else{
			vector[0] = eignevectors(0,2);
			vector[1] = eignevectors(1,2);
			vector[2] = eignevectors(2,2);
		}
	}

	vector = vector/vector.norm();
	return vector;
}

/////////////////////////////////////////////////////////////////////////////////




//bool projectFloatingPointOnMesh(MeshData * mesh_data, Point point0, Point point1, Point point2, Eigen::Vector3f & projection, int faceID)
//{
//	Eigen::Vector3f floatPoint_0, floatPoint_1, floatPoint_2;
//	floatPoint_0[0] = point0.x;
//	floatPoint_0[1] = point0.y;
//	floatPoint_0[2] = point0.z;
//	floatPoint_1[0] = point1.x;
//	floatPoint_1[1] = point1.y;
//	floatPoint_1[2] = point1.z;
//	floatPoint_2[0] = point2.x;
//	floatPoint_2[1] = point2.y;
//	floatPoint_2[2] = point2.z;
//
//
//	Eigen::Vector3f Css, floatingP_0, floatingP_1, floatingP_2;
//	floatingP_0 = floatPoint_0;
//	floatingP_1 = floatPoint_1;
//	floatingP_2 = floatPoint_2;
//
//
//	float g = (floatingP_0 - floatingP_1).norm();
//	float f = (floatingP_2 - floatingP_1).norm();
//
//	Css = 2*floatingP_2 / (f *(g + f)) + 2*floatingP_0 / (g * (g + f)) - 2 * floatingP_1 / (g * f);
//	Css.normalized();
//
//
//
//	int * faceVertexID = mesh_data->getFace(faceID);
//	Eigen::Vector3f v_1,fv_0,fv_1,fv_2,v_3,v_4,v_5,v_n;
//
//	v_1[0] = floatPoint_1[0] - mesh_data->getPoint(faceVertexID[0]).x;
//	v_1[1] = floatPoint_1[1] - mesh_data->getPoint(faceVertexID[0]).y;
//	v_1[2] = floatPoint_1[2] - mesh_data->getPoint(faceVertexID[0]).z;
//
//	//three vertices on a triangle 
//	fv_0[0] = mesh_data->getPoint(faceVertexID[0]).x;
//	fv_0[1] = mesh_data->getPoint(faceVertexID[0]).y;
//	fv_0[2] = mesh_data->getPoint(faceVertexID[0]).z;
//	fv_1[0] = mesh_data->getPoint(faceVertexID[1]).x;
//	fv_1[1] = mesh_data->getPoint(faceVertexID[1]).y;
//	fv_1[2] = mesh_data->getPoint(faceVertexID[1]).z;
//	fv_2[0] = mesh_data->getPoint(faceVertexID[2]).x;
//	fv_2[1] = mesh_data->getPoint(faceVertexID[2]).y;
//	fv_2[2] = mesh_data->getPoint(faceVertexID[2]).z;
//
//	// triangle normal
//	v_n = (fv_1 - fv_0).cross(fv_2 - fv_0);
//	//v_3 = (fv_0 + (v_1 - ( v_1.dot(v_n))*v_n));
//
//
//
//
//	float lamda_n = -1 * ( ( floatingP_1 - fv_0 ).dot(v_n) ) / ( Css.dot(v_n) );
//	v_4 = floatingP_1 + lamda_n * Css;
//	projection = v_4;
//	return is_in_triangle(v_4, fv_0, fv_1, fv_2);
//}

///////////////////////////////////////////////////////////////

bool projectFloatingPointOnMesh(MeshData * mesh_data, Eigen::Vector3f & projectDirection, Point & floatPoint, initPoint & projection, int faceID, float tol)
{
	Eigen::Vector3f floatingP_0, floatingP_1, floatingP_2;

	floatingP_1[0] = floatPoint.x;
	floatingP_1[1] = floatPoint.y;
	floatingP_1[2] = floatPoint.z;

	Eigen::Vector3f Css;
	Css = projectDirection;

	int * faceVertexID = mesh_data->getFace(faceID);
	Eigen::Vector3f v_1,fv_0,fv_1,fv_2,v_3,v_4,v_5,v_n;

	v_1[0] = floatingP_1[0] - mesh_data->getPoint(faceVertexID[0]).x;
	v_1[1] = floatingP_1[1] - mesh_data->getPoint(faceVertexID[0]).y;
	v_1[2] = floatingP_1[2] - mesh_data->getPoint(faceVertexID[0]).z;

	//three vertices on a triangle 
	fv_0[0] = mesh_data->getPoint(faceVertexID[0]).x;
	fv_0[1] = mesh_data->getPoint(faceVertexID[0]).y;
	fv_0[2] = mesh_data->getPoint(faceVertexID[0]).z;
	fv_1[0] = mesh_data->getPoint(faceVertexID[1]).x;
	fv_1[1] = mesh_data->getPoint(faceVertexID[1]).y;
	fv_1[2] = mesh_data->getPoint(faceVertexID[1]).z;
	fv_2[0] = mesh_data->getPoint(faceVertexID[2]).x;
	fv_2[1] = mesh_data->getPoint(faceVertexID[2]).y;
	fv_2[2] = mesh_data->getPoint(faceVertexID[2]).z;

	// triangle normal
	v_n = (fv_1 - fv_0).cross(fv_2 - fv_0);
	//v_3 = (fv_0 + (v_1 - ( v_1.dot(v_n))*v_n));

	float lamda_n = -1 * ( ( floatingP_1 - fv_0 ).dot(v_n) ) / ( Css.dot(v_n) );
	v_4 = floatingP_1 + lamda_n * Css;

	bool is_projection_valid = false;
	is_projection_valid = is_in_triangle(mesh_data, v_4, faceID, faceVertexID[0], faceVertexID[1], faceVertexID[2],  projection,  tol);

	if( is_projection_valid )
	{
		if((v_4 - floatingP_1).norm() < tol)
		{
			projection.isOnMesh = true;
		}
		return true;
	}else
		return false;


	
	//return is_in_triangle(v_4, fv_0, fv_1, fv_2);
}


///////////////////////////////////////////////////////////////

//
//std::vector<int> isAdjacent(MeshData * mesh_data, int faceID0, int faceID1)
//{
//	std::vector<int> adjacentFaces;
//	adjacentFaces = mesh_data->get_one_ring_faces_of_face(faceID0);
//
//	for(int i = 0; i < adjacentFaces.size(); i++)
//	{
//		if(adjacentFaces[i] == faceID1)
//		{
//			return adjacentFaces;
//		}
//	}
//	adjacentFaces.clear();
//	return adjacentFaces;
//}

///////////////////////////////////////////////////////////////

bool isAdjacent(MeshData * mesh_data, int faceID0, int faceID1)
{
	//std::vector<int> adjacentFaces;
	int * faceVertex0 = mesh_data->getFace(faceID0);
	int * faceVertex1 = mesh_data->getFace(faceID1);
	
	int counter = 0;
	for(int a = 0; a < 3;a++)
	{
		if(faceVertex0[a] == faceVertex1[0] || faceVertex0[a] == faceVertex1[1] || faceVertex0[a] == faceVertex1[2] )
		{
			counter++;
		}
	}

	if(counter == 2)
		return true;
	else
		return false;
}

///////////////////////////////////////////////////////////////

template<typename T>
inline void initialize( Eigen::Vector3f& vector , const T& point )
{
	vector[0] = point.x;
	vector[1] = point.y;
	vector[2] = point.z;
}

///////////////////////////////////////////////////////////////

template<typename T1, typename T2>
void buildCuttingPlane(const T1& curvePoint0, const T2& curvePoint1, const Eigen::Vector3f& projectDirection, Eigen::Vector3f & cutPlaneNormal, Eigen::Vector3f & pointOnCutPlane)
{
	Eigen::Vector3f cp_0,cp_1;

	initialize(cp_0,curvePoint0);
	initialize(cp_1,curvePoint1);

	cutPlaneNormal = (cp_1 - cp_0).cross(projectDirection);
	cutPlaneNormal = cutPlaneNormal / cutPlaneNormal.norm();
	pointOnCutPlane = cp_0;
}

///////////////////////////////////////////////////////////////

template<typename T1, typename T2>
void buildCuttingPlane(const T1& curvePoint0, const T2& curvePoint1, Point curvePoint2, Eigen::Vector3f & cutPlaneNormal, Eigen::Vector3f & pointOnCutPlane)
{
	Eigen::Vector3f cp_0,cp_1,cp_2;

	initialize(cp_0,curvePoint0);
	initialize(cp_1,curvePoint1);
	initialize(cp_2,curvePoint2);

	cutPlaneNormal = (cp_1 - cp_0).cross(cp_2 - cp_0);
	cutPlaneNormal = cutPlaneNormal / cutPlaneNormal.norm();
	pointOnCutPlane = cp_0;
}

///////////////////////////////////////////////////////////////

void buildCuttingPlane(Eigen::Vector3f curvePoint0, Point curvePoint1, Eigen::Vector3f projection, Eigen::Vector3f & cutPlaneNormal, Eigen::Vector3f & pointOnCutPlane)
{
	Eigen::Vector3f cp_0,cp_1,cp_2;

	cp_0 = curvePoint0;
	initialize(cp_1,curvePoint1);
	cp_2 = projection;

	cutPlaneNormal = (cp_1 - cp_0).cross(cp_2 - cp_0);
	cutPlaneNormal = cutPlaneNormal / cutPlaneNormal.norm();
	pointOnCutPlane = cp_0;
}

///////////////////////////////////////////////////////////////
//Given a triangle, an input edge or an input vertex, returns the intersection.
bool getCutPoint( int inputPointID, MeshData * mesh_data , Eigen::Vector3f cp_0, Eigen::Vector3f cp_n, int faceID, initPoint & cutPoint)
{
	initPoint cp;
	float lamda = 0.0f;
	float tol = 0.00001f;
	////at source, only one edge per face intersects with cut plane
	int * facePoint = mesh_data->getFace(faceID);
	std::vector<Eigen::Vector3f> edge;
	std::vector<int> edgePointIndex;
	for(int b = 0; b < 3; b++)
	{
		if(facePoint[b] != inputPointID)
		{	
			Eigen::Vector3f ep;
			ep[0] = mesh_data->getPoint(facePoint[b]).x;
			ep[1] = mesh_data->getPoint(facePoint[b]).y;
			ep[2] = mesh_data->getPoint(facePoint[b]).z;
			edge.push_back(ep);
			edgePointIndex.push_back(facePoint[b]);
		}
	}
	Eigen::Vector3f intersection, cutPointNormal;
	if( getIntersection(edge[0], edge[1], cp_0, cp_n, intersection, tol, lamda) )
	{
		initPoint cp;
		if(lamda == 0.0f)
		{
			cp.vertexID = edgePointIndex[1];
			std::vector<int> tempLocal;
			for(int a = 0; a < mesh_data->one_ring_face_array[cp.vertexID].size(); a++)
			{
				int * tempFaceVertexIDs = mesh_data->getFace(mesh_data->one_ring_face_array[cp.vertexID][a]);
				if(tempFaceVertexIDs[0] == inputPointID || tempFaceVertexIDs[1] == inputPointID || tempFaceVertexIDs[2] == inputPointID )
				{
					if(tempFaceVertexIDs[0] == cp.vertexID || tempFaceVertexIDs[1] == cp.vertexID || tempFaceVertexIDs[2] == cp.vertexID )
					{
						tempLocal.push_back(mesh_data->one_ring_face_array[cp.vertexID][a]);
						continue;
					}
				}
			}
			cp.flag = 1;
			cp.location = tempLocal;
			cp.cutFaceIDs = tempLocal;
			cp.neighbourFacesID = mesh_data->one_ring_face_array[cp.vertexID];
			cp.n_x = mesh_data->getVertexNormal(cp.vertexID)[0];
			cp.n_y = mesh_data->getVertexNormal(cp.vertexID)[1];
			cp.n_z = mesh_data->getVertexNormal(cp.vertexID)[2];

		}
		else if(lamda == 1.0f)
		{
			cp.vertexID = edgePointIndex[0];
			std::vector<int> tempLocal;
			for(int a = 0; a < mesh_data->one_ring_face_array[cp.vertexID].size(); a++)
			{
				int * tempFaceVertexIDs = mesh_data->getFace(mesh_data->one_ring_face_array[cp.vertexID][a]);
				if(tempFaceVertexIDs[0] == inputPointID || tempFaceVertexIDs[1] == inputPointID || tempFaceVertexIDs[2] == inputPointID )
				{
					if(tempFaceVertexIDs[0] == cp.vertexID || tempFaceVertexIDs[1] == cp.vertexID || tempFaceVertexIDs[2] == cp.vertexID )
					{
						tempLocal.push_back(mesh_data->one_ring_face_array[cp.vertexID][a]);
						continue;
					}
				}
			}	
			cp.flag = 1;
			cp.location = tempLocal;
			cp.cutFaceIDs = tempLocal;
			cp.neighbourFacesID = mesh_data->one_ring_face_array[cp.vertexID];
			cp.n_x = mesh_data->getVertexNormal(cp.vertexID)[0];
			cp.n_y = mesh_data->getVertexNormal(cp.vertexID)[1];
			cp.n_z = mesh_data->getVertexNormal(cp.vertexID)[2];
		}
		else
		{
			std::vector<int> tempLocal;
			std::vector<std::vector<int>> nextCutEdges;
			tempLocal.push_back(faceID);
			int nextFace = getNextFace(mesh_data, faceID, edgePointIndex, nextCutEdges);
			tempLocal.push_back(nextFace);
			cp.location = tempLocal;
			cp.cutFaceIDs.clear();
			cp.cutFaceIDs.push_back(faceID);
			cp.flag = 0;
			std::vector<int> neighbourFaces2;
			cp.cutEdge = edgePointIndex;
			cp.neighbourFacesID = mesh_data->one_ring_face_array[cp.cutEdge[0]];
			neighbourFaces2 = mesh_data->one_ring_face_array[cp.cutEdge[1]];

			for(int i = 0; i < neighbourFaces2.size(); i++)
			{
				bool is_exist = false;
				for(int j = 0; j < cp.neighbourFacesID.size(); j++)
				{
					if(neighbourFaces2[i] == cp.neighbourFacesID[j])
					{ 
						is_exist = true;
						break;
					}
				}

				if(!is_exist)
				{
					cp.neighbourFacesID.push_back(neighbourFaces2[i]);
				}
			}

			Eigen::Vector3f cutPointNormal;
			cutPointNormal = getNewNormalforCutPoint(mesh_data, edgePointIndex, intersection );
			cp.n_x = cutPointNormal[0];
			cp.n_y = cutPointNormal[1];
			cp.n_z = cutPointNormal[2];

		}


		cp.x = intersection[0];
		cp.y = intersection[1];
		cp.z = intersection[2];


		//projectedPath.push_back(cp);
		cutPoint = cp;
		return true;
	}else
	{
		return false;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////

bool getCutPoint( std::vector<int> inputPointIDs, MeshData * mesh_data , Eigen::Vector3f cp_0, Eigen::Vector3f cp_n, int faceID, std::vector<initPoint> & cutPoint)
{
	float lamda = 0.0f;
	float tol = 0.00001f;
	initPoint cp;
	////at other faces, one edge contains last cutPoint and rest two edges intersect with cut plane 
	int * facePoint = mesh_data->getFace(faceID);
	std::vector<Eigen::Vector3f> edge;
	std::vector<int> edgePointIndex;
	std::vector<std::vector<Eigen::Vector3f>> Edges;
	std::vector<std::vector<int>> EdgePointIndexs;
	for(int b = 0; b < 3; b++)
	{
		if(facePoint[b] != inputPointIDs[0] && facePoint[b] != inputPointIDs[1])
		{
			Eigen::Vector3f ep;
			ep[0] = mesh_data->getPoint(facePoint[b]).x;
			ep[1] = mesh_data->getPoint(facePoint[b]).y;
			ep[2] = mesh_data->getPoint(facePoint[b]).z;
			edge.push_back(ep);
			edgePointIndex.push_back(facePoint[b]);

			ep[0] = mesh_data->getPoint(inputPointIDs[0]).x;
			ep[1] = mesh_data->getPoint(inputPointIDs[0]).y;
			ep[2] = mesh_data->getPoint(inputPointIDs[0]).z;
			edge.push_back(ep);
			edgePointIndex.push_back(inputPointIDs[0]);		

			Edges.push_back(edge);
			EdgePointIndexs.push_back(edgePointIndex);

			edge.clear();
			edgePointIndex.clear();
			///////////////////////////////////////////////////

			ep[0] = mesh_data->getPoint(facePoint[b]).x;
			ep[1] = mesh_data->getPoint(facePoint[b]).y;
			ep[2] = mesh_data->getPoint(facePoint[b]).z;
			edge.push_back(ep);
			edgePointIndex.push_back(facePoint[b]);

			ep[0] = mesh_data->getPoint(inputPointIDs[1]).x;
			ep[1] = mesh_data->getPoint(inputPointIDs[1]).y;
			ep[2] = mesh_data->getPoint(inputPointIDs[1]).z;
			edge.push_back(ep);
			edgePointIndex.push_back(inputPointIDs[1]);		

			Edges.push_back(edge);
			EdgePointIndexs.push_back(edgePointIndex);
			break;
		}			
	}

	for(int i = 0; i < EdgePointIndexs.size() ; ++i)
	{

		Eigen::Vector3f intersection;
		if( getIntersection(Edges[i][0], Edges[i][1], cp_0, cp_n, intersection, tol , lamda) )
		{
			initPoint cp;
			if(lamda == 0.0f)
			{
				cp.vertexID = EdgePointIndexs[i][1];
				std::vector<int> tempLocal;
				tempLocal.push_back(faceID);
				cp.location = tempLocal;
				cp.cutFaceIDs = tempLocal;
				cp.flag = 1;
				cp.neighbourFacesID = mesh_data->one_ring_face_array[cp.vertexID];
				cp.n_x = mesh_data->getVertexNormal(cp.vertexID)[0];
				cp.n_y = mesh_data->getVertexNormal(cp.vertexID)[1];
				cp.n_z = mesh_data->getVertexNormal(cp.vertexID)[2];

			}
			else if(lamda == 1.0f)
			{			
				cp.vertexID = EdgePointIndexs[i][0];				
				std::vector<int> tempLocal;
				tempLocal.push_back(faceID);
				cp.location = tempLocal;
				cp.cutFaceIDs = tempLocal;
				cp.flag = 1;
				cp.neighbourFacesID = mesh_data->one_ring_face_array[cp.vertexID];
				cp.n_x = mesh_data->getVertexNormal(cp.vertexID)[0];
				cp.n_y = mesh_data->getVertexNormal(cp.vertexID)[1];
				cp.n_z = mesh_data->getVertexNormal(cp.vertexID)[2];
			}
			else
			{
				std::vector<int> tempLocal;
				tempLocal.push_back(faceID);
				std::vector<std::vector<int>> nextCutEdges;
				int nextFace = getNextFace(mesh_data, faceID, EdgePointIndexs[i], nextCutEdges);
				tempLocal.push_back(nextFace);
				cp.location = tempLocal;
				cp.cutFaceIDs.clear();
				cp.cutFaceIDs.push_back(faceID);
				cp.flag = 0;
				std::vector<int> neighbourFaces2;
				cp.cutEdge = EdgePointIndexs[i];
				cp.neighbourFacesID = mesh_data->one_ring_face_array[cp.cutEdge[0]];
				neighbourFaces2 = mesh_data->one_ring_face_array[cp.cutEdge[1]];

				for(int i = 0; i < neighbourFaces2.size(); i++)
				{
					bool is_exist = false;
					for(int j = 0; j < cp.neighbourFacesID.size(); j++)
					{
						if(neighbourFaces2[i] == cp.neighbourFacesID[j])
						{ 
							is_exist = true;
							break;
						}
					}

					if(!is_exist)
					{
						cp.neighbourFacesID.push_back(neighbourFaces2[i]);
					}
				}

				Eigen::Vector3f cutPointNormal;
				cutPointNormal = getNewNormalforCutPoint(mesh_data, edgePointIndex, intersection );
				cp.n_x = cutPointNormal[0];
				cp.n_y = cutPointNormal[1];
				cp.n_z = cutPointNormal[2];

			}
			cp.x = intersection[0];
			cp.y = intersection[1];
			cp.z = intersection[2];


			cutPoint.push_back(cp);
		}

	}
	if(cutPoint.size() > 0)
		return true;
	else
		return false;
}

///////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////

///////////////////////////////////////////////////////////////////////////////////////////

//void getPorjectDirection(MeshData * mesh_data, std::vector<initPoint> & initial_path, std::vector<Point> & curvePointArray, int floatingPointIndex , float tol, Eigen::Vector3f & projectDirection)
//{
//		int a = floatingPointIndex;
//		projectDirection = getCurveNormal(curvePointArray[a - 1], curvePointArray[a], curvePointArray[a + 1]);
//		Eigen::Vector3f projectDirection2, initialNormal;
//
//		initialNormal[0] = initial_path[a].n_x;
//		initialNormal[1] = initial_path[a].n_y;
//		initialNormal[2] = initial_path[a].n_z;
//
//		projectDirection2 = projectDirection;
//		projectDirection2 = projectDirection2 / projectDirection2.norm();
//
//		initPoint projectPoint;
//		if(projectDirection.norm() < std::sqrt(tol) || projectDirection2.dot(initialNormal) < 0.1736 )
//		{
//			std::vector<int> one_ring_vertices;
//			std::vector<int> one_ring_faces;
//			one_ring_faces = initial_path[a].neighbourFacesID;
//			int * faceVertexID = mesh_data->getFace(one_ring_faces[one_ring_faces.size() - 1]);
//			one_ring_faces.pop_back();
//			one_ring_vertices.push_back(faceVertexID[0]);
//			one_ring_vertices.push_back(faceVertexID[1]);
//			one_ring_vertices.push_back(faceVertexID[2]);
//
//			for(int i = 0; i < one_ring_faces.size(); ++i)
//			{
//				int * faceVertexID = mesh_data->getFace(one_ring_faces[i]);
//				std::vector<int> tmp;
//				tmp.push_back(faceVertexID[0]);
//				tmp.push_back(faceVertexID[1]);
//				tmp.push_back(faceVertexID[2]);
//				appendVector(tmp, one_ring_vertices);
//
//			}
//
//			Eigen::Vector3f one_ring_vertices_normal;
//			one_ring_vertices_normal = getSVDnomralofOnRingFace(mesh_data, one_ring_vertices);
//			projectDirection = one_ring_vertices_normal;
//		}else
//		{
//			Eigen::Vector3f v0,v1,v2,v3,v4,v5,v6;
//			v0[0] = curvePointArray[a].x -  initial_path[a - 1].x;
//			v0[1] = curvePointArray[a].y -  initial_path[a - 1].y;
//			v0[2] = curvePointArray[a].z -  initial_path[a - 1].z;
//
//			v1[0] = curvePointArray[a].x -  curvePointArray[a + 1].x;
//			v1[1] = curvePointArray[a].y -  curvePointArray[a + 1].y;
//			v1[2] = curvePointArray[a].z -  curvePointArray[a + 1].z;
//
//			Eigen::Vector3f new_css;
//			new_css = getCurveNormal(initial_path[a - 1], curvePointArray[a], curvePointArray[a + 1]);
//
//			if(new_css.norm() < std::sqrt(tol) || new_css.dot(initialNormal) < 0.1736 )
//			{
//				std::vector<int> one_ring_vertices;
//				std::vector<int> one_ring_faces;
//				one_ring_faces = initial_path[a].neighbourFacesID;
//				int * faceVertexID = mesh_data->getFace(one_ring_faces[one_ring_faces.size() - 1]);
//				one_ring_faces.pop_back();
//				one_ring_vertices.push_back(faceVertexID[0]);
//				one_ring_vertices.push_back(faceVertexID[1]);
//				one_ring_vertices.push_back(faceVertexID[2]);
//
//				for(int i = 0; i < one_ring_faces.size(); ++i)
//				{
//					int * faceVertexID = mesh_data->getFace(one_ring_faces[i]);
//					std::vector<int> tmp;
//					tmp.push_back(faceVertexID[0]);
//					tmp.push_back(faceVertexID[1]);
//					tmp.push_back(faceVertexID[2]);
//					appendVector(tmp, one_ring_vertices);
//
//				}
//
//				Eigen::Vector3f one_ring_vertices_normal;
//				one_ring_vertices_normal = getSVDnomralofOnRingFace(mesh_data, one_ring_vertices);
//				projectDirection = one_ring_vertices_normal;
//			}else
//			{
//				v3 = projectDirection;
//				v2 = v0.cross(v1);
//				v6 = v2 / v2.norm();
//				float dotV = (v2 / v2.norm()).dot(v3);
//				v4 = v3 - dotV * v6;
//				projectDirection = v4 / v4.norm();
//			}
//		}
//}

///////////////////////////////////////////////////////////////////////////////////////////

void getPorjectDirection(MeshData * mesh_data, int vertexID, Eigen::Vector3f & projectDirection)
{
	std::vector<int> one_ring_vertices;
	std::vector<int> one_ring_faces;
	one_ring_vertices = mesh_data->one_ring_vertex_array[vertexID];
	one_ring_vertices.push_back(vertexID);


	Eigen::Vector3f one_ring_vertices_normal;

	one_ring_vertices_normal = getSVDnomralofOnRingFace(mesh_data, one_ring_vertices);

	projectDirection = one_ring_vertices_normal;
}




///////////////////////////////////////////////////////////////////////////////////////////


//void getPorjectPointOfFloatingPoint(MeshData * mesh_data, std::vector<initPoint> & initial_path, std::vector<Point> & curvePointArray, int floatingPointIndex , float tol, Eigen::Vector3f & projectDirection, std::vector<Eigen::Vector3f> & projectDirectionArray)
//{
//	initPoint projectPoint;
//	int a = floatingPointIndex;
//
//	std::vector<initPoint> projectPointArray;
//	std::vector<int> searching_faces;
//	searching_faces = initial_path[a].neighbourFacesID;
//	appendVector(initial_path[a].location, searching_faces);
//
//	int ring_number = 0;
//
//
//	do{
//
//		if(ring_number > 0)
//		{
//			std::cout<<"Cannot project into one_ring, expanding neighbour to:"<<ring_number<<std::endl;
//		}
//
//		for(int b = 0; b < searching_faces.size(); b++)
//		{
//			Point floatPoint;
//			floatPoint = curvePointArray[a];
//
//			try{
//				Eigen::Vector3f projectedPoint;
//				bool is_in_triangle = projectFloatingPointOnMesh(mesh_data,  projectDirection, floatPoint, projectPoint, searching_faces[b], tol);
//				if(is_in_triangle)
//				{
//					projectPointArray.push_back(projectPoint);
//				}
//
//			}
//			catch(...)
//			{
//				std::cout<<"error in projection"<<std::endl;
//			}
//
//		}
//		if(projectPointArray.size() == 0)
//		{
//			std::vector<int> next_ring_faces;
//			std::vector<int> tmp_ring_faces;
//			for(int i = 0; i < searching_faces.size(); i++)
//			{
//				appendVector(mesh_data->get_one_ring_faces_of_face(searching_faces[i]),next_ring_faces);
//			}
//			searching_faces = setDiff( next_ring_faces, searching_faces);
//		}
//		
//		ring_number++;
//
//	}while(projectPointArray.size() == 0 && ring_number < 5);
//
//
//
//
//
//
//	if(projectPointArray.size() == 0)
//	{
//		std::cout<<"Cannot project into 5 one_rings, select closest vertex in onering"<<std::endl;
//
//		std::vector<int> searching_faces;
//		std::vector<int> searching_vertex;
//		searching_faces = initial_path[a].location;
//		for(int i = 0; i < searching_faces.size();i++)
//		{
//			int * faceVertex = mesh_data->getFace(searching_faces[i]);
//			std::vector<int> tmp;
//			tmp.push_back(faceVertex[0]);
//			tmp.push_back(faceVertex[1]);
//			tmp.push_back(faceVertex[2]);
//			appendVector(tmp,searching_vertex);
//		}
//
//		float distance = 9999.9f;
//		int index = 0;
//		for(int i = 0; i < searching_vertex.size();i++)
//		{
//			Point point;
//			point = mesh_data->getPoint(searching_vertex[i]);
//			float curDist = Distl(point,curvePointArray[a]);
//			if(curDist < distance)
//			{
//				index = i;
//			}
//		}
//
//		initPoint vertex;
//		vertex.vertexID = searching_vertex[index];
//		vertex.x = mesh_data->getPoint(vertex.vertexID).x;
//		vertex.y = mesh_data->getPoint(vertex.vertexID).y;
//		vertex.z = mesh_data->getPoint(vertex.vertexID).z;
//		vertex.n_x = mesh_data->getVertexNormal(vertex.vertexID)[0];
//		vertex.n_y = mesh_data->getVertexNormal(vertex.vertexID)[1];
//		vertex.n_z = mesh_data->getVertexNormal(vertex.vertexID)[2];
//		vertex.flag = 1;
//		vertex.neighbourFacesID = mesh_data->getFaceIDfromVertex(vertex.vertexID);	
//		vertex.location = vertex.neighbourFacesID;
//
//		initial_path[a] = vertex;
//		projectPointArray.push_back(vertex);
//	}		
//
//
//
//
//
//
//
//
//		//////////////////////////////////////////////////////////////
//		//////////////////////////////////////////////////////////////
//		///// number of face ring that projection will serch
//	//}while(projectPointArray.size() == 0);
//
//
//
//	if(projectPointArray.size() > 1)
//	{	
//		float dist = 9999.9f;
//		int index = 0;
//		initPoint tmpPoint;
//		for(int j = 0; j < projectPointArray.size(); j++)
//		{
//			float dist_c = Distl(projectPointArray[j],initial_path[a]);
//			if(dist_c < dist)
//			{
//				dist = dist_c;
//				index = j;
//			}
//		}
//		tmpPoint = projectPointArray[index];
//		initial_path[a] = tmpPoint;
//	}else if(projectPointArray.size() == 1) 
//	{
//		initial_path[a] = projectPointArray[0];
//	}else
//	{
//		throw 20;
//	}
//
//}

////////////////////////////////////////////////////////////////////////////////

void getPorjectPointOfFloatingPoint(MeshData * mesh_data, std::vector<initPoint> & initial_path, std::vector<Point> & curvePointArray, int floatingPointIndex , float tol, Eigen::Vector3f & projectDirection, std::vector<Eigen::Vector3f> & projectDirectionArray)
{
	initPoint projectPoint;
	int a = floatingPointIndex;

	std::vector<initPoint> projectPointArray;
	std::vector<int> searching_faces;
	std::vector<int> searching_vertices;
	//searching_faces = initial_path[a].neighbourFacesID;
	//appendVector(initial_path[a].location, searching_faces);

	
	Point floatPoint;
	floatPoint = curvePointArray[a];	
	int closestVertexID = -1;

	float minDist = 9999.9f;
	if(initial_path[a].flag == 1)
	{
		searching_vertices.push_back(initial_path[a].vertexID);
	}else if(initial_path[a].flag == 0)
	{
		searching_vertices.push_back(initial_path[a].cutEdge[0]);
		searching_vertices.push_back(initial_path[a].cutEdge[1]);
	}else if(initial_path[a].flag == 2)
	{
		int * faceVertex = mesh_data->getFace(initial_path[a].location[0]);
		searching_vertices.push_back(faceVertex[0]);
		searching_vertices.push_back(faceVertex[1]);		
		searching_vertices.push_back(faceVertex[2]);	
	}else{
		throw 22;
	}
	minDist = 9999.9f;

	for(int b = 0; b < searching_vertices.size(); b++)
	{
		Point tmp = mesh_data->getPoint(searching_vertices[b]);
		float tmpDist = sqrtDist<Point, Point>(tmp, floatPoint);
		if(tmpDist < minDist)
		{
			minDist = tmpDist;
			closestVertexID = searching_vertices[b];
		}
	}
	

	//search closest vertex to floating point, starting from its initial point
	
	bool is_resch_closest_vertex = false;
	do{
		is_resch_closest_vertex = false;
		std::vector<int> one_ring_vertices;
		one_ring_vertices = mesh_data->one_ring_vertex_array[closestVertexID];

		for(int c = 0; c < one_ring_vertices.size();++c)
		{
			Point tmpPoint;
			tmpPoint = mesh_data->getPoint(one_ring_vertices[c]);
			float currentDist = sqrtDist<Point, Point>(tmpPoint, floatPoint);
			if( currentDist < minDist )
			{
				closestVertexID = one_ring_vertices[c];
				minDist = currentDist;
				is_resch_closest_vertex = true;
			}
		}

	}while(is_resch_closest_vertex);

	getPorjectDirection(mesh_data, closestVertexID, projectDirection);

	searching_faces = mesh_data->one_ring_face_array[closestVertexID];
	int ring_number = 0;
	do
	{
		for(int b = 0; b < searching_faces.size(); b++)
		{
			Point floatPoint;
			floatPoint = curvePointArray[a];

			Eigen::Vector3f projectedPoint;
			bool is_in_triangle = projectFloatingPointOnMesh(mesh_data,  projectDirection, floatPoint, projectPoint, searching_faces[b], tol);
			if(is_in_triangle)
			{
				projectPointArray.push_back(projectPoint);
			}
		}

		if(projectPointArray.size() == 0)
		{
			std::vector<int> next_ring_faces;
			std::vector<int> tmp_ring_faces;
			for(int i = 0; i < searching_faces.size(); i++)
			{
				appendVector(mesh_data->get_one_ring_faces_of_face(searching_faces[i]),next_ring_faces);
			}
			searching_faces = setDiff( next_ring_faces, searching_faces);
		}
		ring_number++;
	}while(projectPointArray.size() == 0 && ring_number < 2);
	
	if(projectPointArray.size() == 0)
	{
		initPoint vertex;
		vertex.vertexID = closestVertexID;
		vertex.x = mesh_data->getPoint(closestVertexID).x;
		vertex.y = mesh_data->getPoint(closestVertexID).y;
		vertex.z = mesh_data->getPoint(closestVertexID).z;
		vertex.n_x = mesh_data->getVertexNormal(closestVertexID)[0];
		vertex.n_y = mesh_data->getVertexNormal(closestVertexID)[1];
		vertex.n_z = mesh_data->getVertexNormal(closestVertexID)[2];
		vertex.flag = 1;
		vertex.neighbourFacesID = mesh_data->one_ring_face_array[closestVertexID];	
		vertex.location = vertex.neighbourFacesID;
		initial_path[a] = vertex;
		projectPointArray.push_back(vertex);
	}		








	//////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////
	///// number of face ring that projection will serch
	//}while(projectPointArray.size() == 0);



	if(projectPointArray.size() > 1)
	{	
		float dist = 9999.9f;
		int index = 0;
		initPoint tmpPoint;
		for(int j = 0; j < projectPointArray.size(); j++)
		{
			float dist_c = Distl<initPoint,Point>(projectPointArray[j],floatPoint);
			if(dist_c < dist)
			{
				dist = dist_c;
				index = j;
			}
		}
		tmpPoint = projectPointArray[index];
		initial_path[a] = tmpPoint;
	}else if(projectPointArray.size() == 1) 
	{
		initial_path[a] = projectPointArray[0];
	}else
	{
		throw 20;
	}

}







////////////////////////////////////////////////////////////////////////////////


void getCutPlane(MeshData * mesh_data, std::vector<initPoint> & initial_path, std::vector<Point> & curvePointArray, std::vector<initPoint> projectedPath ,int floatingPointIndex , Eigen::Vector3f projectDirection, bool ic_cut_valid, Eigen::Vector3f & cutPlaneNormal, Eigen::Vector3f & pointOnCutPlane)
{
	int a = floatingPointIndex;

	if( floatingPointIndex < curvePointArray.size() - 1)
	{
		if(initial_path[a].isOnMesh == true )
		{
			if(ic_cut_valid)
			{
				Eigen::Vector3f currentProjectDirection;
				currentProjectDirection[0] = initial_path[a].n_x;
				currentProjectDirection[1] = initial_path[a].n_y;
				currentProjectDirection[2] = initial_path[a].n_z;
				buildCuttingPlane(initial_path[a-1], curvePointArray[a], currentProjectDirection, cutPlaneNormal, pointOnCutPlane);
			}
			else
			{
				Eigen::Vector3f currentProjectDirection;
				currentProjectDirection[0] = initial_path[a].n_x;
				currentProjectDirection[1] = initial_path[a].n_y;
				currentProjectDirection[2] = initial_path[a].n_z;
				buildCuttingPlane(projectedPath[projectedPath.size()-1], curvePointArray[a], currentProjectDirection, cutPlaneNormal, pointOnCutPlane);
			}
		}else 
		{
			if(ic_cut_valid)
			{
				buildCuttingPlane(initial_path[a-1], initial_path[a], curvePointArray[a], cutPlaneNormal, pointOnCutPlane);	
			}else
			{
				buildCuttingPlane(projectedPath[projectedPath.size()-1], initial_path[a], curvePointArray[a], cutPlaneNormal, pointOnCutPlane);	
			}
		}

	}else
	{

		if(ic_cut_valid)
		{
			Eigen::Vector3f currentProjectDirection;
			currentProjectDirection[0] = initial_path[curvePointArray.size() - 1].n_x;
			currentProjectDirection[1] = initial_path[curvePointArray.size() - 1].n_y;
			currentProjectDirection[2] = initial_path[curvePointArray.size() - 1].n_z;
			buildCuttingPlane(initial_path[curvePointArray.size() - 2], curvePointArray[curvePointArray.size() - 1], currentProjectDirection, cutPlaneNormal, pointOnCutPlane);
		}
		else
		{
			Eigen::Vector3f currentProjectDirection;
			currentProjectDirection[0] = initial_path[curvePointArray.size() - 1].n_x;
			currentProjectDirection[1] = initial_path[curvePointArray.size() - 1].n_y;
			currentProjectDirection[2] = initial_path[curvePointArray.size() - 1].n_z;
			buildCuttingPlane(projectedPath[projectedPath.size()-1], curvePointArray[curvePointArray.size() - 1], currentProjectDirection, cutPlaneNormal, pointOnCutPlane);
		}
	}
}


////////////////////////////////////////////////////////////////////////////////

bool getGeodesicDistance(MeshData * mesh_data, std::vector<initPoint> & initial_path, std::vector<Point> & curvePointArray)
{
	int destinationID = initial_path[initial_path.size() - 1].vertexID;
	std::vector< int > one_ring_face = mesh_data->one_ring_face_array[destinationID];
	Eigen::Vector3f destinationNormal;
	float * destNormal = mesh_data->getVertexNormal(destinationID);
	destinationNormal[0] = destNormal[0];
	destinationNormal[1] = destNormal[1];
	destinationNormal[2] = destNormal[2];
	Eigen::Vector3f cutPlaneNormal;
	Eigen::Vector3f pointOnCutPlane;
	buildCuttingPlane(curvePointArray[curvePointArray.size() - 1], curvePointArray[curvePointArray.size() - 2], destinationNormal, cutPlaneNormal, pointOnCutPlane);

	std::vector<int> edge;
	initPoint cutPoint;
	bool is_cut = false;
	for( int i = 0; i < one_ring_face.size(); i++)
	{
		int * faceVertex = mesh_data->getFace(one_ring_face[i]);
		std::vector<int> faceVertexID;
		faceVertexID.assign(faceVertex, faceVertex + 3);
		
		int faceID = one_ring_face[i];
		edge = setDiff( faceVertexID , destinationID );
		
		initPoint point0, point1;

		if(mesh_data->initial_path_array[edge[0]].size() > 0)
			point0 = mesh_data->initial_path_array[edge[0]][mesh_data->initial_path_array[edge[0]].size() - 1 ];
		if(mesh_data->initial_path_array[edge[1]].size() > 0)
			point1 = mesh_data->initial_path_array[edge[1]][mesh_data->initial_path_array[edge[1]].size() - 1 ];

		if(point0.is_geodest_exact == false || point1.is_geodest_exact == false) continue;
		
		
		
		is_cut =  getCutPoint( destinationID, mesh_data , pointOnCutPlane, cutPlaneNormal, faceID, cutPoint);
		if(is_cut) break;
	}
	if(is_cut == false)
		return false;

	float vmvn, svm,svn;
	vmvn = Distl(mesh_data->getPoint(edge[0]),mesh_data->getPoint(edge[1]));
	svm = mesh_data->distance_to_source_array[edge[0]];
	svn = mesh_data->distance_to_source_array[edge[1]];
	float costheta1 = (vmvn * vmvn + svm * svm - svn * svn) / (2 * vmvn * svm);

	float geodesicDist,vmp;
	vmp = Distl(mesh_data->getPoint(edge[0]),cutPoint);
	geodesicDist = sqrt( vmp * vmp + svm * svm - 2 * vmp * svm * costheta1 ) +  Distl(mesh_data->getPoint(destinationID),cutPoint);
	mesh_data->distance_to_source_array[destinationID] = geodesicDist;
	return true;
}


void updateGeodesicDistance(MeshData * mesh_data, int sourceID)
{
	int updatedVertexNumber = 1;
	std::vector<int> propagation_boundary;
	std::vector<int> next_propagation_boundary;
	propagation_boundary = mesh_data->one_ring_vertex_array[sourceID];
	//for(int i = 0; i < propagation_boundary.size() ; ++i)
	//{
	//	updatedVertexNumber++;
	//	std::vector<int> tmp;
	//	tmp = mesh_data->one_ring_vertex_array[propagation_boundary[i]];
	//	for(int j = 0; j < tmp.size(); j++)
	//	{
	//		if(mesh_data->initial_path_array[tmp[j]][mesh_data->initial_path_array[tmp[j]].size() - 1].is_geodest_exact == false)
	//			appendVector(tmp[j],next_propagation_boundary);
	//	}
	//}
	//propagation_boundary = next_propagation_boundary;
	
	
	do
	{


		for(int i = 0; i < propagation_boundary.size() ; ++i)
		{
			std::vector<int> tmp;
			tmp = mesh_data->one_ring_vertex_array[propagation_boundary[i]];
			for(int j = 0; j < tmp.size(); j++)
			{
				if(mesh_data->initial_path_array[tmp[j]][mesh_data->initial_path_array[tmp[j]].size() - 1].is_geodest_exact == false)
					appendVector(tmp[j],next_propagation_boundary);
			}
		}
		propagation_boundary = next_propagation_boundary;
		next_propagation_boundary.clear();



		
		std::vector<int> tmp_boundary;
		tmp_boundary = propagation_boundary;
		do{
			std::vector<int> uncut_points;
			for(int i = 0; i < tmp_boundary.size() ; ++i)
			{
				try
				{
					getGeodesicDistance(mesh_data,mesh_data->initial_path_array[tmp_boundary[i]],mesh_data->floating_path_array[tmp_boundary[i]]);
					updatedVertexNumber++;
					//next_propagation_boundary.push_back(tmp_boundary[i]);
					mesh_data->initial_path_array[tmp_boundary[i]][mesh_data->initial_path_array[tmp_boundary[i]].size() - 1].is_geodest_exact = true;
				}
				catch( int n )
				{
					if(n == 22)
					{
						uncut_points.push_back(tmp_boundary[i]);
					}
				}
			}
			if( uncut_points.size() == 0 ) break;
			tmp_boundary = uncut_points;
		}while(true);

		std::cout<<"geodesic distance wave: "<<propagation_boundary.size()<<std::endl;
	}
	while(propagation_boundary.size());
	std::cout<<"-------------->"<<updatedVertexNumber<<std::endl;
}