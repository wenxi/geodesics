/*
 * geodesic_regression_BLAS_matrix.h
 *
 *  Created on: 11 Mar 2013
 *      Author: wli
 */

#ifndef GEODESIC_REGRESSION_BLAS_MATRIX_H_
#define GEODESIC_REGRESSION_BLAS_MATRIX_H_




#include <vector>
#include "geodesic_base_elements.h"
#include "geodesic_mesh_data.h"


#include <Eigen/Core>
#include <Eigen/Eigen>
#include <Eigen/Eigenvalues>
#include <Eigen/Sparse>

#include "f2c.h"
#include "clapack.h"

//#include <iostream>
//#include <fstream>



#define PI 3.14159265354


float Dist(Point point0, Point point1){

	 return (point0.x - point1.x) * (point0.x - point1.x) + (point0.y - point1.y) * (point0.y - point1.y) + (point0.z - point1.z) * (point0.z - point1.z);

}





void geodesic(MeshData * mesh_data, std::vector<int> initial_path, std::vector<Point> & curvePointArray ,float regression_ratio){


	float u = regression_ratio;
	const int matrix_size = (int)curvePointArray.size();

	int index_offset = 0;

	real * dMat_C_3m_1 = new real[matrix_size * 3]();

	real * dMat_Xt_3m_1 = new real[matrix_size * 3]();

	real * dMat_N_3m_m = new real[matrix_size * 3 * matrix_size]();

	real * dMat_Nt_m_3m = new real[matrix_size * 3 * matrix_size]();

	real * dMat_Nstar_3m_3m = new real[matrix_size * 3 * matrix_size * 3]();

	real * dMat_K_3m_3m = new real[matrix_size * 3 * matrix_size * 3]();

	real * dMat_I_mat_3m_3m = new real[matrix_size * 3 * matrix_size * 3]();

	for(int i = 0 ; i < matrix_size * 3; ++i){
		dMat_I_mat_3m_3m[(matrix_size * 3 + 1) * i] = 1.0;
	}

	for(int i = 0;i< matrix_size ;++i ){
		dMat_Xt_3m_1[i*3] = curvePointArray[i].x;
		dMat_Xt_3m_1[i*3 + 1] = curvePointArray[i].y;
		dMat_Xt_3m_1[i*3 + 2] = curvePointArray[i].z;

		std::vector<int> one_ring_v;
		std::vector<int> one_ring_f;

		one_ring_v = mesh_data->one_ring_vertex_array[initial_path[i + index_offset]];
		one_ring_v.push_back(initial_path[i + index_offset]);

		int closest_point_id = 0;
		float min_dist = 9999.9f;
		for(unsigned int x = 0;x < one_ring_v.size();++x){
			Point point;

			point.x = mesh_data->getVertex(one_ring_v[x])[0];
			point.y = mesh_data->getVertex(one_ring_v[x])[1];
			point.z = mesh_data->getVertex(one_ring_v[x])[2];


			float dist = Dist(point,curvePointArray[i + index_offset] );
			if(dist < min_dist){
				min_dist = dist;
				closest_point_id = one_ring_v[x];
			}
		}
		one_ring_v.clear();

		std::vector<int> one_ring_vertex;
		one_ring_vertex = mesh_data->one_ring_vertex_array[closest_point_id];

		std::vector<int> one_ring_faces;
		one_ring_faces = mesh_data->one_ring_face_array[closest_point_id];


		float x = 0, y = 0,z = 0;
		//float center_point[3];
		for(unsigned int a = 0; a < one_ring_vertex.size() ; ++a){
			x += mesh_data->getVertex(one_ring_vertex[a])[0];
			y += mesh_data->getVertex(one_ring_vertex[a])[1];
			z += mesh_data->getVertex(one_ring_vertex[a])[2];
		}


		dMat_C_3m_1[i*3] =  x /  one_ring_vertex.size();
		dMat_C_3m_1[i*3 + 1] = y /  one_ring_vertex.size();
		dMat_C_3m_1[i*3 + 2] = z /  one_ring_vertex.size();



		int row =  (int)one_ring_faces.size();
		Eigen::MatrixX3f eigenPointMatrix;
		eigenPointMatrix.resize(row,3);
		Eigen::MatrixX3f transposeMatrix;
		transposeMatrix.resize(row,3);
		Eigen::VectorXf meanVector;



		for(unsigned int a = 0; a < one_ring_faces.size();++a ){

			eigenPointMatrix(a,0) = mesh_data->getVertexNormal(one_ring_faces[a])[0];
			eigenPointMatrix(a,1) = mesh_data->getVertexNormal(one_ring_faces[a])[1];
			eigenPointMatrix(a,2) = mesh_data->getVertexNormal(one_ring_faces[a])[2];
		}


		float mean;
		for (int k = 0; k < 3; ++k)
		{
			//compute mean
			mean = (eigenPointMatrix.col(k).sum())/row;
			// create a vector with constant value = mean
			meanVector  = Eigen::VectorXf::Constant(row,mean);
			eigenPointMatrix.col(k) -= meanVector;
		}


		Eigen::MatrixXf covariance = ((float)1/(row-1)) * eigenPointMatrix.transpose()* eigenPointMatrix;

		Eigen::EigenSolver<Eigen::MatrixXf> m_solve(covariance);

		Eigen::VectorXf eigenvalues = m_solve.eigenvalues().real();
		Eigen::MatrixXf eignevectors = m_solve.eigenvectors().real();

		Eigen::Vector3f vector;


		if(eigenvalues(0) < eigenvalues(1))
		{
			if (eigenvalues(0) < eigenvalues(2))
			{
				vector[0] = eignevectors(0,0);
				vector[1] = eignevectors(1,0);
				vector[2] = eignevectors(2,0);
			}
			else
			{
				vector[0] = eignevectors(0,2);
				vector[1] = eignevectors(1,2);
				vector[2] = eignevectors(2,2);
			}
		}
		else if(eigenvalues(1) < eigenvalues(0))
		{
			if (eigenvalues(1) < eigenvalues(2))
			{
				vector[0] = eignevectors(0,1);
				vector[1] = eignevectors(1,1);
				vector[2] = eignevectors(2,1);
			}
			else
			{
				vector[0] = eignevectors(0,2);
				vector[1] = eignevectors(1,2);
				vector[2] = eignevectors(2,2);
			}
		}


		vector.normalized();



		dMat_N_3m_m[(3 + 3 * matrix_size) * i ] = vector[0];
		dMat_N_3m_m[(3 + 3 * matrix_size) * i + 1] = vector[1];
		dMat_N_3m_m[(3 + 3 * matrix_size) * i + 2] = vector[2];

		dMat_Nt_m_3m[(matrix_size * 3 + 1)* i] =  vector[0];
		dMat_Nt_m_3m[(matrix_size * 3 + 1)* i + matrix_size] = vector[1];
		dMat_Nt_m_3m[(matrix_size * 3 + 1)* i + matrix_size * 2] = vector[2];





		if(i > 0 && i < matrix_size - 1){

			dMat_K_3m_3m[(3 * 3 * matrix_size) * (i - 1) + 3 * i] = 1;
			dMat_K_3m_3m[(3 * 3 * matrix_size) * (i - 1) + 3 * i + (3 * matrix_size + 1)] = 1;
			dMat_K_3m_3m[(3 * 3 * matrix_size) * (i - 1) + 3 * i + (3 * matrix_size + 1) * 2] = 1;

			dMat_K_3m_3m[(3 * 3 * matrix_size + 3) * i] = -2;
			dMat_K_3m_3m[(3 * 3 * matrix_size + 3) * i + (3 * matrix_size + 1) ] = -2;
			dMat_K_3m_3m[(3 * 3 * matrix_size + 3) * i + (3 * matrix_size + 1) * 2 ] = -2;


			dMat_K_3m_3m[(3 * 3 * matrix_size) * (i + 1) + 3 * i] = 1;
			dMat_K_3m_3m[(3 * 3 * matrix_size) * (i + 1) + 3 * i + (3 * matrix_size + 1)] = 1;
			dMat_K_3m_3m[(3 * 3 * matrix_size) * (i + 1) + 3 * i + (3 * matrix_size + 1) * 2] = 1;

		}

	}



	char * transA = new char[2];
	char * transB = new char[2];
	std::strcpy(transA,"n");
	std::strcpy(transB,"n");


	integer M = 3 * matrix_size;
	integer N = 3 * matrix_size;
	integer K = matrix_size;
	real alpha = 1.0;
	real beta = 0.0;
	sgemm_(transA, transB, &M , &N, &K, &alpha, dMat_N_3m_m, &M, dMat_Nt_m_3m, &K, &beta, dMat_Nstar_3m_3m, &M);
	////////////////////////////////////////////



	//////////////////////////////////////////////////////////
	//A = dMat_K_3m_3m
	//A = dMat_Nstar_3m_3m * dMat_K_3m_3m + A

	real * A = new real[matrix_size * 3 * matrix_size * 3];

	real sa = 1.0;
	integer incx = 1;
	integer incy = 1;
	integer array_length = matrix_size * 3 * matrix_size* 3;
	beta = 1.0;
	//A = dMat_K_3m_3m
	int result = scopy_(&array_length, dMat_K_3m_3m, &incx, A, &incy);
	//A = dMat_Nstar_3m_3m * dMat_K_3m_3m + A
	sgemm_(transA, transB, &M , &M, &M, &alpha, dMat_Nstar_3m_3m, &M, dMat_K_3m_3m, &M, &beta, A, &M);
	/////////////////////////////////////////////



	///////////////////////////////////////////////////////////
	//to calculate: dMat_Nstar_3m_3m - A
	//swap A and dMat_Nstar_3m_3m
	//A = -1 * A + dMat_Nstar_3m_3m

	real * dMat_Nstar_3m_3m_original = new real[matrix_size * 3 *matrix_size * 3]();
	result = scopy_(&array_length, dMat_Nstar_3m_3m, &incx, dMat_Nstar_3m_3m_original, &incy);


	sswap_(&array_length, A, &incx, dMat_Nstar_3m_3m, &incy);

	sa = -1.0;
	result = saxpy_(&array_length, &sa, dMat_Nstar_3m_3m, &incx, A, &incy);
	///////////////////////////////////////////



	////////////////////////////////////////////////////////////
	//to calculate: dMat_I_mat_3m_3m + u * A
	//AX = B; build A:
	//swap: A and dMat_I_mat_3m_3m
	//A =  u * A + dMat_I_mat_3m_3m

	//swap: A and dMat_I_mat_3m_3m
	sswap_(&array_length, A, &incx, dMat_I_mat_3m_3m, &incy);

	sa = u;
	//A =  u * A + dMat_I_mat_3m_3m
	result = saxpy_(&array_length, &sa, dMat_I_mat_3m_3m, &incx, A, &incy);
	//////////////////////////////////////////



	///////////////////////////////////////////////////////////////
	//to calculate: B = dMat_Xt_3m_1 + u * dMat_Nstar_3m_3m * dMat_C_3m_1;
	//B = dMat_Xt_3m_1
	//B =  u * dMat_Nstar_3m_3m * dMat_C_3m_1 + B;

	real * B = new real[matrix_size * 3 ]();
	array_length = 3 * matrix_size;

	//B = dMat_Xt_3m_1
	scopy_(&array_length, dMat_Xt_3m_1, &incx, B, &incy);

	beta = 0;
	sa = u;

	//B =  u * dMat_Nstar_3m_3m * dMat_C_3m_1 + B;
	integer col = 1;
	beta = 1;
	sgemm_(transA, transB, &M ,&col, &M, &sa, dMat_Nstar_3m_3m_original, &M, dMat_C_3m_1, &M, &beta, B, &M);

	///////////////////////////////////////////



	/////////////////////////////////////////////////////////////////////
	// solve the linear equation: Ax = B, where x is the result
	integer nrhs = 1;
	integer ipiv[matrix_size * 3];
	integer info;

	sgesv_(&M, &nrhs, A, &M, ipiv, B, &M, &info);

	for(unsigned int i = 1; i < curvePointArray.size();++i){

		curvePointArray[i].x = B[i*3];
		curvePointArray[i].y = B[i*3 + 1];
		curvePointArray[i].z = B[i*3 + 2];
	}

}

#endif /* GEODESIC_REGRESSION_BLAS_MATRIX_H_ */
