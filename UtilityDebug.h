/*
 * File Name   : UtilityDebug.h
 * Author      : Rudra Poudel
 * Version     : 1.0.0
 * Copyright   : (c) Rudra Poudel, Bournemouth University, UK.
 * Description : Functions for debuging.
 * Dependencies:
 *
 * Release Date: 31-DEC-2012
*/

#ifndef UTILITYDEBUG_H_
#define UTILITYDEBUG_H_
#include <iostream>
#include "Timer.h"

inline void start_timer(Timer& t1, bool print_info, const char* caption, bool add_decoration) {
  t1.start();

  if (!print_info)
    return;

  if ( add_decoration )
    std::cout<<"\n**********************************************";

  std::cout<<"\n"<<caption << " ... ";
}

inline void stop_timer(Timer& t1, bool print_time, const char* caption, bool add_decoration) {
  t1.stop();

  if (!print_time)
    return;

  float total_time = (float)t1.getElapsedTimeInSec();

  if ( add_decoration )
    std::cout<<"\n**********************************************";

  std::cout<<"\n"<<caption << ": "<<total_time <<" seconds.";

  if ( add_decoration)
    std::cout<<"\n**********************************************";
}


inline void stop_timer(Timer& t1, float & total_time) {
	t1.stop();
	total_time = (float)t1.getElapsedTimeInSec();
}

#endif /* UTILITYDEBUG_H_ */
