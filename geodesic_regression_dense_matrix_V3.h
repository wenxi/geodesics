#ifndef geodesic_regression_dense_matrix_h
#define geodesic_regression_dense_matrix_h






#include "geodesic_base_elements.h"
#include "geodesic_mesh_data.h"


#include <Eigen/Core>
#include <Eigen/Eigen>
#include <Eigen/Eigenvalues>
#include <Eigen/Sparse>

#include <iostream>
#include <fstream>



#define PI 3.14159265354 




float Dist(Point point0, Point point1){
	return (point0.x - point1.x) * (point0.x - point1.x) + (point0.y - point1.y) * (point0.y - point1.y) + (point0.z - point1.z) * (point0.z - point1.z);
}





void geodesic(MeshData * mesh_data, std::vector<initPoint> & initial_path, std::vector<Point> & curvePointArray ,float regression_ratio){


	int dim = 3;
	int k = 1;
	//ANNpoint queryPoint;
	//queryPoint = annAllocPt(dim);
	//ofstream pfile("calculation procedure.txt");
	//int number_point_in_range = 0;
	//ANNidxArray nnIdx; // near neighbor indices
	//ANNdistArray dists; // near neighbor distances
	//float search_radius = 0.0005f;
	//ANNdist sqRad = sqrt(search_radius);
	//std::ofstream regionFile("result.txt");

	float u = regression_ratio;
	int sparse_matrix_size = (int)curvePointArray.size();

	int index_offset = 0;

	//Eigen::MatrixXf dMat_C_3m_1;
	//dMat_C_3m_1.resize(sparse_matrix_size * 3,1);
	//dMat_C_3m_1.setZero();


	Eigen::MatrixXf dMat_Xt_3m_1;
	dMat_Xt_3m_1.resize(sparse_matrix_size * 3 ,1);
	dMat_Xt_3m_1.setZero();

	Eigen::MatrixXf dMat_N_3m_m;
	dMat_N_3m_m.resize(sparse_matrix_size * 3,sparse_matrix_size);
	dMat_N_3m_m.setZero();

	Eigen::MatrixXf dMat_Nt_m_3m;
	dMat_Nt_m_3m.resize(sparse_matrix_size, sparse_matrix_size * 3 );
	dMat_Nt_m_3m.setZero();

	Eigen::MatrixXf dMat_Nstar_3m_3m;
	dMat_Nstar_3m_3m.resize(sparse_matrix_size * 3,sparse_matrix_size * 3);
	dMat_Nstar_3m_3m.setZero();

	Eigen::MatrixXf dMat_K_3m_3m;
	dMat_K_3m_3m.resize(sparse_matrix_size * 3,sparse_matrix_size * 3);
	dMat_K_3m_3m.setZero();

	Eigen::MatrixXf dMat_I_mat_3m_3m;
	dMat_I_mat_3m_3m.resize(sparse_matrix_size * 3 , sparse_matrix_size * 3);
	dMat_I_mat_3m_3m.setZero();

	for(int i = 0 ; i < sparse_matrix_size * 3; ++i){dMat_I_mat_3m_3m(i,i) = 1.0f;}


	



	Eigen::MatrixXf A,B;
	A.resize(sparse_matrix_size * 3,sparse_matrix_size * 3);
	A.setZero();
	B.resize(sparse_matrix_size * 3,1);
	B.setZero();

	Eigen::Matrix3f I_mat_3_3;
	I_mat_3_3.setZero();
	I_mat_3_3(0,0) = 1.0f;
	I_mat_3_3(1,1) = 1.0f;
	I_mat_3_3(2,2) = 1.0f;

	std::vector<Point> mean_array;
	for(int i = 0;i< sparse_matrix_size ;++i ){
		dMat_Xt_3m_1(i*3 ,0) = curvePointArray[i].x;
		dMat_Xt_3m_1(i*3 + 1,0) = curvePointArray[i].y;
		dMat_Xt_3m_1(i*3 + 2,0) = curvePointArray[i].z;

		Eigen::Vector3f vector,tmp;
		vector.normalized();

		
		float * vertex_normal = mesh_data->getVertexNormal(i);
		vector[0] = initial_path[i].n_x;
		vector[1] = initial_path[i].n_y;
		vector[2] = initial_path[i].n_z;

		//dMat_C_3m_1(i*3,0) = initial_path[i].x;
		//dMat_C_3m_1(i*3 + 1,0) = initial_path[i].y;
		//dMat_C_3m_1(i*3 + 2,0) = initial_path[i].z;


		dMat_N_3m_m(i*3,i) = vector[0];
		dMat_N_3m_m(i*3+1,i) = vector[1];
		dMat_N_3m_m(i*3+2,i) = vector[2];

		dMat_Nt_m_3m(i,i*3 ) = vector[0];
		dMat_Nt_m_3m(i,i*3+1) = vector[1];
		dMat_Nt_m_3m(i,i*3+2) = vector[2];


		if(i > 0 && i < sparse_matrix_size - 1){

			float g = Dist(curvePointArray[i-1], curvePointArray[i]);
			float f = Dist(curvePointArray[i+1], curvePointArray[i]);
			g = 1.0f;
			f = 1.0f;

			Eigen::Matrix3f Css_v0, Css_v1, Css_v2;
			Eigen::Vector3f p_0, p_1, p_2;
			Eigen::MatrixXf Css_v, V;
			Css_v.resize(3,9);
			V.resize(9,1);

			Eigen::Vector3f f_0,f_1,g_0,g_1,a,b;

			p_0[0] = curvePointArray[i-1].x;
			p_0[1] = curvePointArray[i-1].y;
			p_0[2] = curvePointArray[i-1].z;
			p_1[0] = curvePointArray[i].x;
			p_1[1] = curvePointArray[i].y;
			p_1[2] = curvePointArray[i].z;
			p_2[0] = curvePointArray[i+1].x;
			p_2[1] = curvePointArray[i+1].y;
			p_2[2] = curvePointArray[i+1].z;

			V(0) = curvePointArray[i-1].x;
			V(1) = curvePointArray[i-1].y;
			V(2) = curvePointArray[i-1].z;
			V(3) = curvePointArray[i].x;
			V(4) = curvePointArray[i].y;
			V(5) = curvePointArray[i].z;
			V(6) = curvePointArray[i+1].x;
			V(7) = curvePointArray[i+1].y;
			V(8) = curvePointArray[i+1].z;


			f_1 = (p_2-p_1)/f;
			f_0 = (p_1-p_2)/f;
			g_1 = (p_1-p_0)/g;
			g_0 = (p_0-p_1)/g;

			Eigen::Matrix3f Css_tmp;
			
			a = (-2 * p_0 / ( g *(g+f)*(g+f)) - 2* p_2/( f*f*(f+g)) -2*p_2/(f*(f+g)*(f+g)) - 2* p_1 / (g*f*f));
			Css_tmp(0,0) = a[0] * f_1(0);
			Css_tmp(0,1) = a[0] * f_1(1);
			Css_tmp(0,2) = a[0] * f_1(2);
			Css_tmp(1,0) = a[1] * f_1(0);
			Css_tmp(1,1) = a[1] * f_1(1);
			Css_tmp(1,2) = a[1] * f_1(2);
			Css_tmp(2,0) = a[2] * f_1(0);
			Css_tmp(2,1) = a[2] * f_1(1);
			Css_tmp(2,2) = a[2] * f_1(2);
			Css_v2 = Css_tmp + I_mat_3_3 * 2 / (f * (g+f));

			a = (-2 * p_2 * (g+ 2 * f)/(f*f * (g+f)*(g+f)) -2 * p_0 /(g*(g+f)*(g+f)) + 2* p_1 / (g * f*f));
			Css_tmp(0,0) = a[0] * f_0(0);
			Css_tmp(0,1) = a[0] * f_0(1);
			Css_tmp(0,2) = a[0] * f_0(2);
			Css_tmp(1,0) = a[1] * f_0(0);
			Css_tmp(1,1) = a[1] * f_0(1);
			Css_tmp(1,2) = a[1] * f_0(2);
			Css_tmp(2,0) = a[2] * f_0(0);
			Css_tmp(2,1) = a[2] * f_0(1);
			Css_tmp(2,2) = a[2] * f_0(2);
			Css_v1 = Css_tmp;

			a = ( -2 * p_2/(f*(g+f)*(g+f)) - (2* p_0 * (2* g +f)/(g*g*(g+f)*(g+f))) + 2*p_1/(f*g*g));
			Css_tmp(0,0) = a[0] * g_1(0);
			Css_tmp(0,1) = a[0] * g_1(1);
			Css_tmp(0,2) = a[0] * g_1(2);
			Css_tmp(1,0) = a[1] * g_1(0);
			Css_tmp(1,1) = a[1] * g_1(1);
			Css_tmp(1,2) = a[1] * g_1(2);
			Css_tmp(2,0) = a[2] * g_1(0);
			Css_tmp(2,1) = a[2] * g_1(1);
			Css_tmp(2,2) = a[2] * g_1(2);			
			Css_v1 = Css_v1 + Css_tmp- (2/(g*f))*I_mat_3_3;



			a = (-2 * p_2 / ( f *(g+f)*(g+f)) - 2* p_0/( g*g*(f+g)) -2*p_0/(g*(f+g)*(f+g)) - 2* p_1 / (f*g*g));
			Css_tmp(0,0) = a[0] * g_0(0);
			Css_tmp(0,1) = a[0] * g_0(1);
			Css_tmp(0,2) = a[0] * g_0(2);
			Css_tmp(1,0) = a[1] * g_0(0);
			Css_tmp(1,1) = a[1] * g_0(1);
			Css_tmp(1,2) = a[1] * g_0(2);
			Css_tmp(2,0) = a[2] * g_0(0);
			Css_tmp(2,1) = a[2] * g_0(1);
			Css_tmp(2,2) = a[2] * g_0(2);					
			Css_v0 = Css_tmp + I_mat_3_3 * 2 / (g * (g+f));

			tmp = 2 * p_2 / (f * (g+f)) + 2*p_0/(g*(g+f)) - 2*p_1/(g*f); 
			B(i*3,0) = tmp(0);
			B(i*3 + 1,0) = tmp(1);
			B(i*3 + 2,0) = tmp(2);

			//for(int r = 0 , r < 3;r++)
			//{
			//	for(int c = 0; c < 3; c++)
			//	{
			//		Css_v(r,c) = Css_v0(r,c);
			//	}
			//	for(int c = 0; c < 3; c++)
			//	{
			//		Css_v(r,c+3) = Css_v1(r,c);
			//	}
			//	for(int c = 0; c < 3; c++)
			//	{
			//		Css_v(r,c+6) = Css_v2(r,c);
			//	}
			//}

			//B - Css_v * V ;

			A(i * 3 , (i - 1) * 3 ) = Css_v0(0,0);
			A(i * 3 , (i - 1) * 3 + 1 ) = Css_v0(0,1);
			A(i * 3 , (i - 1) * 3 + 2 ) = Css_v0(0,2);
			A(i * 3 + 1 , (i - 1) * 3) = Css_v0(1,0);
			A(i * 3 + 1 , (i - 1) * 3 + 1) = Css_v0(1,1);
			A(i * 3 + 1 , (i - 1) * 3 + 2) = Css_v0(1,2);
			A(i * 3 + 2 , (i - 1) * 3) = Css_v0(2,0);
			A(i * 3 + 2 , (i - 1) * 3 + 1) = Css_v0(2,1);
			A(i * 3 + 2 , (i - 1) * 3 + 2) = Css_v0(2,2);

			A(i * 3 , (i ) * 3 ) = Css_v1(0,0);
			A(i * 3 , (i ) * 3 + 1 ) = Css_v1(0,1);
			A(i * 3 , (i ) * 3 + 2 ) = Css_v1(0,2);
			A(i * 3 + 1 , (i ) * 3) = Css_v1(1,0);
			A(i * 3 + 1 , (i ) * 3 + 1) = Css_v1(1,1);
			A(i * 3 + 1 , (i ) * 3 + 2) = Css_v1(1,2);
			A(i * 3 + 2 , (i ) * 3) = Css_v1(2,0);
			A(i * 3 + 2 , (i ) * 3 + 1) = Css_v1(2,1);
			A(i * 3 + 2 , (i ) * 3 + 2) = Css_v1(2,2);

			A(i * 3 , (i + 1) * 3 ) = Css_v2(0,0);
			A(i * 3 , (i + 1) * 3 + 1 ) = Css_v2(0,1);
			A(i * 3 , (i + 1) * 3 + 2 ) = Css_v2(0,2);
			A(i * 3 + 1 , (i + 1) * 3) = Css_v2(1,0);
			A(i * 3 + 1 , (i + 1) * 3 + 1) = Css_v2(1,1);
			A(i * 3 + 1 , (i + 1) * 3 + 2) = Css_v2(1,2);
			A(i * 3 + 2 , (i + 1) * 3) = Css_v2(2,0);
			A(i * 3 + 2 , (i + 1) * 3 + 1) = Css_v2(2,1);
			A(i * 3 + 2 , (i + 1) * 3 + 2) = Css_v2(2,2);
		}
	}
//	mean_array.clear();



	B = B - A * dMat_Xt_3m_1;

	//pfile<<std::endl<<std::endl<<"N"<<std::endl<<std::endl;
	//for(int row = 0; row < sparse_matrix_size * 3;++row){
	//	for(int col = 0; col < sparse_matrix_size;++col){
	//		pfile<<dMat_N_3m_m(row,col)<<" ";
	//	}
	//	pfile<<std::endl;
	//}

	//pfile<<std::endl<<std::endl<<"Nt"<<std::endl<<std::endl;
	//for(int row = 0; row < sparse_matrix_size;++row){
	//	for(int col = 0; col < sparse_matrix_size * 3;++col){
	//		pfile<<dMat_Nt_m_3m(row,col)<<" ";
	//	}
	//	pfile<<std::endl;
	//}

	dMat_Nstar_3m_3m = dMat_N_3m_m * dMat_Nt_m_3m;

	Eigen::MatrixXf A1,A2, A2T,B1,B2;
	A1 = A - dMat_Nstar_3m_3m * A;
	A2.resize(A1.rows()+6, A1.cols());

	A2.setZero();


	for(int r=0; r < A1.rows(); r++)
	{
		for(int c=0; c < A1.cols(); c++)
		{
			A2(r,c) = A1(r,c);
		}
	}

	A2(A1.rows(),0) = 1.0f;
	A2(A1.rows() + 1,1) = 1.0f;
	A2(A1.rows() + 2,2) = 1.0f;

	A2(A1.rows()+3, A1.cols()-3) = 1.0f;
	A2(A1.rows()+4, A1.cols()-2) = 1.0f;
	A2(A1.rows()+5, A1.cols()-1) = 1.0f;

	B1 = dMat_Nstar_3m_3m * B - B;

	std::ofstream pfile("calculation procedure.txt");

	B2.resize(B1.rows() + 6,1);
	B2.setZero();



	for(int row = 1; row < B1.rows()-1;++row)
	{
		B2(row,0) = B1(row,0);
	}


	B2(B2.rows()-6,0) = dMat_Xt_3m_1(0,0);
	B2(B2.rows()-5,0) = dMat_Xt_3m_1(1,0);
	B2(B2.rows()-4,0) = dMat_Xt_3m_1(2,0);
	B2(B2.rows()-3,0) = dMat_Xt_3m_1(dMat_Xt_3m_1.rows() - 3,0);
	B2(B2.rows()-2,0) = dMat_Xt_3m_1(dMat_Xt_3m_1.rows() - 2,0);
	B2(B2.rows()-1,0) = dMat_Xt_3m_1(dMat_Xt_3m_1.rows() - 1,0);

	

	pfile<<std::endl<<std::endl<<"Point"<<std::endl<<std::endl;
	for(int row = 0; row < dMat_Xt_3m_1.rows();++row){
		for(int col = 0; col < dMat_Xt_3m_1.cols();++col){
			pfile<<dMat_Xt_3m_1(row,col)<<" ";
		}
		pfile<<std::endl;
	}

	pfile<<std::endl<<std::endl<<"Normal"<<std::endl<<std::endl;
	for(int row = 0; row < dMat_N_3m_m.rows() ;++row){
		for(int col = 0; col < dMat_N_3m_m.cols();++col){
			pfile<<dMat_N_3m_m(row,col)<<" ";
		}
		pfile<<std::endl;
	}





	pfile<<std::endl<<std::endl<<"A"<<std::endl<<std::endl;
	for(int row = 0; row < A2.rows();++row){
		for(int col = 0; col < A2.cols();++col){
			pfile<<A2(row,col)<<" ";
		}
		pfile<<std::endl;
	}

	pfile<<std::endl<<std::endl<<"B"<<std::endl<<std::endl;
	for(int row = 0; row < B2.rows();++row){
		for(int col = 0; col < B2.cols();++col){
			pfile<<B2(row,col)<<" ";
		}
		pfile<<std::endl;
	}





	A2T = A2.transpose();
	A2 = A2T * A2;
	B2 = A2T * B2;	

	pfile<<std::endl<<std::endl<<"A"<<std::endl<<std::endl;
	for(int row = 0; row < A2.rows();++row){
		for(int col = 0; col < A2.cols();++col){
			pfile<<A2(row,col)<<" ";
		}
		pfile<<std::endl;
	}

	pfile<<std::endl<<std::endl<<"B"<<std::endl<<std::endl;
	for(int row = 0; row < B2.rows();++row){
		for(int col = 0; col < B2.cols();++col){
			pfile<<B2(row,col)<<" ";
		}
		pfile<<std::endl;
	}



	Eigen::MatrixXf dMat_Xt_1_m_3;
	//dMat_Xt_1_m_3 = A2.lu().solve(B2);
	dMat_Xt_1_m_3 = A2.fullPivLu().solve(B2);

	pfile.close();
//At = A.transpose();
//A = At * A;
//B = At * B;	
//dMat_Xt_1_m_3 = A.lu().solve(B);


	//pfile<<std::endl<<std::endl<<"K"<<std::endl<<std::endl;
	//for(int row = 0; row < sparse_matrix_size * 3;++row){
	//	for(int col = 0; col < sparse_matrix_size * 3;++col){
	//		pfile<<dMat_K_3m_3m(row,col)<<" ";
	//	}
	//	pfile<<std::endl;
	//}

	////////////////////////////////////////////////////////////////////////////////
	//Timer t2;

	//start_timer(t2,true,"matrix calculation start: ",true);

	//Eigen::MatrixXf sub_mat,inv_mat,dMat_Xt_1_m_3;





	//sub_mat.resize(sparse_matrix_size * 3 , sparse_matrix_size * 3);
	////inv_mat.resize(sparse_matrix_size * 3 , sparse_matrix_size * 3);
	//dMat_Xt_1_m_3.resize(sparse_matrix_size * 3,1);
	//sub_mat = dMat_I_mat_3m_3m +  u * (dMat_Nstar_3m_3m - dMat_K_3m_3m + dMat_Nstar_3m_3m * dMat_K_3m_3m);

	////FullPivLU<MatrixXd> lu(sub_mat);
	////inv_mat = lu.inverse();
	//
	////dMat_Xt_1_m_3 = inv_mat * ( dMat_Xt_3m_1 + u * dMat_Nstar_3m_3m * dMat_C_3m_1 );

	//Eigen::MatrixXf B;
	//B = dMat_Xt_3m_1 + u * dMat_Nstar_3m_3m * dMat_C_3m_1;

	//Eigen::MatrixXf A,B,A1,A2,B1,At;

	//Eigen::MatrixXf sub_mat,inv_mat,dMat_Xt_1_m_3;
	//dMat_Xt_1_m_3.resize(sparse_matrix_size * 3,1);
	//////////////////////////////////////////////////////////////////////////////
	/////////////////construct A
	//A.resize(sparse_matrix_size * 6 + 6, sparse_matrix_size * 3);
	//A.setZero();

	//A1.resize(sparse_matrix_size * 3 , sparse_matrix_size * 3);
	//A1.setZero();

	//A1 = dMat_K_3m_3m - dMat_Nstar_3m_3m * dMat_K_3m_3m;


	//for(int row = 0; row < sparse_matrix_size * 3;++row){
	//	for(int col = 0; col < sparse_matrix_size * 3;++col){
	//		A(row,col) = A1(row,col);
	//	}
	//}

	//A(sparse_matrix_size * 3,0) = 1.0f;
	//A(sparse_matrix_size * 3 + 1,1) = 1.0f;
	//A(sparse_matrix_size * 3 + 2,2) = 1.0f;
	//A(sparse_matrix_size * 3 + 3,sparse_matrix_size * 3 - 3) = 1.0f;
	//A(sparse_matrix_size * 3 + 4,sparse_matrix_size * 3 - 2) = 1.0f;
	//A(sparse_matrix_size * 3 + 5,sparse_matrix_size * 3 - 1) = 1.0f;


	//for(int row = sparse_matrix_size * 3 + 6; row < sparse_matrix_size * 6 + 6;++row){
	//	for(int col = 0; col < sparse_matrix_size * 3;++col){
	//		A(row,col) = dMat_Nstar_3m_3m(row - (sparse_matrix_size * 3 + 6),col);
	//	}
	//}

	////for(int row = 0; row < sparse_matrix_size * 6 + 6;++row){
	////	for(int col = 0; col < sparse_matrix_size * 3;++col){

	////		regionFile<<A(row,col)<<" ";
	////	}
	////	regionFile<<std::endl;
	////}




	//////////////////////////////////////////////////////////////////////////////
	/////////////////construct B
	//B.resize(sparse_matrix_size * 6 + 6, 1);
	//B.setZero();

	//B1.resize(sparse_matrix_size * 3, 1);
	//B1.setZero();

	//B(sparse_matrix_size * 3,0) = curvePointArray[0].x;
	//B(sparse_matrix_size * 3 + 1,0) = curvePointArray[0].y;
	//B(sparse_matrix_size * 3 + 2,0) = curvePointArray[0].z;
	//B(sparse_matrix_size * 3 + 3,0) = curvePointArray[curvePointArray.size() - 1].x;
	//B(sparse_matrix_size * 3 + 4,0) = curvePointArray[curvePointArray.size() - 1].y;
	//B(sparse_matrix_size * 3 + 5,0) = curvePointArray[curvePointArray.size() - 1].z;

	//B1 = dMat_Nstar_3m_3m * dMat_C_3m_1;

	//for(int row = sparse_matrix_size * 3 + 6 ; row < sparse_matrix_size * 6 + 6;++row){
	//	B(row,0) = B1(row - (sparse_matrix_size * 3 + 6),0);
	//}


	////	pfile.close();
	//At = A.transpose();
	//A = At * A;
	//B = At * B;	
	//dMat_Xt_1_m_3 = A.lu().solve(B);



	//dMat_Xt_1_m_3 = A1.lu().solve(B1);

	///////////////////////////// fullPivLu decomposition :

	//dMat_Xt_1_m_3 = sub_mat.fullPivLu().solve(B);

	///////////////////////////// partial Lu decomposition :

	//dMat_Xt_1_m_3 = sub_mat.lu().solve(B);

	///////////////////////////// LDL^T Cholesky decomposition :
	//dMat_Xt_1_m_3 = sub_mat.ldlt().solve(B);

	///////////////////////////// col QR decomposition with column pivoting:

	//dMat_Xt_1_m_3 = sub_mat.colPivHouseholderQr().solve(B);


	///////////////////////////// full QR decomposition with column pivoting:

	//dMat_Xt_1_m_3 = sub_mat.fullPivHouseholderQr().solve(B);

	//std::cout<<curvePointArray.size()<<" "<<sparse_matrix_size<<std::endl;

	for(int i = 1; i < sparse_matrix_size-1;++i){

		curvePointArray[i].x = dMat_Xt_1_m_3(i*3,0);
		curvePointArray[i].y = dMat_Xt_1_m_3(i*3 + 1,0);
		curvePointArray[i].z = dMat_Xt_1_m_3(i*3 + 2,0);
	}

	//regionFile.close();

}


#endif