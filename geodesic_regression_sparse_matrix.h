#ifndef geodesic_regression_sparse_matrix_h
#define geodesic_regression_sparse_matrix_h






#include "geodesic_base_elements.h"
#include "geodesic_mesh_data.h"
#include "UtilityDebug.h"

#include <Eigen/Core>
#include <Eigen/Eigen>
#include <Eigen/Eigenvalues>
#include <Eigen/Sparse>

#include <iostream>
#include <fstream>

using namespace std;
using namespace Eigen;

#define PI 3.14159265354 


float Dist(Point point0, Point point1){

	 return sqrt((float)((point0.x - point1.x) * (point0.x - point1.x) + (point0.y - point1.y) * (point0.y - point1.y) + (point0.z - point1.z) * (point0.z - point1.z)));
	 
}



void geodesic(MeshData * mesh_data , vector<int> initial_path, vector<Point> & curvePointArray ,float regression_ratio){

	//vector<Point> initial_path;
	//initial_path = curvePointArray;

	float inverse_matrix_solve_time = 0.0f;

	int dim = 3;
	int k = 1;
	ANNpoint queryPoint;
	queryPoint = annAllocPt(dim);


	ofstream pfile("calculation procedure.txt");


	int number_point_in_range = 0;

	ANNidxArray nnIdx; // near neighbor indices
	ANNdistArray dists; // near neighbor distances

	float search_radius = 0.0005f;
	ANNdist sqRad = sqrt(search_radius);



	//ofstream regionFile("result.txt");

	float u = regression_ratio;
	int sparse_matrix_size = 0;
	sparse_matrix_size = (int)curvePointArray.size();

	

	SparseMatrix<float> sMat_C_3m_1(sparse_matrix_size * 3,1);
	SparseMatrix<float> sMat_Xt_3m_1(sparse_matrix_size * 3 ,1);
	//SparseMatrix<float> sMat_Xt1_3m_1(sparse_matrix_size * 3 ,1);
	SparseMatrix<float> sMat_N_3m_m(sparse_matrix_size * 3,sparse_matrix_size);
	SparseMatrix<float> sMat_Nt_m_3m(sparse_matrix_size, sparse_matrix_size * 3 );	
	SparseMatrix<float> sMat_Nstar_3m_3m(sparse_matrix_size * 3,sparse_matrix_size * 3);
	SparseMatrix<float> sMat_K_3m_3m(sparse_matrix_size * 3,sparse_matrix_size * 3);
	SparseMatrix<float> sMat_I_mat_3m_3m(sparse_matrix_size * 3 , sparse_matrix_size * 3);



	Timer t2;

	


	for(int i = 0 ; i < sparse_matrix_size * 3; i++){

		sMat_I_mat_3m_3m.coeffRef(i,i) = 1.0f;
	}





	for(int i = 0;i< sparse_matrix_size ;i++ ){


		queryPoint[0] = curvePointArray[i].x;
		queryPoint[1] = curvePointArray[i].y;
		queryPoint[2] = curvePointArray[i].z;


		sMat_Xt_3m_1.coeffRef(i*3 ,0) = (float)curvePointArray[i].x;
		sMat_Xt_3m_1.coeffRef(i*3 + 1,0) = (float)curvePointArray[i].y;
		sMat_Xt_3m_1.coeffRef(i*3 + 2,0) = (float)curvePointArray[i].z;

		//int num_points = meshKdTree->annkFRSearch(queryPoint,sqRad,0,NULL,NULL,0.0);
		int num_points = 1;

		nnIdx = new ANNidx[num_points];
		dists = new ANNdist[num_points];



		//start_timer(t2,false, "",false);
		// find the closest point on mesh to current curve point
		mesh_data->meshKdTree->annkSearch(queryPoint,num_points,nnIdx,dists,0.00000f);

		//stop_timer(t2,true,"kd_tree searching time: ",true);



		//int current_cell_index = cage->getCellIndex(curvePointArray[i].x,curvePointArray[i].y,curvePointArray[i].z);

		//float max_dist = 99999;
		//int closest_point_id = 0;
		//for(int a = 0; a < cage->CELL_ARRAY[current_cell_index].contained_point_id.size();a++){
		//	Point point;
		//	point.x = mesh_data->getVertex( cage->CELL_ARRAY[current_cell_index].contained_point_id[a])[0];
		//	point.y = mesh_data->getVertex( cage->CELL_ARRAY[current_cell_index].contained_point_id[a])[1];
		//	point.z = mesh_data->getVertex( cage->CELL_ARRAY[current_cell_index].contained_point_id[a])[2];

		//	float distance = Dist(point,curvePointArray[i]);
		//	if(max_dist > distance){
		//		max_dist = distance;
		//		closest_point_id = cage->CELL_ARRAY[current_cell_index].contained_point_id[a];
		//	}
		//}


		//vector<int> one_ring_vertex;
		//one_ring_vertex = mesh_data->one_ring_vertex_array[closest_point_id];

		//vector<int> one_ring_faces;
		//one_ring_faces = mesh_data->one_ring_face_array[closest_point_id];






		//get one ring neighbor vertex of current curve point to compute the mean point
		vector<int> one_ring_vertex;
		one_ring_vertex = mesh_data->one_ring_vertex_array[nnIdx[0]];

		vector<int> one_ring_faces;
		one_ring_faces = mesh_data->one_ring_face_array[nnIdx[0]];

		float x = 0, y = 0,z = 0;
		//float center_point[3];				
		for(int a = 0; a < one_ring_vertex.size() ; ++a){
			x += mesh_data->getVertex(one_ring_vertex[a])[0];
			y += mesh_data->getVertex(one_ring_vertex[a])[1];
			z += mesh_data->getVertex(one_ring_vertex[a])[2];
		}	

		sMat_C_3m_1.coeffRef(i*3,0) = x /  one_ring_vertex.size();
		sMat_C_3m_1.coeffRef(i*3 + 1,0) = y /  one_ring_vertex.size();
		sMat_C_3m_1.coeffRef(i*3 + 2,0) = z /  one_ring_vertex.size();




		// get one-ring neighbor face normal of current curve point


		delete []nnIdx;
		delete []dists;

		//perform SVD on one ring face normals
		int row =  (int)one_ring_faces.size();
		Eigen::MatrixX3d eigenPointMatrix;
		eigenPointMatrix.resize(row,3);
		Eigen::MatrixX3d transposeMatrix;
		transposeMatrix.resize(row,3);
		VectorXd meanVector;



		for(int a = 0; a < one_ring_faces.size();a++ ){

			eigenPointMatrix(a,0) = mesh_data->getVertexNormal(one_ring_faces[a])[0];
			eigenPointMatrix(a,1) = mesh_data->getVertexNormal(one_ring_faces[a])[1];
			eigenPointMatrix(a,2) = mesh_data->getVertexNormal(one_ring_faces[a])[2];	
		}


		float mean;
		for (int k = 0; k < 3; k++)
		{
			//compute mean
			mean = (eigenPointMatrix.col(k).sum())/row;
			// create a vector with constant value = mean
			meanVector  = VectorXd::Constant(row,mean); 
			eigenPointMatrix.col(k) -= meanVector;
		}


		Eigen::MatrixXd covariance = ((float)1/(row-1)) * eigenPointMatrix.transpose()* eigenPointMatrix;

		Eigen::EigenSolver<MatrixXd> m_solve(covariance);

		Eigen::VectorXd eigenvalues = m_solve.eigenvalues().real();
		Eigen::MatrixXd eignevectors = m_solve.eigenvectors().real();

		Eigen::Vector3d vector;


		if(eigenvalues(0) < eigenvalues(1))
		{
			if (eigenvalues(0) < eigenvalues(2))
			{
				vector[0] = eignevectors(0,0);
				vector[1] = eignevectors(1,0);
				vector[2] = eignevectors(2,0);
			}
			else
			{
				vector[0] = eignevectors(0,2);
				vector[1] = eignevectors(1,2);
				vector[2] = eignevectors(2,2);
			}
		}
		else if(eigenvalues(1) < eigenvalues(0))
		{
			if (eigenvalues(1) < eigenvalues(2))
			{
				vector[0] = eignevectors(0,1);
				vector[1] = eignevectors(1,1);
				vector[2] = eignevectors(2,1);
			}
			else
			{
				vector[0] = eignevectors(0,2);
				vector[1] = eignevectors(1,2);
				vector[2] = eignevectors(2,2);
			}
		}


		vector.normalized();







		sMat_N_3m_m.coeffRef(i*3,i) = (float)vector[0];
		sMat_N_3m_m.coeffRef(i*3+1,i) = (float)vector[1];
		sMat_N_3m_m.coeffRef(i*3+2,i) = (float)vector[2];

		sMat_Nt_m_3m.coeffRef(i,i*3 ) = (float)vector[0];
		sMat_Nt_m_3m.coeffRef(i,i*3+1) = (float)vector[1];
		sMat_Nt_m_3m.coeffRef(i,i*3+2) = (float)vector[2];

		if(i > 0 && i < sparse_matrix_size - 1){
			sMat_K_3m_3m.coeffRef(i * 3 , (i - 1) * 3 ) = 1;
			sMat_K_3m_3m.coeffRef(i * 3 + 1 , (i - 1) * 3 +1) = 1;
			sMat_K_3m_3m.coeffRef(i * 3 + 2 , (i - 1) * 3 +2 ) = 1;

			sMat_K_3m_3m.coeffRef(i * 3 , i * 3 ) = -2;
			sMat_K_3m_3m.coeffRef(i * 3 + 1 , i * 3 +1) = -2;
			sMat_K_3m_3m.coeffRef(i * 3 + 2 , i * 3 +2 ) = -2;


			sMat_K_3m_3m.coeffRef(i * 3 , (i + 1) * 3 ) = 1;
			sMat_K_3m_3m.coeffRef(i * 3 + 1 , (i + 1) * 3 +1) = 1;
			sMat_K_3m_3m.coeffRef(i * 3 + 2 , (i + 1) * 3 +2 ) = 1;

		}


	}

	sMat_Nstar_3m_3m = sMat_N_3m_m * sMat_Nt_m_3m;

	//stop_timer(t2,true,"assign value end: ",true);

	////////////////////////////////////////////
	///// output all the matrix for debug: 


	//pfile<<"matrix Xt_3m_1 : "<<endl;
	//for(int i = 0;i<sparse_matrix_size * 3;i++ ){

	//	float value = 0;
	//	value = sMat_Xt_3m_1.coeffRef(i,0);
	//	pfile<<value<<" ";
	//	pfile<<endl;
	//}




	//pfile<<"matrix C_3m_1 : "<<endl;
	//for(int i = 0;i<sparse_matrix_size * 3;i++ ){
	//	float value = 0;
	//	value = sMat_C_3m_1.coeffRef(i,0);
	//	pfile<<value<<" ";
	//	pfile<<endl;
	//}




	//pfile<<"matrix K_3m_3m : "<<endl;
	//for(int i = 0;i<sparse_matrix_size * 3;i++ ){
	//	for(int j = 0; j < sparse_matrix_size * 3;j++){
	//		float value = 0;
	//		value = sMat_K_3m_3m.coeffRef(i,j);
	//		pfile<<value<<" ";
	//	}
	//	pfile<<endl;
	//}



	//pfile<<"matrix N : "<<endl;
	//for(int i = 0;i<sparse_matrix_size*3;i++ ){
	//	for(int j = 0; j < sparse_matrix_size;j++){
	//		float value = 0;
	//		value = sMat_N_3m_m.coeffRef(i,j);
	//		pfile<<value<<" ";
	//	}
	//	pfile<<endl;
	//}



	//pfile<<"matrix Nt : "<<endl;
	//for(int i = 0;i<sparse_matrix_size;i++ ){
	//	for(int j = 0; j < sparse_matrix_size * 3;j++){
	//		float value = 0;
	//		value = sMat_Nt_m_3m.coeffRef(i,j);
	//		pfile<<value<<" ";
	//	}
	//	pfile<<endl;
	//}


	//pfile<<"matrix I_mat_3m_3m : "<<endl;
	//for(int i = 0;i<sparse_matrix_size * 3;i++ ){
	//	for(int j = 0; j < sparse_matrix_size * 3;j++){
	//		float value = 0;
	//		value = sMat_I_mat_3m_3m.coeffRef(i,j);
	//		pfile<<value<<" ";
	//	}
	//	pfile<<endl;
	//}

	//

	//



	//pfile<<"Nstar_3m_3m"<<endl;
	//for(int i = 0;i<sparse_matrix_size*3;i++ ){
	//	for(int j = 0; j < sparse_matrix_size*3;j++){
	//		float value = 0;
	//		value = sMat_Nstar_3m_3m.coeffRef(i,j);
	//		pfile<<value<<" ";
	//	}
	//	pfile<<endl;
	//}

	////////////////////////////////////////////////////////////////////////////////
	//Timer t2;

	//start_timer(t2,true,"matrix calculation start: ",true);

	MatrixXf sub_mat,inv_mat,dMat_Xt_1_m_3;

	sub_mat.resize(sparse_matrix_size * 3 , sparse_matrix_size * 3);
	inv_mat.resize(sparse_matrix_size * 3 , sparse_matrix_size * 3);
	dMat_Xt_1_m_3.resize(sparse_matrix_size * 3,1);
	sub_mat = sMat_I_mat_3m_3m +  u * (sMat_Nstar_3m_3m - sMat_K_3m_3m + sMat_Nstar_3m_3m * sMat_K_3m_3m);

	FullPivLU<MatrixXf> lu(sub_mat);
	inv_mat = lu.inverse();


	dMat_Xt_1_m_3 = inv_mat * ( sMat_Xt_3m_1 + u * sMat_Nstar_3m_3m * sMat_C_3m_1 );

	//stop_timer(t2,true,"matrix calculation end: ",true);


	for(unsigned int i = 0; i < curvePointArray.size();i++){

		curvePointArray[i].x = dMat_Xt_1_m_3(i*3,0);
		curvePointArray[i].y = dMat_Xt_1_m_3(i*3 + 1,0);
		curvePointArray[i].z = dMat_Xt_1_m_3(i*3 + 2,0);

	}

	

	pfile.close();




}





#endif