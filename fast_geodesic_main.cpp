//fast geodesic main function. 
//Wenxi Li NCCA Bournemouth University Oct.2012
//PhD research project

//////// this version propagate wave directily on mesh.

#include <string.h>
//#include "geodesic_cage.h"
#include "geodesic_mesh_data.h"
#include "geodesic_base_elements.h"
//#include "geodesic_updata_normals.h"

#include "geodesic_read_file_V2.h"
#include "geodesic_fast_isometric_expandsion.h"
#include <iostream>
#include <fstream>
#include <string>
#include <iomanip> 





int main(int argc, char *argv[])
{
	int window_size = 0;
	int source_id;
	int iter_limit = 1;
	int is_execute_cut = 0;
	std::cout<<argc<<std::endl;
	if(argc == 6){
		window_size = atoi(argv[1]); 
		source_id = atoi(argv[3]);
		iter_limit = atoi(argv[4]);
		is_execute_cut = atoi(argv[5]);
		std::cout<<"read file from: "<<argv[2]<<std::endl;
		std::cout<<"window size is:"<<window_size<<std::endl;

	}else{
		std::cout<<"wrong argument"<<std::endl;
		return 1;
	}


	MeshData mesh_data;



	std::cout<<"loading mesh from file..."<<std::endl;


	//read the mesh from file
	char* testing_file_name = new char[100];
	strcpy(testing_file_name,argv[2]);
	geodesic_file::read_mesh_from_file(testing_file_name, mesh_data);


	std::cout<<"loading completed ..!"<<std::endl;
	std::cout<<"vertex number: "<<mesh_data.num_vertices_<<" face number: "<<mesh_data.num_faces_<<std::endl;
	

	//calculating geodesics
	try{
		std::cout<<"calculating geodesic path..!"<<std::endl;	
		mesh_virtual_network(source_id,&mesh_data,window_size, iter_limit);

//		std::cout<<"updating geodesic distance..!"<<std::endl;	
//		updateGeodesicDistance( &mesh_data, source_id);
	}
	catch(int n)
	{	
		if(n == 22)
			std::cout<<"error in cutting"<<std::endl;
	}
	catch( std::exception& e)
	{
		std::cout<<"Running Error: "<<e.what()<<std::endl;
	}
	catch( ... )
	{
		std::cout<<"Unknown Error: "<<std::endl;
	}


	std::cout<<"all geodesic has been calculated..!"<<std::endl;
	std::cout<<"Exporting Geodesic pathes..!"<<std::endl;


	//std::ofstream pathfile("floatingPathes.txt");

	//for(int i = 0; i < mesh_data.floating_path_array.size();i++){
	//	int destinationID = i;
	//	pathfile<<"curve -d 1";
	//	if( mesh_data.floating_path_array[destinationID].size() > 0)
	//	{
	//		for(int j = 0; j < mesh_data.floating_path_array[destinationID].size();j++){
	//			pathfile<<" -p "<<mesh_data.floating_path_array[destinationID][j].x<<" "<<mesh_data.floating_path_array[destinationID][j].y<<" "<<mesh_data.floating_path_array[destinationID][j].z;
	//		}
	//	}
	//	else
	//	{
	//		for(int j = 0; j < mesh_data.geodesic_path_array[destinationID].size();j++){
	//			pathfile<<" -p "<<mesh_data.geodesic_path_array[destinationID][j].x<<" "<<mesh_data.geodesic_path_array[destinationID][j].y<<" "<<mesh_data.geodesic_path_array[destinationID][j].z;
	//		}	
	//	}
	//	pathfile<<";"<<std::endl;
	//}
	//pathfile.close();




	//std::ofstream apathfile("initialPathes.txt");

	//for(int i = 0; i < mesh_data.initial_path_array_initial.size();i++){
	//	int destinationID = i;
	//	apathfile<<"curve -d 1";
	//	
	//	for(int j = 0; j < mesh_data.initial_path_array_initial[destinationID].size();j++){
	//		apathfile<<" -p "<<mesh_data.initial_path_array_initial[destinationID][j].x<<" "<<mesh_data.initial_path_array_initial[destinationID][j].y<<" "<<mesh_data.initial_path_array_initial[destinationID][j].z;
	//	}

	//	apathfile<<";"<<std::endl;
	//}
	//apathfile.close();



	//std::ofstream ipathfile("prjectedPathes.txt");

	//for(int i = 0; i < mesh_data.initial_path_array.size();i++){
	//	int destinationID = i;
	//	if(i == source_id)
	//	{
	//		continue;
	//	}

	//	ipathfile<<"curve -d 1";
	//	for(int j = 0; j < mesh_data.initial_path_array[destinationID].size();j++){
	//		ipathfile<<" -p "<<mesh_data.initial_path_array[destinationID][j].x<<" "<<mesh_data.initial_path_array[destinationID][j].y<<" "<<mesh_data.initial_path_array[destinationID][j].z;
	//	}
	//	ipathfile<<";"<<std::endl;
	//}
	//ipathfile.close();



	std::ofstream lpathfile("cosDistance.txt");
	for(int i = 0; i < mesh_data.distance_to_source_array.size();i++){
		if(i == source_id)
		{
			continue;
		}
		lpathfile<<i<<" "<<mesh_data.distance_to_source_array[i]<<std::endl;
	}
	lpathfile.close();
	


//mesh_data->distance_to_source_array[connectedVertices[b]] = curve_length(mesh_data->geodesic_path_array[connectedVertices[b]]);

	std::ofstream ppathfile("projDistance.txt");

	for(int i = 0; i < mesh_data.initial_path_array.size();i++){
		int destinationID = i;
		//ipathfile<<"curve -d 1";
		float distance = 0.0f;

		if(i == source_id)
		{
			continue;
		}

		if(mesh_data.initial_path_array.size() < 2)
		{	
			ppathfile<<i<<" "<<999999<<std::endl;
			continue;
		}
		distance = curve_length(mesh_data.geodesic_path_array[i]);
		ppathfile<<i<<" "<<distance<<std::endl;


		//for(int j = 0; j < mesh_data.initial_path_array[destinationID].size();j++){
		//	ipathfile<<" -p "<<mesh_data.initial_path_array[destinationID][j].x<<" "<<mesh_data.initial_path_array[destinationID][j].y<<" "<<mesh_data.initial_path_array[destinationID][j].z;
		//}
		//ipathfile<<";"<<std::endl;
	}
	ppathfile.close();






	//if(is_execute_cut != 0)
	//{
	//	std::cout<<"Start Cutting on mesh..!"<<std::endl;
	//	for(int i = 0; i < mesh_data.geodesic_path_array.size();i++)
	//	{
	//		if(i==source_id || mesh_data.geodesic_path_array[i].size() < 3)
	//			continue;

	//		try{
	//			std::cout<<"Cutting Path: "<<i<<std::endl;
	//			ProjectPath(&mesh_data, mesh_data.initial_path_array[i], mesh_data.floating_path_array[i]);

	//		}catch(...)
	//		{
	//			std::cout<<"Error in cut path, path ID = "<<i<<std::endl;
	//		}	

	//	}
	//}



	//std::ofstream projfile("cutPath.txt");

	//for(int i = 0; i < mesh_data.projected_path_array.size();i++){
	//	int destinationID = i;
	//	projfile<<"curve -d 1";
	//	if( mesh_data.projected_path_array[destinationID].size() > 0)
	//	{
	//		for(int j = 0; j < mesh_data.projected_path_array[destinationID].size();j++){
	//			projfile<<" -p "<<mesh_data.projected_path_array[destinationID][j].x<<" "<<mesh_data.projected_path_array[destinationID][j].y<<" "<<mesh_data.projected_path_array[destinationID][j].z;
	//		}
	//	}else
	//	{
	//		for(int j = 0; j < mesh_data.geodesic_path_array[destinationID].size();j++){
	//			projfile<<" -p "<<mesh_data.geodesic_path_array[destinationID][j].x<<" "<<mesh_data.geodesic_path_array[destinationID][j].y<<" "<<mesh_data.geodesic_path_array[destinationID][j].z;
	//		}			
	//	}
	//	projfile<<";"<<std::endl;
	//}
	//projfile.close();


	//bool is_exit = false;

	//std::string timeFile = "window_";
	//timeFile += window_size;
	//timeFile += ".txt"
	//std::ofstream timeLog( timeFile );
	//timeLog << geodesic_regression_time;
	//timeLog.close;




	//	//do{
	//std::ofstream allpathfile("pathes_ground_truth.txt");
	//allpathfile<<mesh_data.geodesic_path_array.size()<<" ";
	//for(int i = 0; i < mesh_data.geodesic_path_array.size();++i){
	//	allpathfile<<mesh_data.geodesic_path_array[i].size()<<" ";
	//	if(mesh_data.geodesic_path_array[i].size() == 0){
	//		std::cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!"<<std::endl;
	//	}
	//}
	////allpathfile<<std::endl;
	//for(int i = 0; i < mesh_data.geodesic_path_array.size();++i){
	//	for(int j = 0; j < mesh_data.geodesic_path_array[i].size();++j){
	//		float x,y,z;
	//		x = mesh_data.geodesic_path_array[i][j].x;
	//		y = mesh_data.geodesic_path_array[i][j].y;
	//		z = mesh_data.geodesic_path_array[i][j].z;
	//		allpathfile<<std::setprecision(9)<<x<<" "<<y<<" "<<z<<std::endl;
	//	}
	//	allpathfile<<std::endl;
	//}

	//allpathfile.close();
	//
	//std::ofstream allpathfile("pathes_ground_truth.txt");
	//allpathfile<<mesh_data.initial_path_array.size()<<" ";
	//for(int i = 0; i < mesh_data.initial_path_array.size();++i){
	//	allpathfile<<mesh_data.initial_path_array[i].size()<<" ";
	//	if(mesh_data.initial_path_array[i].size() == 0){
	//		std::cout<<"!!!!!!!!!!!!!!!!!!!!!!!!!!"<<std::endl;
	//	}
	//}
	////allpathfile<<std::endl;
	//for(int i = 0; i < mesh_data.initial_path_array.size();++i){
	//	for(int j = 0; j < mesh_data.initial_path_array[i].size();++j){
	//		float x,y,z;
	//		x = mesh_data.initial_path_array[i][j];
	//		allpathfile<<std::setprecision(9)<<x<<std::endl;
	//	}
	//	allpathfile<<std::endl;
	//}

	//allpathfile.close();




	//		int destinationID = 0;
	////std::cin >> destinationID;
	//





	//is_exit = true;

	//}while(!is_exit);

	//
	//char cha;
	//std::cout<<"press any key to continue..."<<std::endl;
	//std::cin>>cha;
	return 0;
}