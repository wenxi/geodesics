#ifndef geodesic_regression_dense_matrix_h
#define geodesic_regression_dense_matrix_h






#include "geodesic_base_elements.h"
#include "geodesic_mesh_data.h"


#include <Eigen/Core>
#include <Eigen/Eigen>
#include <Eigen/Eigenvalues>
#include <Eigen/Sparse>

#include <iostream>
#include <fstream>



#define PI 3.14159265354 




float Dist(Point point0, Point point1){
	return (point0.x - point1.x) * (point0.x - point1.x) + (point0.y - point1.y) * (point0.y - point1.y) + (point0.z - point1.z) * (point0.z - point1.z);
}





void geodesic(MeshData * mesh_data, std::vector<initPoint> & initial_path, std::vector<Point> & curvePointArray ,float regression_ratio){


	int dim = 3;
	int k = 1;
	//ANNpoint queryPoint;
	//queryPoint = annAllocPt(dim);
	std::ofstream pfile("calculation procedure.txt");
	//int number_point_in_range = 0;
	//ANNidxArray nnIdx; // near neighbor indices
	//ANNdistArray dists; // near neighbor distances
	//float search_radius = 0.0005f;
	//ANNdist sqRad = sqrt(search_radius);
	std::ofstream regionFile("result.txt");

	float u = regression_ratio;
	int sparse_matrix_size = (int)curvePointArray.size();

	int index_offset = 0;

	Eigen::MatrixXf dMat_C_3m_1;
	dMat_C_3m_1.resize(sparse_matrix_size * 3,1);
	dMat_C_3m_1.setZero();


	Eigen::MatrixXf dMat_Xt_3m_1;
	dMat_Xt_3m_1.resize(sparse_matrix_size * 3 ,1);
	dMat_Xt_3m_1.setZero();

	////////////////////////////////////
	Eigen::MatrixXf dMat_N_3m_m;
	dMat_N_3m_m.resize(sparse_matrix_size * 3,sparse_matrix_size);
	dMat_N_3m_m.setZero();

	Eigen::MatrixXf dMat_Nt_m_3m;
	dMat_Nt_m_3m.resize(sparse_matrix_size, sparse_matrix_size * 3 );
	dMat_Nt_m_3m.setZero();
	////////////////////////////////////




	Eigen::MatrixXf dMat_Nstar_3m_3m;
	dMat_Nstar_3m_3m.resize(sparse_matrix_size * 3,sparse_matrix_size * 3);
	dMat_Nstar_3m_3m.setZero();


	Eigen::MatrixXf dMat_K_3m_3m;
	dMat_K_3m_3m.resize(sparse_matrix_size * 3,sparse_matrix_size * 3);
	dMat_K_3m_3m.setZero();

	Eigen::MatrixXf dMat_I_mat_3m_3m;
	dMat_I_mat_3m_3m.resize(sparse_matrix_size * 3 , sparse_matrix_size * 3);
	dMat_I_mat_3m_3m.setZero();

	for(int i = 0 ; i < sparse_matrix_size * 3; ++i){dMat_I_mat_3m_3m(i,i) = 1.0f;}


	//std::ofstream pfile("calculation procedure.txt");





	std::vector<Point> mean_array;
	for(int i = 0;i< sparse_matrix_size ;++i ){
		dMat_Xt_3m_1(i*3 ,0) = curvePointArray[i].x;
		dMat_Xt_3m_1(i*3 + 1,0) = curvePointArray[i].y;
		dMat_Xt_3m_1(i*3 + 2,0) = curvePointArray[i].z;

		Eigen::Vector3f vector;
		vector.normalized();

		
		float * vertex_normal = mesh_data->getVertexNormal(i);
		vector[0] = initial_path[i].n_x;
		vector[1] = initial_path[i].n_y;
		vector[2] = initial_path[i].n_z;

		dMat_C_3m_1(i*3,0) = initial_path[i].x;
		dMat_C_3m_1(i*3 + 1,0) = initial_path[i].y;
		dMat_C_3m_1(i*3 + 2,0) = initial_path[i].z;

		//Point p;
		//p.x = x /  one_ring_vertex.size();
		//p.y = y /  one_ring_vertex.size();
		//p.z = z /  one_ring_vertex.size();

		//mean_array.push_back(p);


		dMat_N_3m_m(i*3,i) = vector[0];
		dMat_N_3m_m(i*3+1,i) = vector[1];
		dMat_N_3m_m(i*3+2,i) = vector[2];

		dMat_Nt_m_3m(i,i*3 ) = vector[0];
		dMat_Nt_m_3m(i,i*3+1) = vector[1];
		dMat_Nt_m_3m(i,i*3+2) = vector[2];



		if(i > 0 && i < sparse_matrix_size - 1){

			float g = sqrt(Dist(curvePointArray[i-1], curvePointArray[i]));
			float f = sqrt(Dist(curvePointArray[i+1], curvePointArray[i]));


			//dMat_K_3m_3m(i * 3 , (i - 1) * 3 ) = 2/(g*(g+f));
			//dMat_K_3m_3m(i * 3 + 1 , (i - 1) * 3 +1) = 2/(g*(g+f));
			//dMat_K_3m_3m(i * 3 + 2 , (i - 1) * 3 +2 ) = 2/(g*(g+f));

			//dMat_K_3m_3m(i * 3 , i * 3 ) = -2/(g*f);
			//dMat_K_3m_3m(i * 3 + 1 , i * 3 +1) =  -2/(g*f);
			//dMat_K_3m_3m(i * 3 + 2 , i * 3 +2 ) =  -2/(g*f);


			//dMat_K_3m_3m(i * 3 , (i + 1) * 3 ) = 2/(f*(g+f));
			//dMat_K_3m_3m(i * 3 + 1 , (i + 1) * 3 +1) = 2/(f*(g+f));
			//dMat_K_3m_3m(i * 3 + 2 , (i + 1) * 3 +2 ) = 2/(f*(g+f));

/////////////////////////////////////////////

			dMat_K_3m_3m(i * 3 , (i - 1) * 3 ) = 1/(2 * g);
			dMat_K_3m_3m(i * 3 + 1 , (i - 1) * 3 +1) = 1/(2 * g);
			dMat_K_3m_3m(i * 3 + 2 , (i - 1) * 3 +2 ) = 1/(2 * g);

			dMat_K_3m_3m(i * 3 , i * 3 ) = -1 * ((f+g)/(2*g*f));
			dMat_K_3m_3m(i * 3 + 1 , i * 3 +1) =  -1 * ((f+g)/(2*g*f));
			dMat_K_3m_3m(i * 3 + 2 , i * 3 +2 ) =  -1 * ((f+g)/(2*g*f));


			dMat_K_3m_3m(i * 3 , (i + 1) * 3 ) = 1/(2 * f);
			dMat_K_3m_3m(i * 3 + 1 , (i + 1) * 3 +1) = 1/(2 * f);
			dMat_K_3m_3m(i * 3 + 2 , (i + 1) * 3 +2 ) = 1/(2 * f);
////////////////////////////////////////////////////

			//dMat_K_3m_3m(i * 3 , (i - 1) * 3 ) = 1;
			//dMat_K_3m_3m(i * 3 + 1 , (i - 1) * 3 +1) = 1;
			//dMat_K_3m_3m(i * 3 + 2 , (i - 1) * 3 +2 ) = 1;

			//dMat_K_3m_3m(i * 3 , i * 3 ) = -2;
			//dMat_K_3m_3m(i * 3 + 1 , i * 3 +1) = -2;
			//dMat_K_3m_3m(i * 3 + 2 , i * 3 +2 ) = -2;


			//dMat_K_3m_3m(i * 3 , (i + 1) * 3 ) = 1;
			//dMat_K_3m_3m(i * 3 + 1 , (i + 1) * 3 +1) = 1;
			//dMat_K_3m_3m(i * 3 + 2 , (i + 1) * 3 +2 ) = 1;

		}
	}
	mean_array.clear();




	//pfile<<std::endl<<std::endl<<"N"<<std::endl<<std::endl;
	//for(int row = 0; row < sparse_matrix_size * 3;++row){
	//	for(int col = 0; col < sparse_matrix_size;++col){
	//		pfile<<dMat_N_3m_m(row,col)<<" ";
	//	}
	//	pfile<<std::endl;
	//}

	//pfile<<std::endl<<std::endl<<"Nt"<<std::endl<<std::endl;
	//for(int row = 0; row < sparse_matrix_size;++row){
	//	for(int col = 0; col < sparse_matrix_size * 3;++col){
	//		pfile<<dMat_Nt_m_3m(row,col)<<" ";
	//	}
	//	pfile<<std::endl;
	//}

	dMat_Nstar_3m_3m = dMat_N_3m_m * dMat_Nt_m_3m;
	//dMat_Nstar_3m_3m_2 = dMat_N_3m_m_2 * dMat_Nt_m_3m_2;
	//pfile<<std::endl<<std::endl<<"Nstar"<<std::endl<<std::endl;
	//for(int row = 0; row < sparse_matrix_size * 3;++row){
	//	for(int col = 0; col < sparse_matrix_size * 3;++col){
	//		pfile<<dMat_Nstar_3m_3m(row,col)<<" ";
	//	}
	//	pfile<<std::endl;
	//}

	pfile<<std::endl<<std::endl<<"K"<<std::endl<<std::endl;
	for(int row = 0; row < sparse_matrix_size * 3;++row){
		for(int col = 0; col < sparse_matrix_size * 3;++col){
			pfile<<dMat_K_3m_3m(row,col)<<" ";
		}
		pfile<<std::endl;
	}
	pfile.close();
	////////////////////////////////////////////////////////////////////////////////
	//Timer t2;

	//start_timer(t2,true,"matrix calculation start: ",true);

	//Eigen::MatrixXf sub_mat,inv_mat,dMat_Xt_1_m_3;





	//sub_mat.resize(sparse_matrix_size * 3 , sparse_matrix_size * 3);
	////inv_mat.resize(sparse_matrix_size * 3 , sparse_matrix_size * 3);
	//dMat_Xt_1_m_3.resize(sparse_matrix_size * 3,1);
	//sub_mat = dMat_I_mat_3m_3m +  u * (dMat_Nstar_3m_3m - dMat_K_3m_3m + dMat_Nstar_3m_3m * dMat_K_3m_3m);

	////FullPivLU<MatrixXd> lu(sub_mat);
	////inv_mat = lu.inverse();
	//
	////dMat_Xt_1_m_3 = inv_mat * ( dMat_Xt_3m_1 + u * dMat_Nstar_3m_3m * dMat_C_3m_1 );

	//Eigen::MatrixXf B;
	//B = dMat_Xt_3m_1 + u * dMat_Nstar_3m_3m * dMat_C_3m_1;

	Eigen::MatrixXf A,B,A1,A2,B1,At;

	Eigen::MatrixXf sub_mat,inv_mat,dMat_Xt_1_m_3;
	dMat_Xt_1_m_3.resize(sparse_matrix_size * 3,1);
	////////////////////////////////////////////////////////////////////////////
	///////////////construct A
	A.resize(sparse_matrix_size * 6 + 6, sparse_matrix_size * 3);
	A.setZero();

	A1.resize(sparse_matrix_size * 3 , sparse_matrix_size * 3);
	A1.setZero();

	A1 = dMat_K_3m_3m - dMat_Nstar_3m_3m * dMat_K_3m_3m;


	for(int row = 0; row < sparse_matrix_size * 3;++row){
		for(int col = 0; col < sparse_matrix_size * 3;++col){
			A(row,col) = A1(row,col);
		}
	}

	A(sparse_matrix_size * 3,0) = 1.0f;
	A(sparse_matrix_size * 3 + 1,1) = 1.0f;
	A(sparse_matrix_size * 3 + 2,2) = 1.0f;
	A(sparse_matrix_size * 3 + 3,sparse_matrix_size * 3 - 3) = 1.0f;
	A(sparse_matrix_size * 3 + 4,sparse_matrix_size * 3 - 2) = 1.0f;
	A(sparse_matrix_size * 3 + 5,sparse_matrix_size * 3 - 1) = 1.0f;


	for(int row = sparse_matrix_size * 3 + 6; row < sparse_matrix_size * 6 + 6;++row){
		for(int col = 0; col < sparse_matrix_size * 3;++col){
			A(row,col) = dMat_Nstar_3m_3m(row - (sparse_matrix_size * 3 + 6),col);
		}
	}

	for(int row = 0; row < A.rows();++row){
		for(int col = 0; col < A.cols();++col){

			regionFile<<A(row,col)<<" ";
		}
		regionFile<<std::endl;
	}




	////////////////////////////////////////////////////////////////////////////
	///////////////construct B
	B.resize(sparse_matrix_size * 6 + 6, 1);
	B.setZero();

	B1.resize(sparse_matrix_size * 3, 1);
	B1.setZero();

	B(sparse_matrix_size * 3,0) = curvePointArray[0].x;
	B(sparse_matrix_size * 3 + 1,0) = curvePointArray[0].y;
	B(sparse_matrix_size * 3 + 2,0) = curvePointArray[0].z;
	B(sparse_matrix_size * 3 + 3,0) = curvePointArray[curvePointArray.size() - 1].x;
	B(sparse_matrix_size * 3 + 4,0) = curvePointArray[curvePointArray.size() - 1].y;
	B(sparse_matrix_size * 3 + 5,0) = curvePointArray[curvePointArray.size() - 1].z;

	B1 = dMat_Nstar_3m_3m * dMat_C_3m_1;

	for(int row = sparse_matrix_size * 3 + 6 ; row < sparse_matrix_size * 6 + 6;++row){
		B(row,0) = B1(row - (sparse_matrix_size * 3 + 6),0);
	}



	for(int row = 0; row < B.rows();++row){
		for(int col = 0; col < B.cols();++col){

			regionFile<<B(row,col)<<" ";
		}
		regionFile<<std::endl;
	}

	regionFile.close();



	At = A.transpose();
	A = At * A;
	B = At * B;	
	dMat_Xt_1_m_3 = A.lu().solve(B);



	//dMat_Xt_1_m_3 = A1.lu().solve(B1);

	///////////////////////////// fullPivLu decomposition :

	//dMat_Xt_1_m_3 = sub_mat.fullPivLu().solve(B);

	///////////////////////////// partial Lu decomposition :

	//dMat_Xt_1_m_3 = sub_mat.lu().solve(B);

	///////////////////////////// LDL^T Cholesky decomposition :
	//dMat_Xt_1_m_3 = sub_mat.ldlt().solve(B);

	///////////////////////////// col QR decomposition with column pivoting:

	//dMat_Xt_1_m_3 = sub_mat.colPivHouseholderQr().solve(B);


	///////////////////////////// full QR decomposition with column pivoting:

	//dMat_Xt_1_m_3 = sub_mat.fullPivHouseholderQr().solve(B);

	//std::cout<<curvePointArray.size()<<" "<<sparse_matrix_size<<std::endl;

	for(int i = 1; i < sparse_matrix_size-1;++i){

		curvePointArray[i].x = dMat_Xt_1_m_3(i*3,0);
		curvePointArray[i].y = dMat_Xt_1_m_3(i*3 + 1,0);
		curvePointArray[i].z = dMat_Xt_1_m_3(i*3 + 2,0);
	}

	//regionFile.close();

}


#endif