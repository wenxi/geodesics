#ifndef geodesic_build_vitrual_network_h
#define geodesic_build_vitrual_network_h


// this function create virtual network from on source to all destination and at each expansion iteration, 
// the geodesic will be calculated and copy to the next closest point on the next ring, thus at each expansion.
// only newly added point will be calculated .



//#include "geodesic_regression_BLAS_matrix.h"
#include "geodesic_mesh_data.h"
#include "geodesic_base_elements.h"
//#include "geodesic_regression_sparse_matrix.h"
//#include "geodesic_regression_sparse_matrix_V2.h"
//#include "geodesic_regression_dense_matrix.h"
//#include "geodesic_regression_dense_ANN_matrix.h"
#include "geodesic_regression_dense_matrix_V2.h"
//#include "geodesic_pull_back.h"
#include "geodesic_update_initialpath.h"
//#include "geodesic_project_path.h"
#include <Eigen/Core>
#include <Eigen/Eigen>
#include <Eigen/Eigenvalues>
#include <Eigen/Sparse>
#include <ctime>
//using namespace std;
//using namespace Eigen;
#include <iostream>
#include <fstream>



float sqrtDis(Point point0, Point point1){

	float sqrt_distance = (float)((point0.x - point1.x) * (point0.x - point1.x) + (point0.y - point1.y) * (point0.y - point1.y) + (point0.z - point1.z) * (point0.z - point1.z));
	return sqrt_distance;
}


float curve_length(std::vector<Point> curvePoints){
	float distance = 0.0;
	for(int i = 1; i < curvePoints.size(); ++i){
		distance += sqrt(sqrtDis(curvePoints[i - 1], curvePoints[i]));
	}
	return distance;
}




void mesh_virtual_network(int source_id , MeshData * mesh_data,int window_size, int iter_limit){

	////// timers 
	//Timer t1,t2,t3,t4;
	//start_timer(t1,false,"",false);
	double t1a = clock();
	float geodesic_regression_time = 0.0f; 



	float regression_ratio = 50000.0f;

	std::vector<Point> new_section;
	new_section.resize(window_size);

	std::vector<initPoint> new_initial_section;
	new_initial_section.resize(window_size);
	std::vector<int> next_wave;
	std::vector<int> current_wave_head;
	std::vector<int> current_wave;
	std::vector<int> connectedVertices;
	std::vector<int> connectedVerticesID;
	int closest_point_id = source_id;

	/////initialize the source point
	Point source_point;
	source_point = mesh_data->getPoint(source_id);
	mesh_data->geodesic_path_array[source_id].push_back(source_point);

	initPoint initSourcePoint;
	initSourcePoint.vertexID = source_id;
	initSourcePoint.x = source_point.x;
	initSourcePoint.y = source_point.y;
	initSourcePoint.z = source_point.z;
	initSourcePoint.n_x = mesh_data->getVertexNormal(source_id)[0];
	initSourcePoint.n_y = mesh_data->getVertexNormal(source_id)[1];
	initSourcePoint.n_z = mesh_data->getVertexNormal(source_id)[2];
	initSourcePoint.flag = 1;
	initSourcePoint.is_geodest_exact = true;
	initSourcePoint.neighbourFacesID = mesh_data->getFaceIDfromVertex(source_id);	
	initSourcePoint.location = initSourcePoint.neighbourFacesID;
	mesh_data->initial_path_array[source_id].push_back(initSourcePoint);
	mesh_data->distance_to_source_array[source_id] = 0;

	int vertex_number = mesh_data->numVertices();
	current_wave.reserve((int)sqrt((float)vertex_number));
	float max_distance_to_source = 0;
	connectedVertices = mesh_data->one_ring_vertex_array[source_id];

	//std::ofstream pathfile("pathe_output");


	for(int a = 0; a < connectedVertices.size();++a){

		float distance_ = sqrt( sqrtDis( mesh_data->getPoint(connectedVertices[a]),source_point ) );
		if( max_distance_to_source < distance_){
			// propgatation limitation
			max_distance_to_source = distance_ ;
		}

		//distance to source
		mesh_data->distance_to_source_array[connectedVertices[a]] = distance_;

		//geodesic path
		mesh_data->geodesic_path_array[connectedVertices[a]] = mesh_data->geodesic_path_array[source_id];
		mesh_data->geodesic_path_array[connectedVertices[a]].push_back(mesh_data->getPoint(connectedVertices[a]));
		mesh_data->floating_path_array[connectedVertices[a]] = mesh_data->geodesic_path_array[connectedVertices[a]];
		//initial path
		mesh_data->initial_path_array[connectedVertices[a]] = mesh_data->initial_path_array[source_id];
		initPoint new_vertex;
		new_vertex.vertexID = connectedVertices[a];
		new_vertex.x = mesh_data->getPoint(connectedVertices[a]).x;
		new_vertex.y = mesh_data->getPoint(connectedVertices[a]).y;
		new_vertex.z = mesh_data->getPoint(connectedVertices[a]).z;
		new_vertex.n_x = mesh_data->getVertexNormal(connectedVertices[a])[0];
		new_vertex.n_y = mesh_data->getVertexNormal(connectedVertices[a])[1];
		new_vertex.n_z = mesh_data->getVertexNormal(connectedVertices[a])[2];
		new_vertex.is_geodest_exact = true;
		new_vertex.flag = 1;
		//initSourcePoint.is_on_edge = false;
		new_vertex.neighbourFacesID = mesh_data->getFaceIDfromVertex(connectedVertices[a]);	
		new_vertex.location = new_vertex.neighbourFacesID;
		mesh_data->initial_path_array[connectedVertices[a]].push_back(new_vertex);
		current_wave.push_back(connectedVertices[a]);
	}
	connectedVertices.clear(); 



	float max_length = 0.0f;
	do{	
		current_wave_head.reserve((int)sqrt((float)vertex_number));
		next_wave.reserve((int)sqrt((float)vertex_number));
		std::vector<int> uncut_points;
		for(int current_wave_index = 0; current_wave_index < current_wave.size(); ++current_wave_index){

			connectedVertices = mesh_data->one_ring_vertex_array[current_wave[current_wave_index]];
			bool is_exist = false;
			for(int b = 0; b < connectedVertices.size();++b){

				if(mesh_data->distance_to_source_array[connectedVertices[b]] < MAX_DIST) continue;

				float min_dist = 99999.9f;
				bool is_calculate_geodesic = true;
				int parent_id = 0;
				float parent_dist_to_source = 0.0f;
				float distance_to_parent = 0.0f;

				float max_angle_cos = 1.0f;

				
	

				//////////////////choose the shortest parent
				double perent_MAX_dist = 999999.9;
				for(int c = 0; c < mesh_data->one_ring_vertex_array[connectedVertices[b]].size();++c){
					bool is_in_ws_ = false;
					for(int ws_id = 0; ws_id < current_wave.size();ws_id++){
						if( mesh_data->one_ring_vertex_array[connectedVertices[b]][c] == current_wave[ws_id]){
							is_in_ws_ = true;
							break;
						}
					}
					if(!is_in_ws_) continue;

					int id_a = mesh_data->one_ring_vertex_array[connectedVertices[b]][c];
					if(mesh_data->distance_to_source_array[id_a] > (MAX_DIST - 2)) continue;
	
					float current_perent_dist = mesh_data->distance_to_source_array[id_a] + sqrt( sqrtDis( mesh_data->getPoint(connectedVertices[b]) , mesh_data->getPoint(mesh_data->one_ring_vertex_array[connectedVertices[b]][c])));
					if( current_perent_dist < perent_MAX_dist )
					{
						perent_MAX_dist = current_perent_dist;
						parent_id = mesh_data->one_ring_vertex_array[connectedVertices[b]][c];
					}
				}

				//estimate geodesic distance
				min_dist = mesh_data->distance_to_source_array[parent_id] + sqrt( sqrtDis( mesh_data->getPoint(connectedVertices[b]) , mesh_data->getPoint( parent_id ) ) );
			
				if(min_dist < max_distance_to_source){
					//geodesic path
					mesh_data->geodesic_path_array[connectedVertices[b]] = mesh_data->geodesic_path_array[parent_id];
					mesh_data->geodesic_path_array[connectedVertices[b]].push_back( mesh_data->getPoint(connectedVertices[b]) );

					//initial path
					mesh_data->initial_path_array[connectedVertices[b]] = mesh_data->initial_path_array[parent_id];
					initPoint new_vertex;
					new_vertex.vertexID = connectedVertices[b];
					new_vertex.x = mesh_data->getPoint(connectedVertices[b]).x;
					new_vertex.y = mesh_data->getPoint(connectedVertices[b]).y;
					new_vertex.z = mesh_data->getPoint(connectedVertices[b]).z;
					new_vertex.n_x = mesh_data->getVertexNormal(connectedVertices[b])[0];
					new_vertex.n_y = mesh_data->getVertexNormal(connectedVertices[b])[1];
					new_vertex.n_z = mesh_data->getVertexNormal(connectedVertices[b])[2];
					new_vertex.neighbourFacesID = mesh_data->getFaceIDfromVertex(connectedVertices[b]);
					new_vertex.location = new_vertex.neighbourFacesID;
					new_vertex.flag = 1;
					mesh_data->initial_path_array[connectedVertices[b]].push_back(new_vertex);

					bool is_geodesic_exact = false;
					//approximate alg
					if(mesh_data->geodesic_path_array[connectedVertices[b]].size() > window_size && window_size != 0){
						size_t index_offset = 0;

						index_offset = mesh_data->geodesic_path_array[connectedVertices[b]].size() - window_size;

						for( size_t e = index_offset; e < mesh_data->geodesic_path_array[connectedVertices[b]].size(); ++e){
							new_section[e - index_offset] =  mesh_data->geodesic_path_array[connectedVertices[b]][e];
							new_initial_section[e - index_offset] =  mesh_data->initial_path_array[connectedVertices[b]][e];
						}

	
						geodesic(mesh_data,new_initial_section, new_section ,regression_ratio);
	
						mesh_data->floating_path_array[connectedVertices[b]] = mesh_data->geodesic_path_array[connectedVertices[b]];
						for( size_t e = index_offset; e < mesh_data->geodesic_path_array[connectedVertices[b]].size(); ++e)
						{
							mesh_data->floating_path_array[connectedVertices[b]][e] = new_section[e - index_offset];
							//mesh_data->geodesic_path_array[connectedVertices[b]][e] = new_section[e - index_offset];
						}

						updata_path(mesh_data,new_initial_section,new_section,false);	
						
						float lo = 0.0f;
						geodesic_regression_time += lo;

						//updata_path(mesh_data,mesh_data->initial_path_array[connectedVertices[b]],mesh_data->geodesic_path_array[connectedVertices[b]],false);	

						for( size_t e = index_offset; e < mesh_data->geodesic_path_array[connectedVertices[b]].size(); ++e){

							mesh_data->geodesic_path_array[connectedVertices[b]][e] = new_section[e - index_offset];
							mesh_data->initial_path_array[connectedVertices[b]][e] = new_initial_section[e - index_offset];
						}
						is_geodesic_exact = getGeodesicDistance(mesh_data,mesh_data->initial_path_array[connectedVertices[b]],mesh_data->floating_path_array[connectedVertices[b]]);
						
						if(!is_geodesic_exact)
						{
							appendVector(connectedVertices[b],uncut_points);
						}
					}else{

						for(int itt = 0; itt < iter_limit ; itt++)
						{
							if(54 == connectedVertices[b])
							{
								std::cout<<"Calculating Vertex id: "<<connectedVertices[b]<<std::endl;
							}

							//try
							//{
							//	mesh_data->initial_path_array_initial[connectedVertices[b]] = mesh_data->geodesic_path_array[connectedVertices[b]];
							//}catch(std::exception& e)
							//{
							//	std::cout<<"error in recording floating path: "<<e.what()<<std::endl;
							//}



							try
							{
							geodesic(mesh_data,mesh_data->initial_path_array[connectedVertices[b]], mesh_data->geodesic_path_array[connectedVertices[b]], regression_ratio);
							}catch(std::exception& e)
							{
								std::cout<<"error in solving equiation: "<<e.what()<<std::endl;
							}							
							
							
							mesh_data->floating_path_array[connectedVertices[b]] = mesh_data->geodesic_path_array[connectedVertices[b]];
	
							try
							{

								if(itt < iter_limit - 1)
									updata_path(mesh_data,mesh_data->initial_path_array[connectedVertices[b]],mesh_data->geodesic_path_array[connectedVertices[b]],false);
								else
									updata_path(mesh_data,mesh_data->initial_path_array[connectedVertices[b]],mesh_data->geodesic_path_array[connectedVertices[b]],false);

							}catch(std::exception & e)
							{
								std::cout<<"error in projection: "<<e.what()<<std::endl;
							}
						}

						is_geodesic_exact = getGeodesicDistance(mesh_data,mesh_data->initial_path_array[connectedVertices[b]],mesh_data->floating_path_array[connectedVertices[b]]);
							//next_propagation_boundary.push_back(tmp_boundary[i]);
							//is_geodesic_exact = true;
						mesh_data->initial_path_array[connectedVertices[b]][mesh_data->initial_path_array[connectedVertices[b]].size() - 1].is_geodest_exact = is_geodesic_exact;
						
						if(!is_geodesic_exact)
						{
							appendVector(connectedVertices[b],uncut_points);
						}
						float lo = 0.0f;
						geodesic_regression_time += lo;
					}

					if(!is_geodesic_exact)
						mesh_data->distance_to_source_array[connectedVertices[b]] = min_dist;
					//mesh_data->distance_to_source_array[connectedVertices[b]] = curve_length(mesh_data->geodesic_path_array[connectedVertices[b]]);
					//try
					//{
					//	getGeodesicDistance(mesh_data,mesh_data->initial_path_array[connectedVertices[b]],mesh_data->floating_path_array[connectedVertices[b]]);
					//	//next_propagation_boundary.push_back(tmp_boundary[i]);
					//	mesh_data->initial_path_array[connectedVertices[b]][mesh_data->initial_path_array[connectedVertices[b]].size() - 1].is_geodest_exact = true;
					//}
					//catch( int n )
					//{
					//	if(n == 22)
					//	{
					//		uncut_points.push_back(connectedVertices[b]);
					//	}
					//}



					//mesh_data->distance_to_source_array[connectedVertices[b]] = curve_length(mesh_data->geodesic_path_array[connectedVertices[b]]);
				
					//mesh_data->distance_to_source_array[connectedVertices[b]] = min_dist;
					current_wave_head.push_back(connectedVertices[b]);

				}else{
					if(max_length < min_dist){
						max_length  = min_dist;
					}
					bool is_in_next_wave = false;
					for(int i_wave_index = 0; i_wave_index < next_wave.size();++i_wave_index){
						if(next_wave[i_wave_index] == parent_id){
							is_in_next_wave = true;
						}
					}

					if(!is_in_next_wave)
						next_wave.push_back(parent_id);
				}
			}
		}


		bool is_current_ring_geodesic_update_finished = false;
		do
		{
			is_current_ring_geodesic_update_finished = false;
			std::vector<int> tmp;
			for(int a = 0; a < uncut_points.size();a++)
			{
				bool is_geodesic_distance_updated =	getGeodesicDistance(mesh_data,mesh_data->initial_path_array[uncut_points[a]],mesh_data->floating_path_array[uncut_points[a]]);
				mesh_data->initial_path_array[uncut_points[a]][mesh_data->initial_path_array[uncut_points[a]].size() - 1].is_geodest_exact = is_geodesic_distance_updated;
				if(!is_geodesic_distance_updated)
				{
						tmp.push_back(uncut_points[a]);
				}else
				{
					is_current_ring_geodesic_update_finished = true;
				}
			}

			uncut_points = tmp;
		}while(uncut_points.size() && is_current_ring_geodesic_update_finished);

		//current_wave = current_wave_head;
		current_wave.swap(current_wave_head);
		current_wave_head.clear();

		if(current_wave.size() == 0){
			//current_wave = next_wave;
			current_wave.swap(next_wave);
			next_wave.clear();
			if(max_length == max_distance_to_source) break;
			max_distance_to_source = max_length;
			max_length = 0.0f;
		}
		connectedVertices.clear();


	std::cout<<"current wave size: "<<current_wave.size()<<std::endl;
	}while(current_wave.size());

	double t1b = clock();
	geodesic_regression_time = (float)((t1b - t1a)/1000);
	std::cout<<"running time: "<<geodesic_regression_time<<std::endl;
	std::ofstream tfile("time_log.txt");
	tfile<<"running time: "<<geodesic_regression_time<<std::endl;
	tfile.close();
//	stop_timer(t1,true,"all geodesics has been calculated\n",true);

	new_section.clear();
	new_initial_section.clear();

//	std::ofstream allpathfile("arc_length_compare.txt");
////allpathfile<<mesh_data->geodesic_path_array.size()<<" ";
//	for(int i = 1; i < mesh_data->geodesic_path_array.size();++i){
//		//allpathfile<<mesh_data->initial_path_array[i][0]<<" "<<mesh_data->initial_path_array[i][mesh_data->initial_path_array[i].size() - 1]<<std::endl;
//		//if(i == 20454){
//		allpathfile<<i<<" "<<curve_length(mesh_data->geodesic_path_array[i])<<std::endl;
//
//		//for(int a = 0; a < mesh_data->geodesic_path_array[i].size();a++){
//		//	//allpathfile<<mesh_data->geodesic_path_array[i][a].x<<" "<<mesh_data->geodesic_path_array[i][a].y<<" "<<mesh_data->geodesic_path_array[i][a].z<<std::endl;
//		//	//allpathfile<<mesh_data->initial_path_array[i][a]<<std::endl;
//		//}
//
//		//}
//		//Eigen::Vector3f sp,ep;
//
//		//sp[0] = mesh_data->geodesic_path_array[i][0].x;
//		//sp[1] = mesh_data->geodesic_path_array[i][0].y;
//		//sp[2] = mesh_data->geodesic_path_array[i][0].z;
//
//		//int size = mesh_data->geodesic_path_array[i].size()-1;
//		//ep[0] = mesh_data->geodesic_path_array[i][size].x;
//		//ep[1] = mesh_data->geodesic_path_array[i][size].y;
//		//ep[2] = mesh_data->geodesic_path_array[i][size].z;
//		//float arc_length = 10.0f * acos(sp.dot(ep)/(sp.norm()*ep.norm()));
//
//		//allpathfile<<arc_length<<std::endl;
//	}
//	allpathfile<<std::endl;





	//pathfile.close();
}





#endif
