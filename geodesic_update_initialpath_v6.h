#include "geodesic_base_elements.h"
#include "geodesic_mesh_data.h"


//#include <Eigen/Core>
//#include <Eigen/Eigen>
//#include <Eigen/Eigenvalues>
//#include <Eigen/Sparse>

#include <iostream>
#include <fstream>

#include "geodesic_project_utilities.h"
#include "geodesic_update_cut.h"


#define PI 3.14159265354 


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

void updata_path(MeshData * mesh_data, std::vector<initPoint> & initial_path, std::vector<Point> & curvePointArray ){
	//define variables
	Eigen::Vector3f cutPlaneNormal;
	Eigen::Vector3f pointOnCutPlane;

	std::vector<initPoint> projectedPath;
	std::vector<Eigen::Vector3f> projectDirectionArray;
	initial_path[0].flag = -2;
	initial_path[initial_path.size() - 1].flag = -2;

	float tol = 0.0001f;

	Eigen::Vector3f projectDirection;
	int sourceID = initial_path[0].vertexID;
	int destinationID = initial_path[initial_path.size() - 1].vertexID;

	Eigen::Vector3f sourceNormal;
	sourceNormal[0] = mesh_data->getNormal(sourceID)[0];
	sourceNormal[1] = mesh_data->getNormal(sourceID)[1];
	sourceNormal[2] = mesh_data->getNormal(sourceID)[2];
	projectDirectionArray.push_back(sourceNormal);


	initial_path[0].location = initial_path[0].neighbourFacesID;
	initial_path[initial_path.size() - 1].location = initial_path[initial_path.size() - 1].neighbourFacesID;





	initPoint sourcePoint;
	sourcePoint.x = curvePointArray[0].x;
	sourcePoint.y = curvePointArray[0].y;
	sourcePoint.z = curvePointArray[0].z;
	sourcePoint.n_x = mesh_data->getNormal(sourceID)[0];
	sourcePoint.n_y = mesh_data->getNormal(sourceID)[1];
	sourcePoint.n_z = mesh_data->getNormal(sourceID)[2];
	sourcePoint.flag = -2;
	sourcePoint.location = initial_path[0].neighbourFacesID;
	sourcePoint.neighbourFacesID = initial_path[0].neighbourFacesID;
	sourcePoint.vertexID = sourceID;

	projectedPath.push_back(sourcePoint);





	//////////////////////////////////////////////////////////////////
	//// STEP 1, Project floating points on their individual face/////
	//////////////////////////////////////////////////////////////////


	bool ic_cut_valid = true;
	for( int a = 1; a < curvePointArray.size()-1;++a)
	{	//////////////////////////////////////////////////////////////////
		///For each floating point, calculate its Css as project direction
		int success = 0;

		projectDirection = getCurveNormal(curvePointArray[a - 1], curvePointArray[a], curvePointArray[a + 1]);
		Eigen::Vector3f projectDirection2, initialNormal;

		initialNormal[0] = initial_path[a].n_x;
		initialNormal[1] = initial_path[a].n_y;
		initialNormal[2] = initial_path[a].n_z;

		projectDirection2 = projectDirection;
		projectDirection2 = projectDirection2 / projectDirection2.norm();

		initPoint projectPoint;
		if(projectDirection.norm() < std::sqrt(tol) || projectDirection2.dot(initialNormal) < 0.1736 )
		{
			std::vector<int> one_ring_vertices;
			std::vector<int> one_ring_faces;
			one_ring_faces = initial_path[a].neighbourFacesID;
			int * faceVertexID = mesh_data->getFace(one_ring_faces[one_ring_faces.size() - 1]);
			one_ring_faces.pop_back();
			one_ring_vertices.push_back(faceVertexID[0]);
			one_ring_vertices.push_back(faceVertexID[1]);
			one_ring_vertices.push_back(faceVertexID[2]);

			for(int i = 0; i < one_ring_faces.size(); ++i)
			{
				int * faceVertexID = mesh_data->getFace(one_ring_faces[i]);
				std::vector<int> tmp;
				tmp.push_back(faceVertexID[0]);
				tmp.push_back(faceVertexID[1]);
				tmp.push_back(faceVertexID[2]);
				appendVector(tmp, one_ring_vertices);

			}

			Eigen::Vector3f one_ring_vertices_normal;
			one_ring_vertices_normal = getSVDnomralofOnRingFace(mesh_data, one_ring_vertices);
			projectDirection = one_ring_vertices_normal;
		}else
		{
			//projectDirection = projectDirection / projectDirection.norm();
			Eigen::Vector3f v0,v1,v2,v3,v4,v5,v6;
			v0[0] = curvePointArray[a].x -  initial_path[a - 1].x;
			v0[1] = curvePointArray[a].y -  initial_path[a - 1].y;
			v0[2] = curvePointArray[a].z -  initial_path[a - 1].z;

			v1[0] = curvePointArray[a].x -  curvePointArray[a + 1].x;
			v1[1] = curvePointArray[a].y -  curvePointArray[a + 1].y;
			v1[2] = curvePointArray[a].z -  curvePointArray[a + 1].z;

			Eigen::Vector3f new_css;
			new_css = getCurveNormal(initial_path[a - 1], curvePointArray[a], curvePointArray[a + 1]);

			if(new_css.norm() < std::sqrt(tol) || new_css.dot(initialNormal) < 0.1736 )
			{
				std::vector<int> one_ring_vertices;
				std::vector<int> one_ring_faces;
				one_ring_faces = initial_path[a].neighbourFacesID;
				int * faceVertexID = mesh_data->getFace(one_ring_faces[one_ring_faces.size() - 1]);
				one_ring_faces.pop_back();
				one_ring_vertices.push_back(faceVertexID[0]);
				one_ring_vertices.push_back(faceVertexID[1]);
				one_ring_vertices.push_back(faceVertexID[2]);

				for(int i = 0; i < one_ring_faces.size(); ++i)
				{
					int * faceVertexID = mesh_data->getFace(one_ring_faces[i]);
					std::vector<int> tmp;
					tmp.push_back(faceVertexID[0]);
					tmp.push_back(faceVertexID[1]);
					tmp.push_back(faceVertexID[2]);
					appendVector(tmp, one_ring_vertices);

				}

				Eigen::Vector3f one_ring_vertices_normal;
				one_ring_vertices_normal = getSVDnomralofOnRingFace(mesh_data, one_ring_vertices);
				projectDirection = one_ring_vertices_normal;
			}else
			{
				v3 = projectDirection;
				v2 = v0.cross(v1);
				v6 = v2 / v2.norm();
				float dotV = (v2 / v2.norm()).dot(v3);
				v4 = v3 - dotV * v6;
				projectDirection = v4 / v4.norm();
			}
		}






		std::vector<int> one_ring_faces;
		one_ring_faces = initial_path[a].neighbourFacesID;
		std::vector<initPoint> projectPointArray;
		for(int b = 0; b < one_ring_faces.size(); b++)
		{
			Point floatPoint;
			floatPoint = curvePointArray[a];

			Eigen::Vector3f projectedPoint;
			bool is_in_triangle = projectFloatingPointOnMesh(mesh_data,  projectDirection, floatPoint, projectPoint, one_ring_faces[b], tol);
			if(is_in_triangle)
			{
				projectPointArray.push_back(projectPoint);
			}
		}

		if(projectPointArray.size() > 1)
		{	
			float dist = 9999.9f;
			int index = 0;
			initPoint tmpPoint;
			for(int j = 0; j < projectPointArray.size(); j++)
			{
				float dist_c = Distl(projectPointArray[j],initial_path[a]);
				if(dist_c < dist)
				{
					dist = dist_c;
					index = j;
				}
			}
			tmpPoint = projectPointArray[index];
			initial_path[a] = tmpPoint;
		}else if(projectPointArray.size() == 1) 
		{
			initial_path[a] = projectPointArray[0];
		}else
		{
			std::cout<<"";
		}

		if(Distl(initial_path[a],initial_path[a-1]) < tol)
		{
			continue;
		}

		projectDirectionArray.push_back(projectDirection);


		initPoint lastCutPoint;
		//////////////////////////////////////////////////////////////////
		//// STEP 2, link previous point to current projection		 /////
		//////////////////////////////////////////////////////////////////	
		std::vector<initPoint> cutPath;
		if (!(initial_path[a-1].flag == 2 && initial_path[a].flag == 2 && setDiff(initial_path[a-1].location, initial_path[a].location).size() == 0 ))
		{
			if(initial_path[a].isFloating == true )
			{
				if(ic_cut_valid)
				{
					Eigen::Vector3f currentProjectDirection;
					currentProjectDirection[0] = initial_path[a].n_x;
					currentProjectDirection[1] = initial_path[a].n_y;
					currentProjectDirection[2] = initial_path[a].n_z;
					buildCuttingPlane(initial_path[a-1], curvePointArray[a], currentProjectDirection, cutPlaneNormal, pointOnCutPlane);
				}
				else
				{
					Eigen::Vector3f currentProjectDirection;
					currentProjectDirection[0] = initial_path[a].n_x;
					currentProjectDirection[1] = initial_path[a].n_y;
					currentProjectDirection[2] = initial_path[a].n_z;
					buildCuttingPlane(projectedPath[projectedPath.size()-1], curvePointArray[a], currentProjectDirection, cutPlaneNormal, pointOnCutPlane);
				}
			}else 
			{
				if(ic_cut_valid)
				{
					buildCuttingPlane(initial_path[a-1], initial_path[a], curvePointArray[a], cutPlaneNormal, pointOnCutPlane);	
				}else
				{
					buildCuttingPlane(projectedPath[projectedPath.size()-1], initial_path[a], curvePointArray[a], cutPlaneNormal, pointOnCutPlane);	
				}
			}



			std::vector<int> tmp;
			if(projectedPath[projectedPath.size() - 1].flag == 1)
				tmp = setIntersection(projectedPath[projectedPath.size() - 1].neighbourFacesID, initial_path[a].location);
			else if(projectedPath[projectedPath.size() - 1].flag == 0)
				tmp = setIntersection(projectedPath[projectedPath.size() - 1].location, initial_path[a].location);


			//tmp = setIntersection(projectedPath[projectedPath.size() - 1].neighbourFacesID, initial_path[a].location);
			if(tmp.size() == 0)
			{
				tmp = setIntersection(initial_path[a-1].neighbourFacesID, initial_path[a].neighbourFacesID);
				if(tmp.size() == 0)
				{
					for(int i = 0; i < initial_path[a-1].neighbourFacesID.size();i++)
					{
						for(int j = 0; j < initial_path[a].neighbourFacesID.size();j++)
						{	
							if(isAdjacent(mesh_data, initial_path[a-1].neighbourFacesID[i], initial_path[a].neighbourFacesID[j]))
							{
								tmp.push_back( initial_path[a-1].neighbourFacesID[i] );
							}
						}
					}

				}
			}else
			{
				success = 1;
			}

			if(success != 1)
			{

				ic_cut_valid = CutBetweenTwoPoints(mesh_data, projectedPath, initial_path[a], cutPlaneNormal, pointOnCutPlane);
				//lastCutPoint = projectedPath[projectedPath.size() - 1];
				projectedPath.pop_back();

			}
		}
	}




	//////////////////////////////////////////////////////////////////
	//// STEP 2, link previous point to current projection		 /////
	//////////////////////////////////////////////////////////////////	
	std::vector<initPoint> cutPath;
	int success = 0;
	if (!(initial_path[curvePointArray.size() - 2].flag == 2 && initial_path[curvePointArray.size() - 1].flag == 2 && setDiff(initial_path[curvePointArray.size() - 2].location, initial_path[curvePointArray.size() - 1].location).size() == 0 ))
	{



		if(ic_cut_valid)
		{
			Eigen::Vector3f currentProjectDirection;
			currentProjectDirection[0] = initial_path[curvePointArray.size() - 1].n_x;
			currentProjectDirection[1] = initial_path[curvePointArray.size() - 1].n_y;
			currentProjectDirection[2] = initial_path[curvePointArray.size() - 1].n_z;
			buildCuttingPlane(initial_path[curvePointArray.size() - 2], curvePointArray[curvePointArray.size() - 1], currentProjectDirection, cutPlaneNormal, pointOnCutPlane);
		}
		else
		{
			Eigen::Vector3f currentProjectDirection;
			currentProjectDirection[0] = initial_path[curvePointArray.size() - 1].n_x;
			currentProjectDirection[1] = initial_path[curvePointArray.size() - 1].n_y;
			currentProjectDirection[2] = initial_path[curvePointArray.size() - 1].n_z;
			buildCuttingPlane(projectedPath[projectedPath.size()-1], curvePointArray[curvePointArray.size() - 1], currentProjectDirection, cutPlaneNormal, pointOnCutPlane);
		}

		std::vector<int> tmp;
		if(projectedPath[projectedPath.size() - 1].flag == 1)
			tmp = setIntersection(projectedPath[projectedPath.size() - 1].neighbourFacesID, initial_path[curvePointArray.size() - 1].location);
		else if(projectedPath[projectedPath.size() - 1].flag == 0)
			tmp = setIntersection(projectedPath[projectedPath.size() - 1].location, initial_path[curvePointArray.size() - 1].location);


		//tmp = setIntersection(projectedPath[projectedPath.size() - 1].neighbourFacesID, initial_path[a].location);
		if(tmp.size() == 0)
		{
			tmp = setIntersection(initial_path[curvePointArray.size() - 2].neighbourFacesID, initial_path[curvePointArray.size() - 1].neighbourFacesID);
			if(tmp.size() == 0)
			{
				for(int i = 0; i < initial_path[curvePointArray.size() - 2].neighbourFacesID.size();i++)
				{
					for(int j = 0; j < initial_path[curvePointArray.size() - 1].neighbourFacesID.size();j++)
					{	
						if(isAdjacent(mesh_data, initial_path[curvePointArray.size() - 2].neighbourFacesID[i], initial_path[curvePointArray.size() - 1].neighbourFacesID[j]))
						{
							tmp.push_back( initial_path[curvePointArray.size() - 2].neighbourFacesID[i] );
						}
					}
				}

			}
		}else
		{
			success = 1;
		}

		if(success != 1)
		{
			ic_cut_valid = CutBetweenTwoPoints(mesh_data, projectedPath, initial_path[curvePointArray.size() - 1], cutPlaneNormal, pointOnCutPlane);
			//lastCutPoint = projectedPath[projectedPath.size() - 1];
			projectedPath.pop_back();

		}
	}


	Eigen::Vector3f destinationNormal;
	destinationNormal[0] = mesh_data->getNormal(sourceID)[0];
	destinationNormal[1] = mesh_data->getNormal(sourceID)[1];
	destinationNormal[2] = mesh_data->getNormal(sourceID)[2];
	projectDirectionArray.push_back(destinationNormal);



	initPoint destinationPoint;
	destinationPoint.x = curvePointArray[curvePointArray.size() - 1].x;
	destinationPoint.y = curvePointArray[curvePointArray.size() - 1].y;
	destinationPoint.z = curvePointArray[curvePointArray.size() - 1].z;
	destinationPoint.n_x = mesh_data->getNormal(destinationID)[0];
	destinationPoint.n_y = mesh_data->getNormal(destinationID)[1];
	destinationPoint.n_z = mesh_data->getNormal(destinationID)[2];
	destinationPoint.flag = 1;
	destinationPoint.location = initial_path[initial_path.size() - 1].neighbourFacesID;
	destinationPoint.neighbourFacesID = initial_path[initial_path.size() - 1].neighbourFacesID;
	destinationPoint.vertexID = destinationID;

	projectedPath.push_back(destinationPoint);




	mesh_data->projected_path_array[ initial_path[initial_path.size() - 1].vertexID ] = projectedPath;
	//mesh_data->initial_path_array[ initial_path[initial_path.size() - 1].vertexID ] = projectedPath;
	//mesh_data->geodesic_path_array[ initial_path[initial_path.size() - 1].vertexID ].clear();


	//for( int a = 0; a < projectedPath.size();++a)
	//{
	//	Point point;
	//	point.x = projectedPath[a].x;
	//	point.y = projectedPath[a].y;
	//	point.z = projectedPath[a].z;
	//	mesh_data->geodesic_path_array[ initial_path[initial_path.size() - 1].vertexID ].push_back(point);
	//}
}




void checkDuplicatedPoint(MeshData * mesh_data, int index, float tol)
{
	std::vector<initPoint> tmpArray;
	tmpArray.push_back(mesh_data->initial_path_array[ index ][0]);



	for(int i = 1; i < mesh_data->projected_path_array[ index ].size();i++)
	{
		if( Distl(mesh_data->geodesic_path_array[index][i-1],mesh_data->geodesic_path_array[index][i]) < tol )
		{
			continue;
		}
		else
		{
			tmpArray.push_back(mesh_data->initial_path_array[ index ][i]);
		}
	}

	mesh_data->projected_path_array[ index ] = tmpArray;
	mesh_data->initial_path_array[ index ] = tmpArray;
	mesh_data->geodesic_path_array[ index ].clear();


	for( int a = 0; a < tmpArray.size();++a)
	{
		Point point;
		point.x = tmpArray[a].x;
		point.y = tmpArray[a].y;
		point.z = tmpArray[a].z;
		mesh_data->geodesic_path_array[ index ].push_back(point);
	}
}