#include "geodesic_base_elements.h"
#include "geodesic_mesh_data.h"


//#include <Eigen/Core>
//#include <Eigen/Eigen>
//#include <Eigen/Eigenvalues>
//#include <Eigen/Sparse>

#include <iostream>
#include <fstream>

#include "geodesic_project_utilities.h"
#define PI 3.14159265354 


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

void updata_path(MeshData * mesh_data, std::vector<initPoint> & initial_path, std::vector<Point> & curvePointArray ){
	//define variables
	Eigen::Vector3f cutPlaneNormal;
	Eigen::Vector3f pointOnCutPlane;
	
	std::vector<CutPoint> projectedPath;
	std::vector<Eigen::Vector3f> projectDirectionArray;



	float tol = 0.0001f;

	Eigen::Vector3f projectDirection;
	int sourceID = initial_path[0].vertexID;
	int destinationID = initial_path[initial_path.size() - 1].vertexID;
	
	Eigen::Vector3f sourceNormal;
	sourceNormal[0] = mesh_data->getNormal(sourceID)[0];
	sourceNormal[1] = mesh_data->getNormal(sourceID)[1];
	sourceNormal[2] = mesh_data->getNormal(sourceID)[2];
	projectDirectionArray.push_back(sourceNormal);


	initial_path[0].location = initial_path[0].neighbourFacesID;
	initial_path[initial_path.size() - 1].location = initial_path[initial_path.size() - 1].neighbourFacesID;

	//////////////////////////////////////////////////////////////////
	//// STEP 1, Project floating points on their individual face/////
	//////////////////////////////////////////////////////////////////

	 

	for( int a = 1; a < curvePointArray.size()-1;++a)
	{	//////////////////////////////////////////////////////////////////
		///For each floating point, calculate its Css as project direction
		projectDirection = getCurveNormal(curvePointArray[a - 1], curvePointArray[a], curvePointArray[a + 1]);
		Eigen::Vector3f projectDirection2, initialNormal;
		
		initialNormal[0] = initial_path[a].n_x;
		initialNormal[1] = initial_path[a].n_y;
		initialNormal[2] = initial_path[a].n_z;

		projectDirection2 = projectDirection;
		projectDirection2 = projectDirection2 / projectDirection2.norm();

		initPoint projectPoint;
		if(projectDirection.norm() < std::sqrt(tol) || projectDirection2.dot(initialNormal) < 0.1736 )
		{
			std::vector<int> one_ring_vertices;
			std::vector<int> one_ring_faces;
			one_ring_faces = initial_path[a].neighbourFacesID;
			int * faceVertexID = mesh_data->getFace(one_ring_faces[one_ring_faces.size() - 1]);
			one_ring_faces.pop_back();
			one_ring_vertices.push_back(faceVertexID[0]);
			one_ring_vertices.push_back(faceVertexID[1]);
			one_ring_vertices.push_back(faceVertexID[2]);

			for(int i = 0; i < one_ring_faces.size(); ++i)
			{
				int * faceVertexID = mesh_data->getFace(one_ring_faces[i]);
				std::vector<int> tmp;
				tmp.push_back(faceVertexID[0]);
				tmp.push_back(faceVertexID[1]);
				tmp.push_back(faceVertexID[2]);
				appendVector(tmp, one_ring_vertices);

			}
	
			Eigen::Vector3f one_ring_vertices_normal;
			one_ring_vertices_normal = getSVDnomralofOnRingFace(mesh_data, one_ring_vertices);
			projectDirection = one_ring_vertices_normal;
		}else
		{
			projectDirection = projectDirection / projectDirection.norm();
		}

		std::vector<int> one_ring_faces;
		one_ring_faces = initial_path[a].neighbourFacesID;
		std::vector<initPoint> projectPointArray;
		for(int b = 0; b < one_ring_faces.size(); b++)
		{
			Point floatPoint;
			floatPoint = curvePointArray[a];

			Eigen::Vector3f projectedPoint;
			bool is_in_triangle = projectFloatingPointOnMesh(mesh_data,  projectDirection, floatPoint, projectPoint, one_ring_faces[b], tol);
			if(is_in_triangle)
			{
				projectPointArray.push_back(projectPoint);
			}
		}

		if(projectPointArray.size() > 1)
		{	
			float dist = 9999.9f;
			int index = 0;
			initPoint tmpPoint;
			for(int j = 0; j < projectPointArray.size(); j++)
			{
				float dist_c = Distl(projectPointArray[j],initial_path[a]);
				if(dist_c < dist)
				{
					dist = dist_c;
					index = j;
				}
			}
			tmpPoint = projectPointArray[index];
			initial_path[a] = tmpPoint;
		}else if(projectPointArray.size() == 1) 
		{
			initial_path[a] = projectPointArray[0];
		}else
		{
			std::cout<<" ";
		}
		
		curvePointArray[a].x = initial_path[a].x;
		curvePointArray[a].y = initial_path[a].y;
		curvePointArray[a].z = initial_path[a].z;

		projectDirectionArray.push_back(projectDirection);
	}

	
	//for(int i = 1; i < projectPointArray.size() - 1; i++)
	//{
	//	if(projectPointArray[i].size() > 1)
	//	{	
	//		float dist = 9999.9f;
	//		int index = 0;
	//		for(int j = 0; j < projectPointArray[i].size(); j++)
	//		{
	//			float dist_c = Distl(projectPointArray[i][j],initial_path[i]);
	//			if(dist_c < dist)
	//			{
	//				dist = dist_c;
	//				index = j;
	//			}
	//		}
	//		std::vector<initPoint> tmpPoint;
	//		tmpPoint.push_back(projectPointArray[i][index]);
	//		projectPointArray[i] = tmpPoint;
	//		tmpPoint.clear();
	//	}
	//	initial_path[i] = projectPointArray[i][0];
	//	curvePointArray[i].x = initial_path[i].x;
	//	curvePointArray[i].y = initial_path[i].y;
	//	curvePointArray[i].z = initial_path[i].z;
	//}



	Eigen::Vector3f destinationNormal;
	destinationNormal[0] = mesh_data->getNormal(sourceID)[0];
	destinationNormal[1] = mesh_data->getNormal(sourceID)[1];
	destinationNormal[2] = mesh_data->getNormal(sourceID)[2];
	projectDirectionArray.push_back(destinationNormal);


/*

	//////////////////////////////////////////////////////////////////
	//// STEP 2, trace path on mesh, start from source			 /////
	//////////////////////////////////////////////////////////////////

	std::vector<int> temp;
	CutPoint sourcePoint;
	sourcePoint.x = curvePointArray[0].x;
	sourcePoint.y = curvePointArray[0].y;
	sourcePoint.z = curvePointArray[0].z;
	sourcePoint.n_x = mesh_data->getNormal(sourceID)[0];
	sourcePoint.n_y = mesh_data->getNormal(sourceID)[1];
	sourcePoint.n_z = mesh_data->getNormal(sourceID)[2];
	sourcePoint.location = initial_path[0].neighbourFacesID;
	sourcePoint.neighbourFaces = initial_path[0].neighbourFacesID;
	sourcePoint.vertexID = sourceID;

	std::vector<CutPoint> cutPath0;
	cutPath0.push_back(sourcePoint);
	std::vector<CutPoint> cutPath1;
	cutPath1.push_back(sourcePoint);

	int initial_path_index = 1;
	while(1)
	{
		temp = setIntersection(mesh_data->one_ring_face_array[sourceID], initial_path[initial_path_index].location );
		if(temp.size())
		{
			initial_path_index++;
		}else
		{
			break;
		}
	}

	buildCuttingPlane(initial_path[initial_path_index - 1], curvePointArray[initial_path_index], projectDirection, cutPlaneNormal, pointOnCutPlane);

	std::vector<int> sharedFace;
	for(int a = 0; a < mesh_data->one_ring_face_array[sourceID].size(); ++a)
	{
		int currentFaceID = mesh_data->one_ring_face_array[sourceID][a];
		CutPoint cutPoint;

		bool is_cut = getCutPoint( sourceID, mesh_data , pointOnCutPlane, cutPlaneNormal,  currentFaceID, cutPoint);
		if(is_cut)
		{
			if(sharedFace.size() && sharedFace[0] == mesh_data->one_ring_face_array[sourceID][a])
				continue;
			std::vector<int> currentFace;
			currentFace.push_back(mesh_data->one_ring_face_array[sourceID][a]);
			sharedFace = setDiff(cutPoint.location, currentFace);


			if(cutPath0.size() == 1)
				cutPath0.push_back(cutPoint);
			else
				cutPath1.push_back(cutPoint);
		}
	}

	// 2 both direction, edgeFlag != 0 path0 direction, edgeFlag != 1 path1 direction,
	int edgeFlag = 0;


	bool is_cutPoint_on_edge = false;
	if(cutPath0.size() == 1 )
	{
		edgeFlag = 0;

	}else if(cutPath1.size() == 1 )
	{
		edgeFlag = 1;
	}else
	{
		edgeFlag = 2;
	}

	//int cutPathIndex = 1;
	while(1)
	{
		if( edgeFlag != 0)
		{
			std::vector<int> cutFaceRange;
			std::vector<int> tmp1;
			CutPoint cutPoint;

			if(cutPath0[cutPath0.size() - 1].vertexID > -1)
			{

				cutFaceRange = setDiff(cutPath0[cutPath0.size() - 1].neighbourFaces, cutPath0[cutPath0.size() - 1].location);
				tmp1 = setIntersection(cutFaceRange, initial_path[initial_path_index].location);
				if(tmp1.size())
				{
					projectedPath = cutPath0;
					break;
				}
			
				buildCuttingPlane(cutPath0[cutPath0.size() - 1], curvePointArray[initial_path_index], projectDirection, cutPlaneNormal, pointOnCutPlane);
				bool is_cut = false;
				for(int c = 0; c < cutFaceRange.size(); ++c)
				{
					is_cut = getCutPoint( cutPath0[cutPath0.size() - 1].vertexID, mesh_data , pointOnCutPlane, cutPlaneNormal,  cutFaceRange[c], cutPoint);
					if(is_cut)
					{
						cutPath0.push_back(cutPoint);

						break;
					}
				}

				if(!is_cut){
					edgeFlag = 0;
				}
			}
			else
			{
				tmp1 = setIntersection(cutPath0[cutPath0.size() - 1].location, initial_path[initial_path_index].location);
				if(tmp1.size())
				{
					projectedPath = cutPath0;
					break;
				}

				cutFaceRange = setDiff(cutPath0[cutPath0.size() - 1].location, cutPath0[cutPath0.size() - 1].cutFaceIDs);
				buildCuttingPlane(cutPath0[cutPath0.size() - 1], curvePointArray[initial_path_index], projectDirection, cutPlaneNormal, pointOnCutPlane);
				bool is_cut = false;
				for(int c = 0; c < cutFaceRange.size(); ++c)
				{
					is_cut = getCutPoint( cutPath0[cutPath0.size() - 1].cutEdge, mesh_data , pointOnCutPlane, cutPlaneNormal,  cutFaceRange[c], cutPoint);
					if(is_cut)
					{
						cutPath0.push_back(cutPoint);

						break;
					}
				}

				if(!is_cut){
					edgeFlag = 0;
				}			
			}

			//if(tmp1.size())
			//{
			//projectedPath = cutPath0;
			//break;
			//}
		}
		/////////////////////////////////////////////////////////////
		if(edgeFlag != 1)
		{
			std::vector<int> cutFaceRange;
			std::vector<int> tmp2;
			CutPoint cutPoint;

			if(cutPath1[cutPath1.size() - 1].vertexID > -1)
			{

				cutFaceRange = setDiff(cutPath1[cutPath1.size() - 1].neighbourFaces, cutPath1[cutPath1.size() - 1].location);
				tmp2 = setIntersection(cutFaceRange, initial_path[initial_path_index].location);
				if(tmp2.size())
				{
					projectedPath = cutPath1;
					break;
				}

				buildCuttingPlane(cutPath1[cutPath1.size() - 1], curvePointArray[initial_path_index], projectDirection, cutPlaneNormal, pointOnCutPlane);
				bool is_cut = false;
				for(int c = 0; c < cutFaceRange.size(); ++c)
				{
					is_cut = getCutPoint( cutPath1[cutPath1.size() - 1].vertexID, mesh_data , pointOnCutPlane, cutPlaneNormal,  cutFaceRange[c], cutPoint);
					if(is_cut)
					{
						cutPath1.push_back(cutPoint);
	
						break;
					}
				}

				if(!is_cut){
					edgeFlag = 1;
				}
			}
			else
			{
				tmp2 = setIntersection(cutPath1[cutPath1.size() - 1].location, initial_path[initial_path_index].location);
				if(tmp2.size())
				{
					projectedPath = cutPath1;
					break;
				}
	
				cutFaceRange = setDiff(cutPath1[cutPath1.size() - 1].location, cutPath1[cutPath1.size() - 1].cutFaceIDs);
				buildCuttingPlane(cutPath1[cutPath1.size() - 1], curvePointArray[initial_path_index], projectDirection, cutPlaneNormal, pointOnCutPlane);
				bool is_cut = false;
				for(int c = 0; c < cutFaceRange.size(); ++c)
				{
					is_cut = getCutPoint( cutPath1[cutPath1.size() - 1].cutEdge, mesh_data , pointOnCutPlane, cutPlaneNormal,  cutFaceRange[c], cutPoint);
					if(is_cut)
					{
						cutPath1.push_back(cutPoint);

						break;
					}
				}

				if(!is_cut){
					edgeFlag = 0;
				}			
			}
			//if(tmp2.size())
			//{
			//	projectedPath = cutPath1;
			//	break;
			//}
		}
	}


	//////////////////////////////////////////////////////////////////
	//// STEP 3, continue trace									 /////
	//////////////////////////////////////////////////////////////////

	//int initial_path_index = 1;


	while(1)
	{
		std::vector<int> cutFaceRange;
		std::vector<int> tmp1;
		CutPoint cutPoint;
		bool is_reach_destination = false;

		do
		{
			temp = setIntersection(projectedPath[projectedPath.size() - 1].neighbourFaces, initial_path[initial_path_index].location );
			
			if(initial_path_index == initial_path.size() - 1)
			{
				is_reach_destination = true;
				break;
			}
	

			if( temp.size() )
			{
				initial_path_index++;				
			}else
			{
				break;
			}
		}while(initial_path_index < initial_path.size() );

		


		projectDirection = projectDirectionArray[initial_path_index];
		buildCuttingPlane(projectedPath[projectedPath.size() - 1], curvePointArray[initial_path_index], projectDirection, cutPlaneNormal, pointOnCutPlane);




		if(projectedPath[projectedPath.size() - 1].vertexID > -1)
		{
			// previous cutPoint is on vertex
			cutFaceRange = setDiff(projectedPath[projectedPath.size() - 1].neighbourFaces, projectedPath[projectedPath.size() - 1].location);
			tmp1 = setIntersection(cutFaceRange, initial_path[initial_path.size() - 1].location);
			//buildCuttingPlane(projectedPath[projectedPath.size() - 1], curvePointArray[initial_path_index], projectDirection, cutPlaneNormal, pointOnCutPlane);
			bool is_cut = false;
			for(int c = 0; c < cutFaceRange.size(); ++c)
			{
				is_cut = getCutPoint( projectedPath[projectedPath.size() - 1].vertexID, mesh_data , pointOnCutPlane, cutPlaneNormal,  cutFaceRange[c], cutPoint);
				if(is_cut)
				{
					projectedPath.push_back(cutPoint);

					break;
				}
			}

		}
		else
		{
			// previous cutPoint is on edge
			tmp1 = setIntersection(projectedPath[projectedPath.size() - 1].location, initial_path[initial_path.size() - 1].location);
			cutFaceRange = setDiff(projectedPath[projectedPath.size() - 1].location, projectedPath[projectedPath.size() - 1].cutFaceIDs);
			//buildCuttingPlane(projectedPath[projectedPath.size() - 1], curvePointArray[initial_path_index], projectDirection, cutPlaneNormal, pointOnCutPlane);
			bool is_cut = false;
			for(int c = 0; c < cutFaceRange.size(); ++c)
			{
				is_cut = getCutPoint( projectedPath[projectedPath.size() - 1].cutEdge, mesh_data , pointOnCutPlane, cutPlaneNormal,  cutFaceRange[c], cutPoint);
				if(is_cut)
				{
					projectedPath.push_back(cutPoint);

					break;
				}
			}

		
		}


		//if( is_reach_destination )
		//{
		//	projectedPath.pop_back();
		//	CutPoint destPoint;
		//	destPoint.x = curvePointArray[curvePointArray.size() - 1].x;
		//	destPoint.y = curvePointArray[curvePointArray.size() - 1].y;
		//	destPoint.z = curvePointArray[curvePointArray.size() - 1].z;
		//	destPoint.n_x = mesh_data->getNormal(destinationID)[0];
		//	destPoint.n_y = mesh_data->getNormal(destinationID)[1];
		//	destPoint.n_z = mesh_data->getNormal(destinationID)[2];
		//	destPoint.location = mesh_data->one_ring_face_array[destinationID];
		//	destPoint.neighbourFaces = mesh_data->one_ring_face_array[destinationID];
		//	destPoint.vertexID = destinationID;
		//	projectedPath.push_back(destPoint);
		//	break;
		//}

		if(tmp1.size())
		{
			projectedPath.pop_back();
			CutPoint destPoint;
			destPoint.x = curvePointArray[curvePointArray.size() - 1].x;
			destPoint.y = curvePointArray[curvePointArray.size() - 1].y;
			destPoint.z = curvePointArray[curvePointArray.size() - 1].z;
			destPoint.n_x = mesh_data->getNormal(destinationID)[0];
			destPoint.n_y = mesh_data->getNormal(destinationID)[1];
			destPoint.n_z = mesh_data->getNormal(destinationID)[2];
			destPoint.location = mesh_data->one_ring_face_array[destinationID];
			destPoint.neighbourFaces = mesh_data->one_ring_face_array[destinationID];
			destPoint.vertexID = destinationID;
			projectedPath.push_back(destPoint);
			break;
		}
	}
*/
//	mesh_data->projected_path_array[ destinationID ] = projectedPath;
//	updataPathInfo(mesh_data,  projectedPath , curvePointArray, initial_path );


}