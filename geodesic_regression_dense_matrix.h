#ifndef geodesic_regression_dense_matrix_h
#define geodesic_regression_dense_matrix_h






#include "geodesic_base_elements.h"
#include "geodesic_mesh_data.h"


#include <Eigen/Core>
#include <Eigen/Eigen>
#include <Eigen/Eigenvalues>
#include <Eigen/Sparse>

#include <iostream>
#include <fstream>



#define PI 3.14159265354 




float Dist(Point point0, Point point1){

	 return (point0.x - point1.x) * (point0.x - point1.x) + (point0.y - point1.y) * (point0.y - point1.y) + (point0.z - point1.z) * (point0.z - point1.z);
	 
}





void geodesic(MeshData * mesh_data, std::vector<int> initial_path, std::vector<Point> & curvePointArray ,float regression_ratio){
	
	int dim = 3;
	int k = 1;
	//ANNpoint queryPoint;
	//queryPoint = annAllocPt(dim);


	//ofstream pfile("calculation procedure.txt");


	//int number_point_in_range = 0;

	//ANNidxArray nnIdx; // near neighbor indices
	//ANNdistArray dists; // near neighbor distances

	//float search_radius = 0.0005f;
	//ANNdist sqRad = sqrt(search_radius);


	//ofstream regionFile("result.txt");

	float u = regression_ratio;
	int sparse_matrix_size = (int)curvePointArray.size();

	int index_offset = 0;

	Eigen::MatrixXf dMat_C_3m_1;
	dMat_C_3m_1.resize(sparse_matrix_size * 3,1);
	dMat_C_3m_1.setZero();

	
	Eigen::MatrixXf dMat_Xt_3m_1;
	dMat_Xt_3m_1.resize(sparse_matrix_size * 3 ,1);
	dMat_Xt_3m_1.setZero();

	Eigen::MatrixXf dMat_N_3m_m;
	dMat_N_3m_m.resize(sparse_matrix_size * 3,sparse_matrix_size);
	dMat_N_3m_m.setZero();

	Eigen::MatrixXf dMat_Nt_m_3m;
	dMat_Nt_m_3m.resize(sparse_matrix_size, sparse_matrix_size * 3 );
	dMat_Nt_m_3m.setZero();

	Eigen::MatrixXf dMat_Nstar_3m_3m;
	dMat_Nstar_3m_3m.resize(sparse_matrix_size * 3,sparse_matrix_size * 3);
	dMat_Nstar_3m_3m.setZero();

	Eigen::MatrixXf dMat_K_3m_3m;
	dMat_K_3m_3m.resize(sparse_matrix_size * 3,sparse_matrix_size * 3);
	dMat_K_3m_3m.setZero();

	Eigen::MatrixXf dMat_I_mat_3m_3m;
	dMat_I_mat_3m_3m.resize(sparse_matrix_size * 3 , sparse_matrix_size * 3);
	dMat_I_mat_3m_3m.setZero();


	for(int i = 0 ; i < sparse_matrix_size * 3; ++i){

		dMat_I_mat_3m_3m(i,i) = 1.0f;
	}




	for(int i = 0;i< sparse_matrix_size ;++i ){


		//queryPoint[0] = curvePointArray[i ].x;
		//queryPoint[1] = curvePointArray[i ].y;
		//queryPoint[2] = curvePointArray[i ].z;


		dMat_Xt_3m_1(i*3 ,0) = curvePointArray[i ].x;
		dMat_Xt_3m_1(i*3 + 1,0) = curvePointArray[i ].y;
		dMat_Xt_3m_1(i*3 + 2,0) = curvePointArray[i ].z;

		//int num_points = meshKdTree->annkFRSearch(queryPoint,sqRad,0,NULL,NULL,0.0);
		int num_points = 1;

		//nnIdx = new ANNidx[num_points];
		//dists = new ANNdist[num_points];

		//////////////////////////////////////////////////////////////////
		// KD-Tree searching for closest point
		//mesh_data->meshKdTree->annkSearch(queryPoint,num_points,nnIdx,dists,0.00000f);
		//get one ring neighbor vertex of current curve point to compute the mean point
		//vector<int> one_ring_vertex;
		//one_ring_vertex = mesh_data->one_ring_vertex_array[nnIdx[0]];

		//vector<int> one_ring_faces;
		//one_ring_faces = mesh_data->one_ring_face_array[nnIdx[0]];



		//////////////////////////////////////////////////////////////////
		//// one-ring searching for closest point

		std::vector<int> one_ring_v;
		std::vector<int> one_ring_f;

		


		one_ring_v = mesh_data->one_ring_vertex_array[initial_path[i + index_offset]];
		one_ring_v.push_back(initial_path[i + index_offset]);

		int closest_point_id = 0;
		float min_dist = 9999.9f;
		for(int x = 0;x < one_ring_v.size();++x){
			Point point;

			point.x = mesh_data->getVertex(one_ring_v[x])[0];
			point.y = mesh_data->getVertex(one_ring_v[x])[1];
			point.z = mesh_data->getVertex(one_ring_v[x])[2];
				

			float dist = Dist(point,curvePointArray[i + index_offset] );	
			if(dist < min_dist){
				min_dist = dist;
				closest_point_id = one_ring_v[x];
			}
		}
		one_ring_v.clear();

		std::vector<int> one_ring_vertex;
		one_ring_vertex = mesh_data->one_ring_vertex_array[closest_point_id];
		
		std::vector<int> one_ring_faces;
		one_ring_faces = mesh_data->one_ring_face_array[closest_point_id];
	




		float x = 0, y = 0,z = 0;
		//float center_point[3];				
		for(int a = 0; a < one_ring_vertex.size() ; ++a){
			x += mesh_data->getVertex(one_ring_vertex[a])[0];
			y += mesh_data->getVertex(one_ring_vertex[a])[1];
			z += mesh_data->getVertex(one_ring_vertex[a])[2];
		}	

		dMat_C_3m_1(i*3,0) = x /  one_ring_vertex.size();
		dMat_C_3m_1(i*3 + 1,0) = y /  one_ring_vertex.size();
		dMat_C_3m_1(i*3 + 2,0) = z /  one_ring_vertex.size();




		// get one-ring neighbor face normal of current curve point


		//delete []nnIdx;
		//delete []dists;

		//perform SVD on one ring face normals
		int row =  (int)one_ring_faces.size();
		//int row =  (int)one_ring_vertex.size();
		Eigen::MatrixX3f eigenPointMatrix;
		eigenPointMatrix.resize(row,3);
		Eigen::MatrixX3f transposeMatrix;
		transposeMatrix.resize(row,3);
		Eigen::VectorXf meanVector;

		//for(int a = 0; a < one_ring_vertex.size() ; ++a){
		//	eigenPointMatrix(a,0) = mesh_data->getVertex(one_ring_vertex[a])[0];
		//	eigenPointMatrix(a,1) = mesh_data->getVertex(one_ring_vertex[a])[1];
		//	eigenPointMatrix(a,2) = mesh_data->getVertex(one_ring_vertex[a])[2];
		//}	

		for(int a = 0; a < one_ring_faces.size();++a ){

			eigenPointMatrix(a,0) = mesh_data->getVertexNormal(one_ring_faces[a])[0];
			eigenPointMatrix(a,1) = mesh_data->getVertexNormal(one_ring_faces[a])[1];
			eigenPointMatrix(a,2) = mesh_data->getVertexNormal(one_ring_faces[a])[2];	
		}


		float mean = 0.0f;
		for (int k = 0; k < 3; ++k)
		{
			//compute mean
			mean = (eigenPointMatrix.col(k).sum())/row;
			// create a vector with constant value = mean
			meanVector  = Eigen::VectorXf::Constant(row,mean); 
			eigenPointMatrix.col(k) -= meanVector;
		}


		Eigen::MatrixXf covariance = ((float)1/(row-1)) * eigenPointMatrix.transpose()* eigenPointMatrix;

		Eigen::EigenSolver<Eigen::MatrixXf> m_solve(covariance);

		Eigen::VectorXf eigenvalues = m_solve.eigenvalues().real();
		Eigen::MatrixXf eignevectors = m_solve.eigenvectors().real();

		Eigen::Vector3f vector(0.0,0.0,0.0);


		if(eigenvalues(0) < eigenvalues(1))
		{
			if (eigenvalues(0) < eigenvalues(2))
			{
				vector[0] = eignevectors(0,0);
				vector[1] = eignevectors(1,0);
				vector[2] = eignevectors(2,0);
			}
			else
			{
				vector[0] = eignevectors(0,2);
				vector[1] = eignevectors(1,2);
				vector[2] = eignevectors(2,2);
			}
		}
		else{
			if(eigenvalues(1) < eigenvalues(2))
			{
				vector[0] = eignevectors(0,1);
				vector[1] = eignevectors(1,1);
				vector[2] = eignevectors(2,1);
			}else{
				vector[0] = eignevectors(0,2);
				vector[1] = eignevectors(1,2);
				vector[2] = eignevectors(2,2);
			}
		}

		vector.normalized();







		dMat_N_3m_m(i*3,i) = vector[0];
		dMat_N_3m_m(i*3+1,i) = vector[1];
		dMat_N_3m_m(i*3+2,i) = vector[2];

		dMat_Nt_m_3m(i,i*3 ) = vector[0];
		dMat_Nt_m_3m(i,i*3+1) = vector[1];
		dMat_Nt_m_3m(i,i*3+2) = vector[2];

		if(i > 0 && i < sparse_matrix_size - 1){
			dMat_K_3m_3m(i * 3 , (i - 1) * 3 ) = 1;
			dMat_K_3m_3m(i * 3 + 1 , (i - 1) * 3 +1) = 1;
			dMat_K_3m_3m(i * 3 + 2 , (i - 1) * 3 +2 ) = 1;

			dMat_K_3m_3m(i * 3 , i * 3 ) = -2;
			dMat_K_3m_3m(i * 3 + 1 , i * 3 +1) = -2;
			dMat_K_3m_3m(i * 3 + 2 , i * 3 +2 ) = -2;
   

			dMat_K_3m_3m(i * 3 , (i + 1) * 3 ) = 1;
			dMat_K_3m_3m(i * 3 + 1 , (i + 1) * 3 +1) = 1;
			dMat_K_3m_3m(i * 3 + 2 , (i + 1) * 3 +2 ) = 1;

		}


	}

	dMat_Nstar_3m_3m = dMat_N_3m_m * dMat_Nt_m_3m;

	//stop_timer(t2,true,"assign value end: ",true);

	//////////////////////////////////////////
	/// output all the matrix for debug: 


	//pfile<<"matrix Xt_3m_1 : "<<endl;
	//for(int i = 0;i<sparse_matrix_size * 3;i++ ){

	//	float value = 0;
	//	value = dMat_Xt_3m_1(i,0);
	//	pfile<<value<<" ";
	//	pfile<<endl;
	//}




	//pfile<<"matrix C_3m_1 : "<<endl;
	//for(int i = 0;i<sparse_matrix_size * 3;i++ ){
	//	float value = 0;
	//	value = dMat_C_3m_1(i,0);
	//	pfile<<value<<" ";
	//	pfile<<endl;
	//}




	//pfile<<"matrix K_3m_3m : "<<endl;
	//for(int i = 0;i<sparse_matrix_size * 3;i++ ){
	//	for(int j = 0; j < sparse_matrix_size * 3;j++){
	//		float value = 0;
	//		value = dMat_K_3m_3m(i,j);
	//		pfile<<value<<" ";
	//	}
	//	pfile<<endl;
	//}



	//pfile<<"matrix N : "<<endl;
	//for(int i = 0;i<sparse_matrix_size*3;i++ ){
	//	for(int j = 0; j < sparse_matrix_size;j++){
	//		float value = 0;
	//		value = dMat_N_3m_m(i,j);
	//		pfile<<value<<" ";
	//	}
	//	pfile<<endl;
	//}



	//pfile<<"matrix Nt : "<<endl;
	//for(int i = 0;i<sparse_matrix_size;i++ ){
	//	for(int j = 0; j < sparse_matrix_size * 3;j++){
	//		float value = 0;
	//		value = dMat_Nt_m_3m(i,j);
	//		pfile<<value<<" ";
	//	}
	//	pfile<<endl;
	//}


	//pfile<<"matrix I_mat_3m_3m : "<<endl;
	//for(int i = 0;i<sparse_matrix_size * 3;i++ ){
	//	for(int j = 0; j < sparse_matrix_size * 3;j++){
	//		float value = 0;
	//		value = dMat_I_mat_3m_3m(i,j);
	//		pfile<<value<<" ";
	//	}
	//	pfile<<endl;
	//}

	//

	//



	//pfile<<"Nstar_3m_3m"<<endl;
	//for(int i = 0;i<sparse_matrix_size*3;i++ ){
	//	for(int j = 0; j < sparse_matrix_size*3;j++){
	//		float value = 0;
	//		value = dMat_Nstar_3m_3m(i,j);
	//		pfile<<value<<" ";
	//	}
	//	pfile<<endl;
	//}

	////////////////////////////////////////////////////////////////////////////////
	//Timer t2;

	//start_timer(t2,true,"matrix calculation start: ",true);

	Eigen::MatrixXf sub_mat,inv_mat,dMat_Xt_1_m_3,C;



	//////////sub_mat.resize(sparse_matrix_size * 3 , sparse_matrix_size * 3);
	////////////inv_mat.resize(sparse_matrix_size * 3 , sparse_matrix_size * 3);
	//////////dMat_Xt_1_m_3.resize(sparse_matrix_size * 3,1);
	//////////sub_mat = dMat_I_mat_3m_3m +  u * (dMat_Nstar_3m_3m - dMat_K_3m_3m + dMat_Nstar_3m_3m * dMat_K_3m_3m);

	////////////FullPivLU<MatrixXd> lu(sub_mat);
	////////////inv_mat = lu.inverse();
	//////////
	////////////dMat_Xt_1_m_3 = inv_mat * ( dMat_Xt_3m_1 + u * dMat_Nstar_3m_3m * dMat_C_3m_1 );

	//////////Eigen::MatrixXf B;
	//////////B = dMat_Xt_3m_1 + u * dMat_Nstar_3m_3m * dMat_C_3m_1;





///////////////////////////////////////////////////////
///////////////////////////////////////////////////////


	u = 500.0;
	
	sub_mat.resize(sparse_matrix_size * 3 , sparse_matrix_size * 3);
	//inv_mat.resize(sparse_matrix_size * 3 , sparse_matrix_size * 3);
	dMat_Xt_1_m_3.resize(sparse_matrix_size * 3,1);
	//sub_mat = dMat_I_mat_3m_3m +  u * (dMat_Nstar_3m_3m - dMat_K_3m_3m + dMat_Nstar_3m_3m * dMat_K_3m_3m);
	sub_mat = dMat_I_mat_3m_3m +  u * ( -1 * dMat_K_3m_3m + dMat_Nstar_3m_3m * dMat_K_3m_3m);
	Eigen::FullPivLU<Eigen::MatrixXf> lu(sub_mat);
	inv_mat = lu.inverse();
	
	
	//dMat_Xt_1_m_3 = inv_mat * ( dMat_Xt_3m_1 + u * dMat_Nstar_3m_3m * dMat_C_3m_1 );

	Eigen::MatrixXf B;
	B.resize(sparse_matrix_size * 3 ,1);
	B.setZero();

	//B = dMat_Xt_3m_1 + u * dMat_Nstar_3m_3m * dMat_C_3m_1;
	B(0,0) = dMat_Xt_3m_1(0,0);
	B(1,0) = dMat_Xt_3m_1(1,0);
	B(2,0) = dMat_Xt_3m_1(2,0);
	B(sparse_matrix_size * 3 - 3,0) = dMat_Xt_3m_1(sparse_matrix_size * 3 - 3,0);
	B(sparse_matrix_size * 3 - 2,0) = dMat_Xt_3m_1(sparse_matrix_size * 3 - 2,0);
	B(sparse_matrix_size * 3 - 1,0) = dMat_Xt_3m_1(sparse_matrix_size * 3 - 1,0);


	//B = dMat_Xt_3m_1;

	dMat_Xt_1_m_3 = (inv_mat - dMat_I_mat_3m_3m).lu().solve(B);
	//dMat_Xt_1_m_3 = sub_mat.lu().solve(B);

	std::ofstream pfile("calculation procedure.txt");
	pfile<<"inv_mat : "<<std::endl;
	for(int i = 0;i<sparse_matrix_size * 3;i++ ){
		for(int j = 0; j < sparse_matrix_size * 3;j++){
			float value = 0;
			value = inv_mat(i,j);
			pfile<<value<<" ";
		}
		pfile<<std::endl;
	}

	pfile<<"B : "<<std::endl;
	for(int i = 0;i<sparse_matrix_size * 3;i++ ){
			float value = 0;
			value = B(i,0);
			pfile<<value<<" ";
		pfile<<std::endl;
	}


	pfile<<"pt+1: "<<std::endl;

	for(int i = 0;i<sparse_matrix_size * 3;i++ ){
		float value = 0;
		value = dMat_Xt_1_m_3(i,0);
		pfile<<value<<" ";
		pfile<<std::endl;
	}
	pfile<<std::endl;
	pfile<<std::endl;

	//}
	pfile.close();

///////////////////////////////////////////////////////
	///////////////////////////////////////////////////////










///////////////////////////// fullPivLu decomposition :

	//dMat_Xt_1_m_3 = sub_mat.fullPivLu().solve(B);

///////////////////////////// partial Lu decomposition :

//	dMat_Xt_1_m_3 = sub_mat.lu().solve(B);

///////////////////////////// LDL^T Cholesky decomposition :

	//dMat_Xt_1_m_3 = sub_mat.ldlt().solve(B);

///////////////////////////// col QR decomposition with column pivoting:
	
	//dMat_Xt_1_m_3 = sub_mat.colPivHouseholderQr().solve(B);


///////////////////////////// full QR decomposition with column pivoting:

	//dMat_Xt_1_m_3 = sub_mat.fullPivHouseholderQr().solve(B);



		//std::cout<<curvePointArray.size()<<" "<<sparse_matrix_size<<std::endl;

	for(int i = 1; i < sparse_matrix_size;++i){

		curvePointArray[i].x = dMat_Xt_1_m_3(i*3,0);
		curvePointArray[i].y = dMat_Xt_1_m_3(i*3 + 1,0);
		curvePointArray[i].z = dMat_Xt_1_m_3(i*3 + 2,0);
	}

	//pfile.close();

}

void geodesic2(MeshData * mesh_data, std::vector<int> initial_path, std::vector<Point> & curvePointArray ,float regression_ratio){
	
	//if(initial_path[initial_path.size() - 1] == 519){
	//	std::cout<<945<<std::endl;
	//}


	int dim = 3;
	int k = 1;

	std::ofstream mfile("meam.txt");

	float u = regression_ratio;
	int sparse_matrix_size = (int)curvePointArray.size();

	int index_offset = 0;

	Eigen::MatrixXf dMat_C_3m_1;
	dMat_C_3m_1.resize(sparse_matrix_size * 3,1);
	dMat_C_3m_1.setZero();

	
	Eigen::MatrixXf dMat_Xt_3m_1;
	dMat_Xt_3m_1.resize(sparse_matrix_size * 3 ,1);
	dMat_Xt_3m_1.setZero();

	Eigen::MatrixXf dMat_N_3m_m;
	dMat_N_3m_m.resize(sparse_matrix_size * 3,sparse_matrix_size);
	dMat_N_3m_m.setZero();

	Eigen::MatrixXf dMat_Nt_m_3m;
	dMat_Nt_m_3m.resize(sparse_matrix_size, sparse_matrix_size * 3 );
	dMat_Nt_m_3m.setZero();

	Eigen::MatrixXf dMat_Nstar_3m_3m;
	dMat_Nstar_3m_3m.resize(sparse_matrix_size * 3,sparse_matrix_size * 3);
	dMat_Nstar_3m_3m.setZero();

	Eigen::MatrixXf dMat_K_3m_3m;
	dMat_K_3m_3m.resize(sparse_matrix_size * 3,sparse_matrix_size * 3);
	dMat_K_3m_3m.setZero();

	Eigen::MatrixXf dMat_I_mat_3m_3m;
	dMat_I_mat_3m_3m.resize(sparse_matrix_size * 3 , sparse_matrix_size * 3);
	dMat_I_mat_3m_3m.setZero();


	for(int i = 0 ; i < sparse_matrix_size * 3; ++i){

		dMat_I_mat_3m_3m(i,i) = 1.0f;
	}



	std::vector<Point> mean_array;
	for(int i = 0;i< sparse_matrix_size ;++i ){

		dMat_Xt_3m_1(i*3 ,0) = curvePointArray[i ].x;
		dMat_Xt_3m_1(i*3 + 1,0) = curvePointArray[i ].y;
		dMat_Xt_3m_1(i*3 + 2,0) = curvePointArray[i ].z;

		int num_points = 1;


		//////////////////////////////////////////////////////////////////
		//// one-ring searching for closest point

		std::vector<int> one_ring_v;
		std::vector<int> one_ring_f;

		


		std::vector<int> one_ring_vertex;
		one_ring_vertex = mesh_data->one_ring_vertex_array[initial_path[i]];
		
		std::vector<int> one_ring_faces;
		one_ring_faces = mesh_data->one_ring_face_array[initial_path[i]];
	
		// get one-ring neighbor face normal of current curve point


		//delete []nnIdx;
		//delete []dists;

		//perform SVD on one ring face normals
		int row =  (int)one_ring_faces.size();
		//int row =  (int)one_ring_vertex.size();
		Eigen::MatrixX3f eigenPointMatrix;
		eigenPointMatrix.resize(row,3);
		Eigen::MatrixX3f transposeMatrix;
		transposeMatrix.resize(row,3);
		Eigen::VectorXf meanVector;

		//for(int a = 0; a < one_ring_vertex.size() ; ++a){
		//	eigenPointMatrix(a,0) = mesh_data->getVertex(one_ring_vertex[a])[0];
		//	eigenPointMatrix(a,1) = mesh_data->getVertex(one_ring_vertex[a])[1];
		//	eigenPointMatrix(a,2) = mesh_data->getVertex(one_ring_vertex[a])[2];
		//}	

		for(int a = 0; a < one_ring_faces.size();++a ){

			eigenPointMatrix(a,0) = mesh_data->getVertexNormal(one_ring_faces[a])[0];
			eigenPointMatrix(a,1) = mesh_data->getVertexNormal(one_ring_faces[a])[1];
			eigenPointMatrix(a,2) = mesh_data->getVertexNormal(one_ring_faces[a])[2];	
		}


		float mean = 0.0f;
		for (int k = 0; k < 3; ++k)
		{
			//compute mean
			mean = (eigenPointMatrix.col(k).sum())/row;
			// create a vector with constant value = mean
			meanVector  = Eigen::VectorXf::Constant(row,mean); 
			eigenPointMatrix.col(k) -= meanVector;
		}


		Eigen::MatrixXf covariance = ((float)1/(row-1)) * eigenPointMatrix.transpose()* eigenPointMatrix;

		Eigen::EigenSolver<Eigen::MatrixXf> m_solve(covariance);

		Eigen::VectorXf eigenvalues = m_solve.eigenvalues().real();
		Eigen::MatrixXf eignevectors = m_solve.eigenvectors().real();

		Eigen::Vector3f vector(0.0,0.0,0.0);


		if(eigenvalues(0) < eigenvalues(1))
		{
			if (eigenvalues(0) < eigenvalues(2))
			{
				vector[0] = eignevectors(0,0);
				vector[1] = eignevectors(1,0);
				vector[2] = eignevectors(2,0);
			}
			else
			{
				vector[0] = eignevectors(0,2);
				vector[1] = eignevectors(1,2);
				vector[2] = eignevectors(2,2);
			}
		}
		else{
			if(eigenvalues(1) < eigenvalues(2))
			{
				vector[0] = eignevectors(0,1);
				vector[1] = eignevectors(1,1);
				vector[2] = eignevectors(2,1);
			}else{
				vector[0] = eignevectors(0,2);
				vector[1] = eignevectors(1,2);
				vector[2] = eignevectors(2,2);
			}
		}

		vector.normalized();




		Eigen::Vector3f v1,v2,v3,v4;

		float x = 0, y = 0,z = 0;
		//float center_point[3];				
		for(int a = 0; a < one_ring_vertex.size() ; ++a){
			v1[0] = mesh_data->getVertex(one_ring_vertex[a])[0] - mesh_data->getVertex(initial_path[i])[0];
			v1[1] = mesh_data->getVertex(one_ring_vertex[a])[1] - mesh_data->getVertex(initial_path[i])[1];
			v1[2] = mesh_data->getVertex(one_ring_vertex[a])[2] - mesh_data->getVertex(initial_path[i])[2];


			v2[0] = mesh_data->getVertex(initial_path[i])[0];
			v2[1] = mesh_data->getVertex(initial_path[i])[1];
			v2[2] = mesh_data->getVertex(initial_path[i])[2];



			x += (v2 + (v1 - (v1.dot(vector))*vector))[0];
			y += (v2 + (v1 - (v1.dot(vector))*vector))[1];
			z += (v2 + (v1 - (v1.dot(vector))*vector))[2];

		}
		
		dMat_C_3m_1(i*3,0) = x /  one_ring_vertex.size();
		dMat_C_3m_1(i*3 + 1,0) = y /  one_ring_vertex.size();
		dMat_C_3m_1(i*3 + 2,0) = z /  one_ring_vertex.size();


		Point p;
		p.x = x /  one_ring_vertex.size();
		p.y = y /  one_ring_vertex.size();
		p.z = z /  one_ring_vertex.size();

		mean_array.push_back(p);



		dMat_N_3m_m(i*3,i) = vector[0];
		dMat_N_3m_m(i*3+1,i) = vector[1];
		dMat_N_3m_m(i*3+2,i) = vector[2];

		dMat_Nt_m_3m(i,i*3 ) = vector[0];
		dMat_Nt_m_3m(i,i*3+1) = vector[1];
		dMat_Nt_m_3m(i,i*3+2) = vector[2];

		if(i > 0 && i < sparse_matrix_size - 1){
			dMat_K_3m_3m(i * 3 , (i - 1) * 3 ) = 1;
			dMat_K_3m_3m(i * 3 + 1 , (i - 1) * 3 +1) = 1;
			dMat_K_3m_3m(i * 3 + 2 , (i - 1) * 3 +2 ) = 1;

			dMat_K_3m_3m(i * 3 , i * 3 ) = -2;
			dMat_K_3m_3m(i * 3 + 1 , i * 3 +1) = -2;
			dMat_K_3m_3m(i * 3 + 2 , i * 3 +2 ) = -2;
   

			dMat_K_3m_3m(i * 3 , (i + 1) * 3 ) = 1;
			dMat_K_3m_3m(i * 3 + 1 , (i + 1) * 3 +1) = 1;
			dMat_K_3m_3m(i * 3 + 2 , (i + 1) * 3 +2 ) = 1;

		}


	}


	if(initial_path[initial_path.size() - 1] == 519){
		for(int a = 0; a < mean_array.size(); ++a){
			mfile<<"spaceLocator -p "<<mean_array[a].x<<" "<<mean_array[a].y<<" "<<mean_array[a].z<<";"<<std::endl;
		}
	}

	mfile.close();
	mean_array.clear();



	dMat_Nstar_3m_3m = dMat_N_3m_m * dMat_Nt_m_3m;


	////////////////////////////////////////////////////////////////////////////////
	//Timer t2;

	//start_timer(t2,true,"matrix calculation start: ",true);

	Eigen::MatrixXf sub_mat,inv_mat,dMat_Xt_1_m_3;

	u = 500.0;
	
	sub_mat.resize(sparse_matrix_size * 3 , sparse_matrix_size * 3);
	//inv_mat.resize(sparse_matrix_size * 3 , sparse_matrix_size * 3);
	dMat_Xt_1_m_3.resize(sparse_matrix_size * 3,1);
	//sub_mat = dMat_I_mat_3m_3m +  u * (dMat_Nstar_3m_3m - dMat_K_3m_3m + dMat_Nstar_3m_3m * dMat_K_3m_3m);
	sub_mat = dMat_I_mat_3m_3m +  u * ( -1 * dMat_K_3m_3m + dMat_Nstar_3m_3m * dMat_K_3m_3m);
	//FullPivLU<MatrixXd> lu(sub_mat);
	//inv_mat = lu.inverse();
	
	//dMat_Xt_1_m_3 = inv_mat * ( dMat_Xt_3m_1 + u * dMat_Nstar_3m_3m * dMat_C_3m_1 );

	Eigen::MatrixXf B;
	//B = dMat_Xt_3m_1 + u * dMat_Nstar_3m_3m * dMat_C_3m_1;
	B = dMat_Xt_3m_1;
	dMat_Xt_1_m_3 = sub_mat.lu().solve(B);

	std::ofstream pfile("calculation procedure.txt");
	pfile<<"pt+1: "<<std::endl;

	for(int i = 0;i<sparse_matrix_size * 3;i++ ){
		float value = 0;
		value = dMat_Xt_1_m_3(i,0);
		pfile<<value<<" ";
		pfile<<std::endl;
	}
	pfile<<std::endl;
	pfile<<std::endl;

	//}
	pfile.close();



	for(int i = 1; i < sparse_matrix_size;++i){

		curvePointArray[i].x = dMat_Xt_1_m_3(i*3,0);
		curvePointArray[i].y = dMat_Xt_1_m_3(i*3 + 1,0);
		curvePointArray[i].z = dMat_Xt_1_m_3(i*3 + 2,0);
	}

	//pfile.close();

}



#endif