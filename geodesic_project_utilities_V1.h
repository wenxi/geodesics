#pragma once

#include "geodesic_base_elements.h"
#include "geodesic_mesh_data.h"


#include <Eigen/Core>
#include <Eigen/Eigen>
#include <Eigen/Eigenvalues>
#include <Eigen/Sparse>






/////////////////////////////////////////////////////////////////

float Distl(Point point0, Point point1){
	return sqrt((point0.x - point1.x) * (point0.x - point1.x) + (point0.y - point1.y) * (point0.y - point1.y) + (point0.z - point1.z) * (point0.z - point1.z)); 
}


float Distl(initPoint point0, initPoint point1){
	return sqrt((point0.x - point1.x) * (point0.x - point1.x) + (point0.y - point1.y) * (point0.y - point1.y) + (point0.z - point1.z) * (point0.z - point1.z)); 
}

/////////////////////////////////////////////////////////////////


void appendVector(std::vector<int> a, std::vector<int> & b)
{
	for(int i = 0; i < a.size();i++)
	{	
		bool is_exist = false;
		for(int j = 0; j < b.size();j++)
		{
			if(a[i] == b[j])
			{
				is_exist = true;
				break;
			}
		}

		if(!is_exist)
		{
			b.push_back(a[i]);
		}
	}
}

//////////////////////////////////////////////////////////////////////


std::vector<int> setIntersection( std::vector<int> a , std::vector<int> b)
{
	std::vector<int> intersection;

	for(int i = 0; i < a.size(); i++)
	{
		for(int j = 0; j < b.size(); j++)
		{
			if( a[i] == b[j] )
			{
				intersection.push_back(a[i]);
			}
		}
	}
	return intersection;
}

//////////////////////////////////////////////////////////////////////

std::vector<int> setDiff( std::vector<int> a , std::vector<int> b )
{
	std::vector<int> difference;
	std::vector<int> intersection;
	intersection = setIntersection( a , b );

	for(int i = 0; i < a.size(); i++)
	{
		bool is_exist = false;
		for(int j = 0; j < intersection.size(); j++)
		{		
			if( intersection[j] == a[i] ) 
			{
				is_exist = true;
				break;
			}
		}
		if(!is_exist)
		{
			difference.push_back(a[i]);
		}
	}

	return difference;
}

//////////////////////////////////////////////////////////////////////

float area(Eigen::Vector3f p0, Eigen::Vector3f p1, Eigen::Vector3f p2)
{
	Eigen::Vector3f edge0,edge1,v1;
	edge0 = p1 - p0;
	edge1 = p2 - p0;
	v1 = edge0.cross(edge1);
	return v1.norm() / 2;
}

/////////////////////////////////////////////////////////////////

int updateProjectionIfOnEdge(Eigen::Vector3f & p, Eigen::Vector3f p1, Eigen::Vector3f p2, float tol)
{
	if((p - p1).norm() < tol)
	{
		p = p1;
		return 0;
	}
	else if((p-p2).norm() < tol)
	{
		p = p2;
		return 1;
	}
	else
	{
		p = ((p - p1).dot(p2 - p1) / (p2 - p1).norm())*(p2 - p1) / (p2 - p1).norm() + p1;
		return 2;
	}
}

/////////////////////////////////////////////////////////////////

bool is_reach_destination(MeshData * mesh_data, CutPoint cp, int destinationID)
{
//	CutPoint lastCutPoint; 
//	lastCutPoint = cp;
//	int currentCutFaceID = lastCutPoint.cutFaceID;
//
//	int * faceVertexID = mesh_data->getFace(currentCutFaceID);
//	if(faceVertexID[0] == destinationID || faceVertexID[1] == destinationID || faceVertexID[2] == destinationID )
//	{
//		return true;
//	}	
//	else
//	{
		return false;
//	}
}

/////////////////////////////////////////////////////////////////
//bool is_in_triangle(Eigen::Vector3f & p, Eigen::Vector3f p0, Eigen::Vector3f p1, Eigen::Vector3f p2, float tol)
//{
//
//	float triangle_area = area(p0,p1,p2);
//	float area1 = area(p,p1,p2);
//	float area2 = area(p0,p,p2);
//	float area3 = area(p0,p1,p);
//	float projected_area = area1 + area2 + area3;
//
//	if (std::abs(projected_area - triangle_area) < tol )
//	{
//		if(area1 < tol )
//		{ 
//			updateProjectionIfOnEdge(p,p1,p2,tol);
//		}
//		if(area2  < tol )
//		{
//			updateProjectionIfOnEdge(p,p0,p2,tol);
//		}
//		if(area3  < tol )
//		{
//			updateProjectionIfOnEdge(p,p0,p1,tol);
//		}
//
//		return true;
//	}
//	else
//	{
//		return false;
//	}
//}


/////////////////////////////////////////////////////////////////
bool is_in_triangle(MeshData * mesh_data, Eigen::Vector3f & p, int faceID, int pID0, int pID1, int pID2, initPoint & projectPoint, float tol)
{
	Eigen::Vector3f p0, p1, p2, n_p0, n_p1, n_p2, new_n;
	p0[0] = mesh_data->getPoint(pID0).x;
	p0[1] = mesh_data->getPoint(pID0).y;
	p0[2] = mesh_data->getPoint(pID0).z;
	p1[0] = mesh_data->getPoint(pID1).x;
	p1[1] = mesh_data->getPoint(pID1).y;
	p1[2] = mesh_data->getPoint(pID1).z;
	p2[0] = mesh_data->getPoint(pID2).x;
	p2[1] = mesh_data->getPoint(pID2).y;
	p2[2] = mesh_data->getPoint(pID2).z;


	float triangle_area = area(p0,p1,p2);
	float area1 = area(p,p1,p2);
	float area2 = area(p0,p,p2);
	float area3 = area(p0,p1,p);

	n_p0[0] = mesh_data->getNormal(pID0)[0];
	n_p0[1] = mesh_data->getNormal(pID0)[1];
	n_p0[2] = mesh_data->getNormal(pID0)[2];
	n_p1[0] = mesh_data->getNormal(pID1)[0];
	n_p1[1] = mesh_data->getNormal(pID1)[1];
	n_p1[2] = mesh_data->getNormal(pID1)[2];
	n_p2[0] = mesh_data->getNormal(pID2)[0];
	n_p2[1] = mesh_data->getNormal(pID2)[1];
	n_p2[2] = mesh_data->getNormal(pID2)[2];
	n_p0 = n_p0 / n_p0.norm();
	n_p1 = n_p1 / n_p1.norm();
	n_p2 = n_p2 / n_p2.norm();


	float projected_area = area1 + area2 + area3;

	if (std::abs(projected_area - triangle_area) < tol )
	{
		if(area1 < tol )
		{ 
			switch( updateProjectionIfOnEdge(p,p1,p2,tol)) 
			{
			case 0:
				projectPoint.x = p1[0];
				projectPoint.y = p1[1];
				projectPoint.z = p1[2];
				projectPoint.n_x = n_p1[0];
				projectPoint.n_y = n_p1[1];
				projectPoint.n_z = n_p1[2];
				projectPoint.location = mesh_data->one_ring_face_array[pID1];
				projectPoint.neighbourFacesID = mesh_data->one_ring_face_array[pID1];
				break;
			case 1:
				projectPoint.x = p2[0];
				projectPoint.y = p2[1];
				projectPoint.z = p2[2];
				projectPoint.n_x = n_p2[0];
				projectPoint.n_y = n_p2[1];
				projectPoint.n_z = n_p2[2];
				projectPoint.location = mesh_data->one_ring_face_array[pID2];
				projectPoint.neighbourFacesID = mesh_data->one_ring_face_array[pID2];
				break;
			case 2:
				projectPoint.x = p[0];
				projectPoint.y = p[1];
				projectPoint.z = p[2];
				std::vector<int> neighbour1;
				std::vector<int> neighbour2;
				std::vector<int> sharedFace;
				Eigen::Vector3f n1,n2,n;
				n1 = n_p1;
				n2 = n_p2;
				n = ((p - p2).norm()/(p1 - p2).norm())*n1 + ((p - p1).norm()/(p1 - p2).norm())*n2;
				n = n/n.norm();
				projectPoint.n_x = n[0];
				projectPoint.n_y = n[1];
				projectPoint.n_z = n[2];
				neighbour1 = mesh_data->one_ring_face_array[pID1];
				neighbour2 = mesh_data->one_ring_face_array[pID2];
				sharedFace = setIntersection(neighbour1 , neighbour2);
				projectPoint.location = sharedFace;
				projectPoint.neighbourFacesID = mesh_data->get_one_ring_faces_of_face(sharedFace[0]);
				appendVector(mesh_data->get_one_ring_faces_of_face(sharedFace[1]) , projectPoint.neighbourFacesID);
				break;
			}
		}
		else if(area2  < tol )
		{
			switch( updateProjectionIfOnEdge(p,p0,p2,tol) )
			{
			case 0:
				projectPoint.x = p0[0];
				projectPoint.y = p0[1];
				projectPoint.z = p0[2];
				projectPoint.n_x = n_p0[0];
				projectPoint.n_y = n_p0[1];
				projectPoint.n_z = n_p0[2];
				projectPoint.location = mesh_data->one_ring_face_array[pID0];
				projectPoint.neighbourFacesID = mesh_data->one_ring_face_array[pID0];
				break;
			case 1:
				projectPoint.x = p2[0];
				projectPoint.y = p2[1];
				projectPoint.z = p2[2];
				projectPoint.n_x = n_p2[0];
				projectPoint.n_y = n_p2[1];
				projectPoint.n_z = n_p2[2];
				projectPoint.location = mesh_data->one_ring_face_array[pID2];
				projectPoint.neighbourFacesID = mesh_data->one_ring_face_array[pID2];
				break;
			case 2:
				projectPoint.x = p[0];
				projectPoint.y = p[1];
				projectPoint.z = p[2];
				std::vector<int> neighbour1;
				std::vector<int> neighbour2;
				std::vector<int> sharedFace;
				Eigen::Vector3f n1,n2,n;
				n1[0] = n_p0[0];
				n1[1] = n_p0[1];
				n1[2] = n_p0[2];
				n2[0] = n_p2[0];
				n2[1] = n_p2[1];
				n2[2] = n_p2[2];
				n = ((p - p2).norm()/(p0 - p2).norm())*n1 + ((p - p0).norm()/(p0 - p2).norm())*n2;
				n = n/n.norm();
				projectPoint.n_x = n[0];
				projectPoint.n_y = n[1];
				projectPoint.n_z = n[2];
				neighbour1 = mesh_data->one_ring_face_array[pID0];
				neighbour2 = mesh_data->one_ring_face_array[pID2];
				sharedFace = setIntersection(neighbour1 , neighbour2);
				projectPoint.location = sharedFace;
				projectPoint.neighbourFacesID = mesh_data->get_one_ring_faces_of_face(sharedFace[0]);
				appendVector(mesh_data->get_one_ring_faces_of_face(sharedFace[1]) , projectPoint.neighbourFacesID);
				break;
			}
		}
		else if(area3  < tol )
		{
			switch (updateProjectionIfOnEdge(p,p0,p1,tol))
			{
			case 0:
				projectPoint.x = p0[0];
				projectPoint.y = p0[1];
				projectPoint.z = p0[2];
				projectPoint.n_x = n_p0[0];
				projectPoint.n_y = n_p0[1];
				projectPoint.n_z = n_p0[2];
				projectPoint.location = mesh_data->one_ring_face_array[pID0];
				projectPoint.neighbourFacesID = mesh_data->one_ring_face_array[pID0];
				break;
			case 1:
				projectPoint.x = p1[0];
				projectPoint.y = p1[1];
				projectPoint.z = p1[2];
				projectPoint.n_x = n_p1[0];
				projectPoint.n_y = n_p1[1];
				projectPoint.n_z = n_p1[2];
				projectPoint.location = mesh_data->one_ring_face_array[pID1];
				projectPoint.neighbourFacesID = mesh_data->one_ring_face_array[pID1];
				break;
			case 2:
				projectPoint.x = p[0];
				projectPoint.y = p[1];
				projectPoint.z = p[2];
				Eigen::Vector3f n1,n2,n;
				n1 = n_p0;
				n2 = n_p1;
				n = ((p - p2).norm()/(p0 - p1).norm())*n1 + ((p - p0).norm()/(p0 - p1).norm())*n2;
				n = n/n.norm();
				projectPoint.n_x = n[0];
				projectPoint.n_y = n[1];
				projectPoint.n_z = n[2];
				std::vector<int> neighbour1;
				std::vector<int> neighbour2;
				std::vector<int> sharedFace;
				neighbour1 = mesh_data->one_ring_face_array[pID0];
				neighbour2 = mesh_data->one_ring_face_array[pID1];
				sharedFace = setIntersection(neighbour1 , neighbour2);
				projectPoint.location = sharedFace;
				projectPoint.neighbourFacesID = mesh_data->get_one_ring_faces_of_face(sharedFace[0]);
				appendVector(mesh_data->get_one_ring_faces_of_face(sharedFace[1]) , projectPoint.neighbourFacesID);
				break;
			}
		}
		else
		{
			projectPoint.x = p[0];
			projectPoint.y = p[1];
			projectPoint.z = p[2];
			new_n = (area1 / triangle_area) * n_p0 + (area2 / triangle_area) * n_p1 + (area3 / triangle_area) * n_p2;
			new_n = new_n/new_n.norm();
			projectPoint.n_x = new_n[0];
			projectPoint.n_y = new_n[1];
			projectPoint.n_z = new_n[2];

			projectPoint.location.push_back( faceID );
			projectPoint.neighbourFacesID = mesh_data->get_one_ring_faces_of_face(faceID);
		}
		return true;
	}
	else
	{
		return false;
	}
}

/////////////////////////////////////////////////////////////////

bool getIntersection(Eigen::Vector3f ep_0, Eigen::Vector3f ep_1, Eigen::Vector3f cp_p, Eigen::Vector3f cp_n, Eigen::Vector3f &ip, float tol ,float & lamda)
{
	lamda = ( (cp_p - ep_1).dot(cp_n) )/( (ep_0 - ep_1).dot(cp_n) );
	ip = lamda * ep_0 + (1 - lamda) * ep_1;

	float n_0 = (ip - ep_0).norm();
	float n_1 = (ip - ep_1).norm();



	if( n_0 < tol )
	{
		ip = ep_0;
		lamda = 1.0f;
	}
	else if( n_1 < tol )
	{
		ip = ep_1;
		lamda = 0.0f;
	}

	if(lamda >= 0.0f && lamda <= 1.0f)
	{	
		return true;
	}
	else
	{
		return false;
	}
}

/////////////////////////////////////////////////////////////////

int getNextFace(MeshData * mesh_data, int cutFace, std::vector<int> previousCutEdge, std::vector<std::vector<int>> & nextCutEdges)
{
	int previousCutFace = cutFace;
	std::vector<int> adjacentFaces;
	adjacentFaces = mesh_data->adjacent_face_array[previousCutFace];
	int nextFaceID = 0;

	for(int a = 0; a < adjacentFaces.size(); ++a)
	{
		int counter = 0;
		int * facePointIDs =  mesh_data->getFace(adjacentFaces[a]);
		for(unsigned int i = 0; i < 3; ++i)
		{
			if(facePointIDs[i] == previousCutEdge[0] || facePointIDs[i] == previousCutEdge[1])
				counter++;
		}
		if( counter == 2 )
		{
			int nextFaceID = adjacentFaces[a];
			int * nextFaceVerticesID = mesh_data->getFace(nextFaceID);
			for(int b = 0; b < 3; b++)
			{
				if(nextFaceVerticesID[b] != previousCutEdge[0] && nextFaceVerticesID[b] != previousCutEdge[1])
				{
					std::vector<int> newCutEdge;
					newCutEdge.push_back(nextFaceVerticesID[b]);
					newCutEdge.push_back(previousCutEdge[0]);
					nextCutEdges.push_back(newCutEdge);
					newCutEdge.clear();

					newCutEdge.push_back(nextFaceVerticesID[b]);
					newCutEdge.push_back(previousCutEdge[1]);
					nextCutEdges.push_back(newCutEdge);
					newCutEdge.clear();
				}
			}
			return adjacentFaces[a];
		}
	}

	return -1;
}

/////////////////////////////////////////////////////////////////

Eigen::Vector3f getNewNormalforCutPoint(MeshData * mesh_data, std::vector<int> cutEdge, Eigen::Vector3f ip )
{
	Eigen::Vector3f n_1, n_2, ep_1, ep_2, normal;
	n_1[0] = mesh_data->getNormal(cutEdge[0])[0];
	n_1[1] = mesh_data->getNormal(cutEdge[0])[1];
	n_1[2] = mesh_data->getNormal(cutEdge[0])[2];
	n_2[0] = mesh_data->getNormal(cutEdge[1])[0];
	n_2[1] = mesh_data->getNormal(cutEdge[1])[1];
	n_2[2] = mesh_data->getNormal(cutEdge[1])[2];

	ep_1[0] = mesh_data->getPoint(cutEdge[0]).x;
	ep_1[1] = mesh_data->getPoint(cutEdge[0]).y;
	ep_1[2] = mesh_data->getPoint(cutEdge[0]).z;
	ep_2[0] = mesh_data->getPoint(cutEdge[1]).x;
	ep_2[1] = mesh_data->getPoint(cutEdge[1]).y;
	ep_2[2] = mesh_data->getPoint(cutEdge[1]).z;

	normal = (ip - ep_2).norm() * n_1 / (ep_1 - ep_2).norm() + (ip - ep_1).norm() * n_2 / (ep_1 - ep_2).norm();
	normal = normal/normal.norm();
	return normal;
}

/////////////////////////////////////////////////////////////////



/////////////////////////////////////////////////////////////////

void updataPathInfo(MeshData * mesh_data, std::vector< CutPoint > projectedPath , std::vector<Point> & geodesic_path, std::vector<initPoint> & initial_path )
{
	CutPoint destination, source;


	destination.x = geodesic_path[geodesic_path.size() - 1].x;
	destination.y = geodesic_path[geodesic_path.size() - 1].y;
	destination.z = geodesic_path[geodesic_path.size() - 1].z;
	destination.n_x = initial_path[initial_path.size() - 1].n_x;
	destination.n_y = initial_path[initial_path.size() - 1].n_y;
	destination.n_z = initial_path[initial_path.size() - 1].n_z;
	destination.location = initial_path[initial_path.size() - 1].neighbourFacesID;
	std::vector<int> destiOneRangFace;
	destiOneRangFace = initial_path[initial_path.size() - 1].neighbourFacesID;
	destination.neighbourFaces = destiOneRangFace;
	int destID = initial_path[initial_path.size() - 1].vertexID;
	destination.vertexID = destID;


	source.x = geodesic_path[0].x;
	source.y = geodesic_path[0].y;
	source.z = geodesic_path[0].z;
	source.n_x = initial_path[0].n_x;
	source.n_y = initial_path[0].n_y;
	source.n_z = initial_path[0].n_z;
	source.location = initial_path[0].neighbourFacesID;
	std::vector<int> sourceOneRangFace;
	sourceOneRangFace = initial_path[0].neighbourFacesID;
	source.neighbourFaces = sourceOneRangFace;
	int sourcetID = initial_path[0].vertexID;
	source.vertexID = sourcetID;



	geodesic_path.clear();
	initial_path.clear();
	geodesic_path.resize(projectedPath.size());
	initial_path.resize(projectedPath.size());

	geodesic_path[0].x = source.x;
	geodesic_path[0].y = source.y;
	geodesic_path[0].z = source.z;

	initial_path[0].x = source.x;
	initial_path[0].y = source.y;
	initial_path[0].z = source.z;
	initial_path[0].n_x = source.n_x;
	initial_path[0].n_y = source.n_y;
	initial_path[0].n_z = source.n_z;
	initial_path[0].neighbourFacesID = sourceOneRangFace;
	initial_path[0].vertexID = sourcetID;
	initial_path[0].location = source.location;

	geodesic_path[projectedPath.size() - 1].x = destination.x;
	geodesic_path[projectedPath.size() - 1].y = destination.y;
	geodesic_path[projectedPath.size() - 1].z = destination.z;

	initial_path[projectedPath.size() - 1].x = destination.x;
	initial_path[projectedPath.size() - 1].y = destination.y;
	initial_path[projectedPath.size() - 1].z = destination.z;
	initial_path[projectedPath.size() - 1].n_x = destination.n_x;
	initial_path[projectedPath.size() - 1].n_y = destination.n_y;
	initial_path[projectedPath.size() - 1].n_z = destination.n_z;
	initial_path[projectedPath.size() - 1].neighbourFacesID = destiOneRangFace;
	initial_path[projectedPath.size() - 1].vertexID = destID;
	initial_path[projectedPath.size() - 1].location = destination.location;

	std::vector<int> neighbourFace;
	std::vector<int> neighbourFace1;
	std::vector<int> neighbourFace2;
	std::vector<int> neighbourFace3;
	for(int i = 1; i < projectedPath.size()-1;i++)
	{
		geodesic_path[i].x = projectedPath[i].x;
		geodesic_path[i].y = projectedPath[i].y;
		geodesic_path[i].z = projectedPath[i].z;

		initial_path[i].x = projectedPath[i].x;
		initial_path[i].y = projectedPath[i].y;
		initial_path[i].z = projectedPath[i].z;
		initial_path[i].n_x = projectedPath[i].n_x;
		initial_path[i].n_y = projectedPath[i].n_y;
		initial_path[i].n_z = projectedPath[i].n_z;
		//neighbourFace = mesh_data->one_ring_face_array[projectedPath[i].cutEdge[0]];
		//neighbourFace1 = mesh_data->one_ring_face_array[projectedPath[i].cutEdge[1]];
		//appendVector(neighbourFace1, neighbourFace);


		//for(int a = 0; a < neighbourFace1.size(); a++)
		//{
		//	int counter = 0;
		//	int * faceVertexIDs = mesh_data->getFace(neighbourFace1[a]);
		//	for(unsigned int b = 0; b < 3; ++b)
		//	{
		//		if(faceVertexIDs[b] == projectedPath[i-1].cutEdge[0] || faceVertexIDs[b] == projectedPath[i-1].cutEdge[1])
		//			counter++;
		//	}
		//	if( counter == 2 )
		//	{
		//		for(unsigned int b = 0; b < 3; ++b)
		//		{
		//			if(faceVertexIDs[b] != projectedPath[i-1].cutEdge[0] && faceVertexIDs[b] != projectedPath[i-1].cutEdge[1])
		//				appendVector(mesh_data->one_ring_face_array[faceVertexIDs[b]], neighbourFace);
		//		}					
		//	}
		//}

		initial_path[i].neighbourFacesID = projectedPath[i].neighbourFaces;
		initial_path[i].vertexID = projectedPath[i].vertexID;
	}


}

/////////////////////////////////////////////////////////////////

bool isPathStraight(Point point0, Point point1, Point point2)
{
	Eigen::Vector3f floatPoint_0, floatPoint_1, floatPoint_2;
	floatPoint_0[0] = point0.x;
	floatPoint_0[1] = point0.y;
	floatPoint_0[2] = point0.z;
	floatPoint_1[0] = point1.x;
	floatPoint_1[1] = point1.y;
	floatPoint_1[2] = point1.z;
	floatPoint_2[0] = point2.x;
	floatPoint_2[1] = point2.y;
	floatPoint_2[2] = point2.z;

	float a = area(floatPoint_0,floatPoint_1,floatPoint_2);

	if(a < 0.00001)
		return true;
	else
		return false;

}

/////////////////////////////////////////////////////////////////////////////////

Eigen::Vector3f getCurveNormal( Point point0, Point point1, Point point2 )
{
	Eigen::Vector3f floatingP_0, floatingP_1, floatingP_2, Css;
	floatingP_0[0] = point0.x;
	floatingP_0[1] = point0.y;
	floatingP_0[2] = point0.z;
	floatingP_1[0] = point1.x;
	floatingP_1[1] = point1.y;
	floatingP_1[2] = point1.z;
	floatingP_2[0] = point2.x;
	floatingP_2[1] = point2.y;
	floatingP_2[2] = point2.z;


	float g = (floatingP_0 - floatingP_1).norm();
	float f = (floatingP_2 - floatingP_1).norm();

	Css = 2*floatingP_2 / (f *(g + f)) + 2*floatingP_0 / (g * (g + f)) - 2 * floatingP_1 / (g * f);

	return Css;
}


Eigen::Vector3f getCurveNormal( initPoint point0, Point point1, Point point2 )
{
	Eigen::Vector3f floatingP_0, floatingP_1, floatingP_2, Css;
	floatingP_0[0] = point0.x;
	floatingP_0[1] = point0.y;
	floatingP_0[2] = point0.z;
	floatingP_1[0] = point1.x;
	floatingP_1[1] = point1.y;
	floatingP_1[2] = point1.z;
	floatingP_2[0] = point2.x;
	floatingP_2[1] = point2.y;
	floatingP_2[2] = point2.z;


	float g = (floatingP_0 - floatingP_1).norm();
	float f = (floatingP_2 - floatingP_1).norm();

	Css = 2*floatingP_2 / (f *(g + f)) + 2*floatingP_0 / (g * (g + f)) - 2 * floatingP_1 / (g * f);

	return Css;
}

/////////////////////////////////////////////////////////////////////////////////

Eigen::Vector3f getSVDnomralofOnRingFace(MeshData * mesh_data, std::vector<int> one_ring_vertices)
{
	int row = (int)one_ring_vertices.size();
	Eigen::MatrixX3f eigenPointMatrix;
	eigenPointMatrix.resize(row,3);
	Eigen::MatrixX3f transposeMatrix;
	transposeMatrix.resize(row,3);
	Eigen::VectorXf meanVector;

	for(int a = 0; a < one_ring_vertices.size();++a ){
		float n_x, n_y, n_z;
		n_x = mesh_data->getNormal(one_ring_vertices[a])[0];
		n_y = mesh_data->getNormal(one_ring_vertices[a])[1];
		n_z = mesh_data->getNormal(one_ring_vertices[a])[2];	
		eigenPointMatrix(a,0) = n_x;
		eigenPointMatrix(a,1) = n_y;
		eigenPointMatrix(a,2) = n_z;	
	}

	//std::ofstream eigenPointMatrixFile("eigenPointMatrix.txt");
	//	for(int a = 0; a < eigenPointMatrix.rows();a++)
	//{
	//	for(int b = 0; b < eigenPointMatrix.cols();b++)
	//	{
	//		eigenPointMatrixFile<<eigenPointMatrix(a,b)<<" ";
	//	}
	//	eigenPointMatrixFile<<std::endl;
	//}
	//eigenPointMatrixFile.close();


	//std::ofstream covarianceFile("covariance.txt");
	Eigen::MatrixXf covariance =  eigenPointMatrix.transpose()* eigenPointMatrix;

	//for(int a = 0; a < covariance.rows();a++)
	//{
	//	for(int b = 0; b < covariance.cols();b++)
	//	{
	//		covarianceFile<<covariance(a,b)<<" ";
	//	}
	//	covarianceFile<<std::endl;
	//}
	//covarianceFile.close();


	Eigen::EigenSolver<Eigen::MatrixXf> m_solve(covariance);
	Eigen::VectorXf eigenvalues = m_solve.eigenvalues().real();
	Eigen::MatrixXf eignevectors = m_solve.eigenvectors().real();
	Eigen::Vector3f vector(0.0,0.0,0.0);
	if(eigenvalues(0) > eigenvalues(1))
	{
		if (eigenvalues(0) > eigenvalues(2))
		{
			vector[0] = eignevectors(0,0);
			vector[1] = eignevectors(1,0);
			vector[2] = eignevectors(2,0);
		}
		else
		{
			vector[0] = eignevectors(0,2);
			vector[1] = eignevectors(1,2);
			vector[2] = eignevectors(2,2);
		}
	}
	else{
		if(eigenvalues(1) > eigenvalues(2))
		{
			vector[0] = eignevectors(0,1);
			vector[1] = eignevectors(1,1);
			vector[2] = eignevectors(2,1);
		}else{
			vector[0] = eignevectors(0,2);
			vector[1] = eignevectors(1,2);
			vector[2] = eignevectors(2,2);
		}
	}

	vector = vector/vector.norm();
	return vector;
}

/////////////////////////////////////////////////////////////////////////////////




//bool projectFloatingPointOnMesh(MeshData * mesh_data, Point point0, Point point1, Point point2, Eigen::Vector3f & projection, int faceID)
//{
//	Eigen::Vector3f floatPoint_0, floatPoint_1, floatPoint_2;
//	floatPoint_0[0] = point0.x;
//	floatPoint_0[1] = point0.y;
//	floatPoint_0[2] = point0.z;
//	floatPoint_1[0] = point1.x;
//	floatPoint_1[1] = point1.y;
//	floatPoint_1[2] = point1.z;
//	floatPoint_2[0] = point2.x;
//	floatPoint_2[1] = point2.y;
//	floatPoint_2[2] = point2.z;
//
//
//	Eigen::Vector3f Css, floatingP_0, floatingP_1, floatingP_2;
//	floatingP_0 = floatPoint_0;
//	floatingP_1 = floatPoint_1;
//	floatingP_2 = floatPoint_2;
//
//
//	float g = (floatingP_0 - floatingP_1).norm();
//	float f = (floatingP_2 - floatingP_1).norm();
//
//	Css = 2*floatingP_2 / (f *(g + f)) + 2*floatingP_0 / (g * (g + f)) - 2 * floatingP_1 / (g * f);
//	Css.normalized();
//
//
//
//	int * faceVertexID = mesh_data->getFace(faceID);
//	Eigen::Vector3f v_1,fv_0,fv_1,fv_2,v_3,v_4,v_5,v_n;
//
//	v_1[0] = floatPoint_1[0] - mesh_data->getPoint(faceVertexID[0]).x;
//	v_1[1] = floatPoint_1[1] - mesh_data->getPoint(faceVertexID[0]).y;
//	v_1[2] = floatPoint_1[2] - mesh_data->getPoint(faceVertexID[0]).z;
//
//	//three vertices on a triangle 
//	fv_0[0] = mesh_data->getPoint(faceVertexID[0]).x;
//	fv_0[1] = mesh_data->getPoint(faceVertexID[0]).y;
//	fv_0[2] = mesh_data->getPoint(faceVertexID[0]).z;
//	fv_1[0] = mesh_data->getPoint(faceVertexID[1]).x;
//	fv_1[1] = mesh_data->getPoint(faceVertexID[1]).y;
//	fv_1[2] = mesh_data->getPoint(faceVertexID[1]).z;
//	fv_2[0] = mesh_data->getPoint(faceVertexID[2]).x;
//	fv_2[1] = mesh_data->getPoint(faceVertexID[2]).y;
//	fv_2[2] = mesh_data->getPoint(faceVertexID[2]).z;
//
//	// triangle normal
//	v_n = (fv_1 - fv_0).cross(fv_2 - fv_0);
//	//v_3 = (fv_0 + (v_1 - ( v_1.dot(v_n))*v_n));
//
//
//
//
//	float lamda_n = -1 * ( ( floatingP_1 - fv_0 ).dot(v_n) ) / ( Css.dot(v_n) );
//	v_4 = floatingP_1 + lamda_n * Css;
//	projection = v_4;
//	return is_in_triangle(v_4, fv_0, fv_1, fv_2);
//}

///////////////////////////////////////////////////////////////

bool projectFloatingPointOnMesh(MeshData * mesh_data, Eigen::Vector3f projectDirection, Point floatPoint, initPoint & projection, int faceID, float tol)
{
	Eigen::Vector3f floatingP_0, floatingP_1, floatingP_2;

	floatingP_1[0] = floatPoint.x;
	floatingP_1[1] = floatPoint.y;
	floatingP_1[2] = floatPoint.z;

	Eigen::Vector3f Css;
	Css = projectDirection;

	int * faceVertexID = mesh_data->getFace(faceID);
	Eigen::Vector3f v_1,fv_0,fv_1,fv_2,v_3,v_4,v_5,v_n;

	v_1[0] = floatingP_1[0] - mesh_data->getPoint(faceVertexID[0]).x;
	v_1[1] = floatingP_1[1] - mesh_data->getPoint(faceVertexID[0]).y;
	v_1[2] = floatingP_1[2] - mesh_data->getPoint(faceVertexID[0]).z;

	//three vertices on a triangle 
	fv_0[0] = mesh_data->getPoint(faceVertexID[0]).x;
	fv_0[1] = mesh_data->getPoint(faceVertexID[0]).y;
	fv_0[2] = mesh_data->getPoint(faceVertexID[0]).z;
	fv_1[0] = mesh_data->getPoint(faceVertexID[1]).x;
	fv_1[1] = mesh_data->getPoint(faceVertexID[1]).y;
	fv_1[2] = mesh_data->getPoint(faceVertexID[1]).z;
	fv_2[0] = mesh_data->getPoint(faceVertexID[2]).x;
	fv_2[1] = mesh_data->getPoint(faceVertexID[2]).y;
	fv_2[2] = mesh_data->getPoint(faceVertexID[2]).z;

	// triangle normal
	v_n = (fv_1 - fv_0).cross(fv_2 - fv_0);
	//v_3 = (fv_0 + (v_1 - ( v_1.dot(v_n))*v_n));

	float lamda_n = -1 * ( ( floatingP_1 - fv_0 ).dot(v_n) ) / ( Css.dot(v_n) );
	v_4 = floatingP_1 + lamda_n * Css;


	if( is_in_triangle(mesh_data, v_4, faceID, faceVertexID[0], faceVertexID[1], faceVertexID[2],  projection,  tol) )
	{
		return true;
	}else
		return false;

	//return is_in_triangle(v_4, fv_0, fv_1, fv_2);
}

///////////////////////////////////////////////////////////////

void buildCuttingPlane(Point curvePoint0, Point curvePoint1, Eigen::Vector3f projectDirection, Eigen::Vector3f & cutPlaneNormal, Eigen::Vector3f & pointOnCutPlane)
{
	Eigen::Vector3f cp_0,cp_1,cp_2;
	cp_0[0] = curvePoint0.x;
	cp_0[1] = curvePoint0.y;
	cp_0[2] = curvePoint0.z;
	cp_1[0] = curvePoint1.x;
	cp_1[1] = curvePoint1.y;
	cp_1[2] = curvePoint1.z;
	cutPlaneNormal = (cp_1 - cp_0).cross(projectDirection);
	cutPlaneNormal = cutPlaneNormal / cutPlaneNormal.norm();
	pointOnCutPlane = cp_0;
}

///////////////////////////////////////////////////////////////

void buildCuttingPlane(CutPoint cutPoint, Point curvePoint1, Eigen::Vector3f projectDirection, Eigen::Vector3f & cutPlaneNormal, Eigen::Vector3f & pointOnCutPlane)
{
	Eigen::Vector3f cp_0,cp_1,cp_2;
	cp_0[0] = cutPoint.x;
	cp_0[1] = cutPoint.y;
	cp_0[2] = cutPoint.z;
	cp_1[0] = curvePoint1.x;
	cp_1[1] = curvePoint1.y;
	cp_1[2] = curvePoint1.z;
	cutPlaneNormal = (cp_1 - cp_0).cross(projectDirection);
	cutPlaneNormal = cutPlaneNormal / cutPlaneNormal.norm();
	pointOnCutPlane = cp_0;
}

///////////////////////////////////////////////////////////////

void buildCuttingPlane(initPoint initpoint, Point curvePoint1, Eigen::Vector3f projectDirection, Eigen::Vector3f & cutPlaneNormal, Eigen::Vector3f & pointOnCutPlane)
{
	Eigen::Vector3f cp_0,cp_1,cp_2;
	cp_0[0] = initpoint.x;
	cp_0[1] = initpoint.y;
	cp_0[2] = initpoint.z;
	cp_1[0] = curvePoint1.x;
	cp_1[1] = curvePoint1.y;
	cp_1[2] = curvePoint1.z;
	cutPlaneNormal = (cp_1 - cp_0).cross(projectDirection);
	cutPlaneNormal = cutPlaneNormal / cutPlaneNormal.norm();
	pointOnCutPlane = cp_0;
}

///////////////////////////////////////////////////////////////
void buildCuttingPlane(CutPoint curvePoint0, Point curvePoint1, Point curvePoint2, Eigen::Vector3f & cutPlaneNormal, Eigen::Vector3f & pointOnCutPlane)
{
	Eigen::Vector3f cp_0,cp_1,cp_2;
	cp_0[0] = curvePoint0.x;
	cp_0[1] = curvePoint0.y;
	cp_0[2] = curvePoint0.z;
	cp_1[0] = curvePoint1.x;
	cp_1[1] = curvePoint1.y;
	cp_1[2] = curvePoint1.z;
	cp_2[0] = curvePoint2.x;
	cp_2[1] = curvePoint2.y;
	cp_2[2] = curvePoint2.z;
	cutPlaneNormal = (cp_1 - cp_0).cross(cp_2 - cp_0);
	cutPlaneNormal = cutPlaneNormal / cutPlaneNormal.norm();
	pointOnCutPlane = cp_0;
}

///////////////////////////////////////////////////////////////



void buildCuttingPlane(Eigen::Vector3f curvePoint0, Point curvePoint1, Eigen::Vector3f projection, Eigen::Vector3f & cutPlaneNormal, Eigen::Vector3f & pointOnCutPlane)
{
	Eigen::Vector3f cp_0,cp_1,cp_2;
	cp_0 = curvePoint0;
	cp_1[0] = curvePoint1.x;
	cp_1[1] = curvePoint1.y;
	cp_1[2] = curvePoint1.z;
	cp_2 = projection;
	cutPlaneNormal = (cp_1 - cp_0).cross(cp_2 - cp_0);
	cutPlaneNormal = cutPlaneNormal / cutPlaneNormal.norm();
	pointOnCutPlane = cp_0;
}

///////////////////////////////////////////////////////////////
//Given a triangle, an input edge or an input vertex, returns the intersection.
bool getCutPoint( int inputPointID, MeshData * mesh_data , Eigen::Vector3f cp_0, Eigen::Vector3f cp_n, int faceID, CutPoint & cutPoint)
{
	CutPoint cp;
	float lamda = 0.0f;
	float tol = 0.00001f;
	////at source, only one edge per face intersects with cut plane
	int * facePoint = mesh_data->getFace(faceID);
	std::vector<Eigen::Vector3f> edge;
	std::vector<int> edgePointIndex;
	for(int b = 0; b < 3; b++)
	{
		if(facePoint[b] != inputPointID)
		{	
			Eigen::Vector3f ep;
			ep[0] = mesh_data->getPoint(facePoint[b]).x;
			ep[1] = mesh_data->getPoint(facePoint[b]).y;
			ep[2] = mesh_data->getPoint(facePoint[b]).z;
			edge.push_back(ep);
			edgePointIndex.push_back(facePoint[b]);
		}
	}
	Eigen::Vector3f intersection, cutPointNormal;
	if( getIntersection(edge[0], edge[1], cp_0, cp_n, intersection, tol, lamda) )
	{
		CutPoint cp;
		if(lamda == 0.0f)
		{
			cp.vertexID = edgePointIndex[1];
			std::vector<int> tempLocal;
			for(int a = 0; a < mesh_data->one_ring_face_array[cp.vertexID].size(); a++)
			{
				int * tempFaceVertexIDs = mesh_data->getFace(mesh_data->one_ring_face_array[cp.vertexID][a]);
				if(tempFaceVertexIDs[0] == inputPointID || tempFaceVertexIDs[1] == inputPointID || tempFaceVertexIDs[2] == inputPointID )
				{
					if(tempFaceVertexIDs[0] == cp.vertexID || tempFaceVertexIDs[1] == cp.vertexID || tempFaceVertexIDs[2] == cp.vertexID )
					{
						tempLocal.push_back(mesh_data->one_ring_face_array[cp.vertexID][a]);
						continue;
					}
				}
			}
			cp.location = tempLocal;
			cp.cutFaceIDs = tempLocal;
			cp.neighbourFaces = mesh_data->one_ring_face_array[cp.vertexID];
			cp.n_x = mesh_data->getNormal(cp.vertexID)[0];
			cp.n_y = mesh_data->getNormal(cp.vertexID)[1];
			cp.n_z = mesh_data->getNormal(cp.vertexID)[2];

		}
		else if(lamda == 1.0f)
		{
			cp.vertexID = edgePointIndex[0];
			std::vector<int> tempLocal;
			for(int a = 0; a < mesh_data->one_ring_face_array[cp.vertexID].size(); a++)
			{
				int * tempFaceVertexIDs = mesh_data->getFace(mesh_data->one_ring_face_array[cp.vertexID][a]);
				if(tempFaceVertexIDs[0] == inputPointID || tempFaceVertexIDs[1] == inputPointID || tempFaceVertexIDs[2] == inputPointID )
				{
					if(tempFaceVertexIDs[0] == cp.vertexID || tempFaceVertexIDs[1] == cp.vertexID || tempFaceVertexIDs[2] == cp.vertexID )
					{
						tempLocal.push_back(mesh_data->one_ring_face_array[cp.vertexID][a]);
						continue;
					}
				}
			}	
			cp.location = tempLocal;
			cp.cutFaceIDs = tempLocal;
			cp.neighbourFaces = mesh_data->one_ring_face_array[cp.vertexID];
			cp.n_x = mesh_data->getNormal(cp.vertexID)[0];
			cp.n_y = mesh_data->getNormal(cp.vertexID)[1];
			cp.n_z = mesh_data->getNormal(cp.vertexID)[2];
		}
		else
		{
			std::vector<int> tempLocal;
			std::vector<std::vector<int>> nextCutEdges;
			tempLocal.push_back(faceID);
			int nextFace = getNextFace(mesh_data, faceID, edgePointIndex, nextCutEdges);
			tempLocal.push_back(nextFace);
			cp.location = tempLocal;
			cp.cutFaceIDs.clear();
			cp.cutFaceIDs.push_back(faceID);

			std::vector<int> neighbourFaces2;
			cp.cutEdge = edgePointIndex;
			cp.neighbourFaces = mesh_data->one_ring_face_array[cp.cutEdge[0]];
			neighbourFaces2 = mesh_data->one_ring_face_array[cp.cutEdge[1]];

			for(int i = 0; i < neighbourFaces2.size(); i++)
			{
				bool is_exist = false;
				for(int j = 0; j < cp.neighbourFaces.size(); j++)
				{
					if(neighbourFaces2[i] == cp.neighbourFaces[j])
					{ 
						is_exist = true;
						break;
					}
				}

				if(!is_exist)
				{
					cp.neighbourFaces.push_back(neighbourFaces2[i]);
				}
			}

			Eigen::Vector3f cutPointNormal;
			cutPointNormal = getNewNormalforCutPoint(mesh_data, edgePointIndex, intersection );
			cp.n_x = cutPointNormal[0];
			cp.n_y = cutPointNormal[1];
			cp.n_z = cutPointNormal[2];

		}


		cp.x = intersection[0];
		cp.y = intersection[1];
		cp.z = intersection[2];


		//projectedPath.push_back(cp);
		cutPoint = cp;
		return true;
	}else
	{
		return false;
	}
}

/////////////////////////////////////////////////////////////////////////////////////////////

bool getCutPoint( std::vector<int> inputPointIDs, MeshData * mesh_data , Eigen::Vector3f cp_0, Eigen::Vector3f cp_n, int faceID, CutPoint & cutPoint)
{
	float lamda = 0.0f;
	float tol = 0.00001f;
	CutPoint cp;
	////at other faces, one edge contains last cutPoint and rest two edges intersect with cut plane 
	int * facePoint = mesh_data->getFace(faceID);
	std::vector<Eigen::Vector3f> edge;
	std::vector<int> edgePointIndex;
	std::vector<std::vector<Eigen::Vector3f>> Edges;
	std::vector<std::vector<int>> EdgePointIndexs;
	for(int b = 0; b < 3; b++)
	{
		if(facePoint[b] != inputPointIDs[0] && facePoint[b] != inputPointIDs[1])
		{
			Eigen::Vector3f ep;
			ep[0] = mesh_data->getPoint(facePoint[b]).x;
			ep[1] = mesh_data->getPoint(facePoint[b]).y;
			ep[2] = mesh_data->getPoint(facePoint[b]).z;
			edge.push_back(ep);
			edgePointIndex.push_back(facePoint[b]);

			ep[0] = mesh_data->getPoint(inputPointIDs[0]).x;
			ep[1] = mesh_data->getPoint(inputPointIDs[0]).y;
			ep[2] = mesh_data->getPoint(inputPointIDs[0]).z;
			edge.push_back(ep);
			edgePointIndex.push_back(inputPointIDs[0]);		

			Edges.push_back(edge);
			EdgePointIndexs.push_back(edgePointIndex);

			edge.clear();
			edgePointIndex.clear();
			///////////////////////////////////////////////////

			ep[0] = mesh_data->getPoint(facePoint[b]).x;
			ep[1] = mesh_data->getPoint(facePoint[b]).y;
			ep[2] = mesh_data->getPoint(facePoint[b]).z;
			edge.push_back(ep);
			edgePointIndex.push_back(facePoint[b]);

			ep[0] = mesh_data->getPoint(inputPointIDs[1]).x;
			ep[1] = mesh_data->getPoint(inputPointIDs[1]).y;
			ep[2] = mesh_data->getPoint(inputPointIDs[1]).z;
			edge.push_back(ep);
			edgePointIndex.push_back(inputPointIDs[1]);		

			Edges.push_back(edge);
			EdgePointIndexs.push_back(edgePointIndex);
			break;
		}			
	}

	for(int i = 0; i < EdgePointIndexs.size() ; ++i)
	{

		Eigen::Vector3f intersection;
		if( getIntersection(Edges[i][0], Edges[i][1], cp_0, cp_n, intersection, tol , lamda) )
		{
			CutPoint cp;
			if(lamda == 0.0f)
			{
				cp.vertexID = EdgePointIndexs[i][1];
				std::vector<int> tempLocal;
				tempLocal.push_back(faceID);
				cp.location = tempLocal;
				cp.cutFaceIDs = tempLocal;

				cp.neighbourFaces = mesh_data->one_ring_face_array[cp.vertexID];
				cp.n_x = mesh_data->getNormal(cp.vertexID)[0];
				cp.n_y = mesh_data->getNormal(cp.vertexID)[1];
				cp.n_z = mesh_data->getNormal(cp.vertexID)[2];

			}
			else if(lamda == 1.0f)
			{			
				cp.vertexID = EdgePointIndexs[i][0];				
				std::vector<int> tempLocal;
				tempLocal.push_back(faceID);
				cp.location = tempLocal;
				cp.cutFaceIDs = tempLocal;

				cp.neighbourFaces = mesh_data->one_ring_face_array[cp.vertexID];
				cp.n_x = mesh_data->getNormal(cp.vertexID)[0];
				cp.n_y = mesh_data->getNormal(cp.vertexID)[1];
				cp.n_z = mesh_data->getNormal(cp.vertexID)[2];
			}
			else
			{
				std::vector<int> tempLocal;
				tempLocal.push_back(faceID);
				std::vector<std::vector<int>> nextCutEdges;
				int nextFace = getNextFace(mesh_data, faceID, EdgePointIndexs[i], nextCutEdges);
				tempLocal.push_back(nextFace);
				cp.location = tempLocal;
				cp.cutFaceIDs.clear();
				cp.cutFaceIDs.push_back(faceID);

				std::vector<int> neighbourFaces2;
				cp.cutEdge = EdgePointIndexs[i];
				cp.neighbourFaces = mesh_data->one_ring_face_array[cp.cutEdge[0]];
				neighbourFaces2 = mesh_data->one_ring_face_array[cp.cutEdge[1]];

				for(int i = 0; i < neighbourFaces2.size(); i++)
				{
					bool is_exist = false;
					for(int j = 0; j < cp.neighbourFaces.size(); j++)
					{
						if(neighbourFaces2[i] == cp.neighbourFaces[j])
						{ 
							is_exist = true;
							break;
						}
					}

					if(!is_exist)
					{
						cp.neighbourFaces.push_back(neighbourFaces2[i]);
					}
				}

				Eigen::Vector3f cutPointNormal;
				cutPointNormal = getNewNormalforCutPoint(mesh_data, edgePointIndex, intersection );
				cp.n_x = cutPointNormal[0];
				cp.n_y = cutPointNormal[1];
				cp.n_z = cutPointNormal[2];

			}
			cp.x = intersection[0];
			cp.y = intersection[1];
			cp.z = intersection[2];


			cutPoint = cp;

			return true;
		}
	}
	return false;
}

///////////////////////////////////////////////////////////////////////////////////////////

void getNextValidFloatingPoint(MeshData * mesh_data, int & floatingPointIndex, std::vector<CutPoint> & cutPath, int faceID, std::vector<Point> curvePointArray, std::vector<initPoint> initialPath, Eigen::Vector3f & cuttingPlanePoint, Eigen::Vector3f & cuttingPlaneNormal, Eigen::Vector3f & projectDirection)
{
	// for each cut, only the projection of a floating point is outside
	// current face is count as a valid floating point. 
	// if the projection of a floating point is in the current face. then advance
	//// the floating path index till one`s projection is outside current face.
	//bool is_in_current_face = false;

	////If the projection is within current face, then cutting direction can be confirmed
	////Next thing needs to be done is to advance the floating point array index, 
	////and find first floating point whose projection is outside current face.
	//while(is_in_current_face )
	//{
	//	floatingPointIndex++;
	//	Eigen::Vector3f projection;
	//	if(floatingPointIndex == curvePointArray.size()-1)
	//		break;
	//	is_in_current_face = projectFloatingPointOnMesh(mesh_data, projectDirection, curvePointArray[floatingPointIndex], projection,faceID);
	//	if(!is_in_current_face)
	//	{
	//		//if current section is not straight, update projection direction and cutting plane
	//		if(!isPathStraight(curvePointArray[floatingPointIndex - 1], curvePointArray[floatingPointIndex], curvePointArray[floatingPointIndex + 1]))
	//		{
	//			projectDirection = getCurveNormal( curvePointArray[floatingPointIndex-1], curvePointArray[floatingPointIndex], curvePointArray[floatingPointIndex+1] );
	//			buildCuttingPlane(curvePointArray[floatingPointIndex-1],curvePointArray[floatingPointIndex],projectDirection, cuttingPlaneNormal, cuttingPlanePoint);
	//		}
	//	}
	//};

}

///////////////////////////////////////////////////////////////////////////////////////////

void edgeCut(MeshData * mesh_data, int & floatingPointIndex, std::vector<CutPoint> & cutPath, std::vector<Point> curvePointArray, std::vector<initPoint> initialPath, Eigen::Vector3f & cuttingPlanePoint, Eigen::Vector3f & cuttingPlaneNormal, Eigen::Vector3f & projectDirection)
{
	////get next cut face
	//std::vector<std::vector<int>> nextCutEdges;
	//int nextCutFace = getNextFace(mesh_data, cutPath[cutPath.size() - 1].cutFaceID, cutPath[cutPath.size() - 1].cutEdge, nextCutEdges);
	//Eigen::Vector3f projection;
	///////////////////////////////////////////////
	//bool is_in_current_face = false;
	//is_in_current_face = projectFloatingPointOnMesh(mesh_data, projectDirection, curvePointArray[floatingPointIndex], projection,nextCutFace);
	//
	////If current face contains projection of the floating point, 
	////advance the floating path point index till one of whose projection is out side current one ring
	//if(is_in_current_face)
	//{
	//	//If the projection is within current face, then cutting direction can be confirmed
	//	//Next thing needs to be done is to advance the floating point array index, 
	//	//and find first floating point whose projection is outside current face.
	//	getNextValidFloatingPoint(mesh_data, floatingPointIndex, cutPath, nextCutFace, curvePointArray, initialPath, cuttingPlanePoint, cuttingPlaneNormal, projectDirection);
	//}
	//
	////after first floating point that is outside current face is found, cuts face

	//CutPoint cp;
	//bool is_cut = getCutPoint( cutPath[cutPath.size() - 1].cutEdge,  mesh_data , cuttingPlanePoint, cuttingPlaneNormal, nextCutFace, cp);
	//if(!is_cut)
	//	return;
	//else
	//{	
	//	cutPath.push_back(cp);	
	//	return;
	//}
}


/////////////////////////////////////////////////////////////////

void vertexCut(MeshData * mesh_data, int & floatingPointIndex, std::vector<CutPoint> & cutPath, std::vector<Point> curvePointArray, std::vector<initPoint> initialPath, Eigen::Vector3f & cuttingPlanePoint, Eigen::Vector3f & cuttingPlaneNormal, Eigen::Vector3f & projectDirection)
{
	//Cutting direction is defined by last point in cutPath. 
	//Cutting only intersets with triangles does not contain the input vertex
	//( if two triangles shares one vertex, these two triangles will not participite the cutting)


	///////////////////////////////////////
	//// step 1
	//// check if last path section goes through a face or an edge.

	//std::vector< int > one_ring_face = cutPath[cutPath.size() - 1].neighbourFaces;
	//int vertexID = cutPath[cutPath.size() - 1].vertexID;
	//int previousVertexID = cutPath[cutPath.size() - 2].vertexID;
	//int previousFaceID = cutPath[cutPath.size() - 2].cutFaceID;
	//if(cutPath[cutPath.size() - 2].vertexID > -1)
	//{
	//	//last path section goes through an edge
	//	std::vector< int > cutFaceArray;
	//	for( int a = 0; a < one_ring_face.size(); a++ )
	//	{
	//		CutPoint cp;
	//		int * faceVerticesID = mesh_data->getFace(one_ring_face[a]);
	//		if(previousVertexID == faceVerticesID[0] || previousVertexID == faceVerticesID[1] || previousVertexID == faceVerticesID[2])
	//		{	
	//			//This face doesnot contains previous cutPoint
	//			bool is_cut = getCutPoint( vertexID,  mesh_data , cuttingPlanePoint, cuttingPlaneNormal, one_ring_face[a], cp);
	//			if(!is_cut)
	//				continue;
	//			else
	//			{		
	//				cutPath.push_back(cp);
	//				return;					
	//			}
	//		}
	//	}
	//}
	//else
	//{
	//	//last path section goes through a face

	//	std::vector< int > cutFaceArray;
	//	for( int a = 0; a < one_ring_face.size(); a++ )
	//	{
	//		CutPoint cp;
	//		int * faceVerticesID = mesh_data->getFace(one_ring_face[a]);
	//		if( previousFaceID != one_ring_face[a])
	//		{	
	//			//This face doesnot contains previous cutPoint
	//			bool is_cut = getCutPoint( vertexID,  mesh_data , cuttingPlanePoint, cuttingPlaneNormal, one_ring_face[a], cp);
	//			if(!is_cut)
	//				continue;
	//			else
	//			{
	//				cutPath.push_back(cp);
	//				return;					
	//			}
	//		}
	//	}			

	//}
}


/////////////////////////////////////////////////////////////////

void sourceCut(MeshData * mesh_data, int & floatingPointIndex, std::vector<CutPoint> & cutPath, std::vector<Point> curvePointArray, std::vector<initPoint> initialPath, Eigen::Vector3f & cuttingPlanePoint, Eigen::Vector3f & cuttingPlaneNormal, Eigen::Vector3f & projectDirection)
{
	////Cutting direction is unknown, therefore, cutting is performed on to oppsite direction. 
	////This usually happened only at source. 
	//std::vector< std::vector<CutPoint> > cutPathes;
	//cutPathes.push_back(cutPath);
	//cutPathes.push_back(cutPath);
	//int sourceID = initialPath[initialPath.size()-1].vertexID;
	////get one ring face of current vertex
	//std::vector< int > one_ring_face = cutPath[0].neighbourFaces;
	//int vertexID = cutPath[0].vertexID;
	//for(int a = 0; a < one_ring_face.size();++a)
	//{
	//	int faceID = one_ring_face[a];	
	//	Eigen::Vector3f projection, floatingPointProjection;
	//	bool is_in_current_face = false;
	//	is_in_current_face = projectFloatingPointOnMesh(mesh_data, projectDirection, curvePointArray[floatingPointIndex], projection,faceID);
	//	
	//	//If current face contains projection of the floating point, 
	//	//advance the floating path point index till one of whose projection is out side current one ring
	//	if(is_in_current_face)
	//	{
	//		//If the projection is within current face, then cutting direction can be confirmed
	//		//Next thing needs to be done is to advance the floating point array index, 
	//		//and find first floating point whose projection is outside current face.
	//		getNextValidFloatingPoint(mesh_data, floatingPointIndex, cutPath, faceID, curvePointArray, initialPath, cuttingPlanePoint, cuttingPlaneNormal, projectDirection);

	//		//after first floating point that is outside current face is found, cuts face

	//		CutPoint cp;
	//		bool is_cut = getCutPoint( sourceID,  mesh_data , cuttingPlanePoint, cuttingPlaneNormal, faceID, cp);
	//		if(!is_cut)
	//			continue;
	//		else
	//		{
	//			cutPath.push_back(cp);
	//			return;
	//		}
	//	}	
	//	else
	//	{
	//		CutPoint cp;
	//		//if current section is not straight, update projection direction and cutting plane
	//		if(!isPathStraight(curvePointArray[floatingPointIndex - 1], curvePointArray[floatingPointIndex], curvePointArray[floatingPointIndex + 1]))
	//		{
	//			projectDirection = getCurveNormal( curvePointArray[floatingPointIndex-1], curvePointArray[floatingPointIndex], curvePointArray[floatingPointIndex+1] );
	//			buildCuttingPlane(curvePointArray[floatingPointIndex-1],curvePointArray[floatingPointIndex],projectDirection, cuttingPlaneNormal, cuttingPlanePoint);
	//		}


	//		bool is_cut =  getCutPoint( sourceID,  mesh_data , cuttingPlanePoint, cuttingPlaneNormal, faceID, cp);
	//		if(!is_cut)
	//			continue;
	//		else
	//		{
	//	
	//			if( is_reach_destination( mesh_data, cp, initialPath[initialPath.size()-1].vertexID) )
	//			{
	//				cutPath.push_back(cp);
	//				return;
	//			}
	//			
	//			if(cutPathes[0].size() == 1)
	//				cutPathes[0].push_back(cp);
	//			else
	//			{
	//				cutPathes[1].push_back(cp);
	//				break;
	//			}
	//		}
	//	}			
	//}	
	//
	//bool is_project_face_reached = false;

	//do
	//{	
	//	CutPoint lastCutPoint;
	//	lastCutPoint = cutPathes[0][cutPathes[0].size() - 1];

	//	//check the cutPoint type(edgeCut or vertexCut) and evoke cooresponding cut function
	//	if(lastCutPoint.vertexID < 0)
	//	{//Last cutPoint is an edge cut point
	//		edgeCut(mesh_data, floatingPointIndex, cutPathes[0], curvePointArray, initialPath, cuttingPlanePoint, cuttingPlaneNormal, projectDirection);
	//	}
	//	else
	//	{//Last cutPoint is a vertex cut point	
	//		vertexCut(mesh_data, floatingPointIndex, cutPathes[0], curvePointArray, initialPath, cuttingPlanePoint, cuttingPlaneNormal, projectDirection);
	//	}


	//	lastCutPoint = cutPathes[0][cutPathes[0].size() - 1];
	//	int lastFaceID = lastCutPoint.cutFaceID;
	//	if( is_reach_destination( mesh_data, lastCutPoint, initialPath[initialPath.size()-1].vertexID) )
	//	{
	//		cutPath = cutPathes[0];
	//		return;
	//	}

	//	//check if currentFace contains the projection of the floating point
	//	Eigen::Vector3f projection;
	//	bool is_in_current_face = projectFloatingPointOnMesh(mesh_data, projectDirection,  curvePointArray[floatingPointIndex], projection, lastFaceID);

	//	if(is_in_current_face)
	//	{
	//		for( int a = 0; a < initialPath[floatingPointIndex].neighbourFacesID.size(); a++)
	//		{
	//			if(	lastFaceID == initialPath[floatingPointIndex].neighbourFacesID[a])
	//			{
	//				is_project_face_reached = true;
	//				cutPath = cutPathes[0];
	//				break;
	//			}
	//		}
	//		if(	is_project_face_reached )
	//			break;
	//	}	
	//	
	//	lastCutPoint = cutPathes[1][cutPathes[1].size() - 1];
	//	if(lastCutPoint.vertexID < 0)
	//	{//Last cutPoint is an edge cut point
	//		edgeCut(mesh_data, floatingPointIndex, cutPathes[1], curvePointArray, initialPath, cuttingPlanePoint, cuttingPlaneNormal, projectDirection);
	//	}
	//	else
	//	{//Last cutPoint is a vertex cut point	
	//		vertexCut(mesh_data, floatingPointIndex, cutPathes[1], curvePointArray, initialPath, cuttingPlanePoint, cuttingPlaneNormal, projectDirection);
	//	}


	//	lastCutPoint = cutPathes[1][cutPathes[1].size() - 1];
	//	lastFaceID = lastCutPoint.cutFaceID;	
	//	if( is_reach_destination( mesh_data, lastCutPoint, initialPath[initialPath.size()-1].vertexID) )
	//	{
	//		cutPath = cutPathes[1];
	//		return;
	//	}

	//	//check if currentFace contains the projection of the floating point

	//	is_in_current_face = projectFloatingPointOnMesh(mesh_data, projectDirection,  curvePointArray[floatingPointIndex], projection, lastFaceID);

	//	if(is_in_current_face)
	//	{
	//		for( int a = 0; a < initialPath[floatingPointIndex].neighbourFacesID.size(); a++)
	//		{
	//			if(	lastFaceID == initialPath[floatingPointIndex].neighbourFacesID[a])
	//			{
	//				is_project_face_reached = true;
	//				cutPath = cutPathes[1];
	//				break;
	//			}
	//		}
	//		if(	is_project_face_reached )
	//			break;
	//	}	
	//}while(!is_project_face_reached);	
}
