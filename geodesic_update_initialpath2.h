#include "geodesic_base_elements.h"
#include "geodesic_mesh_data.h"


//#include <Eigen/Core>
//#include <Eigen/Eigen>
//#include <Eigen/Eigenvalues>
//#include <Eigen/Sparse>

#include <iostream>
#include <fstream>

#include "geodesic_project_utilities.h"
#define PI 3.14159265354 


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

void updata_path(MeshData * mesh_data, std::vector<initPoint> & initial_path, std::vector<Point> & curvePointArray ){
	//define variables
	//Eigen::Vector3f cuttingPlaneNormal;
	//Eigen::Vector3f cuttingPlanePoint;
	//Eigen::Vector3f projectDirection;

	//bool is_current_curve_section_straight = false;
	//bool is_in_current_face = false;
	//int floatingPointIndex = 1;
	//std::vector<int> one_ring_faces;

	////two pathes for two directions at source
	//std::vector< CutPoint > projectedPath;

	////put the source into projectedPath 
	//CutPoint source;
	//int sourceID = initial_path[0].vertexID;
	//int destID = initial_path[initial_path.size() - 1].vertexID;
	//source.x = curvePointArray[0].x;
	//source.y = curvePointArray[0].y;
	//source.z = curvePointArray[0].z;
	//source.n_x = projectDirection[0];
	//source.n_y = projectDirection[1];
	//source.n_z = projectDirection[2];
	//std::vector<int> sourceOneRangFace;
	//source.neighbourFaces = mesh_data->one_ring_face_array[sourceID];
	//projectedPath.push_back(source);


	//Eigen::Vector3f projectionOfCurvePoint;

	////initialize the default cutting which is defined by normal at source and vector from source to 2nd floating point
	//projectDirection[0] = mesh_data->getNormal(initial_path[0].vertexID)[0];
	//projectDirection[1] = mesh_data->getNormal(initial_path[0].vertexID)[1];
	//projectDirection[2] = mesh_data->getNormal(initial_path[0].vertexID)[2];
	//buildCuttingPlane(curvePointArray[floatingPointIndex-1],curvePointArray[floatingPointIndex],projectDirection, cuttingPlaneNormal, cuttingPlanePoint);







	/////////////////////////////
	//////////// Step 1 /////////
	/////////////////////////////

	////find first point that is not in the straight section and calculate project direction
	//for(; floatingPointIndex < curvePointArray.size()-1;++floatingPointIndex)
	//{
	//	if(!isPathStraight(curvePointArray[floatingPointIndex-1], curvePointArray[floatingPointIndex], curvePointArray[floatingPointIndex+1]))
	//	{
	//		//calculate Css of the first non-straight piece-wise float point
	//		projectDirection = getCurveNormal( curvePointArray[floatingPointIndex-1], curvePointArray[floatingPointIndex], curvePointArray[floatingPointIndex+1] );
	//		//build 1st cutting plane
	//		buildCuttingPlane(curvePointArray[floatingPointIndex-1],curvePointArray[floatingPointIndex],projectDirection, cuttingPlaneNormal, cuttingPlanePoint);
	//		break;
	//	}
	//}


	/////////////////////////////
	//////////// Step 2 /////////
	/////////////////////////////
	////Start cutting from source

	////reset index to 1
	//floatingPointIndex = 1;

	//bool is_reach_desti = false;
	//bool is_cut_from_source = true;




	//for(; floatingPointIndex < curvePointArray.size()-1;){

	//	bool is_continue = false;


	//	//////////////////////////////////////////////////////
	//	//start from source
	//	if(is_cut_from_source)
	//	{
	//		is_cut_from_source = false;
	//		sourceCut(mesh_data, floatingPointIndex, projectedPath, curvePointArray, initial_path, cuttingPlanePoint, cuttingPlaneNormal, projectDirection);
	//	}
	//	else
	//	{
	//		if(projectedPath[projectedPath.size() - 1].vertexID < 0)
	//		{
	//			edgeCut(mesh_data, floatingPointIndex, projectedPath, curvePointArray, initial_path, cuttingPlanePoint, cuttingPlaneNormal, projectDirection);
	//		}
	//		else
	//		{
	//			vertexCut(mesh_data, floatingPointIndex, projectedPath, curvePointArray, initial_path, cuttingPlanePoint, cuttingPlaneNormal, projectDirection);
	//		}
	//	}
	//}

	//updataPathInfo( mesh_data, projectedPath ,  curvePointArray,  initial_path );


}