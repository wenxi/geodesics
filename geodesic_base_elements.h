#ifndef geodesic_base_elements_h
#define geodesic_base_elements_h
//#include "geodesic_mesh_data.h"

#include <vector>
#include <Eigen/Core>
#include <Eigen/Eigen>
#include <Eigen/Eigenvalues>
#include <Eigen/Sparse>
#ifndef MAX_DIST
#define MAX_DIST 999999
#endif	

//
//struct Point{
// float x;
// float y;
// float z;
//};

float angle(Point first_point, Point second_point, Point center_point){
	//Point vector0, vector1;
	Eigen::Vector3f vector0,vector1;
	vector0[0] = first_point.x - center_point.x;
	vector0[1] = first_point.y - center_point.y;
	vector0[2] = first_point.z - center_point.z;

	vector1[0]= second_point.x - center_point.x;
	vector1[1] = second_point.y - center_point.y;
	vector1[2] = second_point.z - center_point.z;

	vector0.normalized();
	vector1.normalized();
	

	//float a = vector0.x * vector1.x + vector0.y * vector1.y + vector0.z * vector1.z;
	//float b = sqrt(vector0.x * vector0.x + vector0.y * vector0.y + vector0.z * vector0.z);
	//float c = sqrt(vector1.x * vector1.x + vector1.y * vector1.y + vector1.z * vector1.z);

	float theta = acos(vector0.dot(vector1) / (vector0.norm() * vector1.norm()));
	return theta;
}






class Mesh_Point
{
public:
	Mesh_Point():distance_to_source(999999),ID(0) {}

public:
	Point point;
	std::vector<int> neighbor_vertex_ids;
	std::vector<int> neighbor_face_ids;
	std::vector<Point> geodesic_path; // real geodesic path.
	std::vector<float> node_parameter;
	std::vector<int> initial_path;	// shortest path goes through edges.
	float distance_to_source;
	int ID;
//	int parent_id;
};




//

//template <typename  Block_type>
//Block_type * allocateMemory(Block_type mytype, int block_count)
//{
//mytype = new Block_type[block_count];
//return mytype;
//}




#endif	
