#include "geodesic_base_elements.h"
#include "geodesic_mesh_data.h"


//#include <Eigen/Core>
//#include <Eigen/Eigen>
//#include <Eigen/Eigenvalues>
//#include <Eigen/Sparse>

#include <iostream>
#include <fstream>

#include "geodesic_project_utilities.h"
#include "geodesic_update_cut.h"


#define PI 3.14159265354 


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////

void updata_path(MeshData * mesh_data, std::vector<initPoint> & initial_path, std::vector<Point> & curvePointArray, bool is_execute_cut ){
	//define variables
	Eigen::Vector3f cutPlaneNormal;
	Eigen::Vector3f pointOnCutPlane;

	std::vector<initPoint> projectedPath;
	std::vector<initPoint> new_initial_path;;

	std::vector<Eigen::Vector3f> projectDirectionArray;
	initial_path[0].flag = 1;
	initial_path[initial_path.size() - 1].flag = 1;

	float tol = 0.0001f;

	Eigen::Vector3f projectDirection;
	int destinationID = initial_path[initial_path.size() - 1].vertexID;

	int sourceID = initial_path[0].vertexID;
	Eigen::Vector3f sourceNormal;


	sourceNormal[0] = mesh_data->getVertexNormal(sourceID)[0];
	sourceNormal[1] = mesh_data->getVertexNormal(sourceID)[1];
	sourceNormal[2] = mesh_data->getVertexNormal(sourceID)[2];


	//sourceNormal[0] = initial_path[0].n_x;
	//sourceNormal[1] = initial_path[0].n_y;
	//sourceNormal[2] = initial_path[0].n_z;
	projectDirectionArray.push_back(sourceNormal);

	initial_path[0].location = initial_path[0].neighbourFacesID;
	initial_path[initial_path.size() - 1].location = initial_path[initial_path.size() - 1].neighbourFacesID;

	initPoint sourcePoint;
	sourcePoint.x = curvePointArray[0].x;
	sourcePoint.y = curvePointArray[0].y;
	sourcePoint.z = curvePointArray[0].z;
	sourcePoint.n_x = initial_path[0].n_x;
	sourcePoint.n_y = initial_path[0].n_y;
	sourcePoint.n_z = initial_path[0].n_z;
	sourcePoint.flag = 1;
	sourcePoint.location = initial_path[0].neighbourFacesID;
	sourcePoint.neighbourFacesID = initial_path[0].neighbourFacesID;
	sourcePoint.vertexID = sourceID;

	projectedPath.push_back(sourcePoint);







	//////////////////////////////////////////////////////////////////
	//// STEP 1, Project floating points on their individual face/////
	//////////////////////////////////////////////////////////////////

	for( int floatingPointIndex = 1; floatingPointIndex < curvePointArray.size()-1;++floatingPointIndex)
	{
		//calculate project direction of the first floating point 
		//getPorjectDirection(mesh_data, initial_path,curvePointArray, floatingPointIndex ,tol, projectDirection);

		//Project floating onto a face
		try
		{
			//getPorjectPointOfFloatingPoint(mesh_data, initial_path, curvePointArray, floatingPointIndex , tol, projectDirection, projectDirectionArray);
			getPorjectPointOfFloatingPoint(mesh_data, initial_path, curvePointArray, floatingPointIndex , tol, projectDirection, projectDirectionArray);
		}
		catch(...)
		{
			std::cout<<"Cannot project point ID = "<<floatingPointIndex<<" into its one ring faces"<<std::endl;	
		}
	}




	/////////////////////////////////////////////////////////////
	//// STEP 2, Cut mesh between every two floating point  /////
	/////////////////////////////////////////////////////////////

	//int floatingPointIndex = 1; // start from one next to startPoint.	

	if(is_execute_cut)
	{
		bool ic_cut_valid = true;
		for( int floatingPointIndex = 1; floatingPointIndex < curvePointArray.size();++floatingPointIndex)
		{	
			getCutPlane(mesh_data, initial_path, curvePointArray, projectedPath ,floatingPointIndex , projectDirection, ic_cut_valid, cutPlaneNormal,pointOnCutPlane);
			ic_cut_valid = CutBetweenTwoPoints(mesh_data, projectedPath, initial_path[floatingPointIndex], cutPlaneNormal, pointOnCutPlane);
			if(ic_cut_valid)
				projectedPath.pop_back();
		}


		Eigen::Vector3f destinationNormal;
		destinationNormal[0] = mesh_data->getVertexNormal(sourceID)[0];
		destinationNormal[1] = mesh_data->getVertexNormal(sourceID)[1];
		destinationNormal[2] = mesh_data->getVertexNormal(sourceID)[2];
		projectDirectionArray.push_back(destinationNormal);

		initPoint destinationPoint;
		destinationPoint.x = curvePointArray[curvePointArray.size() - 1].x;
		destinationPoint.y = curvePointArray[curvePointArray.size() - 1].y;
		destinationPoint.z = curvePointArray[curvePointArray.size() - 1].z;
		destinationPoint.n_x = mesh_data->getVertexNormal(destinationID)[0];
		destinationPoint.n_y = mesh_data->getVertexNormal(destinationID)[1];
		destinationPoint.n_z = mesh_data->getVertexNormal(destinationID)[2];
		destinationPoint.flag = 1;
		destinationPoint.location = initial_path[initial_path.size() - 1].neighbourFacesID;
		destinationPoint.neighbourFacesID = initial_path[initial_path.size() - 1].neighbourFacesID;
		destinationPoint.vertexID = destinationID;

		projectedPath.push_back(destinationPoint);


		mesh_data->projected_path_array[ initial_path[initial_path.size() - 1].vertexID ] = projectedPath;
	}


	for( int a = 1; a < curvePointArray.size()-1; ++a)
	{
		curvePointArray[a].x = initial_path[a].x;
		curvePointArray[a].y = initial_path[a].y;
		curvePointArray[a].z = initial_path[a].z;
	}
}




void ProjectPath(MeshData * mesh_data, std::vector<initPoint> & initial_path, std::vector<Point> & curvePointArray)
{
	Eigen::Vector3f cutPlaneNormal;
	Eigen::Vector3f pointOnCutPlane;

	std::vector<initPoint> projectedPath;
	std::vector<initPoint> new_initial_path;;

	std::vector<Eigen::Vector3f> projectDirectionArray;
	initial_path[0].flag = 1;
	initial_path[initial_path.size() - 1].flag = 1;

	float tol = 0.0001f;

	Eigen::Vector3f projectDirection;
	int sourceID = initial_path[0].vertexID;
	int destinationID = initial_path[initial_path.size() - 1].vertexID;

	Eigen::Vector3f sourceNormal;
	sourceNormal[0] = mesh_data->getVertexNormal(sourceID)[0];
	sourceNormal[1] = mesh_data->getVertexNormal(sourceID)[1];
	sourceNormal[2] = mesh_data->getVertexNormal(sourceID)[2];
	projectDirectionArray.push_back(sourceNormal);


	initial_path[0].location = initial_path[0].neighbourFacesID;
	initial_path[initial_path.size() - 1].location = initial_path[initial_path.size() - 1].neighbourFacesID;

	initPoint sourcePoint;
	sourcePoint.x = curvePointArray[0].x;
	sourcePoint.y = curvePointArray[0].y;
	sourcePoint.z = curvePointArray[0].z;
	sourcePoint.n_x = mesh_data->getVertexNormal(sourceID)[0];
	sourcePoint.n_y = mesh_data->getVertexNormal(sourceID)[1];
	sourcePoint.n_z = mesh_data->getVertexNormal(sourceID)[2];
	sourcePoint.flag = 1;
	sourcePoint.location = initial_path[0].neighbourFacesID;
	sourcePoint.neighbourFacesID = initial_path[0].neighbourFacesID;
	sourcePoint.vertexID = sourceID;

	projectedPath.push_back(sourcePoint);




	bool ic_cut_valid = true;
	for( int floatingPointIndex = 1; floatingPointIndex < curvePointArray.size();++floatingPointIndex)
	{	
		getCutPlane(mesh_data, initial_path, curvePointArray, projectedPath ,floatingPointIndex , projectDirection, ic_cut_valid, cutPlaneNormal,pointOnCutPlane);
		ic_cut_valid = CutBetweenTwoPoints(mesh_data, projectedPath, initial_path[floatingPointIndex], cutPlaneNormal, pointOnCutPlane);
		if(ic_cut_valid)
			projectedPath.pop_back();
	}


	Eigen::Vector3f destinationNormal;
	destinationNormal[0] = mesh_data->getVertexNormal(sourceID)[0];
	destinationNormal[1] = mesh_data->getVertexNormal(sourceID)[1];
	destinationNormal[2] = mesh_data->getVertexNormal(sourceID)[2];
	projectDirectionArray.push_back(destinationNormal);

	initPoint destinationPoint;
	destinationPoint.x = curvePointArray[curvePointArray.size() - 1].x;
	destinationPoint.y = curvePointArray[curvePointArray.size() - 1].y;
	destinationPoint.z = curvePointArray[curvePointArray.size() - 1].z;
	destinationPoint.n_x = mesh_data->getVertexNormal(destinationID)[0];
	destinationPoint.n_y = mesh_data->getVertexNormal(destinationID)[1];
	destinationPoint.n_z = mesh_data->getVertexNormal(destinationID)[2];
	destinationPoint.flag = 1;
	destinationPoint.location = initial_path[initial_path.size() - 1].neighbourFacesID;
	destinationPoint.neighbourFacesID = initial_path[initial_path.size() - 1].neighbourFacesID;
	destinationPoint.vertexID = destinationID;

	projectedPath.push_back(destinationPoint);


	mesh_data->projected_path_array[ initial_path[initial_path.size() - 1].vertexID ] = projectedPath;


}

void checkDuplicatedPoint(MeshData * mesh_data, int index, float tol)
{
	std::vector<initPoint> tmpArray;
	tmpArray.push_back(mesh_data->initial_path_array[ index ][0]);

	//for(int i = 1; i < mesh_data->geodesic_path_array[ index ].size();i++)
	//{
	//	if( Distl(mesh_data->geodesic_path_array[index][i-1],mesh_data->geodesic_path_array[index][i]) < tol )
	//	{
	//		continue;
	//	}
	//	else
	//	{
	//		tmpArray.push_back(mesh_data->initial_path_array[ index ][i]);
	//	}
	//}

	//mesh_data->initial_path_array[ index ] = tmpArray;
	//mesh_data->geodesic_path_array[ index ].clear();


	//for( int a = 0; a < tmpArray.size();++a)
	//{
	//	Point point;
	//	point.x = tmpArray[a].x;
	//	point.y = tmpArray[a].y;
	//	point.z = tmpArray[a].z;
	//	mesh_data->geodesic_path_array[ index ].push_back(point);
	//}


	for(int i = 1; i < mesh_data->projected_path_array[ index ].size();i++)
	{
		if( Distl(mesh_data->projected_path_array[index][i-1],mesh_data->projected_path_array[index][i]) < tol )
		{
			continue;
		}
		else
		{
			tmpArray.push_back(mesh_data->projected_path_array[ index ][i]);
		}
	}
	mesh_data->projected_path_array[ index ] = tmpArray;

}