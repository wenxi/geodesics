#include "geodesic_base_elements.h"
#include "geodesic_mesh_data.h"


#include <Eigen/Core>
#include <Eigen/Eigen>
#include <Eigen/Eigenvalues>
#include <Eigen/Sparse>

#include <iostream>
#include <fstream>
#define PI 3.14159265354 

template <class T>
void append_array(std::vector<T> &a, std::vector<T> b){
	for(int i = 0;i < b.size();++i){
		bool is_exist = false;
		for( int j = 0; j < a.size();++j ){
			if(a[j] == b[i]){
				is_exist = true;
				break;
			}
		}
		if(!is_exist){
			a.push_back(b[i]);
		}
	}
}

template <class T>
 std::vector<T> get_boarder(std::vector<T> a, std::vector<T> b){
	//a: visited list,  b: input for each element in a's edge, return: new boarder
	 std::vector<T> c;
	 for(int i = 0;i < b.size();++i){
		bool is_exist = false;
		for( int j = 0; j < a.size();++j ){
			if(a[j] == b[i]){
				is_exist = true;
				break;
			}
		}
		if(!is_exist){
			c.push_back(a[i]);
		}
	}
	return c;
}

template <class T>
 std::vector<T> expand_ring(std::vector<T> wave_crest, std::vector<T> wave_tail, std::vector<T> new_ring){
	std::vector<T> wave_front;
	for(int i = 0; i < new_ring.size();++i){
		bool is_exist = false;
		for(int j = 0; j < wave_crest.size();++j){
			if(new_ring[i] == wave_crest[j]){
				is_exist = true;
				break;
			}
		}

		for(int j = 0; j < wave_tail.size();++j){
			if(new_ring[i] == wave_tail[j]){
				is_exist = true;
				break;
			}
		}

		if(!is_exist){
			wave_front.push_back(new_ring[i]);
		}
	}
	return wave_front;
 }


void find_closest_point(MeshData * mesh_data,std::vector<int> &initial_path, std::vector<Point> curvePointArray,float regression_ratio){
	
	if(initial_path.size() != curvePointArray.size()){
		std::cout<<"initial path and geodesic path doesnot match!!!"<<std::endl;
	}else{
		int k = 1;
		ANNpoint queryPoint;
		queryPoint = annAllocPt(3);
		ANNidxArray nnIdx;
		ANNdistArray dists;
		nnIdx = new ANNidx[1];
		dists = new ANNdist[1];
		std::vector<int> new_initial_path;
		new_initial_path.push_back(initial_path[0]);

		for(unsigned int i = 1; i < curvePointArray.size();++i){
			queryPoint[0] = curvePointArray[i].x;
			queryPoint[1] = curvePointArray[i].y;
			queryPoint[2] = curvePointArray[i].z;
			ANNkd_tree *meshKdTree;

			std::vector<int> wave_front;
			std::vector<int> wave_crest;
			std::vector<int> wave_tail;
			wave_tail.push_back(initial_path[i]);
			wave_crest = mesh_data->one_ring_vertex_array[initial_path[i]];

			int closest_point_index = 0;
			float closest_point_distance = 99999.9f;
			bool is_finished = false;

			do{
				ANNpointArray pointArray = annAllocPts((int)(wave_crest.size()+ wave_tail.size()),3);
				for(int j = 0; j < wave_crest.size();++j){
					Point point = mesh_data->getPoint(wave_crest[j]);
					pointArray[j][0] = point.x;
					pointArray[j][1] = point.y;
					pointArray[j][2] = point.z;
				}

				for(size_t j = wave_crest.size(); j < wave_crest.size()+ wave_tail.size();++j){
					Point point = mesh_data->getPoint(wave_tail[j-wave_crest.size()]);
					pointArray[j-wave_crest.size()][0] = point.x;
					pointArray[j-wave_crest.size()][1] = point.y;
					pointArray[j-wave_crest.size()][2] = point.z;
				}
				//int leafsize = 1;	
				//meshKdTree= new ANNkd_tree(					// build search structure
				//	pointArray,					// the data points
				//	(int)wave_crest.size(),						// number of points
				//	3,
				//	leafsize,
				//	ANN_KD_SUGGEST);

				//meshKdTree->annkSearch(queryPoint,1,nnIdx,dists,0.0000);
				if(dists[0] < closest_point_distance){
					closest_point_index = nnIdx[0];
					closest_point_distance = (float)dists[0];	
					std::vector<int> one_ring;
					for(int j = 0; j < wave_crest.size();++j){
						append_array<int>(one_ring,mesh_data->one_ring_vertex_array[wave_crest[j]]);
					}
					wave_front = expand_ring<int>(wave_crest,wave_tail,one_ring);
					wave_tail = wave_crest;
					wave_crest = wave_front;
					wave_front.clear();
				}else{
					new_initial_path.push_back(wave_crest[closest_point_index]);
					is_finished = true;
				}
				closest_point_index = 0;
			}while(!is_finished);
		}

		new_initial_path.pop_back();
		new_initial_path.push_back(initial_path[initial_path.size() - 1]);
		initial_path = new_initial_path;
	}
}