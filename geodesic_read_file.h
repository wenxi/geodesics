//#ifndef geodesic_read_file_h
//#define geodesic_read_file_h
//
//#include <assert.h>
//#include <math.h>
//#include <limits>
//#include <fstream>
//#include "geodesic_mesh_data.h"
//
//
//
//#include <ANN/ANN.h>
//#include <ANN/ANNperf.h>
//#include <ANN/ANNx.h>
//
//
//
//
//
//namespace geodesic_file{
//
//#ifndef M_PI
//#define M_PI 3.14159265358979323846
//#endif
//
//	template<class Points, class Faces>
//	inline bool read_mesh_from_file(char * filename, MeshData& mesh_data){
//
//
//		std::ifstream file(filename);
//		assert(file.is_open());
//		if(!file.is_open()) return false;
//
//		int num_points;
//		file >> num_points;
//		assert(num_points>=3);
//
//		int num_faces;
//		file >> num_faces;
//
//		mesh_data.init(num_points, num_faces);
//
//		float* points = mesh_data.getVertex(0);
//		int* faces = mesh_data.getFace(0);
//
//
//
//		int dim = 3;
//		ANNpointArray pointArray = annAllocPts(num_points,dim);
//
//
//		//unsigned num_normals;
//		//file >> num_normals;
//
//		//points.resize(num_points*3);
//		for(int index = 0; index < num_points; ++index){
//			file >> *points;
//			pointArray[index][0] =  points[0];
//			++points;
//			file >> *points;
//			pointArray[index][1] =  points[0];
//			++points;
//			file >> *points;
//			pointArray[index][2] =  points[0];
//			++points;
//
//		}
//		//		assert(mesh_data.numVertices() % 3 == 0);
//
//
//		for(int index = 0; index < num_faces; ++index){
//			file >> *faces; ++faces;
//			file >> *faces; ++faces;
//			file >> *faces; ++faces;
//		}
//
//	
//
//
//
//
//
//		int leafsize = 1;
//
//		
//		/*ANNkd_tree *meshKdTree*/ 	mesh_data.meshKdTree= new ANNkd_tree(					// build search structure
//			pointArray,					// the data points
//			num_points,						// number of points
//			dim,
//			leafsize,
//			ANN_KD_SUGGEST);	
//
//
//
//
//
//
//
//
//
//		for(int i = 0; i < num_points;i++){
//
//
//
//			mesh_data.one_ring_array.push_back(mesh_data.get_one_ring_neighbor(i));
//		}
//
//
//
//
//
//
//
//
//		file.close();
//
//		return true;
//	}
//
//
//
//
//
//
//
//
//}
//
//
//#endif