#ifndef geodesic_project_path_h
#define geodesic_project_path_h

#include "geodesic_mesh_data.h"
#include "geodesic_base_elements.h"

#include <Eigen/Core>
#include <Eigen/Eigen>
#include <Eigen/Eigenvalues>
#include <Eigen/Sparse>

//
//float Dist(Point point0, Point point1){
//	 return (point0.x - point1.x) * (point0.x - point1.x) + (point0.y - point1.y) * (point0.y - point1.y) + (point0.z - point1.z) * (point0.z - point1.z);
//}

void assignEigenVectorValue(Point point, Eigen::Vector3f &ePoint)
{
	ePoint[0] = point.x;
	ePoint[1] = point.y;
	ePoint[2] = point.z;
}

////////////////////////////////////////////////////////////////////////////////////////////////

int getAdjacentFace(MeshData * mesh_data,int currentFaceId, int &edgePoint0, int &edgePoint1){

	std::vector<int> oneRingVertex_EdgePoint0;
	std::vector<int> oneRingFace_EdgePoint0;
	mesh_data->get_one_ring_neighbor(edgePoint0, oneRingVertex_EdgePoint0, oneRingFace_EdgePoint0);

	std::vector<int> oneRingVertex_EdgePoint1;
	std::vector<int> oneRingFace_EdgePoint1;
	mesh_data->get_one_ring_neighbor(edgePoint1, oneRingVertex_EdgePoint1, oneRingFace_EdgePoint1);

	std::vector<int> edgePointsSharedFaces;
	for(int i = 0; i < oneRingFace_EdgePoint0.size();++i){
		for(int j = 0; j < oneRingFace_EdgePoint1.size();++j){
			if(oneRingFace_EdgePoint0[i] == oneRingFace_EdgePoint1[j]){
				edgePointsSharedFaces.push_back(oneRingFace_EdgePoint1[j]);
			}
		}
	}


	for(int j = 0; j < edgePointsSharedFaces.size();++j){
		if(currentFaceId == edgePointsSharedFaces[j]){
			edgePointsSharedFaces.erase(edgePointsSharedFaces.begin() + j);
			break;
		}
	}

	if(edgePointsSharedFaces.size() > 1){std::cout<<"error in finding adjacent face"<<std::endl;}
	return edgePointsSharedFaces[0];
}

////////////////////////////////////////////////////////////////////////////////////////////////

void getClosestFace(MeshData * mesh_data,Point point, int initPathPointId, int &closestFaceId){
	std::vector<int> oneRingVertex;
	std::vector<int> oneRingFace;
	mesh_data->get_one_ring_neighbor(initPathPointId, oneRingVertex, oneRingFace);
	float min_dist = MAX_DIST;
	for(int i = 0; i < oneRingFace.size();++i){
		int * containedPointIdArray = mesh_data->getFace(oneRingFace[i]);
		float * point0 = mesh_data->getVertex(containedPointIdArray[0]);	
		float * point1 = mesh_data->getVertex(containedPointIdArray[1]);		
		float * point2 = mesh_data->getVertex(containedPointIdArray[2]);

		Eigen::Vector3f v0,v1,v2;
		float * normal = mesh_data->getVertexNormal(oneRingFace[i]);


		v0[0] = point1[0] - point0[0];
		v0[1] = point1[1] - point0[1];
		v0[2] = point1[2] - point0[2];

		v1[0] = point2[0] - point0[0];
		v1[1] = point2[1] - point0[1];
		v1[2] = point2[2] - point0[2];

	}
}

////////////////////////////////////////////////////////////////////////////////////////////////

bool isOnDifferentTriangle(MeshData * mesh_data, int faceOfPoint0ID, Point point0, Point point1)
{
	int *facePointID = mesh_data->getFace(faceOfPoint0ID);
	Point facePoint0 = mesh_data->getPoint(facePointID[0]);
	Point facePoint1 = mesh_data->getPoint(facePointID[1]);
	Point facePoint2 = mesh_data->getPoint(facePointID[2]);
	Eigen::Vector3f eFacePoint0,eFacePoint1,eFacePoint2,ePoint0,ePoint1,a,b,c,u,v,w;
	assignEigenVectorValue(facePoint0,eFacePoint0);
	assignEigenVectorValue(facePoint1,eFacePoint1);
	assignEigenVectorValue(facePoint2,eFacePoint2);
	assignEigenVectorValue(point0,ePoint0);
	assignEigenVectorValue(point1,ePoint1);

	a = eFacePoint0 - ePoint1;
	b = eFacePoint1 - ePoint1;
	c = eFacePoint2 - ePoint1;

	u = a.cross(b);
	v = b.cross(c);
	w = c.cross(a);

	if( u.dot(v) >= 0 && u.dot(w) >= 0 )
	{
		return true;
	}
	else
	{
		return false;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////

Point projectPointToLine(Point point, Point linePoint0, Point linePoint1)
{
	Point projectPoint;
	Eigen::Vector3f ePoint,eLinePoint0,eLinePoint1,a,b,c,projectionVector;
	ePoint[0] = point.x;
	ePoint[1] = point.y;
	ePoint[2] = point.z;
	eLinePoint0[0] = linePoint0.x; 
	eLinePoint0[1] = linePoint0.y;
	eLinePoint0[2] = linePoint0.z;
	eLinePoint1[0] = linePoint1.x; 
	eLinePoint1[1] = linePoint1.y;
	eLinePoint1[2] = linePoint1.z;

	a = ePoint - eLinePoint0;
	b = eLinePoint1 - eLinePoint0;

	float cosTheta = a.dot(b)/(a.norm() * b.norm());
	projectionVector = b.normalized() * ( a.norm() * cosTheta );
	projectPoint.x = linePoint0.x + projectionVector[0];
	projectPoint.y = linePoint0.y + projectionVector[1];
	projectPoint.z = linePoint0.z + projectionVector[2];
	return projectPoint;
}

////////////////////////////////////////////////////////////////////////////////////////////////

bool isPointOnEdge(Point point, Point edgePoint0, Point edgePoint1)
{
	Eigen::Vector3f a,b;
	a[0] = point.x - edgePoint0.x;
	a[1] = point.y - edgePoint0.y;
	a[2] = point.z - edgePoint0.z;
	b[0] = point.x - edgePoint1.x;
	b[1] = point.y - edgePoint1.y;
	b[2] = point.z - edgePoint1.z;

	float direction = a.dot(b);
	if( direction <= 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////

bool getIntersectedPoint(MeshData * mesh_data, int edgePointID0, int edgePointID1, Point & point0, Point & point1, Point & point2, Eigen::Vector3f &hitPoint){

	Eigen::Vector3f cutting_plane_normal,v0,v1;

	v0[0] = point0.x - point1.x;
	v0[1] = point0.y - point1.y;
	v0[2] = point0.z - point1.z;

	v1[0] = point2.x - point1.x;
	v1[1] = point2.y - point1.y;
	v1[2] = point2.z - point1.z;



	cutting_plane_normal = v0.cross(v1);
	cutting_plane_normal.normalized();

	std::vector<int> vertex_array;
	Eigen::Vector3f cutting_plane_Point;
	cutting_plane_Point[0] = point1.x;
	cutting_plane_Point[1] = point1.y;
	cutting_plane_Point[2] = point1.z;

	float * edgePoint0 = mesh_data->getVertex(edgePointID0);	
	float * edgePoint1 = mesh_data->getVertex(edgePointID1);
	//cutting place is defined by its normal and a point on the plane
	//intersected edge is defined by two points: p0,p1


	Point infPoint;
	infPoint.x = MAX_DIST;
	infPoint.y = MAX_DIST;
	infPoint.z = MAX_DIST;

	Eigen::Vector3f lineVector;
	lineVector[0] = edgePoint1[0] - edgePoint0[0];
	lineVector[1] = edgePoint1[1] - edgePoint0[1];
	lineVector[2] = edgePoint1[2] - edgePoint0[2];

	Eigen::Vector3f linePoint;
	linePoint[0] = edgePoint0[0];
	linePoint[1] = edgePoint0[1];
	linePoint[2] = edgePoint0[2];

	float t = cutting_plane_normal.dot(lineVector);

	if(t == 0){
		hitPoint[0] = infPoint.x;
		hitPoint[1] = infPoint.y;
		hitPoint[2] = infPoint.z;
		return false;
	}else{
		float a = ((cutting_plane_Point[0] - linePoint[0]) * cutting_plane_normal[0] + (cutting_plane_Point[1] - linePoint[1]) * cutting_plane_normal[1] + (cutting_plane_Point[2] - linePoint[2]) * cutting_plane_normal[2]) / t;  
		if(a > 1){
			hitPoint[0] = infPoint.x;
			hitPoint[1] = infPoint.y;
			hitPoint[2] = infPoint.z;
			return false;
		}else{
			hitPoint[0] = linePoint[0] + lineVector[0] * a;
			hitPoint[1] = linePoint[1] + lineVector[1] * a;
			hitPoint[2] = linePoint[2] + lineVector[2] * a;

			Eigen::Vector3f tv0,tv1,tv2,u,v,w;

			tv0[0] = hitPoint[0] - edgePoint0[0];
			tv0[1] = hitPoint[1] - edgePoint0[1];
			tv0[2] = hitPoint[2] - edgePoint0[2];
			tv1[0] = hitPoint[0] - edgePoint1[0];
			tv1[1] = hitPoint[1] - edgePoint1[1];
			tv1[2] = hitPoint[2] - edgePoint1[2];			

			float direction = tv0.dot(tv1);

			if( direction >= 0 ) 
			{
				hitPoint[0] = infPoint.x;
				hitPoint[1] = infPoint.y;
				hitPoint[2] = infPoint.z;
				return false;				
			}else
			{
				return true;
			}
			//


			//tv0[0] = point0.x - hitPoint[0];
			//tv0[1] = point0.y - hitPoint[1];
			//tv0[2] = point0.z - hitPoint[2];

			//tv1[0] = point1.x - hitPoint[0];
			//tv1[1] = point1.y - hitPoint[1];
			//tv1[2] = point1.z - hitPoint[2];

			//tv2[0] = point2.x - hitPoint[0];
			//tv2[1] = point2.y - hitPoint[1];
			//tv2[2] = point2.z - hitPoint[2];

			//u = tv1.cross(tv2);
			//v = tv2.cross(tv0);

			//if(u.dot(v) < 0.0f){
			//	hitPoint[0] = infPoint.x;
			//	hitPoint[1] = infPoint.y;
			//	hitPoint[2] = infPoint.z;
			//	return false;
			//}

			//w = tv0.cross(tv1);

			//if(u.dot(w) < 0.0f){
			//	hitPoint[0] = infPoint.x;
			//	hitPoint[1] = infPoint.y;
			//	hitPoint[2] = infPoint.z;
			//	return false;
			//}
			//return true;
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////

bool is_edge_visited(std::vector<std::vector<int>> &intersectedEdge, int edgePointID0, int edgePointID1)
{
	//	bool is_edge_visited = false;
	if( intersectedEdge.size() == 0 ) return false;
	for(int i = 0; i < intersectedEdge.size();i++)
	{
		if( intersectedEdge[i][0] == edgePointID0 && intersectedEdge[i][1] == edgePointID1 )
		{
			return true;
		}
		else if(intersectedEdge[i][1] == edgePointID0 && intersectedEdge[i][0] == edgePointID1 ) 
		{
			return true;
		}
	}

	return false;

}

////////////////////////////////////////////////////////////////////////////////////////////////

void project_geodesic_onto_mesh(MeshData * mesh_data, std::vector<initPoint> initial_path){
	std::vector<Point> geodesic_path;
	std::vector<Point> projected_curve;
	std::vector<initPoint> new_initial_path;

	geodesic_path = mesh_data->geodesic_path_array[initial_path[initial_path.size()-1]];
	new_initial_path.push_back(initial_path[0]);

	Point startPoint, destinationPoint;
	int startPointID = initial_path[0];
	int destinationPointID = initial_path[initial_path.size() - 1];
	startPoint = mesh_data->getPoint(startPointID);
	destinationPoint = mesh_data->getPoint(destinationPointID);

	int previousFaceID = 0;
	std::vector<std::vector<int>> intersectedEdge;
	projected_curve.push_back(geodesic_path[0]);
	for(int i = 1; i < geodesic_path.size()-1;++i){
		Point point0,point1,point2;

		point0 = geodesic_path[i-1];
		point1 = geodesic_path[i]; 
		point2 = geodesic_path[i+1];

		int initial_point0_id = initial_path[i-1];
		int initial_point1_id = initial_path[i];
		int initial_point2_id = initial_path[i+1];

		//		std::vector<int> face_array
		std::vector<int> vertex_id_array;
		std::vector<int> current_wave_boundary;
		std::vector<int> next_wave_boundary;
		mesh_data->get_one_ring_neighbor(initial_point1_id, vertex_id_array, current_wave_boundary); // wave boundary is faces
		bool is_end_pt_meet = false;
		bool isCurrentPointOnTheSameFace = false;
		int next_face_id = 0;
		int current_face_id = 0;
		int edgePoint0 = 0;
		int edgePoint1 = 0;
		bool isCurrentOneRingHasIntersect = false;
		float min_dist = Dist(mesh_data->getPoint(initial_point1_id),point1);
		int closestVertexID = initial_point1_id;
		//		bool is_end_pt_meet = false; 
		//std::vector<std::vector<int>> intersectedEdge;
		do{

			for(int fId = 0;fId < current_wave_boundary.size();++fId){
				//get a given point, look for the edge intersected with the cutting face that is not the point itself.
				//isCurrentPointOnTheSameFace = false;
				current_face_id = current_wave_boundary[fId];
				int * containedVertexIdArray = mesh_data->getFace(current_face_id);
				int containedVertexId0 = containedVertexIdArray[0];
				int containedVertexId1 = containedVertexIdArray[1];
				int containedVertexId2 = containedVertexIdArray[2];
				Point meshPoint0,meshPoint1,meshPoint2;

				meshPoint0 = mesh_data->getPoint(containedVertexId0);
				meshPoint1 = mesh_data->getPoint(containedVertexId1);
				meshPoint2 = mesh_data->getPoint(containedVertexId2);



				bool is_hit = false;
				bool is_valid = true;
				Eigen::Vector3f eHitPoint;
				Point hitPoint;

				if( is_edge_visited(intersectedEdge, containedVertexId0, containedVertexId1) == false )
				{
					is_hit = getIntersectedPoint(mesh_data,containedVertexId0, containedVertexId1, point0, point1, point2, eHitPoint);
					if(is_hit)
					{
						if( 0.001 > sqrt((startPoint.x - eHitPoint[0])*(startPoint.x - eHitPoint[0]) + (startPoint.y - eHitPoint[1])*(startPoint.y - eHitPoint[1]) + (startPoint.z - eHitPoint[2])*(startPoint.z - eHitPoint[2]))){
							is_valid = false;
						}
						else
						{
							is_valid = true;
							hitPoint.x = eHitPoint[0];
							hitPoint.y = eHitPoint[1];
							hitPoint.z = eHitPoint[2];

							bool isOnFirstEdge = isPointOnEdge(projected_curve[projected_curve.size() - 1], mesh_data->getPoint(containedVertexId2), mesh_data->getPoint(containedVertexId0));
							bool isOnSecondEdge = isPointOnEdge(projected_curve[projected_curve.size() - 1], mesh_data->getPoint(containedVertexId2), mesh_data->getPoint(containedVertexId1));

							if( isOnFirstEdge || isOnSecondEdge)
							{
								projectPointToLine(point1,point0,hitPoint);
								if(isOnDifferentTriangle(mesh_data, fId, point0,hitPoint))
								{
									isCurrentPointOnTheSameFace = true;
									break;
								}
							}else
							{
								is_valid = false;
							}
						}
						if(is_valid){
							Point tmpPoint;
							tmpPoint.x = eHitPoint[0];
							tmpPoint.y = eHitPoint[1];
							tmpPoint.z = eHitPoint[2];


							Point projectedPoint;
							projectedPoint = projectPointToLine(point1, projected_curve[projected_curve.size() - 1], tmpPoint);
							if ( isPointOnEdge(projectedPoint, projected_curve[projected_curve.size() - 1], tmpPoint) )
							{
								is_end_pt_meet = true;
								previousFaceID = current_face_id;
							}else{
								is_end_pt_meet = false;
								edgePoint0 = containedVertexId0;
								edgePoint1 = containedVertexId1;
								std::vector<int> edge;
								edge.push_back(edgePoint0);
								edge.push_back(edgePoint1);
								intersectedEdge.push_back(edge);

								if(Dist(tmpPoint,meshPoint0) < Dist(tmpPoint,meshPoint1)){
									new_initial_path.push_back(containedVertexId0);
								}
								else
								{
									new_initial_path.push_back(containedVertexId1);
								}

								projected_curve.push_back(tmpPoint);
								isCurrentOneRingHasIntersect = true;
								is_hit = false;
								break;
							}
						}
					}
				}

				if( is_edge_visited(intersectedEdge, containedVertexId1, containedVertexId2) == false )
				{
					is_hit = getIntersectedPoint(mesh_data,containedVertexId1, containedVertexId2, point0, point1, point2, eHitPoint);
					if(is_hit)
					{
						if( 0.001 > sqrt((startPoint.x - eHitPoint[0])*(startPoint.x - eHitPoint[0]) + (startPoint.y - eHitPoint[1])*(startPoint.y - eHitPoint[1]) + (startPoint.z - eHitPoint[2])*(startPoint.z - eHitPoint[2]))){
							is_valid = false;
						}
						else
						{
							is_valid = true;
							hitPoint.x = eHitPoint[0];
							hitPoint.y = eHitPoint[1];
							hitPoint.z = eHitPoint[2];
							bool isOnFirstEdge = isPointOnEdge(projected_curve[projected_curve.size() - 1], mesh_data->getPoint(containedVertexId0), mesh_data->getPoint(containedVertexId1));
							bool isOnSecondEdge = isPointOnEdge(projected_curve[projected_curve.size() - 1], mesh_data->getPoint(containedVertexId0), mesh_data->getPoint(containedVertexId2));

							if( isOnFirstEdge || isOnSecondEdge)
							{
								projectPointToLine(point1,point0,hitPoint);
								if(isOnDifferentTriangle(mesh_data, fId, point0,hitPoint))
								{
									isCurrentPointOnTheSameFace = true;
									break;
								}
							}else
							{
								is_valid = false;
							}
						}
						if(is_valid){
							Point tmpPoint;
							tmpPoint.x = eHitPoint[0];
							tmpPoint.y = eHitPoint[1];
							tmpPoint.z = eHitPoint[2];
							Point projectedPoint;
							projectedPoint = projectPointToLine(point1, projected_curve[projected_curve.size() - 1], tmpPoint);
							if ( isPointOnEdge(projectedPoint, projected_curve[projected_curve.size() - 1], tmpPoint) )
							{
								is_end_pt_meet = true;
								previousFaceID = current_face_id;
							}else{
								is_end_pt_meet = false;
								edgePoint0 = containedVertexId1;
								edgePoint1 = containedVertexId2;
								std::vector<int> edge;
								edge.push_back(edgePoint0);
								edge.push_back(edgePoint1);
								intersectedEdge.push_back(edge);

								if(Dist(tmpPoint,meshPoint0) < Dist(tmpPoint,meshPoint1)){
									new_initial_path.push_back(containedVertexId0);
								}
								else
								{
									new_initial_path.push_back(containedVertexId1);
								}

								projected_curve.push_back(tmpPoint);
								isCurrentOneRingHasIntersect = true;
								is_hit = false;
								break;
							}
						}
					}
				}

				if( is_edge_visited(intersectedEdge, containedVertexId2, containedVertexId0) == false )
				{
					is_hit = getIntersectedPoint(mesh_data,containedVertexId2, containedVertexId0, point0, point1, point2, eHitPoint);
					if(is_hit)
					{
						if( 0.001 > sqrt((startPoint.x - eHitPoint[0])*(startPoint.x - eHitPoint[0]) + (startPoint.y - eHitPoint[1])*(startPoint.y - eHitPoint[1]) + (startPoint.z - eHitPoint[2])*(startPoint.z - eHitPoint[2]))){
							is_valid = false;
						}
						else
						{
							is_valid = true;
							hitPoint.x = eHitPoint[0];
							hitPoint.y = eHitPoint[1];
							hitPoint.z = eHitPoint[2];
							bool isOnFirstEdge = isPointOnEdge(projected_curve[projected_curve.size() - 1], mesh_data->getPoint(containedVertexId1), mesh_data->getPoint(containedVertexId0));
							bool isOnSecondEdge = isPointOnEdge(projected_curve[projected_curve.size() - 1], mesh_data->getPoint(containedVertexId1), mesh_data->getPoint(containedVertexId2));

							if( isOnFirstEdge || isOnSecondEdge)
							{
								projectPointToLine(point1,point0,hitPoint);
								if(isOnDifferentTriangle(mesh_data, fId, point0,hitPoint))
								{
									isCurrentPointOnTheSameFace = true;
									break;
								}
							}else
							{
								is_valid = false;
							}
						}
						if(is_valid){
							Point tmpPoint;
							tmpPoint.x = eHitPoint[0];
							tmpPoint.y = eHitPoint[1];
							tmpPoint.z = eHitPoint[2];
							Point projectedPoint;
							projectedPoint = projectPointToLine(point1, projected_curve[projected_curve.size() - 1], tmpPoint);
							if ( isPointOnEdge(projectedPoint, projected_curve[projected_curve.size() - 1], tmpPoint) )
							{
								is_end_pt_meet = true;
								previousFaceID = current_face_id;
							}else{
								is_end_pt_meet = false;
								edgePoint0 = containedVertexId0;
								edgePoint1 = containedVertexId2;
								std::vector<int> edge;
								edge.push_back(edgePoint0);
								edge.push_back(edgePoint1);
								intersectedEdge.push_back(edge);

								if(Dist(tmpPoint,meshPoint0) < Dist(tmpPoint,meshPoint1)){
									new_initial_path.push_back(containedVertexId0);
								}
								else
								{
									new_initial_path.push_back(containedVertexId1);
								}

								projected_curve.push_back(tmpPoint);
								isCurrentOneRingHasIntersect = true;
								is_hit = false;
								break;
							}
						}
					}
				}
				//if(isCurrentOneRingHasIntersect)
				//{
				//	Point projectedPoint;
				//	projectedPoint = projectPointToLine(point1, projected_curve[projected_curve.size() - 1], projected_curve[projected_curve.size() - 2]);
				//	if ( isPointOnEdge(projectedPoint, projected_curve[projected_curve.size() - 1], projected_curve[projected_curve.size() - 2]) )
				//	{
				//		is_end_pt_meet = true;
				//	}else{
				//		is_end_pt_meet = false;
				//	}

				//}
				if( isCurrentPointOnTheSameFace )
				{
					isCurrentPointOnTheSameFace = false;
					is_end_pt_meet = false;
					break;
				}

			}

			//if(isCurrentOneRingHasIntersect == false)
			//{
			//	for( int pID = 0; pID < vertex_id_array.size(); ++pID )
			//	{
			//		if( min_dist > Dist(mesh_data->getPoint(vertex_id_array[pID]),point0) )
			//		{
			//			min_dist = Dist(mesh_data->getPoint(vertex_id_array[pID]),point0);
			//			closestVertexID = vertex_id_array[pID];
			//		}
			//	}

			//	new_initial_path.push_back(closestVertexID);
			//	projected_curve.push_back(mesh_data->getPoint(closestVertexID));
			//}


			std::vector<int> lastOneRingVertex, lastOneRingFace;
			mesh_data->get_one_ring_neighbor(destinationPointID, lastOneRingVertex, lastOneRingFace);

			for(int fid = 0; fid < lastOneRingFace.size();fid++)
			{
				if(lastOneRingFace[fid] == current_face_id)
				{
					is_end_pt_meet = true;
					mesh_data->geodesic_path_array[initial_path[initial_path.size()-1]] = projected_curve;
					mesh_data->geodesic_path_array[initial_path[initial_path.size()-1]].push_back(destinationPoint);
					mesh_data->initial_path_array[initial_path[initial_path.size()-1]] = new_initial_path;
					mesh_data->initial_path_array[initial_path[initial_path.size()-1]].push_back( destinationPointID );
					return;
					break;
				}
			}



			if(!is_end_pt_meet)
			{
				//intersectedEdge.clear();
				next_face_id = getAdjacentFace(mesh_data,current_face_id, edgePoint0, edgePoint1);	
				for(int fid = 0; fid < lastOneRingFace.size();fid++)
				{
					if(lastOneRingFace[fid] == next_face_id)
					{
						is_end_pt_meet = true;
						mesh_data->geodesic_path_array[initial_path[initial_path.size()-1]] = projected_curve;
						mesh_data->geodesic_path_array[initial_path[initial_path.size()-1]].push_back(destinationPoint);
						mesh_data->initial_path_array[initial_path[initial_path.size()-1]] = new_initial_path;
						mesh_data->initial_path_array[initial_path[initial_path.size()-1]].push_back( destinationPointID );
						return;
						break;
					}
				}


				current_wave_boundary.clear();
				current_wave_boundary.push_back(next_face_id);
				isCurrentOneRingHasIntersect = false;
				previousFaceID = next_face_id;
				int * containedVertexIdArray = mesh_data->getFace(next_face_id);
				int containedVertexId0 = containedVertexIdArray[0];
				int containedVertexId1 = containedVertexIdArray[1];
				int containedVertexId2 = containedVertexIdArray[2];
			}
		}while(!is_end_pt_meet);
	}



	Point point0,point1,point2;

	point0 = geodesic_path[geodesic_path.size()-3];
	point1 = geodesic_path[geodesic_path.size()-2];
	point2 = geodesic_path[geodesic_path.size()-1];



	int initial_point0_id = initial_path[initial_path.size()-3];
	int initial_point1_id = initial_path[initial_path.size()-2];
	int initial_point2_id = initial_path[initial_path.size()-1];

	//		std::vector<int> face_array
	std::vector<int> vertex_id_array;
	std::vector<int> current_wave_boundary;
	std::vector<int> next_wave_boundary;
	//mesh_data->get_one_ring_neighbor(initial_point1_id, vertex_id_array, current_wave_boundary);
	current_wave_boundary.push_back(previousFaceID);

	bool is_end_pt_meet = true;
	bool isCurrentPointOnTheSameFace = false;
	int next_face_id = 0;
	int current_face_id = 0;
	int edgePoint0 = 0;
	int edgePoint1 = 0;
	bool isCurrentOneRingHasIntersect = false;
	float min_dist = Dist(mesh_data->getPoint(initial_point1_id),point1);
	int closestVertexID = initial_point1_id;
	//	std::vector<std::vector<int>> intersectedEdge;
	do{

		for(int fId = 0;fId < current_wave_boundary.size();++fId){
			//get a given point, look for the edge intersected with the cutting face that is not the point itself.
			//isCurrentPointOnTheSameFace = false;
			current_face_id = current_wave_boundary[fId];
			int * containedVertexIdArray = mesh_data->getFace(current_face_id);
			int containedVertexId0 = containedVertexIdArray[0];
			int containedVertexId1 = containedVertexIdArray[1];
			int containedVertexId2 = containedVertexIdArray[2];
			Point meshPoint0,meshPoint1,meshPoint2;

			meshPoint0 = mesh_data->getPoint(containedVertexId0);
			meshPoint1 = mesh_data->getPoint(containedVertexId1);
			meshPoint2 = mesh_data->getPoint(containedVertexId2);



			bool is_hit = false;
			bool is_valid = true;
			Eigen::Vector3f eHitPoint;
			Point hitPoint;

			if( is_edge_visited(intersectedEdge, containedVertexId0, containedVertexId1) == false )
			{

				is_hit = getIntersectedPoint(mesh_data,containedVertexId0, containedVertexId1, point0, point1, point2, eHitPoint);
				if(is_hit)
				{
					if( 0.001 > sqrt((startPoint.x - eHitPoint[0])*(startPoint.x - eHitPoint[0]) + (startPoint.y - eHitPoint[1])*(startPoint.y - eHitPoint[1]) + (startPoint.z - eHitPoint[2])*(startPoint.z - eHitPoint[2]))){
						is_valid = false;
					}
					else
					{
						is_valid = true;
						hitPoint.x = eHitPoint[0];
						hitPoint.y = eHitPoint[1];
						hitPoint.z = eHitPoint[2];

						bool isOnFirstEdge = isPointOnEdge(projected_curve[projected_curve.size() - 1], mesh_data->getPoint(containedVertexId2), mesh_data->getPoint(containedVertexId0));
						bool isOnSecondEdge = isPointOnEdge(projected_curve[projected_curve.size() - 1], mesh_data->getPoint(containedVertexId2), mesh_data->getPoint(containedVertexId1));

						if( isOnFirstEdge || isOnSecondEdge)
						{

							projectPointToLine(point1,point0,hitPoint);
							if(isOnDifferentTriangle(mesh_data, fId, point0,hitPoint))
							{
								isCurrentPointOnTheSameFace = true;
								break;
							}
						}else
						{
							is_valid = false;
						}

					}
					if(is_valid){
						Point tmpPoint;
						tmpPoint.x = eHitPoint[0];
						tmpPoint.y = eHitPoint[1];
						tmpPoint.z = eHitPoint[2];


						Point projectedPoint;
						projectedPoint = projectPointToLine(point2, projected_curve[projected_curve.size() - 1], tmpPoint);
						if ( isPointOnEdge(projectedPoint, projected_curve[projected_curve.size() - 1], tmpPoint) )
						{
							is_end_pt_meet = true;
						}else{
							is_end_pt_meet = false;
							edgePoint0 = containedVertexId0;
							edgePoint1 = containedVertexId1;
							std::vector<int> edge;
							edge.push_back(edgePoint0);
							edge.push_back(edgePoint1);
							intersectedEdge.push_back(edge);

							if(Dist(tmpPoint,meshPoint0) < Dist(tmpPoint,meshPoint1)){
								new_initial_path.push_back(containedVertexId0);
							}
							else
							{
								new_initial_path.push_back(containedVertexId1);
							}

							projected_curve.push_back(tmpPoint);
							isCurrentOneRingHasIntersect = true;
							is_hit = false;
							break;
						}
					}
				}
			}

			if( is_edge_visited(intersectedEdge, containedVertexId1, containedVertexId2) == false )
			{
				is_hit = getIntersectedPoint(mesh_data,containedVertexId1, containedVertexId2, point0, point1, point2, eHitPoint);
				if(is_hit)
				{
					if( 0.001 > sqrt((startPoint.x - eHitPoint[0])*(startPoint.x - eHitPoint[0]) + (startPoint.y - eHitPoint[1])*(startPoint.y - eHitPoint[1]) + (startPoint.z - eHitPoint[2])*(startPoint.z - eHitPoint[2]))){
						is_valid = false;
					}
					else
					{
						is_valid = true;
						hitPoint.x = eHitPoint[0];
						hitPoint.y = eHitPoint[1];
						hitPoint.z = eHitPoint[2];
						bool isOnFirstEdge = isPointOnEdge(projected_curve[projected_curve.size() - 1], mesh_data->getPoint(containedVertexId0), mesh_data->getPoint(containedVertexId1));
						bool isOnSecondEdge = isPointOnEdge(projected_curve[projected_curve.size() - 1], mesh_data->getPoint(containedVertexId0), mesh_data->getPoint(containedVertexId2));

						if( isOnFirstEdge || isOnSecondEdge)
						{
							projectPointToLine(point1,point0,hitPoint);
							if(isOnDifferentTriangle(mesh_data, fId, point0,hitPoint))
							{
								isCurrentPointOnTheSameFace = true;
								break;
							}
						}else
						{
							is_valid = false;
						}
					}
					if(is_valid){
						Point tmpPoint;
						tmpPoint.x = eHitPoint[0];
						tmpPoint.y = eHitPoint[1];
						tmpPoint.z = eHitPoint[2];
						Point projectedPoint;
						projectedPoint = projectPointToLine(point2, projected_curve[projected_curve.size() - 1], tmpPoint);
						if ( isPointOnEdge(projectedPoint, projected_curve[projected_curve.size() - 1], tmpPoint) )
						{
							is_end_pt_meet = true;
						}else{
							is_end_pt_meet = false;
							edgePoint0 = containedVertexId1;
							edgePoint1 = containedVertexId2;
							std::vector<int> edge;
							edge.push_back(edgePoint0);
							edge.push_back(edgePoint1);
							intersectedEdge.push_back(edge);

							if(Dist(tmpPoint,meshPoint0) < Dist(tmpPoint,meshPoint1)){
								new_initial_path.push_back(containedVertexId0);
							}
							else
							{
								new_initial_path.push_back(containedVertexId1);
							}

							projected_curve.push_back(tmpPoint);
							isCurrentOneRingHasIntersect = true;
							is_hit = false;
							break;
						}
					}
				}
			}

			if( is_edge_visited(intersectedEdge, containedVertexId2, containedVertexId0) == false )
			{
				is_hit = getIntersectedPoint(mesh_data,containedVertexId2, containedVertexId0, point0, point1, point2, eHitPoint);
				if(is_hit)
				{
					if( 0.001 > sqrt((startPoint.x - eHitPoint[0])*(startPoint.x - eHitPoint[0]) + (startPoint.y - eHitPoint[1])*(startPoint.y - eHitPoint[1]) + (startPoint.z - eHitPoint[2])*(startPoint.z - eHitPoint[2]))){
						is_valid = false;
					}
					else
					{
						is_valid = true;
						hitPoint.x = eHitPoint[0];
						hitPoint.y = eHitPoint[1];
						hitPoint.z = eHitPoint[2];
						bool isOnFirstEdge = isPointOnEdge(projected_curve[projected_curve.size() - 1], mesh_data->getPoint(containedVertexId1), mesh_data->getPoint(containedVertexId0));
						bool isOnSecondEdge = isPointOnEdge(projected_curve[projected_curve.size() - 1], mesh_data->getPoint(containedVertexId1), mesh_data->getPoint(containedVertexId2));

						if( isOnFirstEdge || isOnSecondEdge)
						{
							projectPointToLine(point1,point0,hitPoint);
							if(isOnDifferentTriangle(mesh_data, fId, point0,hitPoint))
							{
								isCurrentPointOnTheSameFace = true;
								break;
							}
						}else
						{
							is_valid = false;
						}
					}
					if(is_valid){
						Point tmpPoint;
						tmpPoint.x = eHitPoint[0];
						tmpPoint.y = eHitPoint[1];
						tmpPoint.z = eHitPoint[2];
						Point projectedPoint;
						projectedPoint = projectPointToLine(point2, projected_curve[projected_curve.size() - 1], tmpPoint);
						if ( isPointOnEdge(projectedPoint, projected_curve[projected_curve.size() - 1], tmpPoint) )
						{
							is_end_pt_meet = true;
						}else{
							is_end_pt_meet = false;
							edgePoint0 = containedVertexId0;
							edgePoint1 = containedVertexId2;
							std::vector<int> edge;
							edge.push_back(edgePoint0);
							edge.push_back(edgePoint1);
							intersectedEdge.push_back(edge);

							if(Dist(tmpPoint,meshPoint0) < Dist(tmpPoint,meshPoint1)){
								new_initial_path.push_back(containedVertexId0);
							}
							else
							{
								new_initial_path.push_back(containedVertexId1);
							}

							projected_curve.push_back(tmpPoint);
							isCurrentOneRingHasIntersect = true;
							is_hit = false;
							break;
						}
					}
				}
			}
			//if(isCurrentOneRingHasIntersect)
			//{
			//	Point projectedPoint;
			//	projectedPoint = projectPointToLine(point1, projected_curve[projected_curve.size() - 1], projected_curve[projected_curve.size() - 2]);
			//	if ( isPointOnEdge(projectedPoint, projected_curve[projected_curve.size() - 1], projected_curve[projected_curve.size() - 2]) )
			//	{
			//		is_end_pt_meet = true;
			//	}else{
			//		is_end_pt_meet = false;
			//	}

			//}
			if( isCurrentPointOnTheSameFace )
			{
				isCurrentPointOnTheSameFace = false;
				is_end_pt_meet = false;
				break;
			}

		}

		//if(isCurrentOneRingHasIntersect == false)
		//{
		//	for( int pID = 0; pID < vertex_id_array.size(); ++pID )
		//	{
		//		if( min_dist > Dist(mesh_data->getPoint(vertex_id_array[pID]),point0) )
		//		{
		//			min_dist = Dist(mesh_data->getPoint(vertex_id_array[pID]),point0);
		//			closestVertexID = vertex_id_array[pID];
		//		}
		//	}

		//	new_initial_path.push_back(closestVertexID);
		//	projected_curve.push_back(mesh_data->getPoint(closestVertexID));
		//}

		std::vector<int> lastOneRingVertex, lastOneRingFace;
		mesh_data->get_one_ring_neighbor(destinationPointID, lastOneRingVertex, lastOneRingFace);
		for(int fid = 0; fid < lastOneRingFace.size();fid++)
		{
			if(lastOneRingFace[fid] == current_face_id)
			{
				is_end_pt_meet = true;
				mesh_data->geodesic_path_array[initial_path[initial_path.size()-1]] = projected_curve;
				mesh_data->geodesic_path_array[initial_path[initial_path.size()-1]].push_back(destinationPoint);
				mesh_data->initial_path_array[initial_path[initial_path.size()-1]] = new_initial_path;
				mesh_data->initial_path_array[initial_path[initial_path.size()-1]].push_back( destinationPointID );
				return;
				break;
			}
		}
		//intersectedEdge.clear();



		next_face_id = getAdjacentFace(mesh_data,current_face_id, edgePoint0, edgePoint1);


		if(isCurrentOneRingHasIntersect && !is_end_pt_meet)
		{
		for(int fid = 0; fid < lastOneRingFace.size();fid++)
		{
			if(lastOneRingFace[fid] == next_face_id)
			{
				is_end_pt_meet = true;
				mesh_data->geodesic_path_array[initial_path[initial_path.size()-1]] = projected_curve;
				mesh_data->geodesic_path_array[initial_path[initial_path.size()-1]].push_back(destinationPoint);
				mesh_data->initial_path_array[initial_path[initial_path.size()-1]] = new_initial_path;
				mesh_data->initial_path_array[initial_path[initial_path.size()-1]].push_back( destinationPointID );
				return;
				break;
			}
		}


		}


		current_wave_boundary.clear();
		current_wave_boundary.push_back(next_face_id);
		int * containedVertexIdArray = mesh_data->getFace(next_face_id);
		int containedVertexId0 = containedVertexIdArray[0];
		int containedVertexId1 = containedVertexIdArray[1];
		int containedVertexId2 = containedVertexIdArray[2];

	}while(!is_end_pt_meet);

	//mesh_data->geodesic_path_array[initial_path[initial_path.size()-1]] = projected_curve;
	//mesh_data->geodesic_path_array[initial_path[initial_path.size()-1]].push_back(destinationPoint);
	//mesh_data->initial_path_array[initial_path[initial_path.size()-1]] = new_initial_path;
	//mesh_data->initial_path_array[initial_path[initial_path.size()-1]].push_back( destinationPointID );
}






#endif 