#include "geodesic_base_elements.h"
#include "geodesic_mesh_data.h"


//#include <Eigen/Core>
//#include <Eigen/Eigen>
//#include <Eigen/Eigenvalues>
//#include <Eigen/Sparse>

#include <iostream>
#include <fstream>

#include "geodesic_project_utilities.h"
#define PI 3.14159265354 


///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////


std::vector<initPoint> cutTwoPathes(MeshData * mesh_data, std::vector<initPoint> tmpPath0, std::vector<initPoint> tmpPath1, initPoint endPoint, Eigen::Vector3f cutPlaneNormal, Eigen::Vector3f pointOnCutPlane)
{
	std::vector<initPoint> cutPath;
	std::vector<int> tmp;	
	std::vector<initPoint> tempPathForCurrentSection;

	int cutIterationLimitation = mesh_data->numFaces();
	int cutCounter = 0;

	
	while(true)
	{

		if(cutCounter > cutIterationLimitation)
		{
			throw 21;
		}

		bool is_cut_valid = false;
		if(tmpPath0.size() > 1)
		{
			if(tmpPath0[tmpPath0.size() - 1].flag == 1 )
			{
				std::vector<int> nbface;
				nbface = setDiff(tmpPath0[tmpPath0.size() - 1].neighbourFacesID, tmpPath0[tmpPath0.size() - 1].location);


				for(int j = 0; j < nbface.size(); j++)
				{
					int faceID = nbface[j];
					initPoint cutPoint;
					bool isCut = getCutPoint( tmpPath0[tmpPath0.size() - 1].vertexID, mesh_data , pointOnCutPlane, cutPlaneNormal, faceID, cutPoint);
					if(isCut)
					{
						tmpPath0.push_back(cutPoint);
						is_cut_valid = true;
						break;
					}
				}
			}
			else
			{
				int faceID;
				faceID = tmpPath0[tmpPath0.size() - 1].location[1];
				std::vector<initPoint> cutPoint;
				bool isCut = getCutPoint( tmpPath0[tmpPath0.size() - 1].cutEdge, mesh_data , pointOnCutPlane, cutPlaneNormal, faceID, cutPoint);
				if(isCut)
				{
					float angle = 0;
					int cutPointIndex = -1;
					for(int index = 0; index < cutPoint.size();index++)
					{
						Eigen::Vector3f p0,p1,p2,v0,v1;
						p0[0] = tmpPath0[tmpPath0.size() - 1].x;
						p0[1] = tmpPath0[tmpPath0.size() - 1].y;
						p0[2] = tmpPath0[tmpPath0.size() - 1].z;

						p1[0] = endPoint.x;
						p1[1] = endPoint.y;
						p1[2] = endPoint.z;

						p2[0] = cutPoint[index].x;
						p2[1] = cutPoint[index].y;
						p2[2] = cutPoint[index].z;

						v0 = (p1 - p0)/(p1 - p0).norm();
						v1 = (p2 - p0)/(p2 - p0).norm();

						float dotResult = v0.dot(v1);
						if(dotResult > angle)
						{
							cutPointIndex = index;
							angle = dotResult;
						}
					}

					if(cutPointIndex != -1)
					{
						tmpPath0.push_back(cutPoint[cutPointIndex]);
						is_cut_valid = true;
					}
				}
			}

			tmp = setIntersection(tmpPath0[tmpPath0.size() - 1].location[0], endPoint.location);

			if(tmp.size())
			{
				tempPathForCurrentSection = tmpPath0;
				tmpPath1.clear();
				tmpPath0.clear();
				for(int index = 1; index < tempPathForCurrentSection.size(); index++)
				{
					cutPath.push_back(tempPathForCurrentSection[index]);
				}

				break;
			}			

		}


		//////////////////////////////////////////////////////////
		//////////////////////////////////////////////////////////

		if(tmpPath1.size() > 1)
		{
			if(tmpPath1[tmpPath1.size() - 1].flag == 1 )
			{
				std::vector<int> nbface;
				nbface = setDiff(tmpPath1[tmpPath1.size() - 1].neighbourFacesID, tmpPath1[tmpPath1.size() - 1].location);

				for(int j = 0; j < nbface.size(); j++)
				{
					int faceID = nbface[j];
					initPoint cutPoint;
					bool isCut = getCutPoint( tmpPath1[tmpPath1.size() - 1].vertexID, mesh_data , pointOnCutPlane, cutPlaneNormal, faceID, cutPoint);
					if(isCut)
					{
						tmpPath1.push_back(cutPoint);
						is_cut_valid = true;
						break;
					}
				}
			}
			else
			{
				int faceID;
				faceID = tmpPath1[tmpPath1.size() - 1].location[1];
				std::vector<initPoint> cutPoint;
				bool isCut = getCutPoint( tmpPath1[tmpPath1.size() - 1].cutEdge, mesh_data , pointOnCutPlane, cutPlaneNormal, faceID, cutPoint);
				if(isCut)
				{
					float angle = 0;
					int cutPointIndex = -1;
					for(int index = 0; index < cutPoint.size();index++)
					{
						Eigen::Vector3f p0,p1,p2,v0,v1;
						p0[0] = tmpPath1[tmpPath1.size() - 1].x;
						p0[1] = tmpPath1[tmpPath1.size() - 1].y;
						p0[2] = tmpPath1[tmpPath1.size() - 1].z;

						p1[0] = endPoint.x;
						p1[1] = endPoint.y;
						p1[2] = endPoint.z;

						p2[0] = cutPoint[index].x;
						p2[1] = cutPoint[index].y;
						p2[2] = cutPoint[index].z;

						v0 = (p1 - p0)/(p1 - p0).norm();
						v1 = (p2 - p0)/(p2 - p0).norm();

						float dotResult = v0.dot(v1);
						if(dotResult > angle)
						{
							cutPointIndex = index;
							angle = dotResult;
						}
					}

					if(cutPointIndex != -1)
					{
						tmpPath1.push_back(cutPoint[cutPointIndex]);
						is_cut_valid = true;
					}
				}
			}


			tmp = setIntersection(tmpPath1[tmpPath1.size() - 1].location[0], endPoint.location);

			if(tmp.size())
			{
				tempPathForCurrentSection = tmpPath1;
				tmpPath1.clear();
				tmpPath0.clear();
				for(int index = 1; index < tempPathForCurrentSection.size(); index++)
				{
					cutPath.push_back(tempPathForCurrentSection[index]);
				}
				break;
			}

		}
		cutCounter++;
		if(is_cut_valid == false)
		{
			std::vector< initPoint > emptyPath;
			return emptyPath;
		}


	}

	return cutPath;
}













bool CutBetweenTwoPoints(MeshData * mesh_data, std::vector<initPoint> & cutPath, initPoint endPoint, Eigen::Vector3f cutPlaneNormal, Eigen::Vector3f pointOnCutPlane){
	//define variables

	initPoint startPoint;
	startPoint = cutPath[cutPath.size() - 1];
	std::vector<initPoint> tmpPath0;
	std::vector<initPoint> tmpPath1;
	tmpPath0.push_back(startPoint);
	tmpPath1.push_back(startPoint);
	std::vector<int> tmp;

	std::vector<initPoint> tempPathForCurrentSection;
	int success = 0;
	bool is_new_cutPlane = true;

	//////////////////////////////////////////////////////////////////
	//// STEP 2, trace path on mesh, start from source			 /////
	//////////////////////////////////////////////////////////////////

	int cutIterationLimitation = mesh_data->numFaces();
	int cutCounter = 0;

	while(true)
	{
		if(cutCounter > cutIterationLimitation)
		{
			throw 21;
		}

		if(cutPath[cutPath.size() - 1].flag == 1 )
		{
			std::vector<int> nbface;
			nbface = setDiff(cutPath[cutPath.size() - 1].neighbourFacesID, cutPath[cutPath.size() - 1].location);

			if(nbface.size() == 0)
				nbface = cutPath[cutPath.size() - 1].neighbourFacesID;

			std::vector<initPoint> tmpCutPoints;
			for(int j = 0; j < nbface.size(); j++)
			{
				int faceID = nbface[j];
				initPoint cutPoint;
				bool isCut = getCutPoint( cutPath[cutPath.size() - 1].vertexID, mesh_data , pointOnCutPlane, cutPlaneNormal, faceID, cutPoint);
				
				if(isCut)
				{
					if(cutPoint.flag == 1)
					{
						bool is_exist = false;
						for(int k = 0; k < tmpCutPoints.size();k++)
						{
							if( cutPoint.vertexID == tmpCutPoints[k].vertexID )
							{
								is_exist = true;
								break;
							}
						}
						if(is_exist == false)
						{	
							tmpCutPoints.push_back(cutPoint);
							if(tmpPath0.size() == 1)
							{
								tmpPath0.push_back(cutPoint);
							}
							else
							{
								tmpPath1.push_back(cutPoint);
							}
						}
					}else
					{
						if(tmpPath0.size() == 1)
						{
							tmpPath0.push_back(cutPoint);
						}
						else
						{
							tmpPath1.push_back(cutPoint);
						}
				}
				}
			}

			tempPathForCurrentSection = cutTwoPathes(mesh_data, tmpPath0,tmpPath1, endPoint, cutPlaneNormal, pointOnCutPlane);
			if(tempPathForCurrentSection.size() == 0)
			{
				return false;
			}else
			{
				for(int indexB = 0; indexB < tempPathForCurrentSection.size(); indexB++)
				{
					cutPath.push_back(tempPathForCurrentSection[indexB]);
				}
				tempPathForCurrentSection.clear();
			}
		}
		else
		{
			int faceID;
			faceID = cutPath[cutPath.size() - 1].location[1];
			std::vector<initPoint> cutPoint;
			bool isCut = getCutPoint( cutPath[cutPath.size() - 1].cutEdge, mesh_data , pointOnCutPlane, cutPlaneNormal, faceID, cutPoint);
			if(isCut)
			{
				float angle = 0;
				int cutPointIndex = -1;
				for(int index = 0; index < cutPoint.size();index++)
				{
					Eigen::Vector3f p0,p1,p2,v0,v1;
					p0[0] = cutPath[cutPath.size() - 1].x;
					p0[1] = cutPath[cutPath.size() - 1].y;
					p0[2] = cutPath[cutPath.size() - 1].z;

					p1[0] = endPoint.x;
					p1[1] = endPoint.y;
					p1[2] = endPoint.z;

					p2[0] = cutPoint[index].x;
					p2[1] = cutPoint[index].y;
					p2[2] = cutPoint[index].z;

					v0 = (p1 - p0)/(p1 - p0).norm();
					v1 = (p2 - p0)/(p2 - p0).norm();

					float dotResult = v0.dot(v1);
					if(dotResult > angle)
					{
						cutPointIndex = index;
						angle = dotResult;
					}
				}
				if(cutPointIndex == -1)
				{
					return false;
				}else
				{
					cutPath.push_back(cutPoint[cutPointIndex]);
				}
			}
		}

		cutCounter++;
		tmp = setIntersection(cutPath[cutPath.size() - 1].location[0], endPoint.location);

		if(tmp.size())
		{
			break;
		}			
	}

	return true;
}