#ifndef geodesic_updata_normals_h
#define geodesic_updata_normals_h

#include "geodesic_base_elements.h"
#include "geodesic_mesh_data.h"
#include <Eigen/Core>
#include <Eigen/Eigen>
#include <Eigen/Eigenvalues>
#include <Eigen/Sparse>


using namespace std;
using namespace Eigen;

void updata_normal(MeshData * mesh_data, vector<int> initial_path,vector<Point> & normalArray){

	

	int dim = 3;

	//number of nearest neighbor returned 
	int num_points = 6;

	int row =  num_points;
	Eigen::MatrixX3f eigenPointMatrix;
	eigenPointMatrix.resize(row,3);
	Eigen::MatrixX3f transposeMatrix;
	transposeMatrix.resize(row,3);
	VectorXf meanVector;




	ANNpoint queryPoint;
	queryPoint = annAllocPt(dim);

	ANNidxArray nnIdx; // near neighbor indices
	nnIdx = new ANNidx[num_points];

	ANNdistArray dists; // near neighbor distances
	dists = new ANNdist[num_points];

	float search_radius = 0.0005f;
	ANNdist sqRad = sqrt(search_radius);


	for(unsigned int i = 0; i < initial_path.size(); i++){
		

		queryPoint[0] = mesh_data->getVertex(initial_path[i])[0];
		queryPoint[1] = mesh_data->getVertex(initial_path[i])[1];
		queryPoint[2] = mesh_data->getVertex(initial_path[i])[2];

		mesh_data->meshKdTree->annkSearch(queryPoint,num_points,nnIdx,dists,0.00000f);


		for(int b = 0; b < num_points ; ++b){
			eigenPointMatrix(b,0) = mesh_data->getVertex(nnIdx[b])[0];
			eigenPointMatrix(b,1) = mesh_data->getVertex(nnIdx[b])[1];
			eigenPointMatrix(b,2) = mesh_data->getVertex(nnIdx[b])[2];
		}





		float mean;
		for (int k = 0; k < 3; k++)
		{
			//compute mean
			mean = (eigenPointMatrix.col(k).sum())/row;
			// create a vector with constant value = mean
			meanVector  = VectorXf::Constant(row,mean); 
			eigenPointMatrix.col(k) -= meanVector;
		}

		Eigen::MatrixXf covariance = ((float)1/(row-1)) * eigenPointMatrix.transpose()* eigenPointMatrix;

		Eigen::EigenSolver<MatrixXd> m_solve(covariance);

		Eigen::VectorXf eigenvalues = m_solve.eigenvalues().real();
		Eigen::MatrixXf eignevectors = m_solve.eigenvectors().real();

		Eigen::Vector3f vector;



		if(eigenvalues(0) < eigenvalues(1))
		{
			if (eigenvalues(0) < eigenvalues(2))
			{
				vector[0] = eignevectors(0,0);
				vector[1] = eignevectors(1,0);
				vector[2] = eignevectors(2,0);
			}
			else
			{
				vector[0] = eignevectors(0,2);
				vector[1] = eignevectors(1,2);
				vector[2] = eignevectors(2,2);
			}
		}
		else if(eigenvalues(1) < eigenvalues(0))
		{
			if (eigenvalues(1) < eigenvalues(2))
			{
				vector[0] = eignevectors(0,1);
				vector[1] = eignevectors(1,1);
				vector[2] = eignevectors(2,1);
			}
			else
			{
				vector[0] = eignevectors(0,2);
				vector[1] = eignevectors(1,2);
				vector[2] = eignevectors(2,2);
			}
		}


	

		Point point;
		vector.normalized();
		point.x = vector[0];
		point.y = vector[1];
		point.z = vector[2];

		normalArray.push_back(point);
	}
}






void updata_normal(MeshData * mesh_data, vector<Point> initial_path,vector<Point> & normalArray){

	

	int dim = 3;

	//number of nearest neighbor returned 
	int num_points = 6;

	int row =  num_points;
	Eigen::MatrixX3d eigenPointMatrix;
	eigenPointMatrix.resize(row,3);
	Eigen::MatrixX3d transposeMatrix;
	transposeMatrix.resize(row,3);
	VectorXd meanVector;




	ANNpoint queryPoint;
	queryPoint = annAllocPt(dim);

	ANNidxArray nnIdx; // near neighbor indices
	nnIdx = new ANNidx[num_points];

	ANNdistArray dists; // near neighbor distances
	dists = new ANNdist[num_points];

	float search_radius = 0.0005f;
	ANNdist sqRad = sqrt(search_radius);


	for(unsigned int i = 0; i < initial_path.size(); i++){
		

		queryPoint[0] = initial_path[i].x;
		queryPoint[1] = initial_path[i].y;
		queryPoint[2] = initial_path[i].z;

		mesh_data->meshKdTree->annkSearch(queryPoint,num_points,nnIdx,dists,0.00000f);


		for(int b = 0; b < num_points ; ++b){
			eigenPointMatrix(b,0) = mesh_data->getVertex(nnIdx[b])[0];
			eigenPointMatrix(b,1) = mesh_data->getVertex(nnIdx[b])[1];
			eigenPointMatrix(b,2) = mesh_data->getVertex(nnIdx[b])[2];
		}





		double mean;
		for (int k = 0; k < 3; k++)
		{
			//compute mean
			mean = (eigenPointMatrix.col(k).sum())/row;
			// create a vector with constant value = mean
			meanVector  = VectorXf::Constant(row,mean); 
			eigenPointMatrix.col(k) -= meanVector;
		}

		Eigen::MatrixXf covariance = ((double)1/(row-1)) * eigenPointMatrix.transpose()* eigenPointMatrix;

		Eigen::EigenSolver<MatrixXd> m_solve(covariance);

		Eigen::VectorXf eigenvalues = m_solve.eigenvalues().real();
		Eigen::MatrixXf eignevectors = m_solve.eigenvectors().real();

		Eigen::Vector3f vector;



		if(eigenvalues(0) < eigenvalues(1))
		{
			if (eigenvalues(0) < eigenvalues(2))
			{
				vector[0] = eignevectors(0,0);
				vector[1] = eignevectors(1,0);
				vector[2] = eignevectors(2,0);
			}
			else
			{
				vector[0] = eignevectors(0,2);
				vector[1] = eignevectors(1,2);
				vector[2] = eignevectors(2,2);
			}
		}
		else if(eigenvalues(1) < eigenvalues(0))
		{
			if (eigenvalues(1) < eigenvalues(2))
			{
				vector[0] = eignevectors(0,1);
				vector[1] = eignevectors(1,1);
				vector[2] = eignevectors(2,1);
			}
			else
			{
				vector[0] = eignevectors(0,2);
				vector[1] = eignevectors(1,2);
				vector[2] = eignevectors(2,2);
			}
		}


	

		Point point;
		vector.normalized();
		point.x = vector[0];
		point.y = vector[1];
		point.z = vector[2];

		normalArray.push_back(point);
	}
}













#endif

