#ifndef geodesic_regression_sparse_matrix_h
#define geodesic_regression_sparse_matrix_h






#include "geodesic_base_elements.h"
#include "geodesic_mesh_data.h"
#include "UtilityDebug.h"

#include <Eigen/Core>
#include <Eigen/Eigen>
#include <Eigen/Eigenvalues>
#include <Eigen/Sparse>

#include <iostream>
#include <fstream>



#define PI 3.14159265354 


float Dist(Point point0, Point point1){

	 return sqrt((float)((point0.x - point1.x) * (point0.x - point1.x) + (point0.y - point1.y) * (point0.y - point1.y) + (point0.z - point1.z) * (point0.z - point1.z)));
	 
}



void geodesic(MeshData * mesh_data , std::vector<int> initial_path, std::vector<Point> & curvePointArray ,float regression_ratio){

	//vector<Point> initial_path;
	//initial_path = curvePointArray;

	float inverse_matrix_solve_time = 0.0f;

	//ofstream regionFile("result.txt");
	int index_offset = 0;
	float u = regression_ratio;
	int sparse_matrix_size = 0;
	sparse_matrix_size = (int)curvePointArray.size();

	

	Eigen::SparseMatrix<float> sMat_C_3m_1(sparse_matrix_size * 3,1);
	Eigen::SparseMatrix<float> sMat_Xt_3m_1(sparse_matrix_size * 3 ,1);
	//SparseMatrix<float> sMat_Xt1_3m_1(sparse_matrix_size * 3 ,1);
	Eigen::SparseMatrix<float> sMat_N_3m_m(sparse_matrix_size * 3,sparse_matrix_size);
	Eigen::SparseMatrix<float> sMat_Nt_m_3m(sparse_matrix_size, sparse_matrix_size * 3 );	
	Eigen::SparseMatrix<float> sMat_Nstar_3m_3m(sparse_matrix_size * 3,sparse_matrix_size * 3);
	Eigen::SparseMatrix<float> sMat_K_3m_3m(sparse_matrix_size * 3,sparse_matrix_size * 3);
	Eigen::SparseMatrix<float> sMat_I_mat_3m_3m(sparse_matrix_size * 3 , sparse_matrix_size * 3);



	Timer t2;

	


	for(int i = 0 ; i < sparse_matrix_size * 3; i++){

		sMat_I_mat_3m_3m.coeffRef(i,i) = 1.0f;
	}



	for(int i = 0;i< sparse_matrix_size ;i++ ){

		sMat_Xt_3m_1.coeffRef(i*3 ,0) = (float)curvePointArray[i].x;
		sMat_Xt_3m_1.coeffRef(i*3 + 1,0) = (float)curvePointArray[i].y;
		sMat_Xt_3m_1.coeffRef(i*3 + 2,0) = (float)curvePointArray[i].z;

		
		//////////////////////////////////////////////////////////////////
		//// one-ring searching for closest point

		std::vector<int> one_ring_v;
		std::vector<int> one_ring_f;

		


		one_ring_v = mesh_data->one_ring_vertex_array[initial_path[i + index_offset]];
		one_ring_v.push_back(initial_path[i + index_offset]);

		int closest_point_id = 0;
		float min_dist = 9999.9f;
		for(int x = 0;x < one_ring_v.size();++x){
			Point point;

			point.x = mesh_data->getVertex(one_ring_v[x])[0];
			point.y = mesh_data->getVertex(one_ring_v[x])[1];
			point.z = mesh_data->getVertex(one_ring_v[x])[2];
				

			float dist = Dist(point,curvePointArray[i + index_offset] );	
			if(dist < min_dist){
				min_dist = dist;
				closest_point_id = one_ring_v[x];
			}
		}
		one_ring_v.clear();

		std::vector<int> one_ring_vertex;
		one_ring_vertex = mesh_data->one_ring_vertex_array[closest_point_id];
		
		std::vector<int> one_ring_faces;
		one_ring_faces = mesh_data->one_ring_face_array[closest_point_id];
	

		//float x = 0, y = 0,z = 0;
		////float center_point[3];				
		//for(int a = 0; a < one_ring_vertex.size() ; ++a){
		//	x += mesh_data->getVertex(one_ring_vertex[a])[0];
		//	y += mesh_data->getVertex(one_ring_vertex[a])[1];
		//	z += mesh_data->getVertex(one_ring_vertex[a])[2];
		//}	

		//sMat_C_3m_1.coeffRef(i*3,0) = x /  one_ring_vertex.size();
		//sMat_C_3m_1.coeffRef(i*3 + 1,0) = y /  one_ring_vertex.size();
		//sMat_C_3m_1.coeffRef(i*3 + 2,0) = z /  one_ring_vertex.size();




		// get one-ring neighbor face normal of current curve point

		//perform SVD on one ring face normals
		int row =  (int)one_ring_faces.size();
		Eigen::MatrixX3f eigenPointMatrix;
		eigenPointMatrix.resize(row,3);
		Eigen::MatrixX3f transposeMatrix;
		transposeMatrix.resize(row,3);
		Eigen::VectorXf meanVector;



		for(int a = 0; a < one_ring_faces.size();a++ ){

			eigenPointMatrix(a,0) = mesh_data->getVertexNormal(one_ring_faces[a])[0];
			eigenPointMatrix(a,1) = mesh_data->getVertexNormal(one_ring_faces[a])[1];
			eigenPointMatrix(a,2) = mesh_data->getVertexNormal(one_ring_faces[a])[2];	
		}


		float mean;
		for (int k = 0; k < 3; k++)
		{
			//compute mean
			mean = (eigenPointMatrix.col(k).sum())/row;
			// create a vector with constant value = mean
			meanVector  = Eigen::VectorXf::Constant(row,mean); 
			eigenPointMatrix.col(k) -= meanVector;
		}


		Eigen::MatrixXf covariance = ((float)1/(row-1)) * eigenPointMatrix.transpose()* eigenPointMatrix;

		Eigen::EigenSolver<Eigen::MatrixXf> m_solve(covariance);

		Eigen::VectorXf eigenvalues = m_solve.eigenvalues().real();
		Eigen::MatrixXf eignevectors = m_solve.eigenvectors().real();

		Eigen::Vector3f vector;


			if(eigenvalues(0) < eigenvalues(1))
		{
			if (eigenvalues(0) < eigenvalues(2))
			{
				vector[0] = eignevectors(0,0);
				vector[1] = eignevectors(1,0);
				vector[2] = eignevectors(2,0);
			}
			else
			{
				vector[0] = eignevectors(0,2);
				vector[1] = eignevectors(1,2);
				vector[2] = eignevectors(2,2);
			}
		}
		else{
			if(eigenvalues(1) < eigenvalues(2))
			{
				vector[0] = eignevectors(0,1);
				vector[1] = eignevectors(1,1);
				vector[2] = eignevectors(2,1);
			}else{
				vector[0] = eignevectors(0,2);
				vector[1] = eignevectors(1,2);
				vector[2] = eignevectors(2,2);
			}
		}

		vector.normalized();


		double min_distToFace = 999.9;
		int closest_face = 0;
		for(int fid = 0; fid < one_ring_faces.size();++fid){
			int face_id = one_ring_faces[fid];
			int * point_per_face = mesh_data->getFace(face_id);		
			Point p0,p1,p2;
			p0 = mesh_data->getPoint(point_per_face[0]);
			p1 = mesh_data->getPoint(point_per_face[1]);
			p2 = mesh_data->getPoint(point_per_face[2]);
			double distToFace = 0.0;
			distToFace = Dist(curvePointArray[i],p0);
			distToFace += Dist(curvePointArray[i],p1);
			distToFace += Dist(curvePointArray[i],p2);
			if(distToFace < min_distToFace){
				min_distToFace = distToFace;
				closest_face = one_ring_faces[fid];


			}
		}


		int * point_per_face = mesh_data->getFace(closest_face);
		float * normal = mesh_data->getVertexNormal(closest_face);

		Eigen::Vector3f v1,v2,v3,f_normal;
		f_normal[0] = normal[0];
		f_normal[1] = normal[1];
		f_normal[2] = normal[2];
		f_normal.normalized();

		v1[0] = curvePointArray[i].x - mesh_data->getVertex(closest_point_id)[0];
		v1[1] = curvePointArray[i].y - mesh_data->getVertex(closest_point_id)[1];
		v1[2] = curvePointArray[i].z - mesh_data->getVertex(closest_point_id)[2];

		v2[0] = mesh_data->getVertex(closest_point_id)[0];
		v2[1] = mesh_data->getVertex(closest_point_id)[1];
		v2[2] = mesh_data->getVertex(closest_point_id)[2];
		v3 = v2 + (v1 - (v1.dot(f_normal))*f_normal);
		dMat_C_3m_1(i*3,0) = v3[0];
		dMat_C_3m_1(i*3 + 1,0) = v3[1];
		dMat_C_3m_1(i*3 + 2,0) = v3[2];




		sMat_N_3m_m.coeffRef(i*3,i) = (float)vector[0];
		sMat_N_3m_m.coeffRef(i*3+1,i) = (float)vector[1];
		sMat_N_3m_m.coeffRef(i*3+2,i) = (float)vector[2];

		sMat_Nt_m_3m.coeffRef(i,i*3 ) = (float)vector[0];
		sMat_Nt_m_3m.coeffRef(i,i*3+1) = (float)vector[1];
		sMat_Nt_m_3m.coeffRef(i,i*3+2) = (float)vector[2];

		if(i > 0 && i < sparse_matrix_size - 1){
			sMat_K_3m_3m.coeffRef(i * 3 , (i - 1) * 3 ) = 1;
			sMat_K_3m_3m.coeffRef(i * 3 + 1 , (i - 1) * 3 +1) = 1;
			sMat_K_3m_3m.coeffRef(i * 3 + 2 , (i - 1) * 3 +2 ) = 1;

			sMat_K_3m_3m.coeffRef(i * 3 , i * 3 ) = -2;
			sMat_K_3m_3m.coeffRef(i * 3 + 1 , i * 3 +1) = -2;
			sMat_K_3m_3m.coeffRef(i * 3 + 2 , i * 3 +2 ) = -2;


			sMat_K_3m_3m.coeffRef(i * 3 , (i + 1) * 3 ) = 1;
			sMat_K_3m_3m.coeffRef(i * 3 + 1 , (i + 1) * 3 +1) = 1;
			sMat_K_3m_3m.coeffRef(i * 3 + 2 , (i + 1) * 3 +2 ) = 1;

		}


	}

	sMat_Nstar_3m_3m = sMat_N_3m_m * sMat_Nt_m_3m;

	////////////////////////////////////////////////////////////////////////////
	///////////////construct A
	Eigen::SparseMatrix<float> sMat_Xt_1_m_3(sparse_matrix_size * 3,1);
	Eigen::SparseMatrix<float> A(sparse_matrix_size * 6 + 6, sparse_matrix_size * 3);
	Eigen::SparseMatrix<float> A1(sparse_matrix_size * 3 , sparse_matrix_size * 3);

	A1 = sMat_K_3m_3m - sMat_Nstar_3m_3m * sMat_K_3m_3m;

	for(int row = 0; row < sparse_matrix_size * 3;++row){
		for(int col = 0; col < sparse_matrix_size * 3;++col){
			A.coeffRef(row,col) = A1.coeffRef(row,col);
		}
	}

	A.coeffRef(sparse_matrix_size * 3,0) = 1.0f;
	A.coeffRef(sparse_matrix_size * 3 +1,1) = 1.0f;
	A.coeffRef(sparse_matrix_size * 3+2,2) = 1.0f;
	A.coeffRef(sparse_matrix_size * 3 + 3,sparse_matrix_size * 3 - 3) = 1.0f;
	A.coeffRef(sparse_matrix_size * 3 + 4,sparse_matrix_size * 3 - 2) = 1.0f;
	A.coeffRef(sparse_matrix_size * 3 + 5,sparse_matrix_size * 3 - 1) = 1.0f;

	for(int row = sparse_matrix_size * 3 + 6; row < sparse_matrix_size * 6 + 6;++row){
		for(int col = 0; col < sparse_matrix_size * 3;++col){
			A.coeffRef(row,col) = sMat_Nstar_3m_3m.coeffRef(row - (sparse_matrix_size * 3 + 6),col);
			//A.coeffRef(row,col) = 99;
		}
	}



	////////////////////////////////////////////////////////////////////////////
	///////////////construct B
	Eigen::SparseMatrix<float> B(sparse_matrix_size * 6 + 6, 1);
	


	B.coeffRef(sparse_matrix_size * 3,0) = curvePointArray[0].x;
	B.coeffRef(sparse_matrix_size * 3 + 1,0) = curvePointArray[0].y;
	B.coeffRef(sparse_matrix_size * 3 + 2,0) = curvePointArray[0].z;
	B.coeffRef(sparse_matrix_size * 3 + 3,0) = curvePointArray[curvePointArray.size() - 1].x;
	B.coeffRef(sparse_matrix_size * 3 + 4,0) = curvePointArray[curvePointArray.size() - 1].y;
	B.coeffRef(sparse_matrix_size * 3 + 5,0) = curvePointArray[curvePointArray.size() - 1].z;

	Eigen::SparseMatrix<float> B1(sparse_matrix_size * 3, 1);
	Eigen::SparseMatrix<float> a(sparse_matrix_size * 3, sparse_matrix_size * 3);
	Eigen::SparseMatrix<float> b(sparse_matrix_size * 3, 1);

	for(int r = 0; r < sMat_Nstar_3m_3m.rows();++r){
		for(int c = 0; c < sMat_Nstar_3m_3m.cols();++c){
			a.coeffRef(r,c) = sMat_Nstar_3m_3m.coeffRef(r,c);
		}
	}


	for(int r = 0; r < sMat_C_3m_1.rows();++r){
		for(int c = 0; c < sMat_C_3m_1.cols();++c){
			b.coeffRef(r,c) = sMat_C_3m_1.coeffRef(r,c);
		}
	}
	
	B1 = a * b;

	//B1 = sMat_Nstar_3m_3m * sMat_C_3m_1;
	
	for(int row = sparse_matrix_size * 3 + 6 ; row < sparse_matrix_size * 6 + 6;++row){
		B.coeffRef(row,0) = B1.coeffRef(row - (sparse_matrix_size * 3 + 6),0);
	}





	//Eigen::MatrixXf Ad,Bd,dMat_Xt_1_m_3;
	//Ad = A;
	//Bd = B;
	//dMat_Xt_1_m_3 = Ad.fullPivLu().solve(Bd);


	//std::ofstream regionFile("result.txt");
	//for(int row = 0; row < sparse_matrix_size * 6 + 6;++row){
	//	for(int col = 0; col < sparse_matrix_size * 3;++col){

	//		regionFile<<Ad(row,col)<<" ";
	//	}
	//	regionFile<<std::endl;
	//}


	//regionFile<<"*************************************************"<<std::endl;

	//for(int row = 0; row < sparse_matrix_size * 6 + 6;++row){
	//	regionFile<<Bd(row,0)<<std::endl;
	//}

	//regionFile.close();


	
	Eigen::SparseMatrix<float> At(sparse_matrix_size * 3 , sparse_matrix_size * 6 + 6);
	At = A.transpose();
	A = At * A;
	B = At * B;
	Eigen::SimplicialCholesky<Eigen::SparseMatrix<float>> chol(A);
	sMat_Xt_1_m_3 = chol.solve(B);
	//sMat_Xt_1_m_3 = A.simplicialLDLT().solve(B);

	//for(int i = 1; i < sparse_matrix_size;++i){

	//	curvePointArray[i].x = dMat_Xt_1_m_3(i*3,0);
	//	curvePointArray[i].y = dMat_Xt_1_m_3(i*3 + 1,0);
	//	curvePointArray[i].z = dMat_Xt_1_m_3(i*3 + 2,0);
	//}

	for(unsigned int i = 0; i < curvePointArray.size();i++){
		curvePointArray[i].x = sMat_Xt_1_m_3.coeffRef(i*3,0);
		curvePointArray[i].y = sMat_Xt_1_m_3.coeffRef(i*3 + 1,0);
		curvePointArray[i].z = sMat_Xt_1_m_3.coeffRef(i*3 + 2,0);
	}



}





#endif