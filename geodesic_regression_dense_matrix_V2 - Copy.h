#ifndef geodesic_regression_dense_matrix_h
#define geodesic_regression_dense_matrix_h






#include "geodesic_base_elements.h"
#include "geodesic_mesh_data.h"


#include <Eigen/Core>
#include <Eigen/Eigen>
#include <Eigen/Eigenvalues>
#include <Eigen/Sparse>

#include <iostream>
#include <fstream>



#define PI 3.14159265354 




float Dist(Point point0, Point point1){
	return (point0.x - point1.x) * (point0.x - point1.x) + (point0.y - point1.y) * (point0.y - point1.y) + (point0.z - point1.z) * (point0.z - point1.z);
}





void geodesic(MeshData * mesh_data, std::vector<initPoint> & initial_path, std::vector<Point> & curvePointArray ,float regression_ratio){

	int dim = 3;
	int k = 1;
	//ANNpoint queryPoint;
	//queryPoint = annAllocPt(dim);
	//ofstream pfile("calculation procedure.txt");
	//int number_point_in_range = 0;
	//ANNidxArray nnIdx; // near neighbor indices
	//ANNdistArray dists; // near neighbor distances
	//float search_radius = 0.0005f;
	//ANNdist sqRad = sqrt(search_radius);
	//std::ofstream regionFile("result.txt");

	float u = regression_ratio;
	int sparse_matrix_size = (int)curvePointArray.size();

	int index_offset = 0;

	Eigen::MatrixXf dMat_C_3m_1;
	dMat_C_3m_1.resize(sparse_matrix_size * 3,1);
	dMat_C_3m_1.setZero();


	Eigen::MatrixXf dMat_Xt_3m_1;
	dMat_Xt_3m_1.resize(sparse_matrix_size * 3 ,1);
	dMat_Xt_3m_1.setZero();

	Eigen::MatrixXf dMat_N_3m_m;
	dMat_N_3m_m.resize(sparse_matrix_size * 3,sparse_matrix_size);
	dMat_N_3m_m.setZero();

	Eigen::MatrixXf dMat_Nt_m_3m;
	dMat_Nt_m_3m.resize(sparse_matrix_size, sparse_matrix_size * 3 );
	dMat_Nt_m_3m.setZero();

	Eigen::MatrixXf dMat_Nstar_3m_3m;
	dMat_Nstar_3m_3m.resize(sparse_matrix_size * 3,sparse_matrix_size * 3);
	dMat_Nstar_3m_3m.setZero();

	Eigen::MatrixXf dMat_K_3m_3m;
	dMat_K_3m_3m.resize(sparse_matrix_size * 3,sparse_matrix_size * 3);
	dMat_K_3m_3m.setZero();

	Eigen::MatrixXf dMat_I_mat_3m_3m;
	dMat_I_mat_3m_3m.resize(sparse_matrix_size * 3 , sparse_matrix_size * 3);
	dMat_I_mat_3m_3m.setZero();

	for(int i = 0 ; i < sparse_matrix_size * 3; ++i){dMat_I_mat_3m_3m(i,i) = 1.0f;}
	//std::ofstream pfile("calculation procedure.txt");

	std::vector<Point> mean_array;
	for(int i = 0;i< sparse_matrix_size ;++i ){
		dMat_Xt_3m_1(i*3 ,0) = curvePointArray[i].x;
		dMat_Xt_3m_1(i*3 + 1,0) = curvePointArray[i].y;
		dMat_Xt_3m_1(i*3 + 2,0) = curvePointArray[i].z;

		//int num_points = meshKdTree->annkFRSearch(queryPoint,sqRad,0,NULL,NULL,0.0);
		int num_points = 1;

		//nnIdx = new ANNidx[num_points];
		//dists = new ANNdist[num_points];

		//////////////////////////////////////////////////////////////////
		// KD-Tree searching for closest point
		//mesh_data->meshKdTree->annkSearch(queryPoint,num_points,nnIdx,dists,0.00000f);
		//get one ring neighbor vertex of current curve point to compute the mean point
		//vector<int> one_ring_vertex;
		//one_ring_vertex = mesh_data->one_ring_vertex_array[nnIdx[0]];

		//vector<int> one_ring_faces;
		//one_ring_faces = mesh_data->one_ring_face_array[nnIdx[0]];



		//////////////////////////////////////////////////////////////////
		//// one-ring searching for closest point

		std::vector<int> one_ring_v;
		std::vector<int> one_ring_f;

		std::vector<int> one_ring_vertex;		
		std::vector<int> one_ring_faces;

		//if( initial_path[i + index_offset].projFaceID < 0){
		one_ring_v = mesh_data->one_ring_vertex_array[initial_path[i + index_offset].vertexID];
		one_ring_v.push_back(initial_path[i + index_offset].vertexID);

		int closest_point_id = 0;
		float min_dist = 9999.9f;
		for(int x = 0;x < one_ring_v.size();++x){
			Point point;

			point.x = mesh_data->getVertex(one_ring_v[x])[0];
			point.y = mesh_data->getVertex(one_ring_v[x])[1];
			point.z = mesh_data->getVertex(one_ring_v[x])[2];


			float dist = Dist(point,curvePointArray[i + index_offset] );	
			if(dist < min_dist){
				min_dist = dist;
				closest_point_id = one_ring_v[x];
			}
		}
		one_ring_v.clear();
		one_ring_vertex = mesh_data->one_ring_vertex_array[closest_point_id];
		//one_ring_faces = mesh_data->one_ring_face_array[closest_point_id];


		//float dist_to_face = 99999.9f;
		//int closest_face_id = 0;
		//for(unsigned a = 0 ; a < one_ring_faces.size(); ++a)
		//{
		//	float currentDist = Dist(mesh_data->getPoint(mesh_data->getFace(one_ring_faces[a])[0]), curvePointArray[i + index_offset]);
		//	currentDist += Dist(mesh_data->getPoint(mesh_data->getFace(one_ring_faces[a])[1]), curvePointArray[i + index_offset]);
		//	currentDist += Dist(mesh_data->getPoint(mesh_data->getFace(one_ring_faces[a])[2]), curvePointArray[i + index_offset]);
		//	if( currentDist < dist_to_face )
		//	{
		//		dist_to_face = currentDist;
		//		closest_face_id = one_ring_faces[a];
		//	}
		//}
		//initial_path[i + index_offset].projFaceID = closest_face_id;
		////}

		//one_ring_faces = mesh_data->adjacent_face_array[initial_path[i + index_offset].projFaceID];
		//one_ring_faces.push_back(initial_path[i + index_offset].projFaceID);





		//int * faceVertices = mesh_data->getFace(initial_path[i + index_offset].projFaceID);
		//one_ring_vertex.push_back(faceVertices[0]);
		//one_ring_vertex.push_back(faceVertices[1]);
		//one_ring_vertex.push_back(faceVertices[2]);


		//Eigen::Vector3f v_1,v_2,v_3,v_n;
		//v_1[0] = mesh_data->getPoint(faceVertices[0]).x - curvePointArray[i].x;
		//v_1[1] = mesh_data->getPoint(faceVertices[0]).y - curvePointArray[i].y;
		//v_1[2] = mesh_data->getPoint(faceVertices[0]).z - curvePointArray[i].z;
		//v_2[0] = curvePointArray[i].x;
		//v_2[1] = curvePointArray[i].y;
		//v_2[2] = curvePointArray[i].z;
		//v_n[0] = mesh_data->getNormal(initial_path[i + index_offset].projFaceID)[0];
		//v_n[1] = mesh_data->getNormal(initial_path[i + index_offset].projFaceID)[1];
		//v_n[2] = mesh_data->getNormal(initial_path[i + index_offset].projFaceID)[2];
		//v_3 = (v_2 + (v_1 - (v_1.dot(v_n))*v_n));

		//initial_path[i + index_offset].x = v_3[0];
		//initial_path[i + index_offset].y = v_3[1];
		//initial_path[i + index_offset].z = v_3[2];



		//Point test_normal;
		//test_normal.x = 0;
		//test_normal.y = 0;
		//test_normal.z = 0;
		//for(int a = 0; a < one_ring_faces.size();++a ){
		//	test_normal.x +=  mesh_data->getNormal(one_ring_faces[a])[0];
		//	test_normal.y +=  mesh_data->getNormal(one_ring_faces[a])[1];
		//	test_normal.z +=  mesh_data->getNormal(one_ring_faces[a])[2];
		//}
		//test_normal.x = test_normal.x / one_ring_faces.size();
		//test_normal.y = test_normal.y / one_ring_faces.size();
		//test_normal.z = test_normal.z / one_ring_faces.size();
		//Eigen::Vector3f vector(0.0,0.0,0.0);

		//vector[0] =	test_normal.x ;
		//vector[1] =	test_normal.y ;
		//vector[2] =	test_normal.z ;
		//float x = 0, y = 0,z = 0;
		////float center_point[3];				
		//for(int a = 0; a < one_ring_vertex.size() ; ++a){
		//	x += mesh_data->getVertex(one_ring_vertex[a])[0];
		//	y += mesh_data->getVertex(one_ring_vertex[a])[1];
		//	z += mesh_data->getVertex(one_ring_vertex[a])[2];
		//}	

		//dMat_C_3m_1(i*3,0) = x /  one_ring_vertex.size();
		//dMat_C_3m_1(i*3 + 1,0) = y /  one_ring_vertex.size();
		//dMat_C_3m_1(i*3 + 2,0) = z /  one_ring_vertex.size();

		// get one-ring neighbor face normal of current curve point

		//delete []nnIdx;
		//delete []dists;

		////perform SVD on one ring face normals
		//int row =  (int)one_ring_faces.size();
		int row =  (int)one_ring_vertex.size();
		Eigen::MatrixX3f eigenPointMatrix,eMatrix2;
		eigenPointMatrix.resize(row,3);
		Eigen::MatrixX3f transposeMatrix;
		transposeMatrix.resize(row,3);
		Eigen::VectorXf meanVector;

		for(int a = 0; a < one_ring_vertex.size() ; ++a){
			eigenPointMatrix(a,0) = mesh_data->getVertex(one_ring_vertex[a])[0];
			eigenPointMatrix(a,1) = mesh_data->getVertex(one_ring_vertex[a])[1];
			eigenPointMatrix(a,2) = mesh_data->getVertex(one_ring_vertex[a])[2];
		}	

		//float S = 1.0f;
		////for(int a = 0; a < one_ring_vertex.size() ; ++a){
		////	S += 1 / Dist(mesh_data->getPoint(one_ring_vertex[a]), curvePointArray[i]);
		////}	

		//for(int a = 0; a < one_ring_vertex.size() ; ++a){
		//	float v_norm = Dist(mesh_data->getPoint(one_ring_vertex[a]), curvePointArray[i]);
		//	eigenPointMatrix(a,0) = (mesh_data->getVertex(one_ring_vertex[a])[0] - curvePointArray[i].x) ;
		//	eigenPointMatrix(a,1) = (mesh_data->getVertex(one_ring_vertex[a])[1] - curvePointArray[i].y) ;
		//	eigenPointMatrix(a,2) = (mesh_data->getVertex(one_ring_vertex[a])[2] - curvePointArray[i].z) ;
		//}	



		float mean;
		for (int k = 0; k < 3; ++k)
		{
			//compute mean
			mean = (eigenPointMatrix.col(k).sum())/row;
			// create a vector with constant value = mean
			meanVector  = Eigen::VectorXf::Constant(row,mean); 
			eigenPointMatrix.col(k) -= meanVector;
		}
		Eigen::MatrixXf covariance = ((float)1/(row-1)) * eigenPointMatrix.transpose()* eigenPointMatrix;
		//Eigen::MatrixXf covariance = eigenPointMatrix.transpose()* eigenPointMatrix;

		//
		//eMatrix2.resize(3,3);
		//eMatrix2 = eigenPointMatrix.transpose() * eigenPointMatrix;
		Eigen::EigenSolver<Eigen::MatrixXf> m_solve(covariance);

		Eigen::VectorXf eigenvalues = m_solve.eigenvalues().real();
		Eigen::MatrixXf eignevectors = m_solve.eigenvectors().real();

		Eigen::Vector3f vector;
		if(eigenvalues(0) < eigenvalues(1))
		{
			if (eigenvalues(0) < eigenvalues(2))
			{
				vector[0] = eignevectors(0,0);
				vector[1] = eignevectors(1,0);
				vector[2] = eignevectors(2,0);
			}
			else
			{
				vector[0] = eignevectors(0,2);
				vector[1] = eignevectors(1,2);
				vector[2] = eignevectors(2,2);
			}
		}
		else{
			if(eigenvalues(1) < eigenvalues(2))
			{
				vector[0] = eignevectors(0,1);
				vector[1] = eignevectors(1,1);
				vector[2] = eignevectors(2,1);
			}else{
				vector[0] = eignevectors(0,2);
				vector[1] = eignevectors(1,2);
				vector[2] = eignevectors(2,2);
			}
		}

		vector.normalized();








		//for(int a = 0; a < one_ring_faces.size();++a ){
		//	eigenPointMatrix(a,0) = mesh_data->getNormal(one_ring_faces[a])[0];
		//	eigenPointMatrix(a,1) = mesh_data->getNormal(one_ring_faces[a])[1];
		//	eigenPointMatrix(a,2) = mesh_data->getNormal(one_ring_faces[a])[2];	
		//}

		//eMatrix2.resize(3,3);
		//eMatrix2 = eigenPointMatrix.transpose() * eigenPointMatrix;
		//Eigen::EigenSolver<Eigen::MatrixXf> m_solve(eMatrix2);



		////////////////pfile<<std::endl<<std::endl<<"F_n"<<std::endl<<std::endl;
		////////////////for(int r = 0; r < row ;++r){
		////////////////	for(int c = 0; c < 3 ;++c){
		////////////////		pfile<<eigenPointMatrix(r,c)<<" ";
		////////////////	}
		////////////////	pfile<<std::endl;
		////////////////}


		////////////////float mean = 0.0f;
		////////////////for (int k = 0; k < 3; ++k)
		////////////////{
		////////////////	//compute mean
		////////////////	mean = (eigenPointMatrix.col(k).sum())/row;
		////////////////	// create a vector with constant value = mean
		////////////////	meanVector  = Eigen::VectorXf::Constant(row,mean); 
		////////////////	eigenPointMatrix.col(k) -= meanVector;
		////////////////}


		////////////////Eigen::MatrixXf covariance = ((float)1/(row-1)) * eigenPointMatrix.transpose()* eigenPointMatrix;

		////////////////Eigen::EigenSolver<Eigen::MatrixXf> m_solve(covariance);

		//Eigen::VectorXf eigenvalues = m_solve.eigenvalues().real();
		//Eigen::MatrixXf eignevectors = m_solve.eigenvectors().real();

		//pfile<<std::endl<<std::endl<<"eigenvalues"<<std::endl<<std::endl;
		//for(int r = 0; r < 3 ;++r){
		//	pfile<<eigenvalues(r,0)<<std::endl;
		//}

		//pfile<<std::endl<<std::endl<<"eignevectors"<<std::endl<<std::endl;
		//for(int r = 0; r < 3 ;++r){
		//	for(int c = 0; c < 3 ;++c){
		//		pfile<<eignevectors(r,c)<<" ";
		//	}
		//	pfile<<std::endl;
		//}

		//Eigen::Vector3f vector(0.0,0.0,0.0);

		//if(eigenvalues(0) > eigenvalues(1))
		//{
		//	if (eigenvalues(0) > eigenvalues(2))
		//	{
		//		vector[0] = eignevectors(0,0);
		//		vector[1] = eignevectors(1,0);
		//		vector[2] = eignevectors(2,0);
		//	}
		//	else
		//	{
		//		vector[0] = eignevectors(0,2);
		//		vector[1] = eignevectors(1,2);
		//		vector[2] = eignevectors(2,2);
		//	}
		//}
		//else{
		//	if(eigenvalues(1) > eigenvalues(2))
		//	{
		//		vector[0] = eignevectors(0,1);
		//		vector[1] = eignevectors(1,1);
		//		vector[2] = eignevectors(2,1);
		//	}else{
		//		vector[0] = eignevectors(0,2);
		//		vector[1] = eignevectors(1,2);
		//		vector[2] = eignevectors(2,2);
		//	}
		//}

		//vector.normalized();





		Eigen::Vector3f v1,v2,v3,v4;

		float x = 0, y = 0,z = 0;
		//float center_point[3];				
		for(int a = 0; a < one_ring_vertex.size() ; ++a){
			v1[0] = mesh_data->getVertex(one_ring_vertex[a])[0] - mesh_data->getVertex(initial_path[i].vertexID)[0];
			v1[1] = mesh_data->getVertex(one_ring_vertex[a])[1] - mesh_data->getVertex(initial_path[i].vertexID)[1];
			v1[2] = mesh_data->getVertex(one_ring_vertex[a])[2] - mesh_data->getVertex(initial_path[i].vertexID)[2];


			v2[0] = mesh_data->getVertex(initial_path[i].vertexID)[0];
			v2[1] = mesh_data->getVertex(initial_path[i].vertexID)[1];
			v2[2] = mesh_data->getVertex(initial_path[i].vertexID)[2];



			x += (v2 + (v1 - (v1.dot(vector))*vector))[0];
			y += (v2 + (v1 - (v1.dot(vector))*vector))[1];
			z += (v2 + (v1 - (v1.dot(vector))*vector))[2];

		}

		dMat_C_3m_1(i*3,0) = x /  one_ring_vertex.size();
		dMat_C_3m_1(i*3 + 1,0) = y /  one_ring_vertex.size();
		dMat_C_3m_1(i*3 + 2,0) = z /  one_ring_vertex.size();


		//dMat_C_3m_1(i*3,0) = initial_path[i + index_offset].x;
		//dMat_C_3m_1(i*3 + 1,0) = initial_path[i + index_offset].y;
		//dMat_C_3m_1(i*3 + 2,0) = initial_path[i + index_offset].z;

		//Point p;
		//p.x = x /  one_ring_vertex.size();
		//p.y = y /  one_ring_vertex.size();
		//p.z = z /  one_ring_vertex.size();

		//mean_array.push_back(p);


		dMat_N_3m_m(i*3,i) = vector[0];
		dMat_N_3m_m(i*3+1,i) = vector[1];
		dMat_N_3m_m(i*3+2,i) = vector[2];

		dMat_Nt_m_3m(i,i*3 ) = vector[0];
		dMat_Nt_m_3m(i,i*3+1) = vector[1];
		dMat_Nt_m_3m(i,i*3+2) = vector[2];

		if(i > 0 && i < sparse_matrix_size - 1){

			float g = Dist(curvePointArray[i-1], curvePointArray[i]);
			float f = Dist(curvePointArray[i+1], curvePointArray[i]);


			dMat_K_3m_3m(i * 3 , (i - 1) * 3 ) = 2/(g*(g+f));
			dMat_K_3m_3m(i * 3 + 1 , (i - 1) * 3 +1) = 2/(g*(g+f));
			dMat_K_3m_3m(i * 3 + 2 , (i - 1) * 3 +2 ) = 2/(g*(g+f));

			dMat_K_3m_3m(i * 3 , i * 3 ) = -2/(g*f);
			dMat_K_3m_3m(i * 3 + 1 , i * 3 +1) =  -2/(g*f);
			dMat_K_3m_3m(i * 3 + 2 , i * 3 +2 ) =  -2/(g*f);


			dMat_K_3m_3m(i * 3 , (i + 1) * 3 ) = 2/(f*(g+f));
			dMat_K_3m_3m(i * 3 + 1 , (i + 1) * 3 +1) = 2/(f*(g+f));
			dMat_K_3m_3m(i * 3 + 2 , (i + 1) * 3 +2 ) = 2/(f*(g+f));
		}
	}
	mean_array.clear();


	//dMat_K_3m_3m(0,0) = 1.0f;
	//dMat_K_3m_3m(1,1) = 1.0f;
	//dMat_K_3m_3m(2,2) = 1.0f;
	//dMat_K_3m_3m(sparse_matrix_size * 3 - 3,sparse_matrix_size * 3 - 3) = 1.0f;
	//dMat_K_3m_3m(sparse_matrix_size * 3 - 2,sparse_matrix_size * 3 - 2) = 1.0f;
	//dMat_K_3m_3m(sparse_matrix_size * 3 - 1,sparse_matrix_size * 3 - 1) = 1.0f;



	//pfile<<std::endl<<std::endl<<"N"<<std::endl<<std::endl;
	//for(int row = 0; row < sparse_matrix_size * 3;++row){
	//	for(int col = 0; col < sparse_matrix_size;++col){
	//		pfile<<dMat_N_3m_m(row,col)<<" ";
	//	}
	//	pfile<<std::endl;
	//}

	//pfile<<std::endl<<std::endl<<"Nt"<<std::endl<<std::endl;
	//for(int row = 0; row < sparse_matrix_size;++row){
	//	for(int col = 0; col < sparse_matrix_size * 3;++col){
	//		pfile<<dMat_Nt_m_3m(row,col)<<" ";
	//	}
	//	pfile<<std::endl;
	//}

	dMat_Nstar_3m_3m = dMat_N_3m_m * dMat_Nt_m_3m;

	//pfile<<std::endl<<std::endl<<"Nstar"<<std::endl<<std::endl;
	//for(int row = 0; row < sparse_matrix_size * 3;++row){
	//	for(int col = 0; col < sparse_matrix_size * 3;++col){
	//		pfile<<dMat_Nstar_3m_3m(row,col)<<" ";
	//	}
	//	pfile<<std::endl;
	//}

	//pfile<<std::endl<<std::endl<<"K"<<std::endl<<std::endl;
	//for(int row = 0; row < sparse_matrix_size * 3;++row){
	//	for(int col = 0; col < sparse_matrix_size * 3;++col){
	//		pfile<<dMat_K_3m_3m(row,col)<<" ";
	//	}
	//	pfile<<std::endl;
	//}

	////////////////////////////////////////////////////////////////////////////////
	//Timer t2;

	//start_timer(t2,true,"matrix calculation start: ",true);

	//Eigen::MatrixXf sub_mat,inv_mat,dMat_Xt_1_m_3;





	//sub_mat.resize(sparse_matrix_size * 3 , sparse_matrix_size * 3);
	////inv_mat.resize(sparse_matrix_size * 3 , sparse_matrix_size * 3);
	//dMat_Xt_1_m_3.resize(sparse_matrix_size * 3,1);
	//sub_mat = dMat_I_mat_3m_3m +  u * (dMat_Nstar_3m_3m - dMat_K_3m_3m + dMat_Nstar_3m_3m * dMat_K_3m_3m);

	////FullPivLU<MatrixXd> lu(sub_mat);
	////inv_mat = lu.inverse();
	//
	////dMat_Xt_1_m_3 = inv_mat * ( dMat_Xt_3m_1 + u * dMat_Nstar_3m_3m * dMat_C_3m_1 );

	//Eigen::MatrixXf B;
	//B = dMat_Xt_3m_1 + u * dMat_Nstar_3m_3m * dMat_C_3m_1;

	Eigen::MatrixXf A,B,A1,A2,B1,At;

	Eigen::MatrixXf sub_mat,inv_mat,dMat_Xt_1_m_3;
	dMat_Xt_1_m_3.resize(sparse_matrix_size * 3,1);
	////////////////////////////////////////////////////////////////////////////
	///////////////construct A
	A.resize(sparse_matrix_size * 6 + 6, sparse_matrix_size * 3);
	A.setZero();

	A1.resize(sparse_matrix_size * 3 , sparse_matrix_size * 3);
	A1.setZero();

	A1 = dMat_K_3m_3m - dMat_Nstar_3m_3m * dMat_K_3m_3m;


	for(int row = 0; row < sparse_matrix_size * 3;++row){
		for(int col = 0; col < sparse_matrix_size * 3;++col){
			A(row,col) = A1(row,col);
		}
	}

	A(sparse_matrix_size * 3,0) = 1.0f;
	A(sparse_matrix_size * 3 + 1,1) = 1.0f;
	A(sparse_matrix_size * 3 + 2,2) = 1.0f;
	A(sparse_matrix_size * 3 + 3,sparse_matrix_size * 3 - 3) = 1.0f;
	A(sparse_matrix_size * 3 + 4,sparse_matrix_size * 3 - 2) = 1.0f;
	A(sparse_matrix_size * 3 + 5,sparse_matrix_size * 3 - 1) = 1.0f;


	for(int row = sparse_matrix_size * 3 + 6; row < sparse_matrix_size * 6 + 6;++row){
		for(int col = 0; col < sparse_matrix_size * 3;++col){
			A(row,col) = dMat_Nstar_3m_3m(row - (sparse_matrix_size * 3 + 6),col);
		}
	}

	//for(int row = 0; row < sparse_matrix_size * 6 + 6;++row){
	//	for(int col = 0; col < sparse_matrix_size * 3;++col){

	//		regionFile<<A(row,col)<<" ";
	//	}
	//	regionFile<<std::endl;
	//}




	////////////////////////////////////////////////////////////////////////////
	///////////////construct B
	B.resize(sparse_matrix_size * 6 + 6, 1);
	B.setZero();

	B1.resize(sparse_matrix_size * 3, 1);
	B1.setZero();

	B(sparse_matrix_size * 3,0) = curvePointArray[0].x;
	B(sparse_matrix_size * 3 + 1,0) = curvePointArray[0].y;
	B(sparse_matrix_size * 3 + 2,0) = curvePointArray[0].z;
	B(sparse_matrix_size * 3 + 3,0) = curvePointArray[curvePointArray.size() - 1].x;
	B(sparse_matrix_size * 3 + 4,0) = curvePointArray[curvePointArray.size() - 1].y;
	B(sparse_matrix_size * 3 + 5,0) = curvePointArray[curvePointArray.size() - 1].z;

	B1 = dMat_Nstar_3m_3m * dMat_C_3m_1;

	for(int row = sparse_matrix_size * 3 + 6 ; row < sparse_matrix_size * 6 + 6;++row){
		B(row,0) = B1(row - (sparse_matrix_size * 3 + 6),0);
	}


	//	pfile.close();
	At = A.transpose();
	A = At * A;
	B = At * B;	
	dMat_Xt_1_m_3 = A.lu().solve(B);



	//dMat_Xt_1_m_3 = A1.lu().solve(B1);

	///////////////////////////// fullPivLu decomposition :

	//dMat_Xt_1_m_3 = sub_mat.fullPivLu().solve(B);

	///////////////////////////// partial Lu decomposition :

	//dMat_Xt_1_m_3 = sub_mat.lu().solve(B);

	///////////////////////////// LDL^T Cholesky decomposition :
	//dMat_Xt_1_m_3 = sub_mat.ldlt().solve(B);

	///////////////////////////// col QR decomposition with column pivoting:

	//dMat_Xt_1_m_3 = sub_mat.colPivHouseholderQr().solve(B);


	///////////////////////////// full QR decomposition with column pivoting:

	//dMat_Xt_1_m_3 = sub_mat.fullPivHouseholderQr().solve(B);

	//std::cout<<curvePointArray.size()<<" "<<sparse_matrix_size<<std::endl;

	for(int i = 0; i < sparse_matrix_size;++i){

		curvePointArray[i].x = dMat_Xt_1_m_3(i*3,0);
		curvePointArray[i].y = dMat_Xt_1_m_3(i*3 + 1,0);
		curvePointArray[i].z = dMat_Xt_1_m_3(i*3 + 2,0);
	}

	//regionFile.close();

}


#endif