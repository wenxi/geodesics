#ifndef geodesic_mesh_data_h
#define geodesic_mesh_data_h
#include <vector>
#include "geodesic_memoryallocator.h"
#include <math.h>

//
//#include <ANN/ANN.h>
//#include <ANN/ANNperf.h>
//#include <ANN/ANNx.h>

#ifndef MAX_DIST
#define MAX_DIST 999999
#endif	


class Point{
public:
	Point(): x(0.0f), y(0.0f),z(0.0f){}

	Point(float _x, float _y, float _z) 
	{
		x = _x;
		y = _y;
		z = _z;
	}
 float x;
 float y;
 float z;
};


class initPoint
{
public:
	initPoint(): x(0), y(0),z(0),n_x(0),n_y(0),n_z(0),flag(1),isOnMesh(false),is_geodest_exact(false),vertexID(-1) 
	{
		location.reserve(4);
		neighbourFacesID.reserve(10);
	}
 float x;
 float y;
 float z;
 float n_x;
 float n_y;
 float n_z;

 int flag; 
 bool isOnMesh;
 std::vector<int> location;
 std::vector<int> cutFaceIDs;
 std::vector<int> cutEdge;
 int vertexID;
 bool is_geodest_exact;
 std::vector<int> neighbourFacesID;
};



class CutPoint{
public:
	CutPoint(): vertexID(-1), projFlag(0)
	{
		location.reserve(4);
		cutEdge.reserve(2);
		neighbourFaces.reserve(10);
		cutFaceIDs.reserve(2);	
	}

	float x;
	float y;
	float z;
	float n_x;
	float n_y;
	float n_z;
	//on edge =0; on vertex =1; inside =2; do not float =3; outside =-1
	int flag;
	std::vector<int> location;
	std::vector<int> cutEdge;
	std::vector<int> neighbourFaces;
	std::vector<int> cutFaceIDs;
	//int cutFaceID;

	int projFlag;
	int vertexID;

};


class ProjPoint
{
public:
	ProjPoint(): vertexID(-1)
	{
		edge.reserve(2);
		neighbourFaces.reserve(10);
	}
	float x;
	float y;
	float z;

	int vertexID;
	int faceID;
	std::vector<int> edge;
	std::vector<int> neighbourFaces;
};


class MeshData
{
public:
	MeshData(): num_vertices_(0), num_faces_(0),num_vertex_normals_(0),vertices(NULL), faces(NULL), vertex_normals(NULL){}

	MeshData(int num_vertices, int num_faces ) {

		num_vertices_ = num_vertices;
		num_faces_ = num_faces;
		num_vertex_normals_ = num_vertices;

		if(num_vertices_ !=0 && num_faces_ != 0 && num_vertex_normals_ != 0 ){
			geodesic_memory_allocator<float> vertex_block(num_vertices_*3);
			vertices = vertex_block.getAddress();

			geodesic_memory_allocator< int> face_block(num_faces_*3);
			faces = face_block.getAddress();

			geodesic_memory_allocator<float> vertex_normals_block(num_vertex_normals_*3);
			vertex_normals = vertex_normals_block.getAddress();

			//geodesic_memory_allocator<float> face_normals_block(num_face_normals_*3);
			//face_normals = face_normals_block.getAddress();
		}
	}

	void init(int num_vertices, int num_faces) {

		if(num_vertices_!=0)
		{
			delete []vertices;
			delete []vertex_normals;
		}
		if(num_faces_!=0)
		{
			delete []faces;
			//delete []face_normals;
		}


		num_vertices_ = num_vertices;
		num_faces_ = num_faces;
		num_vertex_normals_ = num_vertices;
		//num_face_normals_ = num_faces;


		geodesic_path_array.reserve(num_vertices_); // real geodesic path.
		std::vector<Point> path;
		std::vector<initPoint> initial_path;
		std::vector<int> one_ring;
		std::vector<initPoint> projectedPath;

		path.reserve((int)sqrt((float)num_vertices_));
		initial_path.reserve((int)sqrt((float)num_vertices_));
		distance_to_source_array.reserve((int)sqrt((float)num_vertices_));
		projectedPath.reserve((int)sqrt((float)num_vertices_));

		one_ring.reserve(6);


		for(int i = 0; i < num_vertices_;++i){
			geodesic_path_array.push_back(path);
			floating_path_array.push_back(path);
//			initial_path_array_initial.push_back(path);
			initial_path_array.push_back(initial_path);
			distance_to_source_array.push_back(MAX_DIST);
			one_ring_vertex_array.push_back(one_ring);
			one_ring_face_array.push_back(one_ring);
			projected_path_array.push_back(projectedPath);
		}
		for(int i = 0; i < num_faces_;++i){
			adjacent_face_array.push_back(one_ring);
		}

		vertex_block.init(num_vertices_ * 3);
		vertices = vertex_block.getAddress();

		face_block.init(num_faces_ * 3);
		faces = face_block.getAddress();

		vertex_normals_block.init(num_vertices_ * 3);
		vertex_normals = vertex_normals_block.getAddress();

		//face_normals_block.init(num_faces_ * 3);
		//face_normals = face_normals_block.getAddress();
	}

	~MeshData() {
		vertex_block.clear();
		face_block.clear();
		vertex_normals_block.clear();
		//face_normals_block.clear();
		num_vertices_ = 0;
		num_faces_ = 0;
		num_vertex_normals_ = 0;
	//	num_face_normals_ = 0;
	}

	/////////////////////////////////////////////////////////////////

	inline float*	getVertex( int index) {
		return &vertices[ index * 3]; 
	}

	/////////////////////////////////////////////////////////////////

	inline Point getPoint( int index ){
		Point point;
		point.x = getVertex(index)[0];
		point.y = getVertex(index)[1];
		point.z = getVertex(index)[2];
		return point;
	}

	/////////////////////////////////////////////////////////////////

	inline  int* getFace( int index){
		return &faces[ index * 3 ];
	}

	/////////////////////////////////////////////////////////////////

	inline float*	getVertexNormal( int index) {
		return &vertex_normals[ index * 3]; 
	}


	/////////////////////////////////////////////////////////////////

	inline  int numVertices(){
		return num_vertices_;
	}

	/////////////////////////////////////////////////////////////////

	inline  int numFaces(){
		return num_faces_;
	}

	/////////////////////////////////////////////////////////////////

	inline  int numVertexNormals(){
		return num_vertex_normals_;
	}

	/////////////////////////////////////////////////////////////////


	/////////////////////////////////////////////////////////////////

	inline std::vector< int>  getFaceIDfromVertex( int index) {

		//unsigned int * faceId = NULL;
		std::vector< int> faceIDArray;
		for(int a =0; a < num_faces_; ++a){

			if(index == faces[a * 3] || index == faces[a * 3 +1 ] || index == faces[a * 3 +2])
				faceIDArray.push_back(a);
		}
		return faceIDArray;
	}

	/////////////////////////////////////////////////////////////////

	inline void get_one_ring_neighbor( int index, std::vector<int> &vertex_array,  std::vector<int> &face_array){
		//vector< int > one_ring_neighbor;
		std::vector< int> connected_faceIDArray;
		connected_faceIDArray = getFaceIDfromVertex(index);
		face_array = connected_faceIDArray;
		bool is_exist = false;
		for(size_t a = 0; a < connected_faceIDArray.size();a++){

			for(int b = 0; b < 3; b++){
				int * face_vertex = getFace(connected_faceIDArray[a]);

				if(face_vertex[b] == index ){
					is_exist = true;
				}
				for(size_t c = 0; c < vertex_array.size();c++){
					if(face_vertex[b] == vertex_array[c]){
						is_exist = true;
					}
				}
				if(!is_exist){
					vertex_array.push_back(face_vertex[b]);
				}else{
					is_exist = false;
				}

			}
		}

		//return one_ring_neighbor;

	}

	///////////////////////////////////////////////////////////////

	inline std::vector<int> get_one_ring_faces_of_face(int faceID)
	{
		int * faceVertex = getFace(faceID);

		std::vector<int> oneRingFaces;
		oneRingFaces = one_ring_face_array[faceVertex[0]];
		
		for(int a=0; a < one_ring_face_array[faceVertex[1]].size(); ++a )
		{
			bool is_exist = false;
			for(int b = 0; b < oneRingFaces.size(); ++b)
			{
				if(oneRingFaces[b] ==  one_ring_face_array[faceVertex[1]][a])
				{
					is_exist = true;
					break;
				}
			}

			if(!is_exist)
			{
				oneRingFaces.push_back(one_ring_face_array[faceVertex[1]][a]);
			}
		}


		for(int a=0; a < one_ring_face_array[faceVertex[2]].size(); ++a )
		{
			bool is_exist = false;
			for(int b = 0; b < oneRingFaces.size(); ++b)
			{
				if(oneRingFaces[b] ==  one_ring_face_array[faceVertex[2]][a])
				{
					is_exist = true;
					break;
				}
			}

			if(!is_exist)
			{
				oneRingFaces.push_back(one_ring_face_array[faceVertex[2]][a]);
			}
		}
		return oneRingFaces;

	}
 
	/////////////////////////////////////////////////////////////////

public:
	int num_vertices_;
	int num_faces_;
	int num_vertex_normals_;
	//int num_face_normals_;

	float* vertices;
	int* faces;
	float* vertex_normals;
	//float* face_normals;

//	bool * vertex_tags;

	std::vector< std::vector<Point> > geodesic_path_array; // real geodesic path.
	std::vector< std::vector<initPoint> > initial_path_array;	// shortest path goes through edges.
	std::vector< std::vector<initPoint> > projected_path_array;
	std::vector< std::vector<Point> > floating_path_array;
	//std::vector< std::vector<Point> > initial_path_array_initial;

	std::vector<float> distance_to_source_array;
	//std::vector< std::vector<float> > node_parameter_array;

	std::vector< std::vector<int> > one_ring_vertex_array;
	std::vector< std::vector<int> > one_ring_face_array;
	std::vector< std::vector<int> > adjacent_face_array;

	//ANNkd_tree *meshKdTree;

	geodesic_memory_allocator<float> vertex_block;
	geodesic_memory_allocator< int> face_block;
	geodesic_memory_allocator<float> vertex_normals_block;
	//geodesic_memory_allocator<float> face_normals_block;
};


#endif	
