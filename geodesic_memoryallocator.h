#ifndef geodesic_memoryallocator_h
#define geodesic_memoryallocator_h


#include <vector>


template <typename  T>
class geodesic_memory_allocator {
public:
	geodesic_memory_allocator() : num_of_block_(0),my_memory_(NULL) {}

	geodesic_memory_allocator(size_t block_count){
		num_of_block_ = block_count;
		my_memory_ = new T[num_of_block_];


	}

	//~geodesic_memory_allocator(){
	//	if(num_of_block_ != 0){
	//		delete my_memory_;
	//	}
	//}





public:


	inline void init(size_t num_of_block) {
		
		if(my_memory_ != NULL)
			delete my_memory_;

		num_of_block_ = num_of_block;
		my_memory_ =  new T[num_of_block_];	

	}




	inline T * getAddress(){
		return my_memory_;
	}

	inline void clear(){
		if(num_of_block_ != 0){
			delete my_memory_;
		}
	}

private:
	size_t num_of_block_;
	T * my_memory_;
};

#endif