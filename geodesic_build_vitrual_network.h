//#ifndef geodesic_build_vitrual_network_h
//#define geodesic_build_vitrual_network_h
//
//
//// this function create virtual network from on source to all destination and at each expansion iteration, 
//// the geodesic will be calculated and copy to the next closest point on the next ring, thus at each expansion.
//// only newly added point will be calculated .
//
//#include "geodesic_mesh_data.h"
//#include "geodesic_base_elements.h"
//#include "geodesic_regression_V2.h"
//#include "UtilityDebug.h"
//
//#include <Eigen/Core>
//#include <Eigen/Eigen>
//#include <Eigen/Eigenvalues>
//#include <Eigen/Sparse>
//
//using namespace std;
//using namespace Eigen;
//
//double sqrtDis(Point point0, Point point1){
//
//	return (point0.x - point1.x) * (point0.x - point1.x) + (point0.y - point1.y) * (point0.y - point1.y) + (point0.z - point1.z) * (point0.z - point1.z);
//}
//
//
//
//
//void mesh_virtual_network(int source_id , MeshData * mesh_data,vector<vector<int>> &initial_path,vector<vector<Point>> &geodesic_path){
//
//	Timer t1,t2;
//
//	int window_size = 20;
//
//	int num_vertices = 0;
//	num_vertices = mesh_data->num_vertices_;
//	initial_path.resize(num_vertices);
//	geodesic_path.resize(num_vertices);
//
//	vector<int> wave_head;
//	vector<int> wave_tail;
//	vector<int> wave_crest;
//	vector<int> connectedVertices;
//	
//
//	float regression_ratio = 5000.0f;
//
//	size_t count = 0;
//
//	Point point;
//
//	float total_time = 0.0f;;
//	start_timer(t1,true,"start calculating geodesic from one source to all destination",true);
//
//	do{
//		if(wave_crest.size() == 0){
//
//			connectedVertices = mesh_data->one_ring_vertex_array[source_id];
//			for(int b = 0; b < connectedVertices.size();b++){
//				wave_head.push_back(connectedVertices[b]);
//				initial_path[connectedVertices[b]].push_back(source_id);
//				initial_path[connectedVertices[b]].push_back(connectedVertices[b]);
//
//				// store the geodesic path into of all the point on first ring;
//				point.x =  mesh_data->getVertex(source_id)[0];
//				point.y =  mesh_data->getVertex(source_id)[1];
//				point.z =  mesh_data->getVertex(source_id)[2];
//
//				geodesic_path[connectedVertices[b]].push_back(point);
//
//				point.x =  mesh_data->getVertex(connectedVertices[b])[0];
//				point.y =  mesh_data->getVertex(connectedVertices[b])[1];
//				point.z =  mesh_data->getVertex(connectedVertices[b])[2];
//
//				geodesic_path[connectedVertices[b]].push_back(point);
//
//			}
//
//			wave_crest = wave_head;
//			wave_tail.push_back(source_id);
//			connectedVertices.clear();
//
//
//		}else{
//			for(unsigned int a = 0; a < wave_crest.size();a++){
//				connectedVertices = mesh_data->one_ring_vertex_array[wave_crest[a]];
//
//				vector<int> connectedVertices_for_newPoint;
//				connectedVertices_for_newPoint.push_back(wave_crest[a]);
//
//				bool is_exist = false;
//				for(unsigned int b = 0; b < connectedVertices.size();b++){
//
//					for(unsigned int c = 0; c < wave_crest.size();c++){
//
//						if(connectedVertices[b] == wave_crest[c]){
//							connectedVertices_for_newPoint.push_back(wave_crest[c]);
//							is_exist = true;
//							break;
//						}
//					}
//					for(unsigned int c = 0; c < wave_tail.size();c++){
//
//						if(connectedVertices[b] == wave_tail[c]){
//							is_exist = true;
//							break;
//						}
//					}
//					for(unsigned int c = 0; c < wave_head.size();c++){
//
//						if(connectedVertices[b] == wave_head[c]){
//							is_exist = true;
//							break;
//						}
//					}
//
//					if(!is_exist){
//						wave_head.push_back(connectedVertices[b]);
//						Point point0, point1;
//						double max_dis = 9999999.9f;
//						int closest_point_index_on_wave_head = 0;
//						int closest_point_index_on_wave_crest = 0;
//
//
//						//for(int c = 0; c < connectedVertices_for_newPoint.size();++c){
//						for(int c = 0; c < wave_crest.size();++c){
//							
//							point0.x = geodesic_path[wave_crest[c]][geodesic_path[wave_crest[c]].size() - 1].x;
//							point0.y = geodesic_path[wave_crest[c]][geodesic_path[wave_crest[c]].size() - 1].y;
//							point0.z = geodesic_path[wave_crest[c]][geodesic_path[wave_crest[c]].size() - 1].z;
//
//							point1.x = mesh_data->getVertex(connectedVertices[b])[0];
//							point1.y = mesh_data->getVertex(connectedVertices[b])[1];
//							point1.z = mesh_data->getVertex(connectedVertices[b])[2];
//							
//							double dis = sqrtDis(point0,point1);
//							if(dis < max_dis){
//								max_dis = dis;
//								closest_point_index_on_wave_head = connectedVertices[b];
//								closest_point_index_on_wave_crest = wave_crest[c];
//							}
//						
//							
//							//point0.x = geodesic_path[connectedVertices_for_newPoint[c]][geodesic_path[connectedVertices_for_newPoint[c]].size() - 1].x;
//							//point0.y = geodesic_path[connectedVertices_for_newPoint[c]][geodesic_path[connectedVertices_for_newPoint[c]].size() - 1].y;
//							//point0.z = geodesic_path[connectedVertices_for_newPoint[c]][geodesic_path[connectedVertices_for_newPoint[c]].size() - 1].z;
//
//							//point1.x = mesh_data->getVertex(connectedVertices[b])[0];
//							//point1.y = mesh_data->getVertex(connectedVertices[b])[1];
//							//point1.z = mesh_data->getVertex(connectedVertices[b])[2];
//
//							//double dis = sqrtDis(point0,point1);
//							//if(dis < max_dis){
//							//	max_dis = dis;
//							//	closest_point_index_on_wave_head = connectedVertices[b];
//							//	closest_point_index_on_wave_crest = connectedVertices_for_newPoint[c];
//							//}
//						
//						}
//						
//
//						initial_path[connectedVertices[b]] = initial_path[closest_point_index_on_wave_crest];
//						initial_path[connectedVertices[b]].push_back(closest_point_index_on_wave_head);
//
//						geodesic_path[connectedVertices[b]] = geodesic_path[closest_point_index_on_wave_crest];
//						geodesic_path[connectedVertices[b]].push_back(point1);
//
//
//						if(geodesic_path[connectedVertices[b]].size() > window_size){
//							vector<Point> new_section;
//							size_t index_offset = 0;
//							index_offset = geodesic_path[connectedVertices[b]].size() - window_size;
//
//							for(unsigned int a = index_offset; a < geodesic_path[connectedVertices[b]].size(); a++){
//								new_section.push_back(geodesic_path[connectedVertices[b]][a]);
//							}
//
//							geodesic(mesh_data, new_section ,regression_ratio);
//
//							for(unsigned int a = index_offset; a < geodesic_path[connectedVertices[b]].size(); a++){
//								geodesic_path[connectedVertices[b]][a] = new_section[a - index_offset];
//							}
//
//
//							new_section.clear();
//
//
//						}else{
//							geodesic(mesh_data, geodesic_path[connectedVertices[b]] ,regression_ratio);
//						}
//						//count++;
//					}else{
//						is_exist = false;
//					}
//					connectedVertices_for_newPoint.clear();
//				}
//			}
//
//			// go through wave_head and find which point on the crest is closest to the point on wave head
//
//			wave_tail = wave_crest;
//			wave_crest = wave_head;
//		}
//		count += wave_head.size();
//		wave_head.clear();
//
//	}while(count < num_vertices - 2);
//	stop_timer(t1,true,"all geodesics has been calculated",true);
//
//}
//
//
//
//
//
//#endif